/**
 *
 */
package br.com.eventssaas.generalbusiness.entity;

import java.io.Serializable;
import java.util.Calendar;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;

import br.com.eventssaas.generalbusiness.enums.SurveyQuestionType;

@Entity
@Table(name = "survey_question")
public class SurveyQuestion implements Serializable {

	private static final long serialVersionUID = 3037886931420658526L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "creation_date")
	private Calendar creationDate = Calendar.getInstance();

	@NotNull
	@Column(name = "type", columnDefinition = "varchar(30) default 'OPTION_QUESTION'")
	@Enumerated(EnumType.STRING)
	private SurveyQuestionType type = SurveyQuestionType.OPTION_QUESTION;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "survey_id")
	private Survey survey;

	@NotBlank
	@NotNull
	@Length(max = 200)
	@Column(name = "question")
	private String question;

	@OneToMany(fetch = FetchType.EAGER, mappedBy = "surveyQuestion", cascade = CascadeType.ALL)
	private List<SurveyAnswer> listSurveyAnswer;

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Survey getSurvey() {
		return this.survey;
	}

	public void setSurvey(Survey survey) {
		this.survey = survey;
	}

	public String getQuestion() {
		return this.question;
	}

	public void setQuestion(String question) {
		this.question = question;
	}

	public List<SurveyAnswer> getListSurveyAnswer() {
		return this.listSurveyAnswer;
	}

	public void setListSurveyAnswer(List<SurveyAnswer> listSurveyAnswer) {
		this.listSurveyAnswer = listSurveyAnswer;
	}

	public Calendar getCreationDate() {
		return this.creationDate;
	}

	public void setCreationDate(Calendar creationDate) {
		this.creationDate = creationDate;
	}

	public SurveyQuestionType getType() {
		return this.type;
	}

	public void setType(SurveyQuestionType type) {
		this.type = type;
	}

}
