package br.com.eventssaas.generalbusiness.services;

import java.util.Map;

import br.com.eventssaas.generalbusiness.entity.Contact;
import br.com.eventssaas.generalbusiness.entity.Profile;
import br.com.eventssaas.generalbusiness.entity.SocialProfile;
import br.com.eventssaas.generalbusiness.entity.User;
import br.com.eventssaas.generalbusiness.enums.MailBasicTemplateType;
import br.com.eventssaas.generalbusiness.support.Mail;

public interface IMailService {

	void sendEmail(final Mail mail, String templateName, final Map<String, Object> templateParameters);

	void sendEmailSignUp(SocialProfile profile);

	void sendEmailConfirmationSignUp(final User user, final String confirmationUrl);

	void sendEmailConfirmationAdmin(Profile profile, String password);

	void sendEmailSpeaker(Contact contact);

	void sendEmailQRCodeAndPasswordIfExist(final MailBasicTemplateType subjectType, String mailTo, String firstName,
			String password, String qrCode);

	public String getSubjectValue(MailBasicTemplateType subjectType);
}