package br.com.eventssaas.generalbusiness.exceptions;

public class NotFoundException extends Exception {

	/**
	 *
	 */
	private static final long serialVersionUID = -1774117423363715223L;

	public NotFoundException(String message) {
		super(message);
	}

	public NotFoundException() {

	}

}
