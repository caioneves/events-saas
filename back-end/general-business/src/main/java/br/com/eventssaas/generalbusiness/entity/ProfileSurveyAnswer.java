/**
 *
 */
package br.com.eventssaas.generalbusiness.entity;

import java.io.Serializable;
import java.util.Calendar;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;

import br.com.eventssaas.generalbusiness.enums.SurveyQuestionType;

@Entity
@Table(name = "profile_survey_answer")
public class ProfileSurveyAnswer implements Serializable {

	private static final long serialVersionUID = 60910764011722074L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "creation_date")
	private Calendar creationDate = Calendar.getInstance();

	@NotNull
	@Column(name = "type", columnDefinition = "varchar(30) default 'OPTION_QUESTION'")
	@Enumerated(EnumType.STRING)
	private SurveyQuestionType type = SurveyQuestionType.OPTION_QUESTION;

	@NotNull
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "survey_id")
	private Survey survey;

	@NotNull
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "survey_question_id")
	private SurveyQuestion surveyQuestion;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "survey_answer_id")
	private SurveyAnswer surveyAnswer;

	@NotBlank
	@NotNull
	@Length(max = 200)
	@Column(name = "question")
	private String surveyQuestionText;

	@Length(max = 1000)
	@Column(name = "answer")
	private String surveyAnswerText;

	@NotNull
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "event_id")
	private Event event;

	@NotNull
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "author_id")
	private Profile author;

	@Column(name = "is_correct", columnDefinition = "bit(1) default 0")
	private boolean correct = false;

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Calendar getCreationDate() {
		return this.creationDate;
	}

	public void setCreationDate(Calendar creationDate) {
		this.creationDate = creationDate;
	}

	public Survey getSurvey() {
		return this.survey;
	}

	public void setSurvey(Survey survey) {
		this.survey = survey;
	}

	public SurveyQuestion getSurveyQuestion() {
		return this.surveyQuestion;
	}

	public void setSurveyQuestion(SurveyQuestion surveyQuestion) {
		this.surveyQuestion = surveyQuestion;
	}

	public SurveyAnswer getSurveyAnswer() {
		return this.surveyAnswer;
	}

	public void setSurveyAnswer(SurveyAnswer surveyAnswer) {
		this.surveyAnswer = surveyAnswer;
	}

	public String getSurveyQuestionText() {
		return this.surveyQuestionText;
	}

	public void setSurveyQuestionText(String surveyQuestionText) {
		this.surveyQuestionText = surveyQuestionText;
	}

	public String getSurveyAnswerText() {
		return this.surveyAnswerText;
	}

	public void setSurveyAnswerText(String surveyAnswerText) {
		this.surveyAnswerText = surveyAnswerText;
	}

	public Event getEvent() {
		return this.event;
	}

	public void setEvent(Event event) {
		this.event = event;
	}

	public Profile getAuthor() {
		return this.author;
	}

	public void setAuthor(Profile author) {
		this.author = author;
	}

	public boolean isCorrect() {
		return this.correct;
	}

	public void setCorrect(boolean correct) {
		this.correct = correct;
	}

	public SurveyQuestionType getType() {
		return this.type;
	}

	public void setType(SurveyQuestionType type) {
		this.type = type;
	}

}
