package br.com.eventssaas.generalbusiness.enums;

public enum MailBasicTemplateType {

	CREATE_USER("CREATE_USER"), FORGOT_PASSWORD("FORGOT_PASSWORD");

	private String mailBasicTemplateType;

	public String getMailBasicTemplateType() {
		return this.mailBasicTemplateType;
	}

	MailBasicTemplateType(final String mailBasicTemplateType) {
		this.mailBasicTemplateType = mailBasicTemplateType;
	}

}
