/**
 *
 */
package br.com.eventssaas.generalbusiness.entity;

import java.io.Serializable;
import java.util.Calendar;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "profile_message_chat")
public class MessageChat implements Serializable {

	private static final long serialVersionUID = 2895458504811350595L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@JsonIgnore
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "creation_date")
	private Calendar creationDate = Calendar.getInstance();

	@NotNull
	@Column(name = "is_read", columnDefinition = "bit(1) default 0")
	private boolean read = false;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "read_date")
	private Calendar readDate;

	@NotNull
	@Column(name = "is_deleted_by_author", columnDefinition = "bit(1) default 0")
	private boolean deletedByAuthor = false;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "deleted_by_author_date")
	private Calendar deletedByAuthorDate;

	@NotNull
	@Column(name = "is_deleted_by_target", columnDefinition = "bit(1) default 0")
	private boolean deletedByTarget = false;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "deleted_by_target_profile_date")
	private Calendar deletedByTargetProfileDate;

	@NotNull
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "event_id")
	private Event event;

	@NotNull
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "author_profile_id")
	private Profile author;

	@NotNull
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "target_profile_id")
	private Profile targetProfile;

	@Length(max = 1000)
	@Column(name = "text")
	private String text;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "image_id")
	private Image image;

	@Column(name = "image_url")
	private String imageUrl;

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Calendar getCreationDate() {
		return this.creationDate;
	}

	public void setCreationDate(Calendar creationDate) {
		this.creationDate = creationDate;
	}

	public boolean isRead() {
		return this.read;
	}

	public void setRead(boolean read) {
		this.read = read;
	}

	public Profile getAuthor() {
		return this.author;
	}

	public void setAuthor(Profile author) {
		this.author = author;
	}

	public Profile getTargetProfile() {
		return this.targetProfile;
	}

	public void setTargetProfile(Profile targetProfile) {
		this.targetProfile = targetProfile;
	}

	public String getText() {
		return this.text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public Image getImage() {
		return this.image;
	}

	public void setImage(Image image) {
		this.image = image;
	}

	public String getImageUrl() {
		return this.imageUrl;
	}

	public void setImageUrl(String textUrl) {
		this.imageUrl = textUrl;
	}

	public Event getEvent() {
		return this.event;
	}

	public void setEvent(Event event) {
		this.event = event;
	}

	@PrePersist
	void creationDate() {
		this.creationDate = Calendar.getInstance();
	}

	public boolean isDeletedByTarget() {
		return this.deletedByTarget;
	}

	public void setDeletedByTarget(boolean deletedByTarget) {
		this.deletedByTarget = deletedByTarget;
	}

	public Calendar getReadDate() {
		return this.readDate;
	}

	public void setReadDate(Calendar readDate) {
		this.readDate = readDate;
	}

	public boolean isDeletedByAuthor() {
		return this.deletedByAuthor;
	}

	public void setDeletedByAuthor(boolean deletedByAuthor) {
		this.deletedByAuthor = deletedByAuthor;
	}

	public Calendar getDeletedByAuthorDate() {
		return this.deletedByAuthorDate;
	}

	public void setDeletedByAuthorDate(Calendar deletedByAuthorDate) {
		this.deletedByAuthorDate = deletedByAuthorDate;
	}

	public Calendar getDeletedByTargetProfileDate() {
		return this.deletedByTargetProfileDate;
	}

	public void setDeletedByTargetProfileDate(Calendar deletedByTargetProfileDate) {
		this.deletedByTargetProfileDate = deletedByTargetProfileDate;
	}

}
