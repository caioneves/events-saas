package br.com.eventssaas.generalbusiness.repository;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.eventssaas.generalbusiness.entity.Article;

@Repository
public interface ArticleRepository extends JpaRepository<Article, Long> {

	public List<Article> findByEventIdAndBlackPostAndBlockedAndBlockedByAuthorOrderByCreationDateDesc(Long eventId,
			Boolean isBlackPost, Boolean blocked, Boolean blockedByAuthor, Pageable pageable);

	public List<Article> findByEventId(Long eventId, Pageable pageable);

	public Article findByIdAndEventId(Long articleId, Long eventId);

}
