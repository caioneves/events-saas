package br.com.eventssaas.generalbusiness.entity;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Set;

import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "article_commentary")
@Audited
public class Commentary implements Serializable {

	private static final long serialVersionUID = 6160989475867873481L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@JsonIgnore
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "creation_date")
	@NotAudited
	private Calendar creationDate = Calendar.getInstance();

	@NotNull
	@NotBlank
	@Length(max = 1000)
	@Column(name = "comment")
	private String comment;

	@NotNull
	@JsonIgnore
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "event_id")
	@NotAudited
	private Event event;

	@NotNull
	@JsonIgnore
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "article_id")
	private Article article;

	@NotNull
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "author_profile_id")
	@NotAudited
	private Profile author;

	@NotAudited
	@ElementCollection
	@CollectionTable(name = "article_commentary_black_words", joinColumns = @JoinColumn(name = "commentary_id"))
	@Column(name = "black_word")
	private Set<String> listBlackCommentWords;

	@Column(name = "is_black_comment", columnDefinition = "bit(1) default 0")
	private boolean blackComment = false;

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public long getDisplayTime() {
		return this.creationDate.getTimeInMillis();
	}

	public String getComment() {
		return this.comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public Event getEvent() {
		return this.event;
	}

	public void setEvent(Event event) {
		this.event = event;
	}

	public Article getArticle() {
		return this.article;
	}

	public void setArticle(Article article) {
		this.article = article;
	}

	public Profile getAuthor() {
		return this.author;
	}

	public void setAuthor(Profile author) {
		this.author = author;
	}

	public boolean isBlackComment() {
		return this.blackComment;
	}

	public void setBlackComment(boolean blackComment) {
		this.blackComment = blackComment;
	}

	public Calendar getCreationDate() {
		return this.creationDate;
	}

	public void setCreationDate(Calendar creationDate) {
		this.creationDate = creationDate;
	}

	public Set<String> getListBlackCommentWords() {
		return this.listBlackCommentWords;
	}

	public void setListBlackCommentWords(Set<String> listBlackCommentWords) {
		this.listBlackCommentWords = listBlackCommentWords;
	}

}
