package br.com.eventssaas.generalbusiness.init;

import java.util.Collections;

import javax.annotation.Resource;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.connection.jedis.JedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.index.IndexConfiguration;
import org.springframework.data.redis.core.index.IndexDefinition;
import org.springframework.data.redis.core.index.SimpleIndexDefinition;
import org.springframework.data.redis.repository.configuration.EnableRedisRepositories;
import org.springframework.data.redis.serializer.Jackson2JsonRedisSerializer;
import org.springframework.data.redis.serializer.StringRedisSerializer;

import br.com.eventssaas.generalbusiness.entity.RedisToken;
import redis.clients.jedis.JedisPoolConfig;

@Configuration
@EnableRedisRepositories("br.com.eventssaas.generalbusiness.repository.redis")
@PropertySource("classpath:application.properties")
@ComponentScan("br.com.eventssaas")
public class RedisConfig {
	private static final String PROPERTY_NAME_HOST_NAME = "redis.host";
	private static final String PROPERTY_NAME_PORT = "redis.port";
	private static final String PROPERTY_NAME_PASSWORD = "redis.password";

	@Resource
	private Environment env;

	@Bean
	public RedisConnectionFactory connectionFactory() {

		JedisPoolConfig poolConfig = new JedisPoolConfig();
		poolConfig.setMaxTotal(10);
		poolConfig.setMaxIdle(5);
		poolConfig.setMinIdle(1);
		poolConfig.setTestOnBorrow(true);
		poolConfig.setTestOnReturn(true);
		poolConfig.setTestWhileIdle(true);
		poolConfig.setMaxWaitMillis(10 * 1000);

		JedisConnectionFactory factory = new JedisConnectionFactory(poolConfig);

		factory.setHostName(this.env.getRequiredProperty(PROPERTY_NAME_HOST_NAME));
		factory.setPort(Integer.parseInt(this.env.getRequiredProperty(PROPERTY_NAME_PORT)));
		factory.setPassword(this.env.getRequiredProperty(PROPERTY_NAME_PASSWORD));
		factory.setUsePool(true);
		return factory;
	}

	@Bean
	public RedisTemplate<String, RedisToken> redisTemplate() {
		RedisTemplate<String, RedisToken> template = new RedisTemplate<String, RedisToken>();

		template.setKeySerializer(new StringRedisSerializer());
		template.setHashValueSerializer(new Jackson2JsonRedisSerializer<RedisToken>(RedisToken.class));

		template.setValueSerializer(new Jackson2JsonRedisSerializer<RedisToken>(RedisToken.class));

		template.setConnectionFactory(this.connectionFactory());
		return template;
	}

	// @Bean
	// public RedisMappingContext keyValueMappingContext() {
	// return new RedisMappingContext(
	// new MappingConfiguration(
	// new KeyspaceConfiguration(), new ProfileRuleIndexConfiguration()));
	// }

	public static class ProfileRuleIndexConfiguration extends IndexConfiguration {
		@Override
		protected Iterable<IndexDefinition> initialConfiguration() {
			return Collections.singleton(new SimpleIndexDefinition("token", "profileId"));
		}
	}

}
