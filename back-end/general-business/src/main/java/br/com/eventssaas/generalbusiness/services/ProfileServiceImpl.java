package br.com.eventssaas.generalbusiness.services;

import java.util.List;

import org.apache.commons.lang3.RandomStringUtils;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.eventssaas.generalbusiness.entity.Event;
import br.com.eventssaas.generalbusiness.entity.Image;
import br.com.eventssaas.generalbusiness.entity.Profile;
import br.com.eventssaas.generalbusiness.entity.ProfileRole;
import br.com.eventssaas.generalbusiness.entity.User;
import br.com.eventssaas.generalbusiness.enums.MailBasicTemplateType;
import br.com.eventssaas.generalbusiness.enums.ProfileRoleType;
import br.com.eventssaas.generalbusiness.exceptions.NotFoundException;
import br.com.eventssaas.generalbusiness.exceptions.PasswordAndPasswordConfirmationNotMatchException;
import br.com.eventssaas.generalbusiness.repository.EventRepository;
import br.com.eventssaas.generalbusiness.repository.ImageRepository;
import br.com.eventssaas.generalbusiness.repository.ProfileRepository;
import br.com.eventssaas.generalbusiness.repository.ProfileRoleRepository;
import br.com.eventssaas.generalbusiness.repository.UserRepository;
import br.com.eventssaas.generalbusiness.util.QRCodeGenerator;
import br.com.eventssaas.generalbusiness.validation.EmailExistsException;

@Service("br.com.eventssaas.generalbusiness.services.ProfileService")
public class ProfileServiceImpl implements IProfileService {

	@Autowired
	private EventRepository eventRepository;

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private ProfileRepository profileRepository;

	@Autowired
	private ImageRepository imageRepository;

	@Autowired
	private PasswordEncoder passwordEncoder;

	@Autowired
	private ProfileRoleRepository profileRoleRepository;

	@Autowired
	private IMailService mailService;

	@Override
	@Transactional
	public Profile createUser(String registerJson, ProfileRoleType profileRoleType, Long eventId,
			boolean generatePassword) throws Exception {

		Event event = this.eventRepository.findOne(eventId);
		if (event == null) {
			throw new NotFoundException();
		}

		JSONObject jsonObj = new JSONObject(registerJson);

		Profile profile = new Profile();

		if (this.userRepository.exists(profile.getUser().getEmail())) {
			throw new EmailExistsException("Email já cadastrado");
		}

		User userTO = profile.getUser();
		if (!userTO.getPassword().equals(userTO.getPasswordConfirmation())) {
			throw new PasswordAndPasswordConfirmationNotMatchException("Erro na confirmação da senha");
		}

		if (profile.getPicture() != null && this.imageRepository.exists(profile.getPicture().getId())) {
			Image img = this.imageRepository.findOne(profile.getPicture().getId());
			profile.setPicture(img);
		}

		User user = profile.getUser();
		user.setEnabled(true);

		String password = null;
		if (generatePassword) {
			password = RandomStringUtils.randomAlphabetic(8);
		} else {
			password = userTO.getPassword();
		}

		user.setPassword(this.passwordEncoder.encode(password));
		user.setPasswordConfirmation(user.getPassword());

		user = this.userRepository.save(user);

		profile.setUser(user);

		profile = this.profileRepository.save(profile);

		ProfileRole profileRole = new ProfileRole();
		profileRole.setEvent(event);
		profileRole.setProfile(profile);
		profileRole.setType(profile.getListProfileRoles().get(0).getType());
		this.profileRoleRepository.save(profileRole);

		profile.getListProfileRoles().add(profileRole);

		// Gerar QR Code
		profile.setQrcode(QRCodeGenerator.qrCodePNGBase64(profile.getId().toString()));
		profile = this.profileRepository.save(profile);

		if (generatePassword) {
			this.mailService.sendEmailQRCodeAndPasswordIfExist(MailBasicTemplateType.FORGOT_PASSWORD,
					profile.getUser().getEmail(), profile.getFirstName(), password, profile.getQrcode());
		} else {
			this.mailService.sendEmailQRCodeAndPasswordIfExist(MailBasicTemplateType.FORGOT_PASSWORD,
					profile.getUser().getEmail(), profile.getFirstName(), null, profile.getQrcode());
		}

		return profile;

	}

	@Override
	public void createQRCodeInProfileWhenQRCodeIsNull() throws Exception {

		List<Profile> profiles = this.profileRepository.findByQrcodeIsNull();

		for (Profile profile : profiles) {
			profile.setQrcode(QRCodeGenerator.qrCodePNGBase64(profile.getId().toString()));
			this.profileRepository.save(profile);

		}
	}

}
