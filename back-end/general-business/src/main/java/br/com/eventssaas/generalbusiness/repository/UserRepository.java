package br.com.eventssaas.generalbusiness.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import br.com.eventssaas.generalbusiness.entity.User;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {

	User findByEmailIgnoreCase(String email);

	User findByEmailIgnoreCaseAndPassword(String email, String password);

	@Query("select count(u) > 0 from User u where lower(u.email) = lower(:email)")
	boolean exists(@Param("email") String email);
}
