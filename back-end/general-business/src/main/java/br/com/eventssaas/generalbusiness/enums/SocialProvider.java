package br.com.eventssaas.generalbusiness.enums;

public enum SocialProvider {

	FACEBOOK("Facebook"), LINKEDIN("Linkedin"), LOCAL("Local Provider");

	private String providerType;

	public String getProviderType() {
		return this.providerType;
	}

	SocialProvider(final String providerType) {
		this.providerType = providerType;
	}

	/**
	 * Verify if the type of {@link Enum} is {@link #FACEBOOK}
	 *
	 * @author René Araújo Vasconcelos [renearaujo@gmail.com] - 18 de mar de
	 *         2017 20:40:00
	 *
	 * @return Returns <code>true</code> if the {@link #name()} of {@link Enum}
	 *         is {@link #FACEBOOK}, otherwise returns <code>false</code>
	 */
	public boolean isFacebook() {

		if (this.name().equals(FACEBOOK.name())) {
			return true;
		}

		return false;
	}

	/**
	 * Verify if the type of {@link Enum} is {@link #LINKEDIN}
	 *
	 * @author René Araújo Vasconcelos [renearaujo@gmail.com] - 18 de mar de
	 *         2017 20:40:00
	 *
	 * @return Returns <code>true</code> if the {@link #name()} of {@link Enum}
	 *         is {@link #LINKEDIN}, otherwise returns <code>false</code>
	 */
	public boolean isLinkedin() {

		if (this.name().equals(LINKEDIN.name())) {
			return true;
		}

		return false;
	}

}
