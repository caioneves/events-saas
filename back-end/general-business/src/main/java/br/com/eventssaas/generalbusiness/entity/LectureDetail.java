package br.com.eventssaas.generalbusiness.entity;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.Lob;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Max;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;

import com.fasterxml.jackson.annotation.JsonIgnore;

import br.com.eventssaas.generalbusiness.enums.LectureDetailType;

@Entity
@Table(name = "lecture")
public class LectureDetail implements Serializable {

	private static final long serialVersionUID = -6868313199692923871L;

	@Id
	@GeneratedValue(strategy = GenerationType.TABLE)
	@Column(name = "id", updatable = false, nullable = false)
	private Long id;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "creation_date")
	private Calendar creationDate = Calendar.getInstance();

	@NotNull
	@Enumerated(EnumType.STRING)
	@Column(name = "type", columnDefinition = "varchar(20) default 'LECTURE'")
	private LectureDetailType type = LectureDetailType.LECTURE;

	@NotNull
	@NotBlank
	@Column(name = "title")
	private String title;

	@Lob
	@Column(name = "description")
	private String description;

	@NotNull
	@Max(value = 100)
	@Column(name = "priority", columnDefinition = "int(3) default 0")
	private Integer priority = 0;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "start_time")
	private Date startTime;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "end_time")
	private Date endTime;

	@NotNull
	@JsonIgnore
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "event_id")
	private Event event;

	@NotNull
	@JsonIgnore
	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "author_profile_id")
	private Profile author;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "parent_lecture_id", nullable = true)
	@JsonIgnore
	private LectureDetail parentLecture;

	@OneToMany(fetch = FetchType.EAGER, mappedBy = "parentLecture", cascade = CascadeType.ALL)
	private List<LectureDetail> listSubItems;

	@ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.PERSIST)
	@JoinColumn(name = "image_id")
	private Image image;

	@Column(name = "schedule_background_color")
	private String scheduleBackgroundColor;

	@Column(name = "room")
	private String room;

	@Column(name = "detail_url")
	private String detailUrl;

	@ElementCollection
	@CollectionTable(name = "lecture_tag", joinColumns = @JoinColumn(name = "lecture_detail_id"))
	@Column(name = "tag_name")
	private Set<String> listTags;

	@ManyToMany
	@JoinTable(name = "lecture_has_speakers", joinColumns = {
			@JoinColumn(name = "lecture_id", referencedColumnName = "id") }, inverseJoinColumns = {
					@JoinColumn(name = "speaker_profile_id", referencedColumnName = "id") })
	private List<Profile> listSpeakers;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "lectureDetail", cascade = CascadeType.ALL)
	private List<DownloadItem> listFilesDownloadItem;

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getRoom() {
		return this.room;
	}

	public void setRoom(String room) {
		this.room = room;
	}

	public String getDetailUrl() {
		return this.detailUrl;
	}

	public void setDetailUrl(String detailUrl) {
		this.detailUrl = detailUrl;
	}

	public Set<String> getListTags() {
		return this.listTags;
	}

	public void setListTags(Set<String> listTags) {
		this.listTags = listTags;
	}

	public List<DownloadItem> getListFilesDownloadItem() {
		return this.listFilesDownloadItem;
	}

	public void setListFilesDownloadItem(List<DownloadItem> listFilesDownloadItem) {
		this.listFilesDownloadItem = listFilesDownloadItem;
	}

	public List<Profile> getListSpeakers() {
		return this.listSpeakers;
	}

	public void setListSpeakers(List<Profile> listSpeakers) {
		this.listSpeakers = listSpeakers;
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTitle() {
		return this.title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Date getStartTime() {
		return this.startTime;
	}

	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}

	public Date getEndTime() {
		return this.endTime;
	}

	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}

	public Event getEvent() {
		return this.event;
	}

	public void setEvent(Event event) {
		this.event = event;
	}

	public Profile getAuthor() {
		return this.author;
	}

	public void setAuthor(Profile author) {
		this.author = author;
	}

	public List<LectureDetail> getListSubItems() {
		return this.listSubItems;
	}

	public void setListSubItems(List<LectureDetail> listSubItems) {
		this.listSubItems = listSubItems;
	}

	public LectureDetail getParentLecture() {
		return this.parentLecture;
	}

	public void setParentLecture(LectureDetail parentLecture) {
		this.parentLecture = parentLecture;
	}

	public Image getImage() {
		return this.image;
	}

	public void setImage(Image image) {
		this.image = image;
	}

	public Integer getPriority() {
		return this.priority;
	}

	public void setPriority(Integer priority) {
		this.priority = priority;
	}

	public String getScheduleBackgroundColor() {
		return this.scheduleBackgroundColor;
	}

	public void setScheduleBackgroundColor(String scheduleBackgroundColor) {
		this.scheduleBackgroundColor = scheduleBackgroundColor;
	}

	public LectureDetailType getType() {
		return this.type;
	}

	public void setType(LectureDetailType type) {
		this.type = type;
	}

	public Calendar getCreationDate() {
		return this.creationDate;
	}

	public void setCreationDate(Calendar creationDate) {
		this.creationDate = creationDate;
	}

}
