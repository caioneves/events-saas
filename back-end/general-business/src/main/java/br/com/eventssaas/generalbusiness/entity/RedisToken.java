package br.com.eventssaas.generalbusiness.entity;

import java.util.Date;
import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.redis.core.RedisHash;
import org.springframework.data.redis.core.index.Indexed;

@RedisHash("token")
public class RedisToken {

	@Id
	private String token;

	private Date lastModified;
	private List<RedisProfileRole> profileRoles;

	private Long eventId;

	@Indexed
	private Long profileId;

	public String getToken() {
		return this.token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public Date getLastModified() {
		return this.lastModified;
	}

	public void setLastModified(Date lastModified) {
		this.lastModified = lastModified;
	}

	public List<RedisProfileRole> getProfileRoles() {
		return this.profileRoles;
	}

	public void setProfileRoles(List<RedisProfileRole> profileRoles) {
		this.profileRoles = profileRoles;
	}

	public Long getProfileId() {
		return this.profileId;
	}

	public void setProfileId(Long profileId) {
		this.profileId = profileId;
	}

	@Override
	public String toString() {
		return this.token + ":" + this.eventId + ":" + this.profileId;
	}

	public Long getEventId() {
		return this.eventId;
	}

	public void setEventId(Long eventId) {
		this.eventId = eventId;
	}

}
