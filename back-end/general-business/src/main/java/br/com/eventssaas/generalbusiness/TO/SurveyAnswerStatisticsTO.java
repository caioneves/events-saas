package br.com.eventssaas.generalbusiness.TO;

public class SurveyAnswerStatisticsTO {

	private Long answerId;
	private Long questionId;
	private Long count;

	public SurveyAnswerStatisticsTO(Long answerId, Long questionId, Long cnt) {
		this.answerId = answerId;
		this.count = cnt;
		this.questionId = questionId;
	}

	public Long getAnswerId() {
		return this.answerId;
	}

	public void setAnswerId(Long answerId) {
		this.answerId = answerId;
	}

	public Long getQuestionId() {
		return this.questionId;
	}

	public void setQuestionId(Long questionId) {
		this.questionId = questionId;
	}

	public Long getCount() {
		return this.count;
	}

	public void setCount(Long count) {
		this.count = count;
	}

}