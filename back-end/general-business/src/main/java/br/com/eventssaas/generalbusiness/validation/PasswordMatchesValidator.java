package br.com.eventssaas.generalbusiness.validation;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import br.com.eventssaas.generalbusiness.entity.User;

public class PasswordMatchesValidator implements ConstraintValidator<PasswordMatches, Object> {

	@Override
	public void initialize(final PasswordMatches constraintAnnotation) {
		//
	}

	@Override
	public boolean isValid(final Object obj, final ConstraintValidatorContext context) {
		final User user = (User) obj;
		return user.getPassword().equals(user.getPasswordConfirmation());
	}

}
