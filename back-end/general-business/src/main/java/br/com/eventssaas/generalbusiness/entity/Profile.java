/**
 *
 */
package br.com.eventssaas.generalbusiness.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Past;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;

import br.com.eventssaas.generalbusiness.enums.DocumentType;
import br.com.eventssaas.generalbusiness.enums.GenderType;
import br.com.eventssaas.generalbusiness.enums.ProfileRoleType;

@Entity
@Table(name = "profile")
public class Profile implements Serializable {

	private static final long serialVersionUID = 6441398859978622898L;
	private static final String IMAGE_REST = "/app/rest/service/image/";

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy HH:mm:ssZ")
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "creation_date")
	private Calendar creationDate = Calendar.getInstance();

	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy HH:mm:ssZ")
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "last_update_date")
	private Calendar lastUpdateDate;

	@NotBlank
	@NotNull
	@Length(max = 100)
	@Column(name = "first_name")
	private String firstName;

	@NotBlank
	@NotNull
	@Length(max = 100)
	@Column(name = "last_name")
	private String lastName;

	@Enumerated(EnumType.STRING)
	@Column(name = "gender")
	private GenderType gender;

	@Past
	@Temporal(TemporalType.DATE)
	@Column(name = "birth_date")
	private Calendar birthDate;

	@Length(max = 25)
	@Column(name = "document_number", columnDefinition = "varchar(25)")
	private String documentNumber;

	@Enumerated(EnumType.STRING)
	@Column(name = "document_type", columnDefinition = "varchar(30) default 'CPF'")
	private DocumentType documentType = DocumentType.CPF;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "picture")
	private Image picture;

	@Column(name = "picture_url")
	private String pictureUrl;

	@NotNull
	@OneToOne(cascade = CascadeType.MERGE)
	@JoinColumn(name = "user_id")
	private User user;

	// QR Code as Base 64 string
	@Length(max = 3000)
	@Column(name = "qrcode")
	private String qrcode;

	@Length(max = 1000)
	@Column(name = "minibio")
	private String minibio;

	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "contact_info")
	private ContactInfo contactInfo;

	@Column(name = "job_role")
	private String jobRole;

	@Column(name = "is_show_job_role", columnDefinition = "bit(1) default 1")
	private boolean showJobRole = true;

	@Column(name = "company")
	private String company;

	@Column(name = "facebook_url")
	private String facebookUrl;

	@Column(name = "twitter_url")
	private String twitterUrl;

	@Column(name = "linkedin_url")
	private String linkedInUrl;

	@Column(name = "instagram_url")
	private String instagramUrl;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "profile", cascade = CascadeType.ALL)
	@Column(name = "profile_role_id")
	private List<ProfileRole> listProfileRoles;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "profile", cascade = CascadeType.ALL)
	@Column(name = "social_profile_id")
	private List<SocialProfile> listSocialProfile;

	@Transient
	private boolean hasFacebookSocialProfileEnable = false;

	@Transient
	private boolean hasLinkedSocialProfileEnable = false;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "author", cascade = CascadeType.ALL)
	@Column(name = "message_chat_id")
	private List<MessageChat> listMessageChat;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "author", cascade = CascadeType.ALL)
	@Column(name = "profile_survey_answer_id")
	private List<ProfileSurveyAnswer> listProfileSurveyAnswer;

	@Transient
	private String token;

	@Transient
	private String type;

	@Transient
	private Long eventIdDemo = null;

	@Transient
	private List<ProfileRoleType> currentEventRoles;

	@JsonIgnore
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "profileCheckedIn", cascade = CascadeType.ALL)
	@Column(name = "profile_checkin_id")
	private List<CheckIn> listCheckins;

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((this.id == null) ? 0 : this.id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (this.getClass() != obj.getClass()) {
			return false;
		}
		Profile other = (Profile) obj;
		if (this.id == null) {
			if (other.id != null) {
				return false;
			}
		} else if (!this.id.equals(other.id)) {
			return false;
		}
		return true;
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Calendar getCreationDate() {
		return this.creationDate;
	}

	public void setCreationDate(Calendar creationDate) {
		this.creationDate = creationDate;
	}

	public Calendar getLastUpdateDate() {
		return this.lastUpdateDate;
	}

	public void setLastUpdateDate(Calendar lastUpdateDate) {
		this.lastUpdateDate = lastUpdateDate;
	}

	public String getFirstName() {
		return this.firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return this.lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public GenderType getGender() {
		return this.gender;
	}

	public void setGender(GenderType gender) {
		this.gender = gender;
	}

	public String getDocumentNumber() {
		return this.documentNumber;
	}

	public void setDocumentNumber(String documentNumber) {
		this.documentNumber = documentNumber;
	}

	public DocumentType getDocumentType() {
		return this.documentType;
	}

	public void setDocumentType(DocumentType documentType) {
		this.documentType = documentType;
	}

	public Image getPicture() {
		return this.picture;
	}

	public void setPicture(Image picture) {
		this.picture = picture;
	}

	public String getPictureUrl() {
		if (this.getPicture() != null) {
			this.setPictureUrl(IMAGE_REST + this.getPicture().getId());
		}
		return this.pictureUrl;
	}

	public void setPictureUrl(String pictureUrl) {
		this.pictureUrl = pictureUrl;
	}

	public User getUser() {
		return this.user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public String getQrcode() {
		return this.qrcode;
	}

	public void setQrcode(String qrcode) {
		this.qrcode = qrcode;
	}

	public String getMinibio() {
		return this.minibio;
	}

	public void setMinibio(String minibio) {
		this.minibio = minibio;
	}

	@JsonIgnore
	public ContactInfo getContactInfo() {
		return this.contactInfo;
	}

	public void setContactInfo(ContactInfo contactInfo) {
		this.contactInfo = contactInfo;
	}

	public String getJobRole() {
		return this.jobRole;
	}

	public void setJobRole(String jobRole) {
		this.jobRole = jobRole;
	}

	public boolean isShowJobRole() {
		return this.showJobRole;
	}

	public void setShowJobRole(boolean showJobRole) {
		this.showJobRole = showJobRole;
	}

	public String getCompany() {
		return this.company;
	}

	public void setCompany(String company) {
		this.company = company;
	}

	@JsonIgnore
	public List<ProfileRole> getListProfileRoles() {
		if (this.listProfileRoles == null) {
			this.listProfileRoles = new ArrayList<>();
		}
		return this.listProfileRoles;
	}

	public void setListProfileRoles(List<ProfileRole> listProfileRoles) {
		this.listProfileRoles = listProfileRoles;
	}

	@JsonIgnore
	public List<SocialProfile> getListSocialProfile() {
		return this.listSocialProfile;
	}

	public void setListSocialProfile(List<SocialProfile> listSocialProfile) {
		this.listSocialProfile = listSocialProfile;
	}

	public boolean isHasFacebookSocialProfileEnable() {
		this.checkSocialProfile();
		return this.hasFacebookSocialProfileEnable;
	}

	public boolean isHasLinkedSocialProfileEnable() {
		this.checkSocialProfile();
		return this.hasLinkedSocialProfileEnable;
	}

	private void checkSocialProfile() {
		for (SocialProfile sp : this.getListSocialProfile()) {
			if (!this.hasFacebookSocialProfileEnable && sp.getProviderId().equals("facebook") && sp.isEnabled()) {
				this.hasFacebookSocialProfileEnable = true;
			}
			if (!this.hasLinkedSocialProfileEnable && sp.getProviderId().equals("linkedin") && sp.isEnabled()) {
				this.hasLinkedSocialProfileEnable = true;
			}
			if (this.hasFacebookSocialProfileEnable && this.hasLinkedSocialProfileEnable) {
				break;
			}
		}
	}

	@JsonIgnore
	public List<MessageChat> getListMessageChat() {
		return this.listMessageChat;
	}

	public void setListMessageChat(List<MessageChat> listMessageChat) {
		this.listMessageChat = listMessageChat;
	}

	@JsonIgnore
	public List<ProfileSurveyAnswer> getListProfileSurveyAnswer() {
		return this.listProfileSurveyAnswer;
	}

	public void setListProfileSurveyAnswer(List<ProfileSurveyAnswer> listProfileSurveyAnswer) {
		this.listProfileSurveyAnswer = listProfileSurveyAnswer;
	}

	public String getFacebookUrl() {
		return this.facebookUrl;
	}

	public void setFacebookUrl(String facebookUrl) {
		this.facebookUrl = facebookUrl;
	}

	public String getTwitterUrl() {
		return this.twitterUrl;
	}

	public void setTwitterUrl(String twitterUrl) {
		this.twitterUrl = twitterUrl;
	}

	public String getLinkedInUrl() {
		return this.linkedInUrl;
	}

	public void setLinkedInUrl(String linkedInUrl) {
		this.linkedInUrl = linkedInUrl;
	}

	public String getInstagramUrl() {
		return this.instagramUrl;
	}

	public void setInstagramUrl(String instagramUrl) {
		this.instagramUrl = instagramUrl;
	}

	public String getToken() {
		return this.token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	@JsonIgnore
	public List<CheckIn> getListCheckins() {
		return this.listCheckins;
	}

	public void setListCheckins(List<CheckIn> listCheckins) {
		this.listCheckins = listCheckins;
	}

	public String getType() {
		return this.type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getFullName() {
		return this.firstName + " " + this.lastName;
	}

	public List<ProfileRoleType> getCurrentEventRoles() {
		return this.currentEventRoles;
	}

	public void setCurrentEventRoles(List<ProfileRoleType> currentEventRoles) {
		this.currentEventRoles = currentEventRoles;
	}

	public Calendar getBirthDate() {
		return this.birthDate;
	}

	public void setBirthDate(Calendar birthDate) {
		this.birthDate = birthDate;
	}

	public Long getEventIdDemo() {
		return this.eventIdDemo;
	}

	public void setEventIdDemo(Long eventIdDemo) {
		this.eventIdDemo = eventIdDemo;
	}

}
