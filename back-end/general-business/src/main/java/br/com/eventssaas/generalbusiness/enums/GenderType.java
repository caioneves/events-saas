package br.com.eventssaas.generalbusiness.enums;

public enum GenderType {

	MALE("m"), FEMALE("f");

	private String genderType;

	public String getGenderType() {
		return this.genderType;
	}

	GenderType(final String genderType) {
		this.genderType = genderType;
	}
}
