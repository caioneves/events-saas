package br.com.eventssaas.generalbusiness.repository;

import java.util.Date;

import br.com.eventssaas.generalbusiness.entity.Profile;

public interface NoBytes {

	public Long getId();

	public Date getCreationDate();

	public String getFileName();

	public long getSize();

	public Profile getAuthor();

}