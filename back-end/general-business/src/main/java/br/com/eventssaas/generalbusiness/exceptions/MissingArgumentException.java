package br.com.eventssaas.generalbusiness.exceptions;

public class MissingArgumentException extends Exception {

	public MissingArgumentException(final String message) {
		super(message);
	}

	public MissingArgumentException() {
		super();
	}

	/**
	 *
	 */
	private static final long serialVersionUID = -3530360854119820210L;

}
