package br.com.eventssaas.generalbusiness.init;

import java.util.Properties;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;

/**
 * Email settings
 *
 * @author René Araújo Vasconcelos [renearaujo@gmail.com] - 13 de abr de 2017
 *         16:00:50
 */
@Configuration
@PropertySource("classpath:mail.properties")
public class MailConfig {

	@Bean
	public JavaMailSender getMailSender(Environment env) {
		JavaMailSenderImpl mailSender = new JavaMailSenderImpl();

		mailSender.setHost(env.getProperty("spring.mail.host"));
		mailSender.setPort(Integer.valueOf(env.getProperty("spring.mail.port")).intValue());
		mailSender.setUsername(env.getProperty("spring.mail.username"));
		mailSender.setPassword(env.getProperty("spring.mail.password"));

		Properties javaMailProperties = new Properties();
		javaMailProperties.put("mail.smtp.starttls.enable",
				env.getProperty("spring.mail.properties.mail.smtps.starttls.enable"));
		javaMailProperties.put("mail.smtp.auth", env.getProperty("spring.mail.properties.mail.smtps.auth"));
		javaMailProperties.put("mail.transport.protocol", env.getProperty("spring.mail.protocol"));
		javaMailProperties.put("mail.debug", "true");

		javaMailProperties.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
		javaMailProperties.put("mail.smtp.socketFactory.fallback",
				"spring.mail.properties.mail.socketFactory.fallback");

		mailSender.setJavaMailProperties(javaMailProperties);

		return mailSender;
	}

}