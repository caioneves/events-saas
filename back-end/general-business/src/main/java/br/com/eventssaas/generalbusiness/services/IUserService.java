package br.com.eventssaas.generalbusiness.services;

import br.com.eventssaas.generalbusiness.entity.PasswordResetToken;
import br.com.eventssaas.generalbusiness.entity.User;
import br.com.eventssaas.generalbusiness.entity.VerificationToken;
import br.com.eventssaas.generalbusiness.exceptions.NotFoundException;
import br.com.eventssaas.generalbusiness.validation.EmailExistsException;

public interface IUserService {

	User create(User user) throws EmailExistsException;

	User findUserByEmail(String email);

	void createPasswordResetTokenForUser(User user, String token);

	void forgetPassword(final String userEmail) throws NotFoundException;

	String createRandomPasswordForUser(final User user);

	PasswordResetToken getPasswordResetToken(String token);

	void changeUserPassword(final User user, final String password, final String passwordConfirmation);

	void changeUserPassword(User user, String password, String passwordConfirmation, boolean resetedPassword);

	VerificationToken createVerificationTokenForUser(User user);

	VerificationToken getVerificationToken(String token);

	void save(User user);

	User confirmUserRegistration(final VerificationToken token);

}
