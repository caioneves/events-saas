package br.com.eventssaas.generalbusiness.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import br.com.eventssaas.generalbusiness.entity.LectureSavedAlarmProfile;
import br.com.eventssaas.generalbusiness.entity.Profile;
import br.com.eventssaas.generalbusiness.entity.User;

@Repository
public interface ProfileRepository extends JpaRepository<Profile, Long> {

	public Profile findByUserEmailIgnoreCaseAndUserPassword(String login, String password);

	public Profile findByUserEmailIgnoreCase(String email);

	public Profile findByUserEmailOrDocumentNumber(String login, String cpf);

	public Profile findByUserId(Long userId);

	public Profile findByUser(final User User);

	public List<Profile> findByQrcodeIsNull();

	@Query("select lsp from LectureSavedAlarmProfile as lsp where lsp.alarm = :alarm AND lsp.event.id = :eventId AND lsp.profile.id = :profileId AND lsp.lectureDetail.startTime >= :startTime ORDER BY lsp.lectureDetail.startTime desc")
	public List<LectureSavedAlarmProfile> findLectureSavedAlarmProfileByAlarmAndEventIdAndProfileIdAndLectureDetailStartTimeGreatherThanNowOrderByLectureDetailStartTimeDesc(
			@Param("alarm") boolean alarm, @Param("eventId") Long eventId, @Param("profileId") Long profileId,
			@Param("startTime") Date startTime);

	public List<Profile> findByListProfileRolesEventIdOrderByFirstName(Long eventId);

	@Query("SELECT DISTINCT p FROM Profile p inner join p.listProfileRoles profileRoles WHERE profileRoles.event.id = :eventId and p.id != :profileId")
	public List<Profile> findDistinctByListProfileRolesEventIdAndNotSelfProfileLoggedInOrderByFirstName(
			@Param("eventId") Long eventId, @Param("profileId") Long profileId);

	public Profile findByIdAndListProfileRolesEventId(Long profileid, Long eventId);

	public List<Profile> findByListProfileRolesEventId(Long eventId);

	@Query("SELECT  p FROM Profile p inner join p.listSocialProfile  social WHERE social.providerUserId= :providerUserId and social.providerId= :providerId")
	public Profile findProfileByProviderUserIdAndProviderId(@Param("providerUserId") String providerUserId,
			@Param("providerId") String providerId);

	@Query("SELECT p FROM Profile p inner join p.listCheckins checkin WHERE checkin.event.id=:eventId and p.id=:profileId")
	public Profile findProfileCheckedInByEvent(@Param("profileId") Long profileId, @Param("eventId") Long eventId);

	@Query("SELECT DISTINCT p FROM Profile p inner join p.listCheckins checkin WHERE checkin.event.id=:eventId and p.id != :profileId")
	public List<Profile> findProfilesCheckedInEventAndNotSelfProfileLoggedIn(@Param("eventId") Long eventId,
			@Param("profileId") Long profileId);

}
