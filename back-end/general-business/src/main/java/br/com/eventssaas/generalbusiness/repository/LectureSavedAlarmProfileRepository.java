package br.com.eventssaas.generalbusiness.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.eventssaas.generalbusiness.entity.LectureSavedAlarmProfile;
import br.com.eventssaas.generalbusiness.entity.Profile;

/**
 * Created by inafalcao on 2/29/16.
 */
public interface LectureSavedAlarmProfileRepository extends JpaRepository<LectureSavedAlarmProfile, Long> {

	public List<LectureSavedAlarmProfile> findByProfileAndEventIdOrderByLectureDetailStartTimeDesc(Profile profile,
			Long eventId);

	public List<LectureSavedAlarmProfile> findByProfileIdAndEventIdAndLectureDetailId(Long profileId, Long eventId,
			Long lectureId);

	public LectureSavedAlarmProfile findByProfileIdAndEventIdAndLectureDetailIdAndAlarm(Long profileId, Long eventId,
			Long lectureId, boolean isAlarm);
}
