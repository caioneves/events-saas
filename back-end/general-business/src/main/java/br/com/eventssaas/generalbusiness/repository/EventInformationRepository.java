package br.com.eventssaas.generalbusiness.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.eventssaas.generalbusiness.entity.EventInformation;

/**
 * Created by inafalcao on 2/29/16.
 */
public interface EventInformationRepository extends JpaRepository<EventInformation, Long> {

}
