package br.com.eventssaas.generalbusiness.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.eventssaas.generalbusiness.entity.SocialProfile;

public interface SocialProfileRepository extends JpaRepository<SocialProfile, Long> {

	SocialProfile findByUsername(String username);

	SocialProfile findByUserId(String userId);

	SocialProfile findByProviderIdAndProviderUserId(String providerId, String providerUserId);

}
