package br.com.eventssaas.generalbusiness.envers;

import java.io.Serializable;
import java.text.DateFormat;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.envers.RevisionEntity;
import org.hibernate.envers.RevisionNumber;
import org.hibernate.envers.RevisionTimestamp;

import br.com.eventssaas.generalbusiness.entity.Profile;

@Entity
@Table(name = "REVISIONS")
@RevisionEntity(UserRevisionListener.class)
public class CustomRevisionEntity implements Serializable {

	private static final long serialVersionUID = -1255842407304508513L;

	@Id
	@GeneratedValue
	@RevisionNumber
	private int id;

	@RevisionTimestamp
	private long timestamp;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "profile_id")
	private Profile profile;

	private String username;

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	@Transient
	public Date getRevisionDate() {
		return new Date(this.timestamp);
	}

	public long getTimestamp() {
		return this.timestamp;
	}

	public void setTimestamp(long timestamp) {
		this.timestamp = timestamp;
	}

	public String getUsername() {
		return this.username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (!(o instanceof CustomRevisionEntity)) {
			return false;
		}

		CustomRevisionEntity that = (CustomRevisionEntity) o;

		if (this.id != that.id) {
			return false;
		}
		if (this.timestamp != that.timestamp) {
			return false;
		}
		if (this.timestamp != that.timestamp) {
			return false;
		}
		if (this.username != that.username) {
			return false;
		}

		return true;
	}

	public Profile getProfile() {
		return this.profile;
	}

	public void setProfile(Profile profile) {
		this.profile = profile;
	}

	@Override
	public int hashCode() {
		int result;
		result = this.id;
		result = 31 * result + (int) (this.timestamp ^ (this.timestamp >>> 32));
		return result;
	}

	@Override
	public String toString() {
		return "DefaultRevisionEntity(user = " + this.username + "id = " + this.id + ", revisionDate = "
				+ DateFormat.getDateTimeInstance().format(this.getRevisionDate()) + ")";
	}
}