package br.com.eventssaas.generalbusiness.enums;

public enum LectureDetailType {

	ACCREDITION("ACCREDITION"), TRACKING("TRACKING"), LECTURE("LECTURE"), COFFEE("COFFEE"), LUNCH("LUNCH"), DINNER(
			"DINNER"), KEY_NOTE("KEY_NOTE");

	private String lectureDetailType;

	public String getLectureDetailType() {
		return this.lectureDetailType;
	}

	LectureDetailType(final String lectureDetailType) {
		this.lectureDetailType = lectureDetailType;
	}
}
