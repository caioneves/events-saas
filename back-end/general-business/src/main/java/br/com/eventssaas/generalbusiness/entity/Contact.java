/**
 *
 */
package br.com.eventssaas.generalbusiness.entity;

import java.io.Serializable;
import java.util.Calendar;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;

@Entity
@Table(name = "profile_contact")
public class Contact implements Serializable {

	private static final long serialVersionUID = 7589129158494618914L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "creation_date")
	private Calendar creationDate = Calendar.getInstance();

	@NotNull
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "author_profile_id")
	private Profile author;

	@NotNull
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "speaker_profile_id")
	private Profile speaker;

	@Length(max = 100)
	@NotNull
	@NotBlank
	@Column(name = "subject")
	private String subject;

	@Lob
	@NotNull
	@NotBlank
	@Column(name = "body")
	private String body;

	@Column(name = "email")
	private String email;

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Calendar getCreationDate() {
		return this.creationDate;
	}

	public void setCreationDate(Calendar creationDate) {
		this.creationDate = creationDate;
	}

	public Profile getAuthor() {
		return this.author;
	}

	public void setAuthor(Profile author) {
		this.author = author;
	}

	public Profile getSpeaker() {
		return this.speaker;
	}

	public void setSpeaker(Profile speaker) {
		this.speaker = speaker;
	}

	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getSubject() {
		return this.subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getBody() {
		return this.body;
	}

	public void setBody(String body) {
		this.body = body;
	}

}
