package br.com.eventssaas.generalbusiness.services;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.eventssaas.generalbusiness.entity.Article;
import br.com.eventssaas.generalbusiness.entity.Commentary;
import br.com.eventssaas.generalbusiness.repository.BlackListRepository;

@Service
public class BlackListService {

	@Autowired
	private BlackListRepository blackListRepository;

	public Set<String> isCommentHasBlackWord(Commentary comment, Long eventId) {

		Set<String> words = new HashSet<String>(
				Arrays.asList(comment.getComment().toLowerCase().split("\\s|[?!.,|\\/<>{}@#$%^&*()-+=]")));

		Set<String> blackwords = this.blackListRepository.getWordsContainsInBlackList(eventId, words);

		return blackwords;
	}

	public Set<String> isArticleHasBlackWord(Article article, Long eventId) {

		if (article.getDescription() == null && article.getTitle() == null) {
			return null;
		}

		Set<String> words = new HashSet<String>(
				Arrays.asList(((article.getDescription() != null ? article.getDescription().toLowerCase() : "")
						+ (article.getTitle() != null ? " " + article.getTitle().toLowerCase() : ""))
								.split("\\s|[?!.,|\\/<>{}@#$%^&*()-+=]")));

		Set<String> blackwords = this.blackListRepository.getWordsContainsInBlackList(eventId, words);

		return blackwords;
	}

}
