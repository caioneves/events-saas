package br.com.eventssaas.generalbusiness.validation;

public class EmailExistsException extends Exception {

	/**
	 *
	 */
	private static final long serialVersionUID = 1425270288564046892L;

	public EmailExistsException(final String message) {
		super(message);
	}

}
