package br.com.eventssaas.generalbusiness.services;

import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.eventssaas.generalbusiness.entity.ProfileDeviceRegistrationId;
import br.com.eventssaas.generalbusiness.repository.ProfileDeviceRegistrationIdRepository;

@Service("br.com.eventssaas.generalbusiness.services.noticicationService")
public class NoticicationService {

	public final static String AUTH_KEY_FCM = "AIzaSyDTT9akJGsMEwH1bqeXsgIPSdY5kvlyfcg";
	public final static String API_URL_FCM = "https://fcm.googleapis.com/fcm/send";

	// Limite de registration Ids para os quais o gcm deve notificar por vez
	private final int MULTICAST_SIZE = 1000;

	@Autowired
	private ProfileDeviceRegistrationIdRepository profileDeviceRegistrationIdRepository;

	private void pushFCMNotification(String[] userDeviceIdKey, String title, String body) throws Exception {

		String authKey = AUTH_KEY_FCM; // You FCM AUTH key
		String FMCurl = API_URL_FCM;

		URL url = new URL(FMCurl);
		HttpURLConnection conn = (HttpURLConnection) url.openConnection();

		conn.setUseCaches(false);
		conn.setDoInput(true);
		conn.setDoOutput(true);

		conn.setRequestMethod("POST");
		conn.setRequestProperty("Authorization", "key=" + authKey);
		conn.setRequestProperty("Content-Type", "application/json");

		JSONObject json = new JSONObject();
		// json.put("to", userDeviceIdKey.trim());
		json.put("registration_ids", userDeviceIdKey);
		JSONObject info = new JSONObject();
		// info.put("title", "Notificatoin Title"); // Notification title
		// info.put("body", "Hello Test notification"); // Notification body
		info.put("title", title); // Notification title
		info.put("body", body); // Notification body
		json.put("notification", info);

		OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
		wr.write(json.toString());
		wr.flush();
		conn.getInputStream();
	}

	public void sendNotification(String title, String body, Long eventId) throws Exception {
		List<ProfileDeviceRegistrationId> registrationIds = this.profileDeviceRegistrationIdRepository.findByEventId(eventId);
		int total = registrationIds.size();
		List<String> partialDevices = new ArrayList<String>(total);
		int counter = 0;
		for (ProfileDeviceRegistrationId device : registrationIds) {
			counter++;
			partialDevices.add(device.getRegistrationId());
			int partialSize = partialDevices.size();

			// TODO criar grupos para envio das mensagens
			// this.pushFCMNotification(device.getRegistrationId());
			if (partialSize == this.MULTICAST_SIZE || counter == total) {

				String[] values = new String[partialDevices.size()];
				values = partialDevices.toArray(values);
				this.pushFCMNotification(values, title, body);
				partialDevices.clear();
			}
		}
	}

}
