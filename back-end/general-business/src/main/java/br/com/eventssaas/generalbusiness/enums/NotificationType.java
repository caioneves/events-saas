package br.com.eventssaas.generalbusiness.enums;

public enum NotificationType {

	EVENT("EVENT"), LECTURE("LECTURE"), SURVEY("SURVEY"), X("X"), Y("Y");

	private String type;

	NotificationType(final String type) {
		this.type = type;
	}

	public String getType() {
		return this.type;
	}

}
