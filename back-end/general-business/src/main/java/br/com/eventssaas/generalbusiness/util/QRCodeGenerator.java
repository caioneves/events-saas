package br.com.eventssaas.generalbusiness.util;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLEncoder;

import javax.imageio.ImageIO;

import org.apache.xerces.impl.dv.util.Base64;

public class QRCodeGenerator {

	public static String QR_PREFIX = "https://chart.googleapis.com/chart?chs=200x200&chld=M%%7C0&cht=qr&chl=";

	public static String qrCodePNGBase64(String text) throws Exception {
		URL url = new URL(QR_PREFIX + URLEncoder.encode(text));
		BufferedImage img = ImageIO.read(url);
		File file = new File(System.getProperty("java.io.tmpdir") + System.getProperty("file.separator") + "downloaded"
				+ System.currentTimeMillis() + ".png");
		ImageIO.write(img, "png", file);

		return loadImage(file);
	}

	public static String loadImage(File picFile) throws IOException {
		String picBase64 = null;
		byte[] picData = toByteArray(picFile);
		if (picFile.getName().endsWith(".png")) {
			picBase64 = "data:image/png;base64," + Base64.encode(picData);
		} else if (picFile.getName().endsWith(".jpg")) {
			picBase64 = "data:image/jpeg;base64," + Base64.encode(picData);
		}
		return picBase64;
	}

	private static byte[] toByteArray(File file) throws IOException {
		int length = (int) file.length();
		byte[] array = new byte[length];
		InputStream in = new FileInputStream(file);
		int offset = 0;
		while (offset < length) {
			int count = in.read(array, offset, (length - offset));
			offset += count;
		}
		in.close();
		return array;
	}

}
