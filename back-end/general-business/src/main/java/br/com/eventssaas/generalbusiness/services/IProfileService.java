package br.com.eventssaas.generalbusiness.services;

import br.com.eventssaas.generalbusiness.entity.Profile;
import br.com.eventssaas.generalbusiness.enums.ProfileRoleType;

public interface IProfileService {
	void createQRCodeInProfileWhenQRCodeIsNull() throws Exception;

	Profile createUser(String registerJson, ProfileRoleType profileRoleType, Long eventId, boolean generatePassword)
			throws Exception;
}
