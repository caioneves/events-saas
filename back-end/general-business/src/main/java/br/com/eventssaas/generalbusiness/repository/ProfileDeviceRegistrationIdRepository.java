package br.com.eventssaas.generalbusiness.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.eventssaas.generalbusiness.entity.ProfileDeviceRegistrationId;

public interface ProfileDeviceRegistrationIdRepository extends JpaRepository<ProfileDeviceRegistrationId, Long> {

	ProfileDeviceRegistrationId findByRegistrationIdAndEventId(String registrationId, Long eventId);

	List<ProfileDeviceRegistrationId> findByEventId(Long eventId);

}
