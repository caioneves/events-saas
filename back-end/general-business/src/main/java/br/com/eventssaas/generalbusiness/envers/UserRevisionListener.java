package br.com.eventssaas.generalbusiness.envers;

import org.hibernate.envers.RevisionListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import br.com.eventssaas.generalbusiness.entity.Profile;
import br.com.eventssaas.generalbusiness.util.ProfileUtil;

@Component
public class UserRevisionListener implements RevisionListener {

	@Autowired
	private ProfileUtil profileUtil;

	@Override
	public void newRevision(Object revisionEntity) {
		Profile prof = this.profileUtil.getProfileLoggedIn();

		CustomRevisionEntity exampleRevEntity = (CustomRevisionEntity) revisionEntity;
		exampleRevEntity.setProfile(prof);

	}

}