package br.com.eventssaas.generalbusiness.enums;

public enum DocumentType {

	ID("Id"), CPF("CPF"), CNPJ("CNPJ"), PASSPORT("Passport");

	private String providerType;

	public String getProviderType() {
		return this.providerType;
	}

	DocumentType(final String providerType) {
		this.providerType = providerType;
	}
}
