package br.com.eventssaas.generalbusiness.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import br.com.eventssaas.generalbusiness.entity.ProfileRole;

@Repository
public interface ProfileRoleRepository extends JpaRepository<ProfileRole, Long> {

	@Query("SELECT p FROM ProfileRole p WHERE (p.type= 'ADMIN' or p.type= 'ADMIN_EVENT') and p.profile.user.id= :userId")
	// @Query("SELECT p FROM ProfileRole p inner join p.listSocialProfile social
	// WHERE social.providerUserId= :providerUserId and social.providerId=
	// :providerId")
	public List<ProfileRole> findByUser(@Param("userId") Long userId);

	public List<ProfileRole> findByProfileIdAndEventId(@Param("profileId") Long profileId,
			@Param("eventId") Long eventId);

	public List<ProfileRole> findByProfileIdAndEventIdAndBlocked(Long profileId, Long eventId, boolean blocked);

	public List<ProfileRole> findByProfileId(Long profileId);

	@Query("SELECT p FROM ProfileRole p WHERE p.profile.id=:profileId and p.event.eventInformation.startEvent <= CURRENT_DATE and p.event.eventInformation.endEvent >= CURRENT_DATE")
	public List<ProfileRole> findByProfileIdAndActiveEvent(@Param("profileId") Long profileId);

}
