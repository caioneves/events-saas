package br.com.eventssaas.generalbusiness.enums;

/**
 * Type of Profile {@link Enum}
 * <p>
 * Types: <br>
 * <br>
 * {@link #ADMIN} <br>
 * {@link #ADMIN_EVENT} <br>
 * {@link #ATTENDEE} <br>
 * {@link #SPONSOR} <br>
 * {@link #SPEAKER} <br>
 * {@link #CLIENT} <br>
 *
 */
public enum ProfileRoleType {

	ADMIN("Admin Profile", "ROLE_ADMIN"),

	ADMIN_EVENT("Admin Event Profile", "ROLE_ADMIN_EVENT"),

	ATTENDEE("Attendee Profile", "ROLE_ATTENDEE"),

	SPONSOR("Sponsor Profile", "ROLE_SPONSOR"),

	SPEAKER("Speaker Profile", "ROLE_SPEAKER"),

	CLIENT("Client Profile", "ROLE_CLIENT");

	private String description;

	private String role;

	private ProfileRoleType(String description, String role) {
		this.description = description;
		this.role = role;
	}

	public String getDescription() {
		return this.description;
	}

	public String getRole() {
		return this.role;
	}

	public boolean isAdminProfile() {

		if (this.name().equals(ADMIN.name())) {
			return true;
		}

		return false;
	}

	public boolean isAdminEventProfile() {

		if (this.name().equals(ADMIN_EVENT.name())) {
			return true;
		}

		return false;
	}

	public boolean isAttendeeProfile() {

		if (this.name().equals(ATTENDEE.name())) {
			return true;
		}

		return false;
	}

	public boolean isSponsorProfile() {

		if (this.name().equals(SPONSOR.name())) {
			return true;
		}

		return false;
	}

	public boolean isSpeakerProfile() {

		if (this.name().equals(SPEAKER.name())) {
			return true;
		}

		return false;
	}

	public boolean isClientProfile() {

		if (this.name().equals(CLIENT.name())) {
			return true;
		}

		return false;
	}
}