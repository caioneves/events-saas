package br.com.eventssaas.generalbusiness.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import br.com.eventssaas.generalbusiness.TO.SurveyAnswerStatisticsTO;
import br.com.eventssaas.generalbusiness.entity.Profile;
import br.com.eventssaas.generalbusiness.entity.ProfileSurveyAnswer;
import br.com.eventssaas.generalbusiness.entity.SurveyQuestion;

@Repository
public interface ProfileSurveyAnswerRepository extends JpaRepository<ProfileSurveyAnswer, Long> {

	ProfileSurveyAnswer findBySurveyQuestionAndAuthor(SurveyQuestion question, Profile profile);

	List<ProfileSurveyAnswer> findByEventIdAndAuthor(Long eventId, Profile author);

	@Query(value = "select new	br.com.eventssaas.generalbusiness.TO.SurveyAnswerStatisticsTO(p.surveyAnswer.id,p.surveyQuestion.id, count(p)) from ProfileSurveyAnswer p where p.survey.id= :surveyId  group by p.surveyAnswer,p.surveyQuestion")
	List<SurveyAnswerStatisticsTO> findSurveyCount(@Param("surveyId") Long surveyId);

}
