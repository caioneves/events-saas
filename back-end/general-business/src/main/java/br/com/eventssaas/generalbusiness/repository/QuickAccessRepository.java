package br.com.eventssaas.generalbusiness.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.eventssaas.generalbusiness.entity.QuickAccess;

@Repository
public interface QuickAccessRepository extends JpaRepository<QuickAccess, Long> {

	public List<QuickAccess> findByEventId(Long eventId);
}
