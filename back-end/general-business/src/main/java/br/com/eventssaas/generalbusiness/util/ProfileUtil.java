package br.com.eventssaas.generalbusiness.util;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import br.com.eventssaas.generalbusiness.entity.Profile;
import br.com.eventssaas.generalbusiness.entity.ProfileRole;
import br.com.eventssaas.generalbusiness.enums.ProfileRoleType;
import br.com.eventssaas.generalbusiness.repository.ProfileRepository;

@Component
public class ProfileUtil {

	@Autowired
	private ProfileRepository profileRepository;
	private Long userLoggedInId;
	private Long userLoggedEvent;

	public Profile getProfileLoggedIn() {

		Profile userLoggedIn = this.profileRepository.findOne(this.getUserLoggedInId());
		return userLoggedIn;
	}

	public Long getUserLoggedInId() {
		return this.userLoggedInId;
	}

	public void setUserLoggedInId(Long userLoggedInId) {
		this.userLoggedInId = userLoggedInId;
	}

	public Long getUserLoggedEvent() {
		return this.userLoggedEvent;
	}

	public void setUserLoggedEvent(Long userLoggedEvent) {
		this.userLoggedEvent = userLoggedEvent;
	}

	public List<ProfileRoleType> getRoleTypes(List<ProfileRole> roles) {
		List<ProfileRoleType> result = new ArrayList<>();
		for (ProfileRole profileRole : roles) {
			result.add(profileRole.getType());
		}

		return result;

	}
}
