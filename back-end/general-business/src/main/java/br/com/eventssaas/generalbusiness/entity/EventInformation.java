package br.com.eventssaas.generalbusiness.entity;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "event_information")
public class EventInformation implements Serializable {

	private static final long serialVersionUID = -8565075095434248690L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "creation_date")
	private Calendar creationDate = Calendar.getInstance();

	@JsonIgnore
	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "event_id")
	private Event event;

	@NotNull
	// @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd de MMMM, yyyy -
	// HH:mm")
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "start_event_date")
	private Date startEvent;

	@NotNull
	// @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd de MMMM, yyyy -
	// HH:mm")
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "end_event_date")
	private Date endEvent;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "image_id")
	private Image image;

	@Column(name = "image_url")
	private String imageUrl;

	@Column(name = "site_url")
	private String siteUrl;

	@Column(name = "place_name")
	private String placeName;

	@Column(name = "place_map_label")
	private String placeMapLabel;

	@Column(name = "place_map_image_url")
	private String placeMapImageUrl;

	@OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	@JoinColumn(name = "contact_info_id")
	private ContactInfo contactInfo;

	@Column(name = "google_maps_long")
	private Double googleMapsLong;

	@Column(name = "google_maps_lat")
	private Double googleMapsLat;

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Image getImage() {
		return this.image;
	}

	public void setImage(Image image) {
		this.image = image;
	}

	public String getImageUrl() {
		return this.imageUrl;
	}

	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}

	public Date getStartEvent() {
		return this.startEvent;
	}

	public void setStartEvent(Date startEvent) {
		this.startEvent = startEvent;
	}

	public Date getEndEvent() {
		return this.endEvent;
	}

	public void setEndEvent(Date endEvent) {
		this.endEvent = endEvent;
	}

	public String getPlaceName() {
		return this.placeName;
	}

	public void setPlaceName(String placeName) {
		this.placeName = placeName;
	}

	public ContactInfo getContactInfo() {
		return this.contactInfo;
	}

	public void setContactInfo(ContactInfo contactInfo) {
		this.contactInfo = contactInfo;
	}

	public Double getGoogleMapsLong() {
		return this.googleMapsLong;
	}

	public void setGoogleMapsLong(Double googleMapsLong) {
		this.googleMapsLong = googleMapsLong;
	}

	public Event getEvent() {
		return this.event;
	}

	public void setEvent(Event event) {
		this.event = event;
	}

	public Double getGoogleMapsLat() {
		return this.googleMapsLat;
	}

	public void setGoogleMapsLat(Double googleMapsLat) {
		this.googleMapsLat = googleMapsLat;
	}

	public String getPlaceMapLabel() {
		return this.placeMapLabel;
	}

	public void setPlaceMapLabel(String placeMapLabel) {
		this.placeMapLabel = placeMapLabel;
	}

	public String getPlaceMapImageUrl() {
		return this.placeMapImageUrl;
	}

	public void setPlaceMapImageUrl(String placeMapImageUrl) {
		this.placeMapImageUrl = placeMapImageUrl;
	}

	public String getSiteUrl() {
		return this.siteUrl;
	}

	public void setSiteUrl(String siteUrl) {
		this.siteUrl = siteUrl;
	}

	public Calendar getCreationDate() {
		return this.creationDate;
	}

	public void setCreationDate(Calendar creationDate) {
		this.creationDate = creationDate;
	}

}
