/**
 *
 */
package br.com.eventssaas.generalbusiness.entity;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;

@Entity
@Table(name = "survey")
public class Survey implements Serializable {

	private static final long serialVersionUID = -7991284222370673946L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "creation_date")
	private Calendar creationDate = Calendar.getInstance();

	@NotBlank
	@NotNull
	@Length(max = 100)
	@Column(name = "name")
	private String name;

	@Column(name = "is_push_notification", columnDefinition = "bit(1) default 0")
	private boolean pushNotification = false;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "push_notification_date")
	private Date pushNotificationDate;

	@NotNull
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "event_id")
	private Event event;

	@NotNull
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "author_profile_id")
	private Profile author;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "survey", cascade = CascadeType.ALL)
	private List<SurveyQuestion> listSurveyQuestion;

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Calendar getCreationDate() {
		return this.creationDate;
	}

	public void setCreationDate(Calendar creationDate) {
		this.creationDate = creationDate;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public boolean isPushNotification() {
		return this.pushNotification;
	}

	public void setPushNotification(boolean pushNotification) {
		this.pushNotification = pushNotification;
	}

	public Date getPushNotificationDate() {
		return this.pushNotificationDate;
	}

	public void setPushNotificationDate(Date pushNotificationDate) {
		this.pushNotificationDate = pushNotificationDate;
	}

	public Event getEvent() {
		return this.event;
	}

	public void setEvent(Event event) {
		this.event = event;
	}

	public Profile getAuthor() {
		return this.author;
	}

	public void setAuthor(Profile author) {
		this.author = author;
	}

	public List<SurveyQuestion> getListSurveyQuestion() {
		return this.listSurveyQuestion;
	}

	public void setListSurveyQuestion(List<SurveyQuestion> listSurveyQuestion) {
		this.listSurveyQuestion = listSurveyQuestion;
	}

}
