package br.com.eventssaas.generalbusiness.TO;

import java.io.Serializable;
import java.util.Date;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import br.com.eventssaas.generalbusiness.enums.DocumentType;
import br.com.eventssaas.generalbusiness.enums.GenderType;
import br.com.eventssaas.generalbusiness.enums.ProfileRoleType;
import br.com.eventssaas.generalbusiness.serializer.CustomDateDeserializer;

public class ProfileTO implements Serializable {
	/**
	 *
	 */
	private static final long serialVersionUID = 1L;

	private String firstName;

	private String lastName;

	private GenderType gender;

	@JsonDeserialize(using = CustomDateDeserializer.class)
	private Date birthDate;

	private String documentNumber;

	private DocumentType documentType;

	private String jobRole;

	private boolean showJobRole;

	private String company;

	private String facebookUrl;

	private String twitterUrl;

	private String linkedInUrl;

	private String instagramUrl;

	private String minibio;

	private Long imageId;

	private UserTO user;

	private ProfileRoleType type;

	private boolean hasFacebookSocialProfileEnable = false;

	private boolean hasLinkedSocialProfileEnable = false;

	public String getFirstName() {
		return this.firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return this.lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public GenderType getGender() {
		return this.gender;
	}

	public void setGender(GenderType gender) {
		this.gender = gender;
	}

	public Date getBirthDate() {
		return this.birthDate;
	}

	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}

	public String getDocumentNumber() {
		return this.documentNumber;
	}

	public void setDocumentNumber(String documentNumber) {
		this.documentNumber = documentNumber;
	}

	public DocumentType getDocumentType() {
		return this.documentType;
	}

	public void setDocumentType(DocumentType documentType) {
		this.documentType = documentType;
	}

	public String getJobRole() {
		return this.jobRole;
	}

	public void setJobRole(String jobRole) {
		this.jobRole = jobRole;
	}

	public boolean isShowJobRole() {
		return this.showJobRole;
	}

	public void setShowJobRole(boolean showJobRole) {
		this.showJobRole = showJobRole;
	}

	public String getCompany() {
		return this.company;
	}

	public void setCompany(String company) {
		this.company = company;
	}

	public String getFacebookUrl() {
		return this.facebookUrl;
	}

	public void setFacebookUrl(String facebookUrl) {
		this.facebookUrl = facebookUrl;
	}

	public String getTwitterUrl() {
		return this.twitterUrl;
	}

	public void setTwitterUrl(String twitterUrl) {
		this.twitterUrl = twitterUrl;
	}

	public String getLinkedInUrl() {
		return this.linkedInUrl;
	}

	public void setLinkedInUrl(String linkedInUrl) {
		this.linkedInUrl = linkedInUrl;
	}

	public String getInstagramUrl() {
		return this.instagramUrl;
	}

	public void setInstagramUrl(String instagramUrl) {
		this.instagramUrl = instagramUrl;
	}

	public String getMinibio() {
		return this.minibio;
	}

	public void setMinibio(String minibio) {
		this.minibio = minibio;
	}

	public Long getImageId() {
		return this.imageId;
	}

	public void setImageId(Long imageId) {
		this.imageId = imageId;
	}

	public ProfileRoleType getType() {
		return this.type;
	}

	public void setType(ProfileRoleType type) {
		this.type = type;
	}

	public UserTO getUser() {
		return this.user;
	}

	public void setUser(UserTO user) {
		this.user = user;
	}

	public boolean isHasFacebookSocialProfileEnable() {
		return this.hasFacebookSocialProfileEnable;
	}

	public void setHasFacebookSocialProfileEnable(boolean hasFacebookSocialProfileEnable) {
		this.hasFacebookSocialProfileEnable = hasFacebookSocialProfileEnable;
	}

	public boolean isHasLinkedSocialProfileEnable() {
		return this.hasLinkedSocialProfileEnable;
	}

	public void setHasLinkedSocialProfileEnable(boolean hasLinkedSocialProfileEnable) {
		this.hasLinkedSocialProfileEnable = hasLinkedSocialProfileEnable;
	}

}
