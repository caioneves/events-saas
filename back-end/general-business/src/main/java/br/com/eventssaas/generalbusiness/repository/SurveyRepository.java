package br.com.eventssaas.generalbusiness.repository;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import br.com.eventssaas.generalbusiness.entity.Event;
import br.com.eventssaas.generalbusiness.entity.Profile;
import br.com.eventssaas.generalbusiness.entity.Survey;

@Repository
public interface SurveyRepository extends JpaRepository<Survey, Long> {

	List<Survey> findByEventId(Long eventId, Pageable pageRequest);

	@Query("Select s from Survey s where s.id = :id and s.event = :event and "
			+ "0 < (Select count(*) from ProfileRole pr where pr.event = :event and pr.profile = :profile)")
	Survey findByIdAndEventAndProfile(@Param("id") Long id, @Param("event") Event event,
			@Param("profile") Profile profile);

	@Query("Select s from Survey s where s.event.id = :eventId and "
			+ "s.listSurveyQuestion.size != (Select count(*) from ProfileSurveyAnswer ps where ps.survey = s and ps.author = :profile) and "
			+ "0 < (Select count(*) from ProfileRole pr where pr.event.id = :eventId and pr.profile = :profile)")
	List<Survey> findSurveysNotFullyAnswered(@Param("profile") Profile profile, @Param("eventId") Long eventId,
			Pageable pageRequest);

	@Query("Select count(*) from ProfileRole pr where pr.event = :event and pr.profile = :profile")
	long countProfileEvent(@Param("event") Event event, @Param("profile") Profile profile);

}
