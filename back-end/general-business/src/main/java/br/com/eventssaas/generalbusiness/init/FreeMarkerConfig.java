package br.com.eventssaas.generalbusiness.init;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.ui.freemarker.FreeMarkerConfigurationFactoryBean;

/**
 * FreeMarker settings
 *
 * @author René Araújo Vasconcelos [renearaujo@gmail.com] - 13 de abr de 2017
 *         15:59:05
 */
@Configuration
public class FreeMarkerConfig {

	/**
	 * Path for email templates
	 *
	 * @author René Araújo Vasconcelos [renearaujo@gmail.com] - 13 de abr de
	 *         2017 15:57:59
	 */
	private static final String TEMPLATE_PATH = "/WEB-INF/mail-templates/";
	private static final String TEMPLATE_PATH_2 = "/mail-templates/";

	/**
	 * Get the setup configuration
	 *
	 * @author René Araújo Vasconcelos [renearaujo@gmail.com] - 13 de abr de
	 *         2017 15:58:29
	 *
	 * @return {@link FreeMarkerConfigurationFactoryBean} bean configured
	 */
	@Bean
	public FreeMarkerConfigurationFactoryBean getFreeMarkerConfiguration() {

		FreeMarkerConfigurationFactoryBean fmConfigFactoryBean = new FreeMarkerConfigurationFactoryBean();
		// set the path
		fmConfigFactoryBean.setTemplateLoaderPaths(TEMPLATE_PATH, TEMPLATE_PATH_2);

		return fmConfigFactoryBean;

	}
}