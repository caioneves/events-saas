package br.com.eventssaas.generalbusiness.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.eventssaas.generalbusiness.entity.Contact;

@Repository
public interface ContactRepository extends JpaRepository<Contact, Long> {

}
