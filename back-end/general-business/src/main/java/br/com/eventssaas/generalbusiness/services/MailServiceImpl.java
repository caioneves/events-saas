package br.com.eventssaas.generalbusiness.services;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import org.springframework.ui.freemarker.FreeMarkerTemplateUtils;

import br.com.eventssaas.generalbusiness.entity.Contact;
import br.com.eventssaas.generalbusiness.entity.Profile;
import br.com.eventssaas.generalbusiness.entity.SocialProfile;
import br.com.eventssaas.generalbusiness.entity.User;
import br.com.eventssaas.generalbusiness.enums.MailBasicTemplateType;
import br.com.eventssaas.generalbusiness.support.Mail;
import freemarker.template.Configuration;

@Service("mailService")
public class MailServiceImpl implements IMailService {

	@Autowired
	private JavaMailSender mailSender;

	@Autowired
	private Configuration fmConfiguration;

	@Override
	public void sendEmail(final Mail mail, String templateName, final Map<String, Object> templateParameters) {
		MimeMessage mimeMessage = this.mailSender.createMimeMessage();

		try {

			MimeMessageHelper mimeMessageHelper = new MimeMessageHelper(mimeMessage, true);

			mimeMessageHelper.setSubject(mail.getMailSubject());
			mimeMessageHelper.setFrom(mail.getMailFrom());
			mimeMessageHelper.setTo(mail.getMailTo());

			if (templateName == null || templateName.isEmpty()) {
				templateName = "email-template.txt";
			}

			mail.setMailContent(this.getContentFromTemplate(templateParameters, templateName));
			mimeMessageHelper.setText(mail.getMailContent(), true);

			this.mailSender.send(mimeMessageHelper.getMimeMessage());
		} catch (MessagingException e) {
			e.printStackTrace();
		}
	}

	public String getContentFromTemplate(Map<String, Object> templateParameters, String templateName) {
		StringBuffer content = new StringBuffer();

		try {
			content.append(FreeMarkerTemplateUtils
					.processTemplateIntoString(this.fmConfiguration.getTemplate(templateName), templateParameters));
		} catch (Exception e) {
			e.printStackTrace();
		}
		System.err.println(templateName);
		System.err.println(content);

		return content.toString();
	}

	/*
	 * private Mail getEmailConfig() { Mail mail = new Mail();
	 * mail.setMailFrom("contato@events-saas.com"); return mail; }
	 */

	@Override
	public void sendEmailSignUp(SocialProfile profile) {

		Mail mail = new Mail();
		mail.setMailFrom("contato@events-saas.com");
		mail.setMailTo(profile.getEmail());
		mail.setMailSubject("Events-SaaS - Bem Vindo ao Events Saas");

		Map<String, Object> parameters = new HashMap<>();
		parameters.put("firstName", profile.getFirstName());

		String dataFormatada = new SimpleDateFormat("yyyy").format(new Date());

		parameters.put("ano", dataFormatada);
		parameters.put("signature", "Equipe Events-SaaS - www.events-saas.com");
		mail.setModel(parameters);

		this.sendEmail(mail, "email-template-signup.txt", parameters);

	}

	@Override
	public void sendEmailConfirmationSignUp(final User user, final String confirmationUrl) {
		Mail mail = new Mail();
		mail.setMailFrom("contato@events-saas.com");
		mail.setMailTo(user.getEmail());
		mail.setMailSubject("Events-SaaS - Confirmação de Registro");

		Map<String, Object> parameters = new HashMap<>();
		parameters.put("firstName", user.getEmail());
		parameters.put("confirmationUrl", confirmationUrl);

		String dataFormatada = new SimpleDateFormat("yyyy").format(new Date());

		parameters.put("ano", dataFormatada);
		parameters.put("signature", "Equipe Events-SaaS - www.events-saas.com");
		mail.setModel(parameters);

		this.sendEmail(mail, "email-template-confirmation-signup.txt", parameters);

	}

	@Override
	public void sendEmailConfirmationAdmin(Profile profile, String password) {

		Mail mail = new Mail();
		mail.setMailFrom("contato@events-saas.com");
		mail.setMailTo(profile.getUser().getEmail());
		mail.setMailSubject("Events-SaaS - Bem Vindo ao Events Saas");

		Map<String, Object> parameters = new HashMap<>();
		parameters.put("firstName", profile.getFirstName());
		parameters.put("password", password);

		String dataFormatada = new SimpleDateFormat("yyyy").format(new Date());

		parameters.put("ano", dataFormatada);
		parameters.put("signature", "Equipe Events-SaaS - www.events-saas.com");
		mail.setModel(parameters);

		this.sendEmail(mail, "email-template-confirmation-admin.txt", parameters);

	}

	@Override
	public void sendEmailSpeaker(Contact contact) {

		Mail mail = new Mail();
		mail.setMailFrom("contato@events-saas.com");
		mail.setMailTo(contact.getSpeaker().getUser().getEmail());
		mail.setMailSubject(contact.getSubject());

		Map<String, Object> parameters = new HashMap<>();
		parameters.put("firstName", contact.getSpeaker().getFirstName());
		parameters.put("emailFrom", contact.getAuthor().getUser().getEmail());
		parameters.put("nameAuthor", contact.getAuthor().getFirstName());
		parameters.put("menssage", contact.getBody());

		String dataFormatada = new SimpleDateFormat("yyyy").format(new Date());

		parameters.put("ano", dataFormatada);

		mail.setModel(parameters);

		this.sendEmail(mail, "email-contact-speaker.txt", parameters);

	}

	/*
	 * newuser forgetpassword
	 */
	@Override
	public void sendEmailQRCodeAndPasswordIfExist(final MailBasicTemplateType subjectType, final String mailTo,
			final String firstName, String password, String qrCode) {
		Mail mail = new Mail();
		mail.setMailFrom("contato@events-saas.com");
		mail.setMailTo(mailTo);
		mail.setMailSubject(this.getSubjectValue(subjectType));

		Map<String, Object> parameters = new HashMap<>();
		parameters.put("firstName", firstName);
		if (password != null) {
			parameters.put("hasPassword", true);
			parameters.put("password", password);
		}
		if (qrCode == null) {
			qrCode = "";
		}
		parameters.put("qrcode", qrCode);

		String dataFormatada = new SimpleDateFormat("yyyy").format(new Date());
		parameters.put("ano", dataFormatada);

		parameters.put("signature", "Equipe Events-SaaS - www.events-saas.com");
		mail.setModel(parameters);

		this.sendEmail(mail, "email-template-confirmation-qrcode-admin.txt", parameters);

	}

	/*
	 * newuser forgetpassword
	 */
	@Override
	public String getSubjectValue(MailBasicTemplateType subjectType) {
		switch (subjectType) {
		case CREATE_USER:
			return "Events-SaaS - Bem vindo";

		case FORGOT_PASSWORD:

			return "Events-SaaS - Nova senha";
		default:

			return "Events-SaaS - Bem vindo";
		}

	}
}