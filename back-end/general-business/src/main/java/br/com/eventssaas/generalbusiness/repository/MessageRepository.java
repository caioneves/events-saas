package br.com.eventssaas.generalbusiness.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import br.com.eventssaas.generalbusiness.entity.MessageChat;

public interface MessageRepository extends JpaRepository<MessageChat, Long> {

	@Query("select msg from MessageChat as msg where msg.event.id = :eventId AND (((msg.author.id = :authorId AND msg.deletedByAuthor = false) AND"
			+ " msg.targetProfile.id = :targetId) OR (msg.author.id = :targetId AND (msg.targetProfile.id = :authorId AND msg.deletedByTarget = false))) "
			+ "ORDER BY msg.creationDate desc")
	public List<MessageChat> getMessagesByProfileLoggeInAndProfileId(@Param("eventId") Long eventId,
			@Param("authorId") Long authorId, @Param("targetId") Long targetId);

	@Query("select count(msg) from MessageChat as msg where msg.event.id = :eventId AND "
			+ "msg.targetProfile.id = :targetId AND msg.read=false ORDER BY msg.creationDate desc")
	public Long getMessagesPendingQuantity(@Param("eventId") Long eventId, @Param("targetId") Long targetId);

	@Query("select m1 from MessageChat as m1 where m1.creationDate IN (select max(m2.creationDate) "
			+ "from MessageChat as m2 where (m1.targetProfile.id=m2.targetProfile.id and m1.author.id=m2.author.id) or "
			+ "(m1.author.id=m2.targetProfile.id and m1.targetProfile.id=m2.author.id)) "
			+ "and (m1.targetProfile.id=:targetId or m1.author.id=:targetId) and m1.event.id = :eventId ORDER BY m1.creationDate desc")
	public List<MessageChat> getMessages(@Param("eventId") Long eventId, @Param("targetId") Long targetId);

	@Transactional
	@Modifying
	@Query("delete from MessageChat as msg where msg.event.id = :eventId AND ((msg.author.id = :authorId AND"
			+ " msg.targetProfile.id = :targetId) OR (msg.author.id = :targetId AND msg.targetProfile.id = :authorId)) ")
	void deleteChat(@Param("eventId") Long eventId, @Param("authorId") Long authorId, @Param("targetId") Long targetId);

}
