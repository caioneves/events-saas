package br.com.eventssaas.generalbusiness.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.eventssaas.generalbusiness.entity.ProfileNotification;

public interface ProfileNotificationRepository extends JpaRepository<ProfileNotification, Long> {

	public ProfileNotification findNotificationByProfileIdAndNotificationId(Long profileId, Long notificationId);

}
