package br.com.eventssaas.generalbusiness.enums;

public enum ArticleType {

	APP_POST("APP"), ADMIN_POST("ADMIN"), SPONSOR_POST("SPONSOR"), SOCIAL_POST("SOCIAL"), ADMIN_ARTICLE(
			"ADMIN_ARTICLE"), SPONSOR_ARTICLE("SPONSOR_ARTICLE");

	private String articleType;

	public String getArticleType() {
		return this.articleType;
	}

	ArticleType(final String articleType) {
		this.articleType = articleType;
	}
}
