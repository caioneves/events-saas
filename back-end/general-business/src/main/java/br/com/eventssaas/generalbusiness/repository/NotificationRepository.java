package br.com.eventssaas.generalbusiness.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import br.com.eventssaas.generalbusiness.entity.Notification;

public interface NotificationRepository extends JpaRepository<Notification, Long> {

	List<Notification> findByEventId(Long eventId);

	@Query("SELECT n FROM Notification n inner join n.listProfileNotification profile WHERE profile.profile.id= :profileId and n.event.id= :eventId and profile.readingDate is null")
	public List<Notification> findNotificationByProfileAndEvent(@Param("profileId") Long profileId,
			@Param("eventId") Long eventId);

}
