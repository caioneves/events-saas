/**
 *
 */
package br.com.eventssaas.generalbusiness.entity;

import java.io.Serializable;
import java.util.Calendar;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "event")
public class Event implements Serializable {

	private static final long serialVersionUID = -1591441189861707657L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "creation_date")
	private Calendar creationDate = Calendar.getInstance();

	@NotNull
	@NotBlank
	@Column(name = "name")
	private String name;

	@Lob
	@Column(name = "description")
	private String description;

	@NotNull
	@OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	@JoinColumn(name = "event_information_id")
	private EventInformation eventInformation;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "event", cascade = CascadeType.ALL)
	@JsonIgnore
	private List<Article> listArticle;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "event", cascade = CascadeType.ALL)
	@JsonIgnore
	private List<LectureDetail> listSchedule;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "event", cascade = CascadeType.ALL)
	@JsonIgnore
	private List<QuickAccess> listQuickAccess;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "event", cascade = CascadeType.ALL)
	@JsonIgnore
	private List<Survey> listSurvey;

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescrition(String descrition) {
		this.description = descrition;
	}

	public EventInformation getEventInformation() {
		return this.eventInformation;
	}

	public void setEventInformation(EventInformation eventInformation) {
		this.eventInformation = eventInformation;
	}

	public List<Article> getListArticle() {
		return this.listArticle;
	}

	public void setListArticle(List<Article> listArticle) {
		this.listArticle = listArticle;
	}

	public List<LectureDetail> getListSchedule() {
		return this.listSchedule;
	}

	public void setListSchedule(List<LectureDetail> listSchedule) {
		this.listSchedule = listSchedule;
	}

	public List<QuickAccess> getListQuickAccess() {
		return this.listQuickAccess;
	}

	public void setListQuickAccess(List<QuickAccess> listQuickAccess) {
		this.listQuickAccess = listQuickAccess;
	}

	public List<Survey> getListSurvey() {
		return this.listSurvey;
	}

	public void setListSurvey(List<Survey> listSurvey) {
		this.listSurvey = listSurvey;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}
