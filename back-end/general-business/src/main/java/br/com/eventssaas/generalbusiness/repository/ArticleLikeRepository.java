package br.com.eventssaas.generalbusiness.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.eventssaas.generalbusiness.entity.ArticleLike;
import br.com.eventssaas.generalbusiness.entity.Profile;

/**
 * Created by inafalcao on 2/29/16.
 */
public interface ArticleLikeRepository extends JpaRepository<ArticleLike, Long> {
	public ArticleLike findByArticleIdAndProfile(Long articleId, Profile profile);
}
