package br.com.eventssaas.generalbusiness.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.eventssaas.generalbusiness.entity.Image;

/**
 * Created by inafalcao on 2/29/16.
 */
public interface ImageRepository extends JpaRepository<Image, Long> {

}
