package br.com.eventssaas.generalbusiness.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.eventssaas.generalbusiness.entity.DownloadItem;

/**
 * Created by inafalcao on 2/29/16.
 */
public interface DownloadItemRepository extends JpaRepository<DownloadItem, Long> {

	public List<NoBytes> findByLectureDetailIdOrderByCreationDateDesc(Long lectureDetailId);

}
