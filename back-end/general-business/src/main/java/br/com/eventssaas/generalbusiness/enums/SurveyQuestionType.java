package br.com.eventssaas.generalbusiness.enums;

public enum SurveyQuestionType {
	QUIZZ("QUIZZ"), OPENED_QUESTION("OPENED_QUESTION"), OPTION_QUESTION("OPTION_QUESTION");

	private String surveyQuestionType;

	public String getSurveyQuestionType() {
		return this.surveyQuestionType;
	}

	SurveyQuestionType(final String surveyQuestionType) {
		this.surveyQuestionType = surveyQuestionType;
	}
}