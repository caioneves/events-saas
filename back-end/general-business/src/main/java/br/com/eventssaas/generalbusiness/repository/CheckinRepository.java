package br.com.eventssaas.generalbusiness.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.eventssaas.generalbusiness.entity.CheckIn;

public interface CheckinRepository extends JpaRepository<CheckIn, Long> {

}
