package br.com.eventssaas.generalbusiness.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import br.com.eventssaas.generalbusiness.entity.SurveyQuestion;

@Repository
public interface SurveyQuestionRepository extends JpaRepository<SurveyQuestion, Long> {
	@Query("select count(sq) from SurveyQuestion as sq where sq.id NOT IN ("
			+ "select psa.surveyQuestion.id from ProfileSurveyAnswer psa where psa.event.id = :eventId AND psa.author.id = :profileId)")
	Long countSurveyQuestionPendingQuantity(@Param("eventId") Long eventId, @Param("profileId") Long profileId);
}
