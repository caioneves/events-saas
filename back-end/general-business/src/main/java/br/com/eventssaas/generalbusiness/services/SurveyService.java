package br.com.eventssaas.generalbusiness.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import br.com.eventssaas.generalbusiness.entity.Event;
import br.com.eventssaas.generalbusiness.entity.Profile;
import br.com.eventssaas.generalbusiness.entity.ProfileSurveyAnswer;
import br.com.eventssaas.generalbusiness.entity.Survey;
import br.com.eventssaas.generalbusiness.entity.SurveyAnswer;
import br.com.eventssaas.generalbusiness.entity.SurveyQuestion;
import br.com.eventssaas.generalbusiness.enums.SurveyQuestionType;
import br.com.eventssaas.generalbusiness.exceptions.MissingArgumentException;
import br.com.eventssaas.generalbusiness.exceptions.NotFoundException;
import br.com.eventssaas.generalbusiness.repository.EventRepository;
import br.com.eventssaas.generalbusiness.repository.ProfileRepository;
import br.com.eventssaas.generalbusiness.repository.ProfileSurveyAnswerRepository;
import br.com.eventssaas.generalbusiness.repository.SurveyQuestionRepository;
import br.com.eventssaas.generalbusiness.repository.SurveyRepository;

@Service
public class SurveyService {

	private SurveyRepository surveyRepostiory;
	private EventRepository eventRepository;
	private ProfileSurveyAnswerRepository profileSurveyRepository;
	private SurveyQuestionRepository surveyQuestionRepository;
	private ProfileRepository profileRepository;

	@Autowired
	public SurveyService(SurveyRepository surveyRepository, EventRepository eventRepository,
			ProfileSurveyAnswerRepository profileSurverRepository, SurveyQuestionRepository surveyQuestionRepository,
			ProfileRepository profileRepository) {

		this.surveyRepostiory = surveyRepository;
		this.eventRepository = eventRepository;
		this.profileSurveyRepository = profileSurverRepository;
		this.surveyQuestionRepository = surveyQuestionRepository;
		this.profileRepository = profileRepository;
	}

	public Survey getSurveyFromEvent(Long eventId, Long surveyId, Profile profile)
			throws NotFoundException, MissingArgumentException {

		if (eventId == null || surveyId == null || profile == null) {
			throw new MissingArgumentException();
		}

		Event event = this.eventRepository.findOne(eventId);

		if (event == null) {
			throw new NotFoundException();
		}

		Survey survey = this.surveyRepostiory.findByIdAndEventAndProfile(surveyId, event, profile);

		if (survey == null) {
			throw new NotFoundException();
		}

		return survey;
	}

	private ProfileSurveyAnswer fetchOrCreateAnswerFromProfile(SurveyQuestion question, SurveyAnswer answer,
			String answerText, Profile profile) {

		ProfileSurveyAnswer result = this.profileSurveyRepository.findBySurveyQuestionAndAuthor(question, profile);

		if (result == null) {
			Survey survey = question.getSurvey();
			Event event = survey.getEvent();

			result = new ProfileSurveyAnswer();
			result.setAuthor(profile);
			result.setEvent(event);
			result.setSurvey(survey);
			result.setSurveyAnswer(answer);
			result.setSurveyQuestion(question);
			result.setType(question.getType());
			if (question.getType() == SurveyQuestionType.OPENED_QUESTION) {
				result.setSurveyAnswerText(answerText);
			} else {
				result.setCorrect(answer.isCorrect());
				result.setSurveyAnswerText(answer.getAnswer());
			}

			result.setSurveyQuestionText(question.getQuestion());
		}
		return result;
	}

	public void answerQuestion(Long questionId, Long answerId, String answerText, Long eventId, Profile profile)
			throws NotFoundException, MissingArgumentException {

		if (questionId == null || profile == null) {
			throw new MissingArgumentException();
		}

		SurveyQuestion question = this.surveyQuestionRepository.findOne(questionId);
		if (question == null) {
			throw new NotFoundException();
		}

		try {
			if (eventId == null || !eventId.equals(question.getSurvey().getEvent().getId())) {
				throw new NotFoundException();
			}
		} catch (Exception e) {
			throw new NotFoundException();
		}

		SurveyAnswer answer = null;

		if (question.getType() != SurveyQuestionType.OPENED_QUESTION) {
			if (answerId == null) {
				throw new MissingArgumentException();
			}

			if (question.getListSurveyAnswer() != null) {
				for (SurveyAnswer lAnswer : question.getListSurveyAnswer()) {
					if (lAnswer.getId().equals(answerId)) {
						answer = lAnswer;
						break;
					}
				}
			}

			if (answer == null) {
				throw new NotFoundException();
			}
		} else {
			if (answerText == null || answerText.trim().isEmpty()) {
				throw new MissingArgumentException();
			}
		}

		ProfileSurveyAnswer result = this.fetchOrCreateAnswerFromProfile(question, answer, answerText, profile);

		this.profileSurveyRepository.save(result);

	}

	public List<Survey> getSurveyNotAnswered(Profile profile, Long eventId, Integer page, Integer maxResults)
			throws MissingArgumentException, NotFoundException {

		if (eventId == null || profile == null) {
			throw new MissingArgumentException();
		}
		// TODO: usar creationDate, nao esta funcionando
		PageRequest pageRequest = new PageRequest(page.intValue(), maxResults.intValue(),
				new Sort(Sort.Direction.DESC, "id"));

		List<Survey> surveys = this.surveyRepostiory.findSurveysNotFullyAnswered(profile, eventId, pageRequest);

		if (surveys == null || surveys.size() == 0) {
			throw new NotFoundException();
		}

		List<ProfileSurveyAnswer> pAnswers = this.profileSurveyRepository.findByEventIdAndAuthor(eventId, profile);

		if (pAnswers == null || pAnswers.size() == 0) {
			return surveys;
		}

		for (ProfileSurveyAnswer pAnswer : pAnswers) {
			pAnswer.getSurveyQuestion().getSurvey().getListSurveyQuestion().remove(pAnswer.getSurveyQuestion());
		}

		return surveys;

	}

	public Long getSurveyQuestionPendingQuantity(Long eventId, Long profileId)
			throws NotFoundException, MissingArgumentException {

		if (eventId == null) {
			throw new MissingArgumentException("request.fail.event.id.missingargument");
		}

		if (profileId == null) {
			throw new MissingArgumentException("request.fail.event.profile.id.missingargument");
		}

		this.eventValidation(eventId);

		return this.surveyQuestionRepository.countSurveyQuestionPendingQuantity(eventId, profileId);
	}

	private void profileValidation(Long profile) throws NotFoundException {
		if (profile == null) {
			throw new NotFoundException("request.fail.event.profile.notfound");
		} else if (!this.profileRepository.exists(profile)) {
			throw new NotFoundException("request.fail.event.profile.notfound");
		}
	}

	private void eventValidation(Long eventId) throws NotFoundException {
		if (eventId == null) {
			throw new NotFoundException("request.fail.event.notfound");
		} else if (!this.eventRepository.exists(eventId)) {
			throw new NotFoundException("request.fail.event.notfound");
		}
	}
}
