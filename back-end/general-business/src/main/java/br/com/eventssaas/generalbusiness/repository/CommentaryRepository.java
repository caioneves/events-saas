package br.com.eventssaas.generalbusiness.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.eventssaas.generalbusiness.entity.Commentary;

/**
 * Created by inafalcao on 2/29/16.
 */
public interface CommentaryRepository extends JpaRepository<Commentary, Long> {
	public Commentary findByArticleIdOrderByCreationDateDesc(Long articleId);

	public List<Commentary> findByArticleIdAndBlackCommentOrderByCreationDateDesc(Long articleId,
			Boolean isBlackComment);
}
