package br.com.eventssaas.generalbusiness.repository;

import java.util.Set;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import br.com.eventssaas.generalbusiness.entity.BlackWord;

@Repository
public interface BlackListRepository extends JpaRepository<BlackWord, Long> {

	@Query("select wrd.word from BlackWord as wrd where wrd.event.id = :eventId AND lower(wrd.word) IN :words")
	public Set<String> getWordsContainsInBlackList(@Param("eventId") Long eventId, @Param("words") Set<String> words);
}
