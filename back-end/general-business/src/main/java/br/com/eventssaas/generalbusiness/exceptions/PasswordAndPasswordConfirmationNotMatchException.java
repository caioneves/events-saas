package br.com.eventssaas.generalbusiness.exceptions;

public class PasswordAndPasswordConfirmationNotMatchException extends Exception {

	/**
	 *
	 */
	private static final long serialVersionUID = -1774117423363715223L;

	public PasswordAndPasswordConfirmationNotMatchException(String message) {
		super(message);
	}

	public PasswordAndPasswordConfirmationNotMatchException() {

	}

}
