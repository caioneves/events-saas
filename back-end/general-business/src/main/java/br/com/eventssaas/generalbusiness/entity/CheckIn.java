package br.com.eventssaas.generalbusiness.entity;

import java.io.Serializable;
import java.util.Calendar;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonFormat;

import br.com.eventssaas.generalbusiness.enums.CheckinType;

@Entity
@Table(name = "profile_checkin")
public class CheckIn implements Serializable {

	private static final long serialVersionUID = 839489568576395835L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy HH:mm:ssZ")
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "creation_date")
	private Calendar creationDate = Calendar.getInstance();

	@NotNull
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "profile_checked_id")
	private Profile profileCheckedIn;

	@NotNull
	@Column(name = "type", columnDefinition = "int(1) default 0")
	@Enumerated(EnumType.ORDINAL)
	private CheckinType type = CheckinType.EVENT_CHEKIN;

	@NotNull
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "profile_agent_id")
	private Profile profileAgent;

	@NotNull
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "event_id")
	private Event event;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "lecture_id")
	private LectureDetail lecture;

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Profile getProfileCheckedIn() {
		return this.profileCheckedIn;
	}

	public void setProfileCheckedIn(Profile profileCheckedIn) {
		this.profileCheckedIn = profileCheckedIn;
	}

	public Profile getProfileAgent() {
		return this.profileAgent;
	}

	public void setProfileAgent(Profile profileAgent) {
		this.profileAgent = profileAgent;
	}

	public Event getEvent() {
		return this.event;
	}

	public void setEvent(Event event) {
		this.event = event;
	}

	public Calendar getCreationDate() {
		return this.creationDate;
	}

	public void setCreationDate(Calendar creationDate) {
		this.creationDate = creationDate;
	}

	public CheckinType getType() {
		return this.type;
	}

	public void setType(CheckinType type) {
		this.type = type;
	}

	public LectureDetail getLecture() {
		return this.lecture;
	}

	public void setLecture(LectureDetail lecture) {
		this.lecture = lecture;
	}
}
