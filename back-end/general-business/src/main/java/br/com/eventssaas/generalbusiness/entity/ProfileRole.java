/**
 *
 */
package br.com.eventssaas.generalbusiness.entity;

import java.io.Serializable;
import java.util.Calendar;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonFormat;

import br.com.eventssaas.generalbusiness.enums.ProfileRoleType;

@Entity
@Table(name = "profile_role")
public class ProfileRole implements Serializable {

	private static final long serialVersionUID = 1871105356870219797L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy hh:mm:ss")
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "creation_date")
	private Calendar creationDate = Calendar.getInstance();

	@NotNull
	@Column(name = "type", columnDefinition = "varchar(20) default 'ATTENDEE'")
	@Enumerated(EnumType.STRING)
	private ProfileRoleType type;

	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "event_id")
	private Event event;

	// Como estamos utilizando o profile role para fazer relacao entre o usuario
	// e o evento, se o site necessitar de informacao adicional
	@Lob
	@Column(name = "extra_info")
	private String extraInfo;

	// Caso o cliente necessite inserir informacao extra no badge que vem do
	// registro
	@Column(name = "extra_info_badge")
	private String extraInfoBadge;

	@NotNull
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "profile_id")
	private Profile profile;

	@Column(name = "is_blocked", columnDefinition = "bit(1) default 0")
	private boolean blocked = false;

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public ProfileRoleType getType() {
		return this.type;
	}

	public void setType(ProfileRoleType type) {
		this.type = type;
	}

	public Event getEvent() {
		return this.event;
	}

	public void setEvent(Event event) {
		this.event = event;
	}

	public Profile getProfile() {
		return this.profile;
	}

	public void setProfile(Profile profile) {
		this.profile = profile;
	}

	public boolean isBlocked() {
		return this.blocked;
	}

	public void setBlocked(boolean blocked) {
		this.blocked = blocked;
	}

	public String getExtraInfo() {
		return this.extraInfo;
	}

	public void setExtraInfo(String extraInfo) {
		this.extraInfo = extraInfo;
	}

	public String getExtraInfoBadge() {
		return this.extraInfoBadge;
	}

	public void setExtraInfoBadge(String extraInfoBadge) {
		this.extraInfoBadge = extraInfoBadge;
	}

	public void setCreationDate(Calendar creationDate) {
		this.creationDate = creationDate;
	}

	public Calendar getCreationDate() {
		return this.creationDate;
	}
}