package br.com.eventssaas.generalbusiness.services;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import br.com.eventssaas.generalbusiness.entity.Profile;
import br.com.eventssaas.generalbusiness.entity.ProfileRole;
import br.com.eventssaas.generalbusiness.entity.RedisProfileRole;
import br.com.eventssaas.generalbusiness.entity.RedisToken;

@PropertySource("classpath:application.properties")
@Service("br.com.eventssaas.generalbusiness.services.ServiceToken")
public class ServiceToken {

	public static final long IDLE = 5 * 60 * 1000;
	private static final String PROPERTY_NAME_PREFIX = "redis.key.prefix";

	@Resource
	private Environment env;

	@Autowired
	private RedisTemplate<String, RedisToken> template;

	public void addToken(String token, Date lastAccess, Profile profile, List<ProfileRole> roles, Long eventId) {
		RedisToken redisToken = new RedisToken();

		List<RedisProfileRole> redisRoles = new ArrayList<RedisProfileRole>();

		for (ProfileRole profileRole : roles) {
			RedisProfileRole r = new RedisProfileRole();
			r.setId(profileRole.getId());
			r.setProfileId(profileRole.getProfile().getId());
			r.setType(profileRole.getType());
			redisRoles.add(r);
		}

		redisToken.setProfileRoles(redisRoles);
		redisToken.setToken(token);
		redisToken.setLastModified(lastAccess);
		redisToken.setProfileId(profile.getId());
		redisToken.setEventId(eventId);

		this.template.opsForValue().set(this.getPrefix() + "event:" + eventId + ":profile:" + profile.getId(),
				redisToken);
		this.template.opsForValue().set(this.getPrefix() + "token:" + token, redisToken);
	}

	public void updateLastModifiedFromToken(RedisToken redisToken, Date lastAccess) {

		redisToken.setLastModified(lastAccess);

		this.template.opsForValue().set(
				this.getPrefix() + "event:" + redisToken.getEventId() + ":profile:" + redisToken.getProfileId(),
				redisToken);
		this.template.opsForValue().set(this.getPrefix() + "token:" + redisToken.getToken(), redisToken);
	}

	public void removeFromToken(String token) {
		RedisToken result = this.template.opsForValue().get(this.getPrefix() + "token:" + token);
		if (result != null) {
			this.template.delete(this.getPrefix() + "token:" + token);
			this.template
					.delete(this.getPrefix() + "event:" + result.getEventId() + ":profile:" + result.getProfileId());
		}
	}

	public void removeFromUser(Profile profile, Long eventId) {
		RedisToken result = this.template.opsForValue()
				.get(this.getPrefix() + "event:" + eventId + ":profile:" + profile.getId().toString());
		if (result != null) {
			this.template.delete(this.getPrefix() + "token:" + result.getToken());
			this.template.delete(this.getPrefix() + "event:" + eventId + ":profile:" + result.getProfileId());
		}
	}

	public Date getDateFromToken(String token) {
		return this.template.opsForValue().get("token:" + token).getLastModified();
	}

	public Date getDateFromProfile(Long idProfile, Long eventId) {
		RedisToken result = this.template.opsForValue()
				.get(this.getPrefix() + "event:" + eventId + ":profile:" + idProfile);
		Date resultDate = null;
		if (result != null) {
			resultDate = result.getLastModified();
		}
		return resultDate;
	}

	public boolean isOnline(Long idProfile, Long eventId) {
		boolean result = false;

		Date lastModified = this.getDateFromProfile(idProfile, eventId);

		if (lastModified != null) {
			long now = new Date().getTime();
			if ((now - lastModified.getTime()) < IDLE) {
				result = true;
			}
		}

		return result;
	}

	public RedisToken getRedisToken(String token) {
		return this.template.opsForValue().get(this.getPrefix() + "token:" + token);
	}

	public String getPrefix() {
		return this.env.getRequiredProperty(PROPERTY_NAME_PREFIX);
	}

}
