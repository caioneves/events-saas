package br.com.eventssaas.generalbusiness.services;

import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.eventssaas.generalbusiness.entity.PasswordResetToken;
import br.com.eventssaas.generalbusiness.entity.Profile;
import br.com.eventssaas.generalbusiness.entity.User;
import br.com.eventssaas.generalbusiness.entity.VerificationToken;
import br.com.eventssaas.generalbusiness.enums.MailBasicTemplateType;
import br.com.eventssaas.generalbusiness.exceptions.NotFoundException;
import br.com.eventssaas.generalbusiness.repository.PasswordResetTokenRepository;
import br.com.eventssaas.generalbusiness.repository.ProfileRepository;
import br.com.eventssaas.generalbusiness.repository.UserRepository;
import br.com.eventssaas.generalbusiness.repository.VerificationTokenRepository;
import br.com.eventssaas.generalbusiness.validation.EmailExistsException;

@Service("br.com.eventssaas.generalbusiness.services.UserService")
class UserService implements IUserService {

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private ProfileRepository profileRepository;

	@Autowired
	private VerificationTokenRepository verificationTokenRepository;

	@Autowired
	private PasswordResetTokenRepository passwordTokenRepository;

	@Autowired
	private PasswordEncoder passwordEncoder;

	@Autowired
	private IMailService mailService;

	@Override
	public User create(final User user) throws EmailExistsException {

		if (this.emailExist(user.getEmail())) {
			throw new EmailExistsException("Existe uma conta com este email: " + user.getEmail());
		}

		return this.userRepository.save(user);
	}

	@Override
	public User findUserByEmail(final String email) {
		return this.userRepository.findByEmailIgnoreCase(email);
	}

	@Transactional
	@Override
	public void forgetPassword(final String userEmail) throws NotFoundException {
		this.userValidation(userEmail);

		Profile profile = this.profileRepository.findByUserEmailIgnoreCase(userEmail);

		final String temporaryPassword = this.createRandomPasswordForUser(profile.getUser());
		this.mailService.sendEmailQRCodeAndPasswordIfExist(MailBasicTemplateType.FORGOT_PASSWORD,
				profile.getUser().getEmail(), profile.getFirstName(), temporaryPassword, profile.getQrcode());
	}

	@Override
	public void createPasswordResetTokenForUser(final User user, final String token) {
		final PasswordResetToken myToken = new PasswordResetToken(token, user);
		this.passwordTokenRepository.save(myToken);
	}

	@Override
	public PasswordResetToken getPasswordResetToken(final String token) {
		return this.passwordTokenRepository.findByToken(token);
	}

	@Override
	public String createRandomPasswordForUser(final User user) {
		final String temporaryPassword = RandomStringUtils.randomAlphabetic(8);
		this.changeUserPassword(user, temporaryPassword, temporaryPassword, true);
		return temporaryPassword;
	}

	@Override
	public void changeUserPassword(final User user, final String password, final String passwordConfirmation) {
		this.changeUserPassword(user, password, passwordConfirmation, false);
	}

	@Override
	public void changeUserPassword(final User user, final String password, final String passwordConfirmation,
			boolean resetedPassword) {

		User userDB = this.findUserByEmail(user.getEmail());

		userDB.setResetedPassword(resetedPassword);
		// encoding the password
		userDB.setPassword(this.passwordEncoder.encode(password));
		userDB.setPasswordConfirmation(passwordConfirmation);

		this.userRepository.save(userDB);
	}

	@Override
	public VerificationToken createVerificationTokenForUser(final User user) {
		final VerificationToken myToken = new VerificationToken(user);
		return this.verificationTokenRepository.save(myToken);
	}

	@Override
	public VerificationToken getVerificationToken(final String token) {
		return this.verificationTokenRepository.findByToken(token);
	}

	@Override
	public void save(final User user) {
		this.userRepository.save(user);
	}

	private boolean emailExist(final String email) {
		final User user = this.userRepository.findByEmailIgnoreCase(email);
		return user != null;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see br.com.eventssaas.sitefrontend.service.IUserService#
	 * confirmUserRegistration(br.
	 * com.eventssaas.generalbusiness.entity.VerificationToken)
	 */
	@Override
	public User confirmUserRegistration(final VerificationToken token) {

		VerificationToken verificationTokenDB = this.verificationTokenRepository.findByToken(token.getToken());

		verificationTokenDB.setVerified(true);
		this.verificationTokenRepository.save(verificationTokenDB);

		User userDB = this.findUserByEmail(verificationTokenDB.getUser().getEmail());
		// FIXME: ConstraintViolationImpl{interpolatedMessage='Confirmação de
		// senha é Obrigatória.', propertyPath=passwordConfirmation
		userDB.setPasswordConfirmation(userDB.getPassword());
		userDB.setEnabled(true);

		return this.userRepository.save(userDB);
	}

	private void userValidation(String email) throws NotFoundException {
		if (email == null || email.trim().isEmpty()) {
			throw new NotFoundException("request.fail.user.email.notfound");
		} else if (!this.userRepository.exists(email)) {
			throw new NotFoundException("request.fail.user.email.notfound");
		}
	}

}
