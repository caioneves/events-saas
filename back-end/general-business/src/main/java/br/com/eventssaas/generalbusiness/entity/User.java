package br.com.eventssaas.generalbusiness.entity;

import java.io.Serializable;
import java.util.Calendar;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotEmpty;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "user")
public class User implements Serializable {

	private static final long serialVersionUID = 5883454450090755841L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@NotNull
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "creation_date")
	private Calendar creationDate = Calendar.getInstance();

	@Email
	@Length(max = 100)
	@NotNull
	@NotEmpty
	@Column(name = "email")
	private String email;

	@Length(max = 100)
	@NotNull
	@NotEmpty
	@Column(name = "password")
	private String password;

	@Transient
	@NotEmpty
	private String passwordConfirmation;

	@NotNull
	@Column(name = "is_enabled", columnDefinition = "bit(1) default 1")
	private boolean enabled = true;

	@NotNull
	@Column(name = "is_reseted_password", columnDefinition = "bit(1) default 0")
	private boolean resetedPassword = false;

	public User() {
		super();
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getEmail() {
		return this.email;
	}

	public void setEmail(final String email) {

		if (email != null) {
			// to prevent user erros (entered an email address with upper case
			email.toLowerCase();
		}

		this.email = email;
	}

	@JsonIgnore
	public String getPassword() {
		return this.password;
	}

	public void setPassword(final String password) {
		this.password = password;
	}

	@JsonIgnore
	public String getPasswordConfirmation() {
		return this.passwordConfirmation;
	}

	public void setPasswordConfirmation(final String passwordConfirmation) {
		this.passwordConfirmation = passwordConfirmation;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	public boolean isEnabled() {
		return this.enabled;
	}

	public Calendar getCreationDate() {
		return this.creationDate;
	}

	public void setCreationDate(Calendar creationDate) {
		this.creationDate = creationDate;
	}

	public boolean isResetedPassword() {
		return this.resetedPassword;
	}

	public void setResetedPassword(boolean resetedPassword) {
		this.resetedPassword = resetedPassword;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((this.creationDate == null) ? 0 : this.creationDate.hashCode());
		result = prime * result + ((this.email == null) ? 0 : this.email.hashCode());
		result = prime * result + (this.enabled ? 1231 : 1237);
		result = prime * result + ((this.id == null) ? 0 : this.id.hashCode());
		result = prime * result + ((this.password == null) ? 0 : this.password.hashCode());
		result = prime * result + ((this.passwordConfirmation == null) ? 0 : this.passwordConfirmation.hashCode());
		result = prime * result + (this.resetedPassword ? 1231 : 1237);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (this.getClass() != obj.getClass()) {
			return false;
		}
		User other = (User) obj;
		if (this.creationDate == null) {
			if (other.creationDate != null) {
				return false;
			}
		} else if (!this.creationDate.equals(other.creationDate)) {
			return false;
		}
		if (this.email == null) {
			if (other.email != null) {
				return false;
			}
		} else if (!this.email.equals(other.email)) {
			return false;
		}
		if (this.enabled != other.enabled) {
			return false;
		}
		if (this.id == null) {
			if (other.id != null) {
				return false;
			}
		} else if (!this.id.equals(other.id)) {
			return false;
		}
		if (this.password == null) {
			if (other.password != null) {
				return false;
			}
		} else if (!this.password.equals(other.password)) {
			return false;
		}
		if (this.passwordConfirmation == null) {
			if (other.passwordConfirmation != null) {
				return false;
			}
		} else if (!this.passwordConfirmation.equals(other.passwordConfirmation)) {
			return false;
		}
		if (this.resetedPassword != other.resetedPassword) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "User [id=" + this.id + ", creationDate=" + this.creationDate + ", email=" + this.email + ", password="
				+ this.password + ", passwordConfirmation=" + this.passwordConfirmation + ", enabled=" + this.enabled
				+ ", resetedPassword=" + this.resetedPassword + "]";
	}

}
