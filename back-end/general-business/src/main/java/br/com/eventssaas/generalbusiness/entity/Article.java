package br.com.eventssaas.generalbusiness.entity;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;

import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;
import org.hibernate.envers.RelationTargetAuditMode;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;

import br.com.eventssaas.generalbusiness.enums.ArticleType;

@Entity
@Table(name = "article")
@Audited
public class Article implements Serializable {

	private static final long serialVersionUID = 1698899130106770766L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@JsonIgnore
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "creation_date")
	private Calendar creationDate = Calendar.getInstance();

	@NotNull
	@Enumerated(EnumType.STRING)
	@Column(name = "type", columnDefinition = "varchar(30) default 'APP_POST'")
	private ArticleType type = ArticleType.APP_POST;

	@Column(name = "source")
	private String source;

	@Column(name = "detail_url")
	private String detaillUrl;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "image_id")
	@NotAudited
	private Image image;

	@Column(name = "image_url")
	private String imageUrl;

	@Column(name = "title")
	private String title;

	@Lob
	@Column(name = "description")
	private String description;

	@NotNull
	@JsonIgnore
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "event_id")
	@NotAudited
	private Event event;

	@NotNull
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "author_profile_id")
	@NotAudited
	private Profile author;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "sponsor_profile_id")
	@NotAudited
	private Profile sponsor;

	@NotNull
	@Column(name = "is_push_notification", columnDefinition = "bit(1) default 0")
	private boolean pushNotification = false;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "push_notification_date")
	private Calendar pushNotificationDate;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "article", cascade = CascadeType.ALL)
	@JsonProperty(access = Access.WRITE_ONLY)
	@Audited(targetAuditMode = RelationTargetAuditMode.AUDITED)
	private List<Commentary> listCommentary;

	@NotAudited
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "article", cascade = CascadeType.ALL)
	private List<ArticleLike> listLikes;

	@NotAudited
	@ElementCollection
	@CollectionTable(name = "article_black_post_words", joinColumns = @JoinColumn(name = "article_id"))
	@Column(name = "black_word")
	private Set<String> listBlackPostWords;

	@NotNull
	@Column(name = "is_black_post", columnDefinition = "bit(1) default 0")
	private boolean blackPost = false;

	@NotNull
	@Column(name = "is_blocked", columnDefinition = "bit(1) default 0")
	private boolean blocked = false;

	@NotNull
	@Column(name = "is_blocked_by_author", columnDefinition = "bit(1) default 0")
	private boolean blockedByAuthor = false;

	@Transient
	private int numberComment;

	@Transient
	private int numberLike;

	@Transient
	private boolean liked;

	@Transient
	private boolean hasComment;

	public long getDisplayTime() {
		return this.creationDate.getTimeInMillis();
	}

	public String getSource() {
		return this.source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public String getDetaillUrl() {
		return this.detaillUrl;
	}

	public void setDetaillUrl(String detaillUrl) {
		this.detaillUrl = detaillUrl;
	}

	public String getTitle() {
		return this.title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Event getEvent() {
		return this.event;
	}

	public void setEvent(Event event) {
		this.event = event;
	}

	@JsonIgnore
	public List<Commentary> getListCommentary() {
		return this.listCommentary;
	}

	public void setListCommentary(List<Commentary> listCommentary) {
		this.listCommentary = listCommentary;
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public int getNumberComment() {
		if (this.listCommentary == null) {
			return 0;
		}
		Iterator<Commentary> commIterator = this.listCommentary.iterator();
		while (commIterator.hasNext()) {
			Commentary c = commIterator.next();
			if (c.isBlackComment()) {
				commIterator.remove();
			}
		}

		return this.listCommentary.size();
	}

	public int getNumberLike() {
		if (this.listLikes == null) {
			return 0;
		}
		return this.listLikes.size();
	}

	public void setHasComment(Profile userLoggedIn) {
		this.hasComment = false;
		if (this.getListCommentary() != null) {
			for (Commentary commentary : this.getListCommentary()) {
				if (commentary.getAuthor().getId() == userLoggedIn.getId() && !commentary.isBlackComment()) {
					this.hasComment = true;
					break;
				}

			}
		}
	}

	public boolean getHasComment() {
		return this.hasComment;
	}

	public void setNumberLike(int numberLike) {
		this.numberLike = numberLike;
	}

	public void setNumberComment(int numberComment) {
		this.numberComment = numberComment;
	}

	public boolean isLiked() {
		return this.liked;
	}

	public void setLiked(boolean liked) {
		this.liked = liked;
	}

	public Image getImage() {
		return this.image;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((this.author == null) ? 0 : this.author.hashCode());
		result = prime * result + ((this.creationDate == null) ? 0 : this.creationDate.hashCode());
		result = prime * result + ((this.description == null) ? 0 : this.description.hashCode());
		result = prime * result + ((this.detaillUrl == null) ? 0 : this.detaillUrl.hashCode());
		result = prime * result + ((this.event == null) ? 0 : this.event.hashCode());
		result = prime * result + ((this.id == null) ? 0 : this.id.hashCode());
		result = prime * result + ((this.image == null) ? 0 : this.image.hashCode());
		result = prime * result + ((this.imageUrl == null) ? 0 : this.imageUrl.hashCode());
		result = prime * result + (this.liked ? 1231 : 1237);
		result = prime * result + ((this.listCommentary == null) ? 0 : this.listCommentary.hashCode());
		result = prime * result + ((this.listLikes == null) ? 0 : this.listLikes.hashCode());
		result = prime * result + this.numberComment;
		result = prime * result + this.numberLike;
		result = prime * result + (this.pushNotification ? 1231 : 1237);
		result = prime * result + ((this.pushNotificationDate == null) ? 0 : this.pushNotificationDate.hashCode());
		result = prime * result + ((this.source == null) ? 0 : this.source.hashCode());
		result = prime * result + ((this.sponsor == null) ? 0 : this.sponsor.hashCode());
		result = prime * result + ((this.title == null) ? 0 : this.title.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (this.getClass() != obj.getClass()) {
			return false;
		}
		Article other = (Article) obj;
		if (this.author == null) {
			if (other.author != null) {
				return false;
			}
		} else if (!this.author.equals(other.author)) {
			return false;
		}
		if (this.creationDate == null) {
			if (other.creationDate != null) {
				return false;
			}
		} else if (!this.creationDate.equals(other.creationDate)) {
			return false;
		}
		if (this.description == null) {
			if (other.description != null) {
				return false;
			}
		} else if (!this.description.equals(other.description)) {
			return false;
		}
		if (this.detaillUrl == null) {
			if (other.detaillUrl != null) {
				return false;
			}
		} else if (!this.detaillUrl.equals(other.detaillUrl)) {
			return false;
		}
		if (this.event == null) {
			if (other.event != null) {
				return false;
			}
		} else if (!this.event.equals(other.event)) {
			return false;
		}
		if (this.id == null) {
			if (other.id != null) {
				return false;
			}
		} else if (!this.id.equals(other.id)) {
			return false;
		}

		if (this.image == null) {
			if (other.image != null) {
				return false;
			}
		} else if (this.image.getId() != other.image.getId()) {
			return false;
		}
		if (this.imageUrl == null) {
			if (other.imageUrl != null) {
				return false;
			}
		} else if (!this.imageUrl.equals(other.imageUrl)) {
			return false;
		}
		if (this.liked != other.liked) {
			return false;
		}
		if (this.listCommentary == null) {
			if (other.listCommentary != null) {
				return false;
			}
		} else if (!this.listCommentary.equals(other.listCommentary)) {
			return false;
		}
		if (this.listLikes == null) {
			if (other.listLikes != null) {
				return false;
			}
		} else if (!this.listLikes.equals(other.listLikes)) {
			return false;
		}
		if (this.numberComment != other.numberComment) {
			return false;
		}
		if (this.numberLike != other.numberLike) {
			return false;
		}
		if (this.pushNotification != other.pushNotification) {
			return false;
		}
		if (this.pushNotificationDate == null) {
			if (other.pushNotificationDate != null) {
				return false;
			}
		} else if (!this.pushNotificationDate.equals(other.pushNotificationDate)) {
			return false;
		}
		if (this.source == null) {
			if (other.source != null) {
				return false;
			}
		} else if (!this.source.equals(other.source)) {
			return false;
		}
		if (this.sponsor == null) {
			if (other.sponsor != null) {
				return false;
			}
		} else if (!this.sponsor.equals(other.sponsor)) {
			return false;
		}
		if (this.title == null) {
			if (other.title != null) {
				return false;
			}
		} else if (!this.title.equals(other.title)) {
			return false;
		}
		return true;
	}

	public String getImageUrl() {

		if (this.getImage() != null) {
			this.setImageUrl(Image.IMAGE_REST_PATH + this.getImage().getId());
		}

		return this.imageUrl;
	}

	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}

	public Profile getAuthor() {
		return this.author;
	}

	public void setAuthor(Profile author) {
		this.author = author;
	}

	public Profile getSponsor() {
		return this.sponsor;
	}

	public void setSponsor(Profile sponsor) {
		this.sponsor = sponsor;
	}

	public boolean isPushNotification() {
		return this.pushNotification;
	}

	public void setPushNotification(boolean pushNotification) {
		this.pushNotification = pushNotification;
	}

	public void setImage(Image image) {
		this.image = image;
	}

	public ArticleType getType() {
		return this.type;
	}

	public void setType(ArticleType type) {
		this.type = type;
	}

	public boolean isBlackPost() {
		return this.blackPost;
	}

	public void setBlackPost(boolean blackPost) {
		this.blackPost = blackPost;
	}

	public boolean isBlocked() {
		return this.blocked;
	}

	public void setBlocked(boolean blocked) {
		this.blocked = blocked;
	}

	public Calendar getCreationDate() {
		return this.creationDate;
	}

	public void setCreationDate(Calendar creationDate) {
		this.creationDate = creationDate;
	}

	public void setPushNotificationDate(Calendar pushNotificationDate) {
		this.pushNotificationDate = pushNotificationDate;
	}

	public void setHasComment(boolean hasComment) {
		this.hasComment = hasComment;
	}

	public Calendar getPushNotificationDate() {
		return this.pushNotificationDate;
	}

	@JsonIgnore
	public List<ArticleLike> getListLikes() {
		return this.listLikes;
	}

	public void setListLikes(List<ArticleLike> listLikes) {
		this.listLikes = listLikes;
	}

	public Set<String> getListBlackPostWords() {
		return this.listBlackPostWords;
	}

	public void setListBlackPostWords(Set<String> listBlackPostWords) {
		this.listBlackPostWords = listBlackPostWords;
	}

	public boolean isBlockedByAuthor() {
		return this.blockedByAuthor;
	}

	public void setBlockedByAuthor(boolean blockedByAuthor) {
		this.blockedByAuthor = blockedByAuthor;
	}

}