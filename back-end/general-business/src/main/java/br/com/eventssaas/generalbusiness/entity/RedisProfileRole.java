package br.com.eventssaas.generalbusiness.entity;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;

import br.com.eventssaas.generalbusiness.enums.ProfileRoleType;

public class RedisProfileRole {

	private Long id;

	private Long profileId;

	@Enumerated(EnumType.STRING)
	private ProfileRoleType type;

	public ProfileRoleType getType() {
		return this.type;
	}

	public void setType(ProfileRoleType type) {
		this.type = type;
	}

	public Long getProfileId() {
		return this.profileId;
	}

	public void setProfileId(Long profileId) {
		this.profileId = profileId;
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

}
