package br.com.eventssaas.generalbusiness.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import br.com.eventssaas.generalbusiness.entity.LectureDetail;

public interface LectureDetailRepository extends JpaRepository<LectureDetail, Long> {

	@Query("select distinct l.startTime from LectureDetail as l where  l.event.id = :eventId ORDER BY l.startTime asc")
	public List<Date> findDistinctStartTimeByEventId(@Param("eventId") Long eventId);

	@Query("select l from LectureDetail as l where  l.event.id = :eventId AND l.parentLecture = NULL ORDER BY l.priority desc, l.startTime asc")
	public List<LectureDetail> findByEventIdAndNoParent(@Param("eventId") Long eventId);

	LectureDetail findByEventIdAndId(Long eventId, Long id);

	@Query("select l from LectureDetail as l where  l.event.id = :eventId AND l.startTime >= :startTime AND type != br.com.eventssaas.generalbusiness.enums.LectureDetailType.TRACKING ORDER BY l.startTime asc")
	public List<LectureDetail> findByEventIdAndStartTimeOrderedPagined(@Param("eventId") Long eventId,
			@Param("startTime") Date startTime, Pageable pageable);
}
