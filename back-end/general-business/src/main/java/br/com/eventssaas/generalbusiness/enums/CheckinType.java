package br.com.eventssaas.generalbusiness.enums;

public enum CheckinType {

	EVENT_CHEKIN(0), LECTURE_CHECKIN(1), ACCREDITION(2), ENTRY(3);

	private int chekinType;

	public int getArticleType() {
		return this.chekinType;
	}

	CheckinType(final int chekinType) {
		this.chekinType = chekinType;
	}

}
