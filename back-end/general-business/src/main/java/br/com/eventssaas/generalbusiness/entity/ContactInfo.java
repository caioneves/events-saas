package br.com.eventssaas.generalbusiness.entity;

import java.io.Serializable;
import java.util.Calendar;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Proxy;
import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.Length;

@Entity
@Table(name = "contact_info")
@Proxy(lazy = false)
public class ContactInfo implements Serializable {

	private static final long serialVersionUID = 8728991622734028682L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "creation_date")
	private Calendar creationDate = Calendar.getInstance();

	@Length(max = 100)
	@Email
	@Column(name = "email")
	private String email;

	@Length(max = 30)
	@Column(name = "phone1")
	private String phone1;

	@Length(max = 30)
	@Column(name = "phone2")
	private String phone2;

	@Length(max = 30)
	@Column(name = "phone3")
	private String phone3;

	@Length(max = 30)
	@Column(name = "phone4")
	private String phone4;

	@Length(max = 100)
	@Column(name = "address1")
	private String address1;

	@Length(max = 100)
	@Column(name = "address2")
	private String address2;

	@Length(max = 50)
	@Column(name = "number")
	private String number;

	@Length(max = 100)
	// neiborhood
	@Column(name = "county")
	private String county;

	@Length(max = 100)
	@Column(name = "city")
	private String city;

	@Length(max = 100)
	@Column(name = "state", columnDefinition = "varchar(100) default 'SP'")
	private String state;

	@Length(max = 50)
	@Column(name = "country", columnDefinition = "varchar(50) default 'BRASIL'")
	private String country;

	@Length(max = 15)
	@Column(name = "cep")
	private String CEP;

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhone1() {
		return this.phone1;
	}

	public void setPhone1(String phone1) {
		this.phone1 = phone1;
	}

	public String getPhone2() {
		return this.phone2;
	}

	public void setPhone2(String phone2) {
		this.phone2 = phone2;
	}

	public String getPhone3() {
		return this.phone3;
	}

	public void setPhone3(String phone3) {
		this.phone3 = phone3;
	}

	public String getPhone4() {
		return this.phone4;
	}

	public void setPhone4(String phone4) {
		this.phone4 = phone4;
	}

	public String getAddress1() {
		return this.address1;
	}

	public void setAddress1(String address1) {
		this.address1 = address1;
	}

	public String getAddress2() {
		return this.address2;
	}

	public void setAddress2(String address2) {
		this.address2 = address2;
	}

	public String getNumber() {
		return this.number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	public String getCounty() {
		return this.county;
	}

	public void setCounty(String county) {
		this.county = county;
	}

	public String getCity() {
		return this.city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getState() {
		return this.state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getCountry() {
		return this.country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getCEP() {
		return this.CEP;
	}

	public void setCEP(String cEP) {
		this.CEP = cEP;
	}

	public Calendar getCreationDate() {
		return this.creationDate;
	}

	public void setCreationDate(Calendar creationDate) {
		this.creationDate = creationDate;
	}

}
