package br.com.eventssaas.apprest.token;

import java.io.Serializable;
import java.util.Date;

public class Token implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;

	private String value;

	private Date lastAccess;

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public Date getLastAccess() {
		return lastAccess;
	}

	public void setLastAccess(Date lastAccess) {
		this.lastAccess = lastAccess;
	}

}
