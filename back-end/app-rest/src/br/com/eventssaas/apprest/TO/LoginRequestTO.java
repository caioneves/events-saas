package br.com.eventssaas.apprest.TO;

public class LoginRequestTO {
	private String login;
	private String password;

	public String getLogin() {
		if (this.login != null) {
			this.login = this.login.trim();
		}

		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getPassword() {
		if (this.password != null) {
			this.password = this.password.trim();
		}
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

}
