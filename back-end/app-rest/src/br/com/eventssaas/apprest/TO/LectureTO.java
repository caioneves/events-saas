package br.com.eventssaas.apprest.TO;

import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Set;

import br.com.eventssaas.generalbusiness.enums.LectureDetailType;

public class LectureTO {

	private Long id;

	private Calendar creationDate;

	private String title;

	private Date startTime;

	private Date endTime;

	private EventInformationTO event;

	private MiniProfileTO author;

	private LectureTO parentLecture;

	private List<LectureTO> listSubItems;

	private String imageUrl;

	private boolean keyNote;

	private String description;

	private String room;

	private String detailUrl;

	private Set<String> listTags;

	private List<MiniProfileTO> listSpeakers;

	private List<MiniProfileTO> registeredUsers;

	private Long filesSize;

	private LectureDetailType type = LectureDetailType.LECTURE;

	private String scheduleBackgroundColor;

	private Integer priority;

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTitle() {
		return this.title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Date getStartTime() {
		return this.startTime;
	}

	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}

	public Date getEndTime() {
		return this.endTime;
	}

	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}

	public EventInformationTO getEvent() {
		return this.event;
	}

	public void setEvent(EventInformationTO event) {
		this.event = event;
	}

	public MiniProfileTO getAuthor() {
		return this.author;
	}

	public void setAuthor(MiniProfileTO author) {
		this.author = author;
	}

	public LectureTO getParentLecture() {
		return this.parentLecture;
	}

	public void setParentLecture(LectureTO parentLecture) {
		this.parentLecture = parentLecture;
	}

	public List<LectureTO> getListSubItems() {
		return this.listSubItems;
	}

	public void setListSubItems(List<LectureTO> listSubItems) {
		this.listSubItems = listSubItems;
	}

	public String getImageUrl() {
		return this.imageUrl;
	}

	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}

	public boolean isKeyNote() {
		return this.keyNote;
	}

	public void setKeyNote(boolean keyNote) {
		this.keyNote = keyNote;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getRoom() {
		return this.room;
	}

	public void setRoom(String room) {
		this.room = room;
	}

	public String getDetailUrl() {
		return this.detailUrl;
	}

	public void setDetailUrl(String detailUrl) {
		this.detailUrl = detailUrl;
	}

	public Set<String> getListTags() {
		return this.listTags;
	}

	public void setListTags(Set<String> listTags) {
		this.listTags = listTags;
	}

	public List<MiniProfileTO> getListSpeakers() {
		return this.listSpeakers;
	}

	public void setListSpeakers(List<MiniProfileTO> listSpeakers) {
		this.listSpeakers = listSpeakers;
	}

	public Long getFilesSize() {
		return this.filesSize;
	}

	public void setFilesSize(Long filesSize) {
		this.filesSize = filesSize;
	}

	public String getScheduleBackgroundColor() {
		return scheduleBackgroundColor;
	}

	public void setScheduleBackgroundColor(String scheduleBackgroundColor) {
		this.scheduleBackgroundColor = scheduleBackgroundColor;
	}

	public Integer getPriority() {
		return priority;
	}

	public void setPriority(Integer priority) {
		this.priority = priority;
	}

	public List<MiniProfileTO> getRegisteredUsers() {
		return this.registeredUsers;
	}

	public void setRegisteredUsers(List<MiniProfileTO> registeredUsers) {
		this.registeredUsers = registeredUsers;
	}

	public LectureDetailType getType() {
		return type;
	}

	public void setType(LectureDetailType type) {
		this.type = type;
	}

	public Calendar getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Calendar creationDate) {
		this.creationDate = creationDate;
	}

}
