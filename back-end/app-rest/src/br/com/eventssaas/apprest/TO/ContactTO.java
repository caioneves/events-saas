package br.com.eventssaas.apprest.TO;

import java.io.Serializable;

public class ContactTO implements Serializable {
	/**
	 *
	 */
	private static final long serialVersionUID = 1074711773336345838L;

	private String subject;
	private String message;
	private MiniProfileTO profileFrom;
	private MiniProfileTO profileTo;

	public String getSubject() {
		return this.subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getMessage() {
		return this.message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public MiniProfileTO getProfileFrom() {
		return this.profileFrom;
	}

	public void setProfileFrom(MiniProfileTO profileFrom) {
		this.profileFrom = profileFrom;
	}

	public MiniProfileTO getProfileTo() {
		return this.profileTo;
	}

	public void setProfileTo(MiniProfileTO profileTo) {
		this.profileTo = profileTo;
	}

}
