package br.com.eventssaas.apprest.TO;

public class SearchRequestTO {
	private String searchText;

	public String getSearchText() {
		if (this.searchText != null) {
			this.searchText = this.searchText.trim();
		}
		return searchText;
	}

	public void setSearchText(String searchText) {
		this.searchText = searchText;
	}

}
