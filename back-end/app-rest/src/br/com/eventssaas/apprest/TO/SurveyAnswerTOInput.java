package br.com.eventssaas.apprest.TO;

public class SurveyAnswerTOInput {
	private Long question;
	private Long answer;
	private String answerText;

	public Long getQuestion() {
		return this.question;
	}

	public void setQuestion(Long question) {
		this.question = question;
	}

	public Long getAnswer() {
		return this.answer;
	}

	public void setAnswer(Long answer) {
		this.answer = answer;
	}

	public String getAnswerText() {
		return answerText;
	}

	public void setAnswerText(String answerText) {
		this.answerText = answerText;
	}

}
