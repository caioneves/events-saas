package br.com.eventssaas.apprest.TO;

import java.io.Serializable;

import br.com.eventssaas.generalbusiness.enums.ArticleType;

public class PostTextRequestTO implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = 7922675841578395421L;

	private String postMessage;

	private String title;

	private ArticleType articleType;

	private Long imageId;

	public boolean isTitleAndMessageNullOrEmpty() {

		return isTitleNullOrEmpty() && isPostMessageNullOrEmpty();

	}

	private boolean isTitleNullOrEmpty() {
		return getTitle() == null || getTitle().trim().isEmpty();
	}

	private boolean isPostMessageNullOrEmpty() {
		return getPostMessage() == null || getPostMessage().trim().isEmpty();
	}

	public String getPostMessage() {
		return postMessage;
	}

	public void setPostMessage(String postMessage) {
		this.postMessage = postMessage;
	}

	public ArticleType getArticleType() {
		return articleType;
	}

	public void setArticleType(ArticleType articleType) {
		this.articleType = articleType;
	}

	public Long getImageId() {
		return imageId;
	}

	public void setImageId(Long imageId) {
		this.imageId = imageId;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

}
