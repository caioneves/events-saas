package br.com.eventssaas.apprest.TO;

import java.util.List;

import br.com.eventssaas.generalbusiness.enums.SurveyQuestionType;

public class SurveyQuestionTO {

	private Long id;

	private Long surveyId;

	private SurveyQuestionType type = SurveyQuestionType.OPTION_QUESTION;

	private String question;

	private List<SurveyAnswerTO> surveyAnswers;

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getSurveyId() {
		return this.surveyId;
	}

	public void setSurveyId(Long surveyId) {
		this.surveyId = surveyId;
	}

	public String getQuestion() {
		return this.question;
	}

	public void setQuestion(String question) {
		this.question = question;
	}

	public List<SurveyAnswerTO> getSurveyAnswers() {
		return this.surveyAnswers;
	}

	public void setSurveyAnswers(List<SurveyAnswerTO> surveyAnswers) {
		this.surveyAnswers = surveyAnswers;
	}

	public SurveyQuestionType getType() {
		return type;
	}

	public void setType(SurveyQuestionType type) {
		this.type = type;
	}
}
