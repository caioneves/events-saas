package br.com.eventssaas.apprest.TO;

import java.io.Serializable;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

public class EventInformationTO implements Serializable {
	/**
	 *
	 */
	private static final long serialVersionUID = 7547360211944657784L;

	private Long idEvent;
	private Long id;
	private String image;
	private String name;

	// Period of event
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy HH:mm:ssZ")
	private Date startEvent;

	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy HH:mm:ssZ")
	private Date endEvent;

	// Contact
	private String siteUrl;
	private String email;
	private String phone1;
	private String phone2;
	private String phone3;
	private String phone4;

	// Place and Localization
	private String placeName;
	private String placeMapLabel;
	private String placeMapImageUrl;
	private String address1;
	private String address2;
	private String number;
	private String county;
	private String city;
	private String state;
	private String country;
	private String CEP;
	private Double googleMapsLong;
	private Double googleMapsLat;

	// Description
	private String description;

	public Long getIdEvent() {
		return this.idEvent;
	}

	public void setIdEvent(Long idEvent) {
		this.idEvent = idEvent;
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getImage() {
		return this.image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public long getStartEvent() {
		return this.startEvent.getTime();
	}

	public void setStartEvent(Date startEvent) {
		this.startEvent = startEvent;
	}

	public long getEndEvent() {
		return this.endEvent.getTime();
	}

	public void setEndEvent(Date endEvent) {
		this.endEvent = endEvent;
	}

	public String getPlaceName() {
		return this.placeName;
	}

	public void setPlaceName(String placeName) {
		this.placeName = placeName;
	}

	public String getAddress1() {
		return this.address1;
	}

	public void setAddress1(String address1) {
		this.address1 = address1;
	}

	public String getAddress2() {
		return this.address2;
	}

	public void setAddress2(String address2) {
		this.address2 = address2;
	}

	public String getNumber() {
		return this.number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	public String getCounty() {
		return this.county;
	}

	public void setCounty(String county) {
		this.county = county;
	}

	public String getCity() {
		return this.city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getState() {
		return this.state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getCountry() {
		return this.country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getCEP() {
		return this.CEP;
	}

	public void setCEP(String cEP) {
		this.CEP = cEP;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhone1() {
		return this.phone1;
	}

	public void setPhone1(String phone1) {
		this.phone1 = phone1;
	}

	public String getPhone2() {
		return this.phone2;
	}

	public void setPhone2(String phone2) {
		this.phone2 = phone2;
	}

	public String getPhone3() {
		return this.phone3;
	}

	public void setPhone3(String phone3) {
		this.phone3 = phone3;
	}

	public String getPhone4() {
		return this.phone4;
	}

	public void setPhone4(String phone4) {
		this.phone4 = phone4;
	}

	public Double getGoogleMapsLong() {
		return this.googleMapsLong;
	}

	public void setGoogleMapsLong(Double googleMapsLong) {
		this.googleMapsLong = googleMapsLong;
	}

	public Double getGoogleMapsLat() {
		return this.googleMapsLat;
	}

	public void setGoogleMapsLat(Double googleMapsLat) {
		this.googleMapsLat = googleMapsLat;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSiteUrl() {
		return siteUrl;
	}

	public void setSiteUrl(String siteUrl) {
		this.siteUrl = siteUrl;
	}

	public String getPlaceMapLabel() {
		return placeMapLabel;
	}

	public void setPlaceMapLabel(String placeMapLabel) {
		this.placeMapLabel = placeMapLabel;
	}

	public String getPlaceMapImageUrl() {
		return placeMapImageUrl;
	}

	public void setPlaceMapImageUrl(String placeMapImageUrl) {
		this.placeMapImageUrl = placeMapImageUrl;
	}

}
