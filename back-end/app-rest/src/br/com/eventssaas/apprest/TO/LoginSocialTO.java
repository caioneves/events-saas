package br.com.eventssaas.apprest.TO;

import java.io.Serializable;

public class LoginSocialTO implements Serializable {
	/**
	 *
	 */
	private static final long serialVersionUID = 1L;

	// facebook or linkedin
	private String providerId;

	private String userId;

	public String getProviderId() {
		return providerId;
	}

	public void setProviderId(String providerId) {
		this.providerId = providerId;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

}
