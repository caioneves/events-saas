package br.com.eventssaas.apprest.TO;

import java.io.Serializable;
import java.util.List;

public class ProfileAdminTO implements Serializable {

	private MiniProfileTO miniProfile;

	private List<EventTO> events;

	public MiniProfileTO getMiniProfile() {
		return miniProfile;
	}

	public void setMiniProfile(MiniProfileTO miniProfile) {
		this.miniProfile = miniProfile;
	}

	public List<EventTO> getEvents() {
		return events;
	}

	public void setEvents(List<EventTO> events) {
		this.events = events;
	}

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;

}
