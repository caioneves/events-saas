package br.com.eventssaas.apprest.TO;

import java.io.Serializable;

public class ArticleBlockTO implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = 7922675841578395421L;

	private boolean block;

	public boolean isBlock() {
		return block;
	}

	public void setBlock(boolean block) {
		this.block = block;
	}

}
