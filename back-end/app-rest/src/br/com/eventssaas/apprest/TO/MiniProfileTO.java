package br.com.eventssaas.apprest.TO;

import java.io.Serializable;

public class MiniProfileTO implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = 4749635681887587717L;

	private Long id;
	private String pictureUrl;
	private String name;
	private String jobRole;
	private String company;
	private boolean showJobRole;
	private boolean onLine;
	private String minibio;
	private String email;
	private String emailAlternative;
	private String phone1;
	private String phone2;
	private String phone3;
	private String phone4;
	private String documentNumber;
	private String documentType;
	private String facebookUrl;
	private String twitterUrl;
	private String linkedInUrl;
	private String instagramUrl;

	public String getPictureUrl() {
		return this.pictureUrl;
	}

	public void setPictureUrl(String pictureUrl) {
		this.pictureUrl = pictureUrl;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getJobRole() {
		return this.jobRole;
	}

	public void setJobRole(String jobRole) {
		this.jobRole = jobRole;
	}

	public String getCompany() {
		return this.company;
	}

	public void setCompany(String company) {
		this.company = company;
	}

	public boolean isShowJobRole() {
		return this.showJobRole;
	}

	public void setShowJobRole(boolean showJobRole) {
		this.showJobRole = showJobRole;
	}

	public String getMinibio() {
		return this.minibio;
	}

	public void setMinibio(String minibio) {
		this.minibio = minibio;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getPhone1() {
		return this.phone1;
	}

	public void setPhone1(String phone1) {
		this.phone1 = phone1;
	}

	public String getPhone2() {
		return this.phone2;
	}

	public void setPhone2(String phone2) {
		this.phone2 = phone2;
	}

	public String getPhone3() {
		return this.phone3;
	}

	public void setPhone3(String phone3) {
		this.phone3 = phone3;
	}

	public String getPhone4() {
		return this.phone4;
	}

	public void setPhone4(String phone4) {
		this.phone4 = phone4;
	}

	public Long getId() {
		return this.id;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getEmailAlternative() {
		return this.emailAlternative;
	}

	public void setEmailAlternative(String emailAlternative) {
		this.emailAlternative = emailAlternative;
	}

	public String getEmail() {
		return this.email;
	}

	public String getDocumentNumber() {
		return this.documentNumber;
	}

	public void setDocumentNumber(String documentNumber) {
		this.documentNumber = documentNumber;
	}

	public String getDocumentType() {
		return this.documentType;
	}

	public void setDocumentType(String documentType) {
		this.documentType = documentType;
	}

	public String getFacebookUrl() {
		return this.facebookUrl;
	}

	public void setFacebookUrl(String facebookUrl) {
		this.facebookUrl = facebookUrl;
	}

	public String getTwitterUrl() {
		return this.twitterUrl;
	}

	public void setTwitterUrl(String twitterUrl) {
		this.twitterUrl = twitterUrl;
	}

	public String getLinkedInUrl() {
		return this.linkedInUrl;
	}

	public void setLinkedInUrl(String linkedInUrl) {
		this.linkedInUrl = linkedInUrl;
	}

	public String getInstagramUrl() {
		return this.instagramUrl;
	}

	public void setInstagramUrl(String instagramUrl) {
		this.instagramUrl = instagramUrl;
	}

	public boolean isOnLine() {
		return this.onLine;
	}

	public void setOnLine(boolean onLine) {
		this.onLine = onLine;
	}

}
