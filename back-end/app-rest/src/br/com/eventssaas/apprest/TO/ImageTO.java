/**
 *
 */
package br.com.eventssaas.apprest.TO;

import java.io.Serializable;

/**
 * @author Caio Neves
 *
 *         Transfer Object for MessageChat Entity
 */
public class ImageTO implements Serializable {

	/**
	 * Generated UID
	 */
	private static final long serialVersionUID = 2070759098144343514L;

	private Long id;

	private String bytes;
	private String url;
	private String contentType;

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getBytes() {
		return this.bytes;
	}

	public void setBytes(String bytes) {
		this.bytes = bytes;
	}

	public String getUrl() {
		return this.url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getContentType() {
		return this.contentType;
	}

	public void setContentType(String contentType) {
		this.contentType = contentType;
	}

}
