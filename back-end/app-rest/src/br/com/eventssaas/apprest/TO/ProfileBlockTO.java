package br.com.eventssaas.apprest.TO;

import java.io.Serializable;

public class ProfileBlockTO implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = 7922675841578395421L;

	private boolean block;

	private Long profileId;

	public boolean isBlock() {
		return block;
	}

	public void setBlock(boolean block) {
		this.block = block;
	}

	public Long getProfileId() {
		return profileId;
	}

	public void setProfileId(Long profileId) {
		this.profileId = profileId;
	}

}
