package br.com.eventssaas.apprest.TO;

import java.util.Date;
import java.util.List;

import br.com.eventssaas.generalbusiness.enums.LectureDetailType;

public class MiniLectureTO {

	private Long id;

	private String title;

	private Long startTime;

	private Long endTime;

	private boolean detailPage;

	private List<String> speakers;

	private List<String> tags;

	private String room;

	private LectureDetailType type = LectureDetailType.LECTURE;

	private String scheduleBackgroundColor;

	private Integer priority;

	public String getRoom() {
		return room;
	}

	public void setRoom(String room) {
		this.room = room;
	}

	public void setStartTime(Long startTime) {
		this.startTime = startTime;
	}

	private int childCount;

	private Long dateToLong(Date d) {
		if (d != null) {
			return d.getTime();

		}
		return null;
	}

	public String getTitle() {
		return this.title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Long getStartTime() {
		return this.startTime;
	}

	public void setStartTime(Date startTime) {
		this.startTime = this.dateToLong(startTime);
	}

	public Long getEndTime() {
		return this.endTime;
	}

	public void setEndTime(Date endTime) {
		this.endTime = this.dateToLong(endTime);
	}

	public List<String> getSpeakers() {
		return this.speakers;
	}

	public void setSpeakers(List<String> speakers) {
		this.speakers = speakers;
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public List<String> getTags() {
		return this.tags;
	}

	public void setTags(List<String> tags) {
		this.tags = tags;
	}

	public int getChildCount() {
		return childCount;
	}

	public void setChildCount(int childCount) {
		this.childCount = childCount;
	}

	public boolean isDetailPage() {
		return detailPage;
	}

	public void setDetailPage(boolean detailPage) {
		this.detailPage = detailPage;
	}

	public String getScheduleBackgroundColor() {
		return scheduleBackgroundColor;
	}

	public void setScheduleBackgroundColor(String scheduleBackgroundColor) {
		this.scheduleBackgroundColor = scheduleBackgroundColor;
	}

	public Integer getPriority() {
		return priority;
	}

	public void setPriority(Integer priority) {
		this.priority = priority;
	}

	public void setEndTime(Long endTime) {
		this.endTime = endTime;
	}

	public LectureDetailType getType() {
		return type;
	}

	public void setType(LectureDetailType type) {
		this.type = type;
	}

}

class MiniChildLectureTO {
	private String id;

	public String getId() {
		return this.id;
	}

	public void setId(String id) {
		this.id = id;
	}
}
