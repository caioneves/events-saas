package br.com.eventssaas.apprest.TO;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class SurveyTO {

	private Long id;

	private Calendar creationDate;

	private String name;

	private boolean pushNotification;

	private Date pushNotificationDate;

	private Long eventId;

	private Long authorId;

	private List<SurveyQuestionTO> surveyQuestions;

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Calendar getCreationDate() {
		return this.creationDate;
	}

	public void setCreationDate(Calendar creationDate) {
		this.creationDate = creationDate;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public boolean isPushNotification() {
		return this.pushNotification;
	}

	public void setPushNotification(boolean pushNotification) {
		this.pushNotification = pushNotification;
	}

	public Date getPushNotificationDate() {
		return this.pushNotificationDate;
	}

	public void setPushNotificationDate(Date pushNotificationDate) {
		this.pushNotificationDate = pushNotificationDate;
	}

	public Long getEventId() {
		return this.eventId;
	}

	public void setEventId(Long eventId) {
		this.eventId = eventId;
	}

	public Long getAuthorId() {
		return this.authorId;
	}

	public void setAuthorId(Long profileId) {
		this.authorId = profileId;
	}

	public List<SurveyQuestionTO> getSurveyQuestions() {
		return this.surveyQuestions;
	}

	public void setSurveyQuestions(List<SurveyQuestionTO> surveyQuestions) {
		this.surveyQuestions = surveyQuestions;
	}

}
