package br.com.eventssaas.apprest.TO;

import java.util.Calendar;

public class SavedLectureTO {

	private Long id;

	private Calendar creationDate;

	private boolean alarm;

	private LectureTO lectureDetail;

	private MiniProfileTO profile;

	private EventTO event;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public boolean isAlarm() {
		return alarm;
	}

	public void setAlarm(boolean alarm) {
		this.alarm = alarm;
	}

	public LectureTO getLectureDetail() {
		return lectureDetail;
	}

	public void setLectureDetail(LectureTO lectureDetail) {
		this.lectureDetail = lectureDetail;
	}

	public MiniProfileTO getProfile() {
		return profile;
	}

	public void setProfile(MiniProfileTO profile) {
		this.profile = profile;
	}

	public EventTO getEvent() {
		return event;
	}

	public void setEvent(EventTO event) {
		this.event = event;
	}

	public Calendar getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Calendar creationDate) {
		this.creationDate = creationDate;
	}

}
