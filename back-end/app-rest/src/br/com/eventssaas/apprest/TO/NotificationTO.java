package br.com.eventssaas.apprest.TO;

import java.io.Serializable;

import br.com.eventssaas.generalbusiness.enums.NotificationType;

public class NotificationTO implements Serializable {
	/**
	 *
	 */
	private static final long serialVersionUID = 1L;

	private String title;

	private String body;

	private NotificationType notificationType;

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getBody() {
		return body;
	}

	public void setBody(String body) {
		this.body = body;
	}

	public NotificationType getNotificationType() {
		return notificationType;
	}

	public void setNotificationType(NotificationType notificationType) {
		this.notificationType = notificationType;
	}

}
