package br.com.eventssaas.apprest.TO;

public class ForgotPasswordRequestTO {
	private String email;

	public String getEmail() {
		if (this.email != null) {
			this.email = this.email.trim();
		}

		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

}
