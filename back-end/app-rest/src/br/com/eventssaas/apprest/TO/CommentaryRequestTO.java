package br.com.eventssaas.apprest.TO;

import java.io.Serializable;

public class CommentaryRequestTO implements Serializable {
	/**
	 *
	 */
	private static final long serialVersionUID = 1L;
	private String comment;

	public String getComment() {
		if (this.comment != null) {
			this.comment = this.comment.trim();
		}
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}
}
