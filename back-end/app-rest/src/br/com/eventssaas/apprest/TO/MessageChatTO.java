/**
 *
 */
package br.com.eventssaas.apprest.TO;

import java.io.Serializable;
import java.util.Calendar;

/**
 * @author Caio Neves
 *
 *         Transfer Object for MessageChat Entity
 */
public class MessageChatTO implements Serializable {

	/**
	 * Generated UID
	 */
	private static final long serialVersionUID = 2070759098144343514L;

	private Long id;

	private Calendar creationDate;

	private boolean read;
	private Calendar readDate;
	private boolean deletedByAuthor = false;
	private Calendar deletedByAuthorDate;
	private boolean deletedByTarget = false;
	private Calendar deletedByTargetProfileDate;
	private String text;
	private Long image_id;
	private String imageUrl;
	private MiniProfileTO author;
	private MiniProfileTO target;

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public long getCreationDate() {
		return this.creationDate.getTimeInMillis();
	}

	public void setCreationDate(Calendar creationDate) {
		this.creationDate = creationDate;
	}

	public boolean isRead() {
		return this.read;
	}

	public void setRead(boolean read) {
		this.read = read;
	}

	public String getText() {
		return this.text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public Long getImage() {
		return this.image_id;
	}

	public void setImage(Long image) {
		this.image_id = image;
	}

	public String getImageUrl() {
		return this.imageUrl;
	}

	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}

	public MiniProfileTO getAuthor() {
		return this.author;
	}

	public void setAuthor(MiniProfileTO authos) {
		this.author = authos;
	}

	public MiniProfileTO getTarget() {
		return this.target;
	}

	public void setTarget(MiniProfileTO target) {
		this.target = target;
	}

	public Calendar getReadDate() {
		return readDate;
	}

	public void setReadDate(Calendar readDate) {
		this.readDate = readDate;
	}

	public boolean isDeletedByAuthor() {
		return deletedByAuthor;
	}

	public void setDeletedByAuthor(boolean deletedByAuthor) {
		this.deletedByAuthor = deletedByAuthor;
	}

	public Calendar getDeletedByAuthorDate() {
		return deletedByAuthorDate;
	}

	public void setDeletedByAuthorDate(Calendar deletedByAuthorDate) {
		this.deletedByAuthorDate = deletedByAuthorDate;
	}

	public boolean isDeletedByTarget() {
		return deletedByTarget;
	}

	public void setDeletedByTarget(boolean deletedByTarget) {
		this.deletedByTarget = deletedByTarget;
	}

	public Calendar getDeletedByTargetProfileDate() {
		return deletedByTargetProfileDate;
	}

	public void setDeletedByTargetProfileDate(Calendar deletedByTargetProfileDate) {
		this.deletedByTargetProfileDate = deletedByTargetProfileDate;
	}

	public Long getImage_id() {
		return image_id;
	}

	public void setImage_id(Long image_id) {
		this.image_id = image_id;
	}

}
