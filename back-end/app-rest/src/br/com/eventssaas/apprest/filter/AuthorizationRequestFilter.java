package br.com.eventssaas.apprest.filter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import br.com.eventssaas.generalbusiness.entity.RedisProfileRole;
import br.com.eventssaas.generalbusiness.entity.RedisToken;
import br.com.eventssaas.generalbusiness.services.ServiceToken;
import br.com.eventssaas.generalbusiness.util.ProfileUtil;

@Component(value = "authorizationRequestFilter")
public class AuthorizationRequestFilter implements Filter {

	@Autowired
	private ProfileUtil profileUtil;

	@Autowired
	private ServiceToken serviceToken;

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {

		HttpServletRequest req = (HttpServletRequest) request;
		StringBuffer path = req.getRequestURL();
		if (path.toString().contains("/login") || path.toString().contains("/fill")
				|| path.toString().contains("/register-site") || path.toString().contains("/qrcodemissing")
				|| path.toString().contains("/forgotpassword")
				|| req.getMethod().equalsIgnoreCase(HttpMethod.OPTIONS.name()) || path.toString().contains("/download/")
				|| (path.toString().contains("service/image")
						&& req.getMethod().equalsIgnoreCase(HttpMethod.GET.name()))) {

			chain.doFilter(request, response);
		} else {
			String token = (((HttpServletRequest) request).getHeader("token"));
			RedisToken redis = this.serviceToken.getRedisToken(token);
			if (token == null || redis == null) {
				HttpServletResponse res = (HttpServletResponse) response;
				res.sendError(HttpServletResponse.SC_UNAUTHORIZED, "Unauthorized");
			} else {

				if (getEventId(path.toString()) == redis.getEventId()) {
					Authentication auth = new UsernamePasswordAuthenticationToken(redis, null,
							getAuthorities(redis.getProfileRoles()));

					SecurityContextHolder.getContext().setAuthentication(auth);
					this.profileUtil.setUserLoggedInId(redis.getProfileId());
					this.profileUtil.setUserLoggedEvent(redis.getEventId());

					serviceToken.updateLastModifiedFromToken(redis, new Date());

					chain.doFilter(request, response);

				} else {
					HttpServletResponse res = (HttpServletResponse) response;
					res.sendError(HttpServletResponse.SC_UNAUTHORIZED, "Unauthorized");
				}
			}
		}

	}

	public Collection<GrantedAuthority> getAuthorities(List<RedisProfileRole> profileRoles) {
		// make everyone ROLE_USER
		Collection<GrantedAuthority> grantedAuthorities = new ArrayList<>();
		for (RedisProfileRole role : profileRoles) {
			GrantedAuthority grantedAuthority = new GrantedAuthority() {
				/**
				 *
				 */
				private static final long serialVersionUID = 1L;

				// anonymous inner type
				@Override
				public String getAuthority() {
					return role.getType().getRole();
				}
			};
			grantedAuthorities.add(grantedAuthority);
		}

		return grantedAuthorities;
	}

	private Long getEventId(String path) {

		Long result = null;

		String[] splitPath = path.toString().split("/");

		try {
			result = Long.parseLong(splitPath[6]);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return result;
	}

	@Override
	public void init(FilterConfig arg0) throws ServletException {
	}

	@Override
	public void destroy() {
	}

}
