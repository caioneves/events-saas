package br.com.eventssaas.apprest.controller;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.com.eventssaas.generalbusiness.entity.Article;
import br.com.eventssaas.generalbusiness.entity.ContactInfo;
import br.com.eventssaas.generalbusiness.entity.Event;
import br.com.eventssaas.generalbusiness.entity.EventInformation;
import br.com.eventssaas.generalbusiness.entity.LectureDetail;
import br.com.eventssaas.generalbusiness.entity.Profile;
import br.com.eventssaas.generalbusiness.entity.ProfileRole;
import br.com.eventssaas.generalbusiness.entity.User;
import br.com.eventssaas.generalbusiness.enums.ArticleType;
import br.com.eventssaas.generalbusiness.enums.DocumentType;
import br.com.eventssaas.generalbusiness.enums.GenderType;
import br.com.eventssaas.generalbusiness.enums.ProfileRoleType;
import br.com.eventssaas.generalbusiness.repository.ArticleRepository;
import br.com.eventssaas.generalbusiness.repository.ContactInfoRepository;
import br.com.eventssaas.generalbusiness.repository.EventInformationRepository;
import br.com.eventssaas.generalbusiness.repository.EventRepository;
import br.com.eventssaas.generalbusiness.repository.LectureDetailRepository;
import br.com.eventssaas.generalbusiness.repository.ProfileRepository;
import br.com.eventssaas.generalbusiness.repository.ProfileRoleRepository;
import br.com.eventssaas.generalbusiness.repository.UserRepository;
import br.com.eventssaas.generalbusiness.util.ProfileUtil;

@RestController
@RequestMapping(value = "/test")
public class TestController {

	@Autowired
	private ArticleRepository articleRepository;

	@Autowired
	private EventRepository eventRepository;

	@Autowired
	private EventInformationRepository eventInformationRepository;

	@Autowired
	private ContactInfoRepository contactInfoRepository;

	@Autowired
	private ProfileRepository profileRepository;

	@Autowired
	private ProfileUtil profileUtil;

	// @Autowired
	// private PasswordEncoder passwordEncoder;

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private PasswordEncoder passwordEncoder;

	@Autowired
	private LectureDetailRepository lectureDetailRepository;

	@Autowired
	private ProfileRoleRepository profileRoleRepository;

	// @RequestMapping(method = RequestMethod.GET, value = "/{eventId}")
	public String welcome(@PathVariable() String eventId) {
		return "Bem vindo" + eventId;
	}

	@Transactional
	@RequestMapping(method = RequestMethod.GET, value = "/fill2")
	public String fillData() {

		Event event = new Event();
		event.setName("Evento de Teste");
		event.setDescrition(
				"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum varius commodo nisl. Vestibulum dapibus diam sapien, eget convallis mi accumsan vitae. Sed quis rhoncus lectus. Sed condimentum est ut ipsum condimentum sodales.");
		// event information
		EventInformation eventInformation = new EventInformation();
		event.setEventInformation(eventInformation);
		eventInformation.setImage(null);
		eventInformation.setStartEvent(new Date());
		eventInformation.setEndEvent(new Date());
		eventInformation.setSiteUrl("http://siteevento.com.br");
		eventInformation.setPlaceName("Centro de Eventos Sao Paulo");
		// eventInformation.setGoogleMapsPlace("google maps latitude");
		eventInformation.setEvent(event);

		// Contact info, para evento
		ContactInfo contactInfo = new ContactInfo();
		eventInformation.setContactInfo(contactInfo);

		contactInfo.setEmail("email@teste.com.br");
		contactInfo.setPhone1("11-5623xxxx");

		contactInfo.setAddress1("Rodovia Imigrantes");
		contactInfo.setAddress2("Saida 3");
		contactInfo.setNumber("1111");
		contactInfo.setCounty("Jabaquara");
		contactInfo.setCity("São Paulo");
		contactInfo.setState("SP");
		contactInfo.setCountry("Brasil");
		contactInfo.setCEP("00000000");

		try {
			this.contactInfoRepository.save(contactInfo);
			this.eventInformationRepository.save(eventInformation);
			this.eventRepository.save(event);
		} catch (Exception ex) {

		}

		User user = new User();
		user.setEmail("rondinelli132@gmail.com");
		user.setPassword(passwordEncoder.encode("123123"));
		user.setPasswordConfirmation(user.getPassword());
		// user.setPassword("123123");
		user.setEnabled(true);

		user = userRepository.save(user);

		Profile profile = new Profile();

		profile.setBirthDate(Calendar.getInstance());
		profile.setDocumentNumber("111.111.111-11");
		profile.setUser(user);
		// profile.setLogin("profile");
		profile.setFirstName("profile joao");
		// profile.setPassword("profile");
		// profile.setPictureUrl("img/user2.png");

		ProfileRole profileRole1 = new ProfileRole();
		profileRole1.setProfile(profile);
		profileRole1.setType(ProfileRoleType.SPEAKER);
		profileRole1.setEvent(event);
		profileRoleRepository.save(profileRole1);

		ProfileRole profileRole2 = new ProfileRole();
		profileRole2.setProfile(profile);
		profileRole2.setType(ProfileRoleType.SPEAKER);
		profileRole2.setEvent(event);
		profileRoleRepository.save(profileRole2);

		profile.setListProfileRoles(new ArrayList<>());
		profile.getListProfileRoles().add(profileRole1);
		profile = profileRepository.save(profile);

		Article article = new Article();
		article.setType(ArticleType.ADMIN_ARTICLE);
		article.setTitle("Artigo 1");
		article.setDescription(
				"Lero 1 Lero 1 Lero 1 Lero 1 Lero 1 Lero 1 Lero 1 Lero 1 Lero 1 Lero 1 Lero 1 Lero 1 Lero 1 Lero 1 Lero 1");
		article.setDetaillUrl("htt://google.com.br");
		article.setSource("facebook");
		article.setEvent(event);
		article.setAuthor(profile);
		articleRepository.save(article);

		article = new Article();
		article.setType(ArticleType.APP_POST);
		article.setTitle("Artigo 2");
		article.setDescription(
				"Lero 2 Lero 2 Lero 2 Lero 2 Lero 2 Lero 2 Lero 2 Lero 2 Lero 2 Lero 2 Lero 2 Lero 2 Lero 2 Lero 2 Lero 2");
		article.setDetaillUrl("htt://google.com.br");
		article.setSource("facebook");
		article.setEvent(event);
		article.setAuthor(profile);
		articleRepository.save(article);

		return "Bem vindo";
	}

	@Transactional
	@RequestMapping(method = RequestMethod.GET, value = "/fillSpeakerProfile")
	public Profile fillDataSpeakerProfile() {
		// Contact info, para evento
		ContactInfo contactInfo = new ContactInfo();

		contactInfo.setEmail("email@teste.com.br");
		contactInfo.setPhone1("11-5623xxxx");

		contactInfo.setAddress1("Rodovia Imigrantes");
		contactInfo.setAddress2("Saida 3");
		contactInfo.setNumber("1111");
		contactInfo.setCounty("Jabaquara");
		contactInfo.setCity("São Paulo");
		contactInfo.setState("SP");
		contactInfo.setCountry("Brasil");
		contactInfo.setCEP("00000000");
		contactInfoRepository.save(contactInfo);

		// User user = new User();
		// user.setEmail("test@speaker.com.br");
		//
		// String password = "123123";
		// // TODO : inserir encoding the password
		// // user.setPassword(passwordEncoder.encode(password));
		// user.setEnabled(true);
		// user.setPassword(password);
		// user.setPasswordConfirmation(password);
		// userRepository.save(user);

		Profile p = new Profile();
		p.setCreationDate(Calendar.getInstance());
		p.setFirstName("Primeiro Nome");
		p.setLastName("Last Name");
		p.setGender(GenderType.MALE);
		p.setBirthDate(Calendar.getInstance());
		p.setDocumentNumber("111111111111");
		p.setDocumentType(DocumentType.CPF);
		p.setPictureUrl("");
		// p.setUser(user);

		p.setMinibio("Exemplo de profile speaker");
		p.setContactInfo(contactInfo);
		p.setJobRole("Trabalhador");
		p.setShowJobRole(true);
		p.setCompany("company");

		if (p.getListProfileRoles() == null) {
			p.setListProfileRoles(new ArrayList<ProfileRole>());
		}
		Event e = eventRepository.findOne(1l);

		ProfileRole profileRole = new ProfileRole();
		profileRole.setProfile(p);
		profileRole.setType(ProfileRoleType.SPEAKER);
		profileRole.setEvent(e);
		profileRoleRepository.save(profileRole);

		p.getListProfileRoles().add(profileRole);
		profileRepository.save(p);

		return p;

	}

	@Transactional
	@RequestMapping(method = RequestMethod.GET, value = "/fillData")
	public String fillDataLecture() {
		Event event = eventRepository.findOne(1l);
		LectureDetail lectureDetail = new LectureDetail();
		lectureDetail.setEndTime(new Date());
		lectureDetail.setStartTime(new Date());
		lectureDetail.setDescription("teste 1 teste 2 teste 3");
		lectureDetail.setEvent(event);
		lectureDetail.setTitle("title 1");
		lectureDetailRepository.save(lectureDetail);

		return "Ok";

	}

	@RequestMapping(method = RequestMethod.POST, value = "/setProfileLogged/{profile_id}")
	public ResponseEntity<?> setProfileLogged(@PathVariable("profile_id") Long profileId) {

		this.profileUtil.setUserLoggedInId(profileId);

		return new ResponseEntity<>(this.profileUtil.getProfileLoggedIn(), HttpStatus.OK);

	}

}
