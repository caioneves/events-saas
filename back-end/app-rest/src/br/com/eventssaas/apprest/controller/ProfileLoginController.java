// package br.com.eventssaas.apprest.controller;
//
// import org.springframework.beans.factory.annotation.Autowired;
// import org.springframework.http.HttpStatus;
// import org.springframework.http.ResponseEntity;
// import org.springframework.transaction.annotation.Transactional;
// import org.springframework.web.bind.annotation.PathVariable;
// import org.springframework.web.bind.annotation.RequestBody;
// import org.springframework.web.bind.annotation.RequestMapping;
// import org.springframework.web.bind.annotation.RequestMethod;
// import org.springframework.web.bind.annotation.RestController;
//
// import br.com.eventssaas.apprest.TO.ForgotPasswordRequestTO;
// import br.com.eventssaas.apprest.TO.LoginRequestTO;
// import br.com.eventssaas.generalbusiness.entity.Profile;
// import br.com.eventssaas.generalbusiness.repository.ProfileRepository;
//
// @RestController
// @RequestMapping(value = "/event/{event_id}/login")
// public class ProfileLoginController {
//
// @Autowired
// private ProfileRepository profileRepository;
//
// @RequestMapping(method = RequestMethod.POST, value = "")
// public ResponseEntity login(@PathVariable("event_id") Long eventId,
// @RequestBody LoginRequestTO loginTO) {
//
// try {
// // getting profile from
// Profile profile =
// this.profileRepository.findByUserEmailIgnoreCaseAndUserPassword(loginTO.getLogin(),
// loginTO.getPassword());
// // TODO fazer processo de autenticacao e criar objeto de sessao
//
// if (profile == null) {
// return new ResponseEntity<>("request.fail.event.profile.notfound",
// HttpStatus.NOT_FOUND);
// }
//
// return new ResponseEntity<>(profile, HttpStatus.OK);
//
// } catch (Exception e) {
// return new ResponseEntity<>(e.getMessage(),
// HttpStatus.INTERNAL_SERVER_ERROR);
// }
// }
//
// @Transactional
// @RequestMapping(method = RequestMethod.POST, value = "/forgotpassword")
// public ResponseEntity forgotPassword(@PathVariable("event_id") Long eventId,
// @RequestBody ForgotPasswordRequestTO forgetPasswordTO) {
//
// try {
// // getting profile
// Profile profile =
// this.profileRepository.findByUserEmailOrDocumentNumber(forgetPasswordTO.getLogin(),
// forgetPasswordTO.getCpf());
// // TODO fazer processo de esqueci minha senha
//
// if (profile == null) {
// return new ResponseEntity<>("request.fail.event.profile.notfound",
// HttpStatus.NOT_FOUND);
// }
//
// return new ResponseEntity<>(profile, HttpStatus.OK);
//
// } catch (Exception e) {
// return new ResponseEntity<>(e.getMessage(),
// HttpStatus.INTERNAL_SERVER_ERROR);
// }
// }
//
// }
