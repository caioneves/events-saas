package br.com.eventssaas.apprest.controller;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.com.eventssaas.apprest.TO.MessageChatTO;
import br.com.eventssaas.apprest.converter.MessageChatConverter;
import br.com.eventssaas.generalbusiness.entity.Event;
import br.com.eventssaas.generalbusiness.entity.Image;
import br.com.eventssaas.generalbusiness.entity.MessageChat;
import br.com.eventssaas.generalbusiness.entity.Profile;
import br.com.eventssaas.generalbusiness.exceptions.NotFoundException;
import br.com.eventssaas.generalbusiness.repository.EventRepository;
import br.com.eventssaas.generalbusiness.repository.ImageRepository;
import br.com.eventssaas.generalbusiness.repository.MessageRepository;
import br.com.eventssaas.generalbusiness.repository.ProfileRepository;
import br.com.eventssaas.generalbusiness.util.ProfileUtil;

@RestController
@RequestMapping(value = "/event/{event_id}/message")
public class MessageController {

	@Autowired
	private ProfileUtil profileUtil;

	@Autowired
	private MessageChatConverter messageConverter;

	@Autowired
	private EventRepository eventRepository;

	@Autowired
	private ProfileRepository profileRepository;

	@Autowired
	private MessageRepository messageRepository;

	@Autowired
	private ImageRepository imageRepository;

	@Transactional
	@RequestMapping(method = RequestMethod.POST, value = "/{profile_id}")
	public ResponseEntity<Object> sendMessagesByProfileLoggeInToProfileId(@PathVariable("event_id") Long eventId,
			@PathVariable("profile_id") Long profileId, @RequestBody MessageChatTO mChat) {
		try {

			this.eventValidation(eventId);
			this.profileValidation(profileId);

			Profile userLoggedIn = this.profileUtil.getProfileLoggedIn();

			Profile profileTo = this.profileRepository.findOne(profileId);
			Event event = this.eventRepository.findOne(eventId);

			MessageChat messageChat = new MessageChat();
			messageChat.setAuthor(userLoggedIn);
			messageChat.setEvent(event);
			messageChat.setTargetProfile(profileTo);
			messageChat.setText(mChat.getText());
			// messageChat.setTextUrl(textUrl);
			if (mChat.getImage() != null && this.imageRepository.exists(mChat.getImage())) {
				Image img = this.imageRepository.findOne(mChat.getImage());
				messageChat.setImage(img);
			}

			this.messageRepository.save(messageChat);

			Map<String, String> result = new HashMap<>();
			result.put("message", "Message saved");

			return new ResponseEntity<>(result, HttpStatus.OK);

		} catch (NotFoundException e) {
			e.printStackTrace();
			return new ResponseEntity<>(e.getMessage(), HttpStatus.NOT_FOUND);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@Transactional
	@RequestMapping(method = RequestMethod.GET, value = "/{profile_id}")
	public ResponseEntity<Object> getMessagesByProfileLoggeInToProfileId(@PathVariable("event_id") Long eventId,
			@PathVariable("profile_id") Long profileId) {
		try {

			this.eventValidation(eventId);
			this.profileValidation(profileId);

			Profile userLoggedIn = this.profileUtil.getProfileLoggedIn();

			ArrayList<MessageChat> messages = (ArrayList<MessageChat>) this.messageRepository
					.getMessagesByProfileLoggeInAndProfileId(eventId, userLoggedIn.getId(), profileId);

			messages.forEach(item -> {
				// Only the messages to the user loggedIn will be setted to
				// read.
				if (item.getTargetProfile().getId() == userLoggedIn.getId()) {
					item.setRead(true);
					item.setReadDate(Calendar.getInstance());
				}
			});

			this.messageRepository.save(messages);

			List<MessageChatTO> mChatTOs = messages.stream()
					.map(this.messageConverter.convertMessageChatToMessageChatTO)
					.collect(Collectors.<MessageChatTO>toList());

			return new ResponseEntity<>(mChatTOs, HttpStatus.OK);

		} catch (NotFoundException e) {
			e.printStackTrace();
			return new ResponseEntity<>(e.getMessage(), HttpStatus.NOT_FOUND);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@Transactional
	@RequestMapping(method = RequestMethod.DELETE, value = "/chat/{profile_id}")
	public ResponseEntity<Object> deleteChat(@PathVariable("event_id") Long eventId,
			@PathVariable("profile_id") Long profileId) {
		try {

			this.eventValidation(eventId);
			this.profileValidation(profileId);

			Profile userLoggedIn = this.profileUtil.getProfileLoggedIn();

			// TODO : criar feature para marcar como deletados

			ArrayList<MessageChat> messages = (ArrayList<MessageChat>) this.messageRepository
					.getMessagesByProfileLoggeInAndProfileId(eventId, userLoggedIn.getId(), profileId);

			for (MessageChat messageChat : messages) {
				if (messageChat.getAuthor().getId() == userLoggedIn.getId()) {
					messageChat.setDeletedByAuthor(true);
					messageChat.setDeletedByAuthorDate(new GregorianCalendar());

				} else {
					messageChat.setDeletedByTarget(true);
					messageChat.setDeletedByTargetProfileDate(new GregorianCalendar());
				}
				this.messageRepository.save(messageChat);
			}

			Map<String, String> result = new HashMap<>();
			result.put("message", "Chat deleted by logged user");

			return new ResponseEntity<>(result, HttpStatus.OK);

		} catch (NotFoundException e) {
			e.printStackTrace();
			return new ResponseEntity<>(e.getMessage(), HttpStatus.NOT_FOUND);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@RequestMapping(method = RequestMethod.GET, value = "/countpending")
	public ResponseEntity<Object> getMessagesPendingQuantity(@PathVariable("event_id") Long eventId) {
		try {
			this.eventValidation(eventId);

			Profile userLoggedIn = this.profileUtil.getProfileLoggedIn();

			// TODO : apenas os nao deletados
			Long unreadMessages = this.messageRepository.getMessagesPendingQuantity(eventId, userLoggedIn.getId());

			Map<String, Long> result = new HashMap<>();
			result.put("unreadMessages", unreadMessages);

			return new ResponseEntity<>(result, HttpStatus.OK);

		} catch (NotFoundException e) {
			e.printStackTrace();
			return new ResponseEntity<>(e.getMessage(), HttpStatus.NOT_FOUND);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@RequestMapping(method = RequestMethod.GET, value = "/list")
	public ResponseEntity<Object> getMessages(@PathVariable("event_id") Long eventId) {
		try {
			this.eventValidation(eventId);

			Profile userLoggedIn = this.profileUtil.getProfileLoggedIn();

			ArrayList<MessageChat> messages = (ArrayList<MessageChat>) this.messageRepository.getMessages(eventId,
					userLoggedIn.getId());

			List<MessageChatTO> mChatTOs = messages.stream()
					.map(this.messageConverter.convertMessageChatToMessageChatTO)
					.collect(Collectors.<MessageChatTO>toList());

			return new ResponseEntity<>(mChatTOs, HttpStatus.OK);

		} catch (NotFoundException e) {
			e.printStackTrace();
			return new ResponseEntity<>(e.getMessage(), HttpStatus.NOT_FOUND);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@Transactional
	@RequestMapping(method = RequestMethod.DELETE, value = "/{message_id}")
	public ResponseEntity<Object> deleteMessage(@PathVariable("event_id") Long eventId,
			@PathVariable("message_id") Long messageId) {
		try {
			this.eventValidation(eventId);

			Profile userLoggedIn = this.profileUtil.getProfileLoggedIn();

			MessageChat message = this.messageRepository.findOne(messageId);

			if (message != null) {
				if (message.getAuthor().getId() == userLoggedIn.getId()) {
					message.setDeletedByAuthor(true);
					message.setDeletedByAuthorDate(Calendar.getInstance());
				} else {
					message.setDeletedByTarget(true);
					message.setDeletedByTargetProfileDate(Calendar.getInstance());
				}

				this.messageRepository.saveAndFlush(message);

				// Removido para nao remover mais a mensagem do repositorio
				// apenas marcar como deletada
				// this.messageRepository.delete(messageId);
			}

			Map<String, String> result = new HashMap<>();
			result.put("message", "message.deleted");

			return new ResponseEntity<>(result, HttpStatus.OK);

		} catch (NotFoundException e) {
			e.printStackTrace();
			return new ResponseEntity<>(e.getMessage(), HttpStatus.NOT_FOUND);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	private void profileValidation(Long profile) throws NotFoundException {
		if (profile == null) {
			throw new NotFoundException("request.fail.event.profile.notfound");
		} else if (!this.profileRepository.exists(profile)) {
			throw new NotFoundException("request.fail.event.profile.notfound");
		}
	}

	private void eventValidation(Long eventId) throws NotFoundException {
		if (eventId == null) {
			throw new NotFoundException("request.fail.event.notfound");
		} else if (!this.eventRepository.exists(eventId)) {
			throw new NotFoundException("request.fail.event.notfound");
		}
	}

}
