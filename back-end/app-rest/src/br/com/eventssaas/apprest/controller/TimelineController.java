package br.com.eventssaas.apprest.controller;

import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import br.com.eventssaas.apprest.TO.ArticleBlockTO;
import br.com.eventssaas.apprest.TO.PostTextRequestTO;
import br.com.eventssaas.generalbusiness.entity.Article;
import br.com.eventssaas.generalbusiness.entity.ArticleLike;
import br.com.eventssaas.generalbusiness.entity.Event;
import br.com.eventssaas.generalbusiness.entity.Image;
import br.com.eventssaas.generalbusiness.entity.Profile;
import br.com.eventssaas.generalbusiness.entity.ProfileRole;
import br.com.eventssaas.generalbusiness.enums.ArticleType;
import br.com.eventssaas.generalbusiness.repository.ArticleRepository;
import br.com.eventssaas.generalbusiness.repository.EventRepository;
import br.com.eventssaas.generalbusiness.repository.ImageRepository;
import br.com.eventssaas.generalbusiness.repository.ProfileRoleRepository;
import br.com.eventssaas.generalbusiness.services.BlackListService;
import br.com.eventssaas.generalbusiness.util.ProfileUtil;

@RestController
@RequestMapping(value = "/event/{event_id}/timeline")
public class TimelineController {

	@Autowired
	private ArticleRepository articleRepository;

	@Autowired
	private EventRepository eventRepository;

	@Autowired
	private ProfileUtil profileUtil;

	@Autowired
	private BlackListService blackListService;

	@Autowired
	private ProfileRoleRepository profileRoleRepository;

	@Autowired
	private ImageRepository imageRepository;

	// page starts in 0
	// size quantity to return
	@RequestMapping(method = RequestMethod.GET)
	// @Secured(value = "ROLE_USER")
	public List getTimelinePaginated(@PathVariable("event_id") Long eventId,
			@RequestParam(value = "page", required = true) int page,
			@RequestParam(value = "size", required = true) int size) {

		List<Article> timelineList = this.articleRepository
				.findByEventIdAndBlackPostAndBlockedAndBlockedByAuthorOrderByCreationDateDesc(eventId, false, false,
						false, new PageRequest(page, size));

		Profile userLoggedIn = this.profileUtil.getProfileLoggedIn();

		for (Article article : timelineList) {
			for (ArticleLike like : article.getListLikes()) {
				if (like.getProfile().equals(userLoggedIn)) {
					article.setLiked(true);
					break;
				}

			}
			article.setHasComment(userLoggedIn);
		}

		return timelineList;
	}

	@RequestMapping(method = RequestMethod.DELETE, value = "/post/{post_id}")
	public ResponseEntity<?> removePostToTimeline(@PathVariable("event_id") Long eventId,
			@PathVariable("post_id") Long postId) {
		if (!this.articleRepository.exists(postId)) {
			return new ResponseEntity<>("request.fail.post.notfound", HttpStatus.NOT_FOUND);
		}

		Article post = this.articleRepository.findOne(postId);
		post.setBlockedByAuthor(true);

		this.articleRepository.save(post);

		return new ResponseEntity<>(post, HttpStatus.OK);

	}

	@RequestMapping(method = RequestMethod.POST, value = "/post")
	public ResponseEntity<?> addPostToTimeline(@PathVariable("event_id") Long eventId,
			@RequestBody PostTextRequestTO postTextRequestTO) {

		try {

			Event event = this.eventRepository.findOne(eventId);

			if (event == null) {
				return new ResponseEntity<>("request.fail.event.notfound", HttpStatus.NOT_FOUND);
			}

			Profile userLoggedIn = this.profileUtil.getProfileLoggedIn();

			List<ProfileRole> listProfileRolesBlocked = this.profileRoleRepository
					.findByProfileIdAndEventIdAndBlocked(userLoggedIn.getId(), eventId, true);

			if (listProfileRolesBlocked != null && listProfileRolesBlocked.size() > 0) {
				return new ResponseEntity<>("request.fail.event.user.block", HttpStatus.UNAUTHORIZED);
			}

			Article article = new Article();
			article.setAuthor(userLoggedIn);
			article.setDescription(postTextRequestTO.getPostMessage());
			article.setPushNotification(false);
			article.setEvent(event);
			article.setSource("app_participant"); // Source
			article.setSponsor(null);
			article.setType(ArticleType.APP_POST);
			article.setTitle(postTextRequestTO.getTitle());
			if (postTextRequestTO.getImageId() != null && this.imageRepository.exists(postTextRequestTO.getImageId())) {
				Image img = this.imageRepository.findOne(postTextRequestTO.getImageId());
				article.setImage(img);
			}

			if (!postTextRequestTO.isTitleAndMessageNullOrEmpty()) {
				Set<String> blackwords = this.blackListService.isArticleHasBlackWord(article, eventId);
				article.setListBlackPostWords(blackwords);
				article.setBlackPost(blackwords != null && !blackwords.isEmpty());
			}

			this.articleRepository.save(article);

			return new ResponseEntity<>(article, HttpStatus.OK);

		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@RequestMapping(method = RequestMethod.POST, value = "/admin/post")
	public ResponseEntity<?> addPostAdminToTimeline(@PathVariable("event_id") Long eventId,
			@RequestBody PostTextRequestTO postTextRequestTO) {

		try {

			Event event = this.eventRepository.findOne(eventId);

			if (event == null) {
				return new ResponseEntity<>("request.fail.event.notfound", HttpStatus.NOT_FOUND);
			}

			Profile userLoggedIn = this.profileUtil.getProfileLoggedIn();

			// TODO Ajustar adicionar post pois formato esta incorreto, criar um
			// service para ser utilizando tanto pelo admin quando pela timeline
			List<ProfileRole> profileRoles = this.profileRoleRepository.findByProfileIdAndEventId(userLoggedIn.getId(),
					eventId);

			if (profileRoles == null) {
				return new ResponseEntity<>("request.fail.profile.role.notfound", HttpStatus.NOT_FOUND);
			}

			for (ProfileRole role : profileRoles) {
				if (role.getType().isAttendeeProfile() && role.isBlocked()) {
					return new ResponseEntity<>("request.fail.event.user.block", HttpStatus.UNAUTHORIZED);
				}
			}

			Article article = new Article();
			article.setAuthor(userLoggedIn);
			article.setDescription(postTextRequestTO.getPostMessage());
			article.setPushNotification(false);
			article.setEvent(event);
			article.setSource("app_participant"); // Source
			article.setSponsor(null);
			article.setType(postTextRequestTO.getArticleType());
			article.setTitle(postTextRequestTO.getTitle());
			article.setBlocked(false);

			if (!postTextRequestTO.isTitleAndMessageNullOrEmpty()) {
				Set<String> blackwords = this.blackListService.isArticleHasBlackWord(article, eventId);
				article.setListBlackPostWords(blackwords);
				article.setBlackPost(blackwords != null && !blackwords.isEmpty());
			}

			this.articleRepository.save(article);

			return new ResponseEntity<>(article, HttpStatus.OK);

		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@RequestMapping(method = RequestMethod.GET, value = "/admin")
	public List getTimelinePaginatedAdmin(@PathVariable("event_id") Long eventId,
			@RequestParam(value = "page", required = true) int page,
			@RequestParam(value = "size", required = true) int size) {

		List<Article> timelineList = this.articleRepository.findByEventId(eventId, new PageRequest(page, size));

		Profile userLoggedIn = this.profileUtil.getProfileLoggedIn();

		return timelineList;
	}

	@RequestMapping(value = "/update/{id}", method = RequestMethod.PUT)
	public ResponseEntity<Article> updateArticle(@PathVariable("id") long id,
			@RequestBody PostTextRequestTO postTextRequestTO) {
		System.out.println("Updating User " + id);
		Article currentArticle = articleRepository.findOne(id);
		//
		if (currentArticle == null) {
			return new ResponseEntity<Article>(HttpStatus.NOT_FOUND);
		}

		currentArticle.setDescription(postTextRequestTO.getPostMessage());
		currentArticle.setType(postTextRequestTO.getArticleType());
		currentArticle.setTitle(postTextRequestTO.getTitle());

		if (postTextRequestTO.getImageId() != null && this.imageRepository.exists(postTextRequestTO.getImageId())) {
			Image img = this.imageRepository.findOne(postTextRequestTO.getImageId());
			currentArticle.setImage(img);
		}

		this.articleRepository.save(currentArticle);

		return new ResponseEntity<Article>(currentArticle, HttpStatus.OK);
	}

	@RequestMapping(value = "/block/{id}", method = RequestMethod.PUT)
	public ResponseEntity<Article> updateUser(@PathVariable("id") long id, @RequestBody ArticleBlockTO articleBlockTO) {
		System.out.println("Updating User " + id);
		Article currentArticle = articleRepository.findOne(id);
		//
		if (currentArticle == null) {
			return new ResponseEntity<Article>(HttpStatus.NOT_FOUND);
		}
		currentArticle.setBlocked(articleBlockTO.isBlock());
		this.articleRepository.save(currentArticle);
		return new ResponseEntity<Article>(currentArticle, HttpStatus.OK);
	}
}
