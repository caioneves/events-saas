package br.com.eventssaas.apprest.controller;

import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.com.eventssaas.apprest.TO.ChangeEmailTO;
import br.com.eventssaas.apprest.TO.ChangePasswordTO;
import br.com.eventssaas.apprest.TO.ConnectionTO;
import br.com.eventssaas.apprest.TO.ContactTO;
import br.com.eventssaas.apprest.TO.MiniProfileTO;
import br.com.eventssaas.apprest.TO.ProfileBlockTO;
import br.com.eventssaas.apprest.TO.ProfileTO;
import br.com.eventssaas.apprest.TO.SavedLectureTO;
import br.com.eventssaas.apprest.converter.EventConverterUtil;
import br.com.eventssaas.apprest.converter.ProfileConverterUtil;
import br.com.eventssaas.generalbusiness.entity.Article;
import br.com.eventssaas.generalbusiness.entity.CheckIn;
import br.com.eventssaas.generalbusiness.entity.Contact;
import br.com.eventssaas.generalbusiness.entity.Event;
import br.com.eventssaas.generalbusiness.entity.Image;
import br.com.eventssaas.generalbusiness.entity.LectureDetail;
import br.com.eventssaas.generalbusiness.entity.LectureSavedAlarmProfile;
import br.com.eventssaas.generalbusiness.entity.Profile;
import br.com.eventssaas.generalbusiness.entity.ProfileRole;
import br.com.eventssaas.generalbusiness.entity.SocialProfile;
import br.com.eventssaas.generalbusiness.enums.CheckinType;
import br.com.eventssaas.generalbusiness.enums.ProfileRoleType;
import br.com.eventssaas.generalbusiness.exceptions.NotFoundException;
import br.com.eventssaas.generalbusiness.exceptions.PasswordAndPasswordConfirmationNotMatchException;
import br.com.eventssaas.generalbusiness.repository.CheckinRepository;
import br.com.eventssaas.generalbusiness.repository.ContactRepository;
import br.com.eventssaas.generalbusiness.repository.EventRepository;
import br.com.eventssaas.generalbusiness.repository.ImageRepository;
import br.com.eventssaas.generalbusiness.repository.LectureDetailRepository;
import br.com.eventssaas.generalbusiness.repository.LectureSavedAlarmProfileRepository;
import br.com.eventssaas.generalbusiness.repository.ProfileRepository;
import br.com.eventssaas.generalbusiness.repository.ProfileRoleRepository;
import br.com.eventssaas.generalbusiness.repository.SocialProfileRepository;
import br.com.eventssaas.generalbusiness.repository.UserRepository;
import br.com.eventssaas.generalbusiness.services.IMailService;
import br.com.eventssaas.generalbusiness.services.IProfileService;
import br.com.eventssaas.generalbusiness.services.IUserService;
import br.com.eventssaas.generalbusiness.util.ProfileUtil;
import br.com.eventssaas.generalbusiness.validation.EmailExistsException;

@RestController
@RequestMapping(value = "/event/{event_id}/profile")
public class ProfileController {

	@Autowired
	private ProfileRepository profileRepository;

	@Autowired
	private SocialProfileRepository socialProfileRepository;

	@Autowired
	private LectureSavedAlarmProfileRepository lectureSavedAlarmProfileRepository;

	@Autowired
	private ProfileConverterUtil profileConverterUtil;

	@Autowired
	private ProfileUtil profileUtil;

	@Autowired
	private CheckinRepository checkinRepository;

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private EventRepository eventRepository;

	@Autowired
	private ImageRepository imageRepository;

	@Autowired
	private ProfileRoleRepository profileRoleRepository;

	@Autowired
	private IMailService mailService;

	@Autowired
	private ContactRepository contactRepository;

	@Autowired
	private EventConverterUtil eventConverter;

	@Autowired
	private IProfileService profileService;

	@Autowired
	private IUserService userService;

	@Autowired
	private LectureDetailRepository lectureDetailRepository;

	@RequestMapping(method = RequestMethod.GET, value = "")
	public Profile getProfileLoggedIn(@PathVariable("event_id") Long eventId) {

		Profile userLoggedIn = this.profileUtil.getProfileLoggedIn();
		return userLoggedIn;
	}

	@RequestMapping(method = RequestMethod.GET, value = "/list")
	public List<MiniProfileTO> getMiniProfilesByEventId(@PathVariable("event_id") Long eventId) {

		List<Profile> profiles = this.profileRepository.findByListProfileRolesEventId(eventId);

		List<MiniProfileTO> mProfilesTOs = profiles.stream()
				.map(this.profileConverterUtil.convertProfileToMiniProfileTO)
				.collect(Collectors.<MiniProfileTO>toList());

		return mProfilesTOs;
	}

	@RequestMapping(method = RequestMethod.GET, value = "/attendee/{profile_id}")
	public Profile getProfileByProfileId(@PathVariable("event_id") Long eventId,
			@PathVariable("profile_id") Long profileId) {

		Profile profile = this.profileRepository.findByIdAndListProfileRolesEventId(profileId, eventId);
		boolean profileIsAttendeeAndHansthisEvent = false;
		for (ProfileRole role : profile.getListProfileRoles()) {
			if (role.getEvent().getId() == eventId && role.getType() == ProfileRoleType.ATTENDEE) {
				profileIsAttendeeAndHansthisEvent = true;
				break;
			}
		}

		if (!profileIsAttendeeAndHansthisEvent) {
			return null;
		}
		return profile;
	}

	@RequestMapping(method = RequestMethod.GET, value = "/speaker/{profile_id}")
	public Profile getProfileSpeakerByProfileId(@PathVariable("event_id") Long eventId,
			@PathVariable("profile_id") Long profileId) {

		Profile profile = this.profileRepository.findByIdAndListProfileRolesEventId(profileId, eventId);
		boolean profileIsSpeakerAndHansthisEvent = false;
		for (ProfileRole role : profile.getListProfileRoles()) {
			if (role.getEvent().getId() == eventId && role.getType() == ProfileRoleType.SPEAKER) {
				profileIsSpeakerAndHansthisEvent = true;
				break;
			}
		}

		if (!profileIsSpeakerAndHansthisEvent) {
			return null;
		}
		return profile;
	}

	@RequestMapping(method = RequestMethod.GET, value = "/miniprofile/{profile_id}")
	public MiniProfileTO getMiniProfileByProfileId(@PathVariable("event_id") Long eventId,
			@PathVariable("profile_id") Long profileId) {

		Profile profile = this.profileRepository.findOne(profileId);

		MiniProfileTO miniProfile = this.profileConverterUtil.convertProfileToMiniProfileTO.apply(profile);
		return miniProfile;
	}

	@RequestMapping(method = RequestMethod.GET, value = "/myactivities")
	public ResponseEntity<List<SavedLectureTO>> getMyActivitiesSaved(@PathVariable("event_id") Long eventId) {

		Profile userLoggedIn = this.profileUtil.getProfileLoggedIn();

		List<LectureSavedAlarmProfile> list = this.lectureSavedAlarmProfileRepository
				.findByProfileAndEventIdOrderByLectureDetailStartTimeDesc(userLoggedIn, eventId);

		List<SavedLectureTO> result = list.stream().map(eventConverter.convertLectureSavedAlarmProfile)
				.collect(Collectors.<SavedLectureTO>toList());
		return new ResponseEntity<List<SavedLectureTO>>(result, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/mynextactivities", produces = "application/json", consumes = "application/json")
	public List<LectureSavedAlarmProfile> getMyNextActivities(@PathVariable("event_id") Long eventId) {

		Profile userLoggedIn = this.profileUtil.getProfileLoggedIn();

		List<LectureSavedAlarmProfile> list = this.profileRepository
				.findLectureSavedAlarmProfileByAlarmAndEventIdAndProfileIdAndLectureDetailStartTimeGreatherThanNowOrderByLectureDetailStartTimeDesc(
						false, eventId, userLoggedIn.getId(), new Date());

		return list;
	}

	@RequestMapping(method = RequestMethod.GET, value = "/registered/list")
	public ResponseEntity<?> getRegisteredByEventId(@PathVariable("event_id") Long eventId) {
		Profile userLoggedIn = this.profileUtil.getProfileLoggedIn();

		// I belive this method is equals to /registered
		List<Profile> profiles = this.profileRepository
				.findDistinctByListProfileRolesEventIdAndNotSelfProfileLoggedInOrderByFirstName(eventId,
						userLoggedIn.getId());

		ResponseEntity<?> response = null;

		if (profiles != null && profiles.size() > 0) {
			List<MiniProfileTO> miniProfiles =

					profiles.stream().map(this.profileConverterUtil.convertProfileToMiniProfileTO)
							.collect(Collectors.<MiniProfileTO>toList());

			response = new ResponseEntity<>(miniProfiles, HttpStatus.OK);
		} else {
			response = new ResponseEntity<>("request.fail.event.profiles.registered.notfound", HttpStatus.NOT_FOUND);
		}
		return response;
	}

	@RequestMapping(method = RequestMethod.GET, value = "/checkedin/list")
	public ResponseEntity<?> getCheckedinByEventId(@PathVariable("event_id") Long eventId) {

		Profile userLoggedIn = this.profileUtil.getProfileLoggedIn();
		List<Profile> profiles = this.profileRepository.findProfilesCheckedInEventAndNotSelfProfileLoggedIn(eventId,
				userLoggedIn.getId());

		ResponseEntity<?> response = null;

		if (profiles != null && profiles.size() > 0) {
			List<MiniProfileTO> miniProfiles =

					profiles.stream().map(this.profileConverterUtil.convertProfileToMiniProfileTO)
							.collect(Collectors.<MiniProfileTO>toList());

			response = new ResponseEntity<>(miniProfiles, HttpStatus.OK);
		} else {
			Map<String, String> result = new HashMap<>();
			result.put("message", "request.fail.event.profiles.checkedin.notfound");

			response = new ResponseEntity<>(result, HttpStatus.NOT_FOUND);
		}
		return response;
	}

	@RequestMapping(method = RequestMethod.POST, value = "/checkins")
	public ResponseEntity<?> checkedinsBatchByEventId(@PathVariable("event_id") Long eventId,
			@RequestBody Long[] profileCheckinIds) {

		ResponseEntity<?> response = null;

		int chekingNum = 0;

		for (int i = 0; i < profileCheckinIds.length; i++) {
			try {
				Profile profile = this.profileRepository.findByIdAndListProfileRolesEventId(profileCheckinIds[i],
						eventId);
				if (profile == null) {
					throw new NotFoundException("request.fail.event.profile.notfound");
				}

				Event event = this.eventRepository.findOne(eventId);

				Profile agent = this.profileUtil.getProfileLoggedIn();

				CheckIn checkin = new CheckIn();
				checkin.setEvent(event);
				checkin.setProfileCheckedIn(profile);
				checkin.setProfileAgent(agent);
				checkin.setType(CheckinType.EVENT_CHEKIN);

				this.checkinRepository.save(checkin);

				chekingNum++;

			} catch (NotFoundException e) {
				response = new ResponseEntity<>(e.getMessage(), HttpStatus.NOT_FOUND);
			}
		}

		Map<String, Integer> result = new HashMap<>();
		result.put("chekingNum", chekingNum);

		response = new ResponseEntity<>(result, HttpStatus.OK);

		return response;
	}

	@RequestMapping(method = RequestMethod.POST, value = "/lecture/{lecture_id}/checkins")
	public ResponseEntity<?> checkedinsBatchByEventIdAndLecture(@PathVariable("event_id") Long eventId,
			@PathVariable("lecture_id") Long lectureId, @RequestBody Long[] profileCheckinIds) {

		ResponseEntity<?> response = null;

		int chekingNum = 0;

		for (int i = 0; i < profileCheckinIds.length; i++) {
			try {
				Profile profile = this.profileRepository.findByIdAndListProfileRolesEventId(profileCheckinIds[i],
						eventId);

				LectureDetail lecture = this.lectureDetailRepository.findByEventIdAndId(eventId, lectureId);

				if (profile == null) {
					throw new NotFoundException("request.fail.event.profile.notfound");
				}

				if (lecture == null) {
					throw new NotFoundException("request.fail.event.lecture.notfound");
				}

				Event event = this.eventRepository.findOne(eventId);

				// Verificar se o evento tem a config de palestra free ou nao

				Profile agent = this.profileUtil.getProfileLoggedIn();

				CheckIn checkin = new CheckIn();
				checkin.setEvent(event);
				checkin.setProfileCheckedIn(profile);
				checkin.setProfileAgent(agent);
				checkin.setType(CheckinType.LECTURE_CHECKIN);
				checkin.setLecture(lecture);

				this.checkinRepository.save(checkin);

				chekingNum++;

			} catch (NotFoundException e) {
				response = new ResponseEntity<>(e.getMessage(), HttpStatus.NOT_FOUND);
			}
		}

		Map<String, Integer> result = new HashMap<>();
		result.put("chekingNum", chekingNum);

		response = new ResponseEntity<>(result, HttpStatus.OK);

		return response;
	}

	@RequestMapping(method = RequestMethod.POST, value = "/checkin/{profile_checkin_id}")
	public ResponseEntity<?> checkedinByEventId(@PathVariable("event_id") Long eventId,
			@PathVariable("profile_checkin_id") Long profileCheckinId) {

		ResponseEntity<?> response = null;

		try {
			Profile profile = this.profileRepository.findByIdAndListProfileRolesEventId(profileCheckinId, eventId);
			if (profile == null) {
				throw new NotFoundException("request.fail.event.profile.notfound");
			}

			Event event = this.eventRepository.findOne(eventId);
			if (event == null) {
				throw new NotFoundException("request.fail.event.notfound");
			}

			Profile agent = this.profileUtil.getProfileLoggedIn();

			CheckIn checkin = new CheckIn();
			checkin.setEvent(event);
			checkin.setProfileCheckedIn(profile);
			checkin.setProfileAgent(agent);
			checkin.setType(CheckinType.EVENT_CHEKIN);

			this.checkinRepository.save(checkin);

			MiniProfileTO miniProfile = this.profileConverterUtil.convertProfileToMiniProfileTO.apply(profile);

			response = new ResponseEntity<>(miniProfile, HttpStatus.OK);
		} catch (NotFoundException e) {
			response = new ResponseEntity<>(e.getMessage(), HttpStatus.NOT_FOUND);
		}
		return response;
	}

	@RequestMapping(value = "/social/configure", method = RequestMethod.POST)
	public ResponseEntity<?> resetPassword(@RequestBody ConnectionTO connection) {

		try {

			Profile prof = profileUtil.getProfileLoggedIn();

			SocialProfile socialProfile = socialProfileRepository
					.findByProviderIdAndProviderUserId(connection.getProviderId(), connection.getProviderUserId());

			if (socialProfile == null) {

				socialProfile = new SocialProfile();
				socialProfile.setEnabled(connection.isEnabled());
				socialProfile.setProviderId(connection.getProviderId());
				socialProfile.setProviderUserId(connection.getProviderUserId());
				socialProfile.setProfileUrl(connection.getProfileUrl());
				socialProfile.setImageUrl(socialProfile.getImageUrl());
				socialProfile.setProfile(prof);

			} else {
				socialProfile.setEnabled(connection.isEnabled());
			}

			socialProfileRepository.save(socialProfile);

			return new ResponseEntity<>(HttpStatus.OK);

		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}

	}

	@RequestMapping(method = RequestMethod.POST, value = "/changeEmail")
	public ResponseEntity<?> changeEmail(HttpServletRequest request, @RequestBody ChangeEmailTO changeEmailTo) {

		try {

			byte[] decodedBytes = Base64.decodeBase64(changeEmailTo.getPassword().getBytes());

			String pass = new String(decodedBytes, "UTF-8");
			BCryptPasswordEncoder cc = new BCryptPasswordEncoder();
			Profile prof = profileUtil.getProfileLoggedIn();

			if (!cc.matches(pass, prof.getUser().getPassword())) {
				return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
			}

			prof.getUser().setEmail(changeEmailTo.getEmail());
			prof.getUser().setPasswordConfirmation(prof.getUser().getPassword());
			userRepository.save(prof.getUser());

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);

		}

		return new ResponseEntity<>(HttpStatus.OK);

	}

	@RequestMapping(method = RequestMethod.POST, value = "/changePassword")
	public ResponseEntity<?> changePassword(@RequestBody ChangePasswordTO changePasswordTo) {

		try {

			byte[] decodedBytes = Base64.decodeBase64(changePasswordTo.getOldPassword().getBytes());

			String oldPass = new String(decodedBytes, "UTF-8");
			BCryptPasswordEncoder cc = new BCryptPasswordEncoder();
			Profile prof = profileUtil.getProfileLoggedIn();

			if (!cc.matches(oldPass, prof.getUser().getPassword())) {
				Map<String, String> result = new HashMap<>();
				result.put("message", "request.fail.event.profile.password.nomatch");
				return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
			}

			decodedBytes = Base64.decodeBase64(changePasswordTo.getNewPassword().getBytes());
			String newPass = new String(decodedBytes, "UTF-8");
			userService.changeUserPassword(prof.getUser(), newPass, newPass);

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);

		}

		return new ResponseEntity<>(HttpStatus.OK);

	}

	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public ResponseEntity<?> edit(@RequestBody ProfileTO profileTO) {

		try {

			Profile prof = profileUtil.getProfileLoggedIn();

			if (profileTO.getBirthDate() != null) {
				Calendar birth = Calendar.getInstance();
				birth.setTime(profileTO.getBirthDate());
				prof.setBirthDate(birth);
			}

			prof.setLastName(profileTO.getLastName());
			prof.setFirstName(profileTO.getFirstName());
			prof.setGender(profileTO.getGender());
			prof.setDocumentNumber(profileTO.getDocumentNumber());
			prof.setDocumentType(profileTO.getDocumentType());
			prof.setJobRole(profileTO.getJobRole());
			prof.setShowJobRole(profileTO.isShowJobRole());
			prof.setCompany(profileTO.getCompany());
			prof.setFacebookUrl(profileTO.getFacebookUrl());
			prof.setTwitterUrl(profileTO.getTwitterUrl());
			prof.setLinkedInUrl(profileTO.getLinkedInUrl());
			prof.setMinibio(profileTO.getMinibio());
			prof.setInstagramUrl(profileTO.getInstagramUrl());
			if (profileTO.getImageId() != null && this.imageRepository.exists(profileTO.getImageId())) {
				Image img = this.imageRepository.findOne(profileTO.getImageId());
				prof.setPicture(img);
			}

			if ((prof.getListSocialProfile() != null && !prof.getListSocialProfile().isEmpty())
					&& (!profileTO.isHasFacebookSocialProfileEnable() || !profileTO.isHasLinkedSocialProfileEnable())) {
				for (SocialProfile sp : prof.getListSocialProfile()) {
					if (!profileTO.isHasFacebookSocialProfileEnable() && sp.getProviderId().equals("facebook")
							&& sp.isEnabled()) {
						sp.setEnabled(false);
						socialProfileRepository.save(sp);
					}
					if (!profileTO.isHasLinkedSocialProfileEnable() && sp.getProviderId().equals("linkedin")
							&& sp.isEnabled()) {
						sp.setEnabled(false);
						socialProfileRepository.save(sp);
					}
				}
			}

			profileRepository.save(prof);

			return new ResponseEntity<>(HttpStatus.OK);

		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}

	}

	@RequestMapping(method = RequestMethod.GET, value = "/listSocialProfile")
	public List<SocialProfile> getSocialsProfiles(@PathVariable("event_id") Long eventId) {

		Profile userLoggedIn = this.profileUtil.getProfileLoggedIn();

		return userLoggedIn.getListSocialProfile();
	}

	@RequestMapping(value = "/blockProfile", method = RequestMethod.POST)
	public ResponseEntity<?> blockProfile(@PathVariable("event_id") Long eventId,
			@RequestBody ProfileBlockTO profileBlockTO) {

		try {

			List<ProfileRole> profileRoles = this.profileRoleRepository
					.findByProfileIdAndEventId(profileBlockTO.getProfileId(), eventId);

			if (profileRoles == null) {
				return new ResponseEntity<Article>(HttpStatus.NOT_FOUND);
			}

			for (ProfileRole role : profileRoles) {
				if (role.getType().isAttendeeProfile()) {

					role.setBlocked(profileBlockTO.isBlock());

					profileRoleRepository.save(role);
				}
			}

			return new ResponseEntity<>(HttpStatus.OK);

		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}

	}

	@RequestMapping(value = "/contact/{profile_id}", method = RequestMethod.POST)
	public ResponseEntity<?> blockProfile(@PathVariable("event_id") Long eventId,
			@PathVariable("profile_id") Long profileId, @RequestBody ContactTO contactTo) {

		try {

			Profile profile = this.profileRepository.findByIdAndListProfileRolesEventId(profileId, eventId);

			Profile userLoggedIn = this.profileUtil.getProfileLoggedIn();

			if (profile == null) {
				return new ResponseEntity<Contact>(HttpStatus.NOT_FOUND);
			}

			Contact contact = new Contact();
			contact.setBody(contactTo.getMessage());
			contact.setSubject(contactTo.getSubject());
			contact.setAuthor(userLoggedIn);
			contact.setSpeaker(profile);

			this.contactRepository.save(contact);

			this.mailService.sendEmailSpeaker(contact);

			return new ResponseEntity<>(HttpStatus.OK);

		} catch (Exception e) {
			Map<String, Object> parameters = new HashMap<>();
			parameters.put("error", e.getMessage());

			return new ResponseEntity<>(parameters, HttpStatus.INTERNAL_SERVER_ERROR);
		}

	}

	/*
	 *
	 * Classes para registro para auxiliar o admin e registro externo
	 *
	 */

	@RequestMapping(value = "/admin/create/attendee", method = RequestMethod.POST)
	public ResponseEntity<?> createUserAttendeeByAdmin(@RequestBody String profileJson,
			@PathVariable("event_id") Long eventId) {

		try {
			this.profileService.createUser(profileJson, ProfileRoleType.ATTENDEE, eventId, true);

			return new ResponseEntity<>(HttpStatus.OK);

		} catch (NotFoundException e) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		} catch (EmailExistsException e) {
			return ResponseEntity.badRequest().body(e.getMessage());
		} catch (PasswordAndPasswordConfirmationNotMatchException e) {
			return ResponseEntity.badRequest().body(e.getMessage());
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}

	}

	@RequestMapping(value = "/admin/create/speaker", method = RequestMethod.POST)
	public ResponseEntity<?> createUserSpeakerByAdmin(@RequestBody String profileJson,
			@PathVariable("event_id") Long eventId) {

		try {
			this.profileService.createUser(profileJson, ProfileRoleType.SPEAKER, eventId, true);

			return new ResponseEntity<>(HttpStatus.OK);

		} catch (NotFoundException e) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		} catch (EmailExistsException e) {
			return ResponseEntity.badRequest().body(e.getMessage());
		} catch (PasswordAndPasswordConfirmationNotMatchException e) {
			return ResponseEntity.badRequest().body(e.getMessage());
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}

	}

	@RequestMapping(value = "/admin/create/adminevent", method = RequestMethod.POST)
	public ResponseEntity<?> createUserByAdmin(@RequestBody String profileJson,
			@PathVariable("event_id") Long eventId) {

		try {
			this.profileService.createUser(profileJson, ProfileRoleType.ADMIN_EVENT, eventId, true);

			return new ResponseEntity<>(HttpStatus.OK);

		} catch (NotFoundException e) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		} catch (EmailExistsException e) {
			return ResponseEntity.badRequest().body(e.getMessage());
		} catch (PasswordAndPasswordConfirmationNotMatchException e) {
			return ResponseEntity.badRequest().body(e.getMessage());
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}

	}

	@RequestMapping(value = "/register-site", method = RequestMethod.POST)
	public ResponseEntity<?> createUserBySiteRegistration(@RequestBody String profileJson,
			@PathVariable("event_id") Long eventId) {

		try {
			this.profileService.createUser(profileJson, ProfileRoleType.ATTENDEE, eventId, true);

			return new ResponseEntity<>(HttpStatus.OK);

		} catch (NotFoundException e) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		} catch (EmailExistsException e) {
			return ResponseEntity.badRequest().body(e.getMessage());
		} catch (PasswordAndPasswordConfirmationNotMatchException e) {
			return ResponseEntity.badRequest().body(e.getMessage());
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@Transactional
	@RequestMapping(value = "/create/qrcodemissing", method = RequestMethod.POST)
	public ResponseEntity<?> ajustarQRCodeUsers(@PathVariable("event_id") Long eventId) {
		try {
			this.profileService.createQRCodeInProfileWhenQRCodeIsNull();

			return new ResponseEntity<>(HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}

}
