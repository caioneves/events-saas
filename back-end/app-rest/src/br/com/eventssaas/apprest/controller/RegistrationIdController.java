package br.com.eventssaas.apprest.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import br.com.eventssaas.generalbusiness.entity.Event;
import br.com.eventssaas.generalbusiness.entity.Profile;
import br.com.eventssaas.generalbusiness.entity.ProfileDeviceRegistrationId;
import br.com.eventssaas.generalbusiness.repository.EventRepository;
import br.com.eventssaas.generalbusiness.repository.ProfileDeviceRegistrationIdRepository;
import br.com.eventssaas.generalbusiness.util.ProfileUtil;

@Controller
@RequestMapping(value = "/gcm")
public class RegistrationIdController {

	private final String API_KEY = "AIzaSyDTT9akJGsMEwH1bqeXsgIPSdY5kvlyfcg";
	private final int NUMBER_OF_RETRIES = 2;
	// private final int INTERVAL_OF_NOTIFICATIONS = 10;
	private final int INTERVAL_OF_NOTIFICATIONS = 1;

	// Limite de registration Ids para os quais o gcm deve notificar por vez
	private final int MULTICAST_SIZE = 1000;

	@Autowired
	private ProfileDeviceRegistrationIdRepository profileDeviceRegistrationIdRepository;

	@Autowired
	private EventRepository eventRepository;

	@Autowired
	private ProfileUtil profileUtil;

	@RequestMapping(value = "/{event_id}/register/{regId}", method = RequestMethod.GET)
	private ResponseEntity<?> create(@PathVariable("regId") String regId, @PathVariable("event_id") Long eventId) {

		if (this.profileDeviceRegistrationIdRepository.findByRegistrationIdAndEventId(regId, eventId) == null) {
			Profile prof = profileUtil.getProfileLoggedIn();
			Event event = eventRepository.findOne(eventId);
			ProfileDeviceRegistrationId reg = new ProfileDeviceRegistrationId();
			reg.setRegistrationId(regId);
			reg.setProfile(prof);
			reg.setEvent(event);
			try {
				this.profileDeviceRegistrationIdRepository.save(reg);
				return new ResponseEntity<>(HttpStatus.OK);
			} catch (Exception e) {
				e.printStackTrace();
				return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
			}
		}
		return new ResponseEntity<>(HttpStatus.OK);

	}

}
