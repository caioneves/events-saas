package br.com.eventssaas.apprest.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import br.com.eventssaas.apprest.TO.EventInformationTO;
import br.com.eventssaas.apprest.TO.FileTO;
import br.com.eventssaas.apprest.TO.MiniLectureTO;
import br.com.eventssaas.apprest.converter.DownloadItemConverter;
import br.com.eventssaas.apprest.converter.EventConverterUtil;
import br.com.eventssaas.generalbusiness.entity.DownloadItem;
import br.com.eventssaas.generalbusiness.entity.Event;
import br.com.eventssaas.generalbusiness.entity.LectureDetail;
import br.com.eventssaas.generalbusiness.repository.DownloadItemRepository;
import br.com.eventssaas.generalbusiness.repository.EventRepository;
import br.com.eventssaas.generalbusiness.repository.LectureDetailRepository;

@RestController
@RequestMapping(value = "/event/{event_id}")
public class EventController {

	@Autowired
	private EventRepository eventRepository;

	@Autowired
	private LectureDetailRepository lectureRepository;

	@Autowired
	private EventConverterUtil eventConverter;

	@Autowired
	private DownloadItemConverter downloadItemConverter;

	@Autowired
	private DownloadItemRepository downloadItemRepository;

	/*
	 * Get event information by event Id
	 *
	 * Param: Id : id of Event
	 *
	 * Return: Event data
	 */

	@RequestMapping(method = RequestMethod.GET, value = "")
	public Event getEvent(@PathVariable("event_id") Long eventId) {

		Event event = this.eventRepository.findOne(eventId);
		return event;
	}

	@RequestMapping(method = RequestMethod.GET, value = "/information")
	public EventInformationTO getEventInformation(@PathVariable("event_id") Long eventId) {

		Event event = this.eventRepository.findOne(eventId);
		EventInformationTO evtInfTO = this.eventConverter.convertEventToEventInformationTO.apply(event);
		return evtInfTO;
	}

	@RequestMapping(method = RequestMethod.GET, value = "/schedule/date")
	public List<Date> getScheduleDates(@PathVariable("event_id") Long eventId) {

		List<Date> datesEvent = this.lectureRepository.findDistinctStartTimeByEventId(eventId);
		return datesEvent;
	}

	@RequestMapping(method = RequestMethod.GET, value = "/schedule/summary")
	public ResponseEntity<List<MiniLectureTO>> getScheduleSummary(@PathVariable("event_id") Long eventId) {

		List<LectureDetail> lectures = this.lectureRepository.findByEventIdAndNoParent(eventId);
		List<MiniLectureTO> result = lectures.stream().map(this.eventConverter.convertLectureToMiniLectureTO)
				.collect(Collectors.<MiniLectureTO>toList());
		return new ResponseEntity<>(result, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/schedule/summary/{id}")
	public ResponseEntity<List<MiniLectureTO>> getSchedule(@PathVariable("event_id") Long eventId,
			@PathVariable("id") Long id) {

		List<LectureDetail> lectures = this.lectureRepository.findByEventIdAndId(eventId, id).getListSubItems();
		List<MiniLectureTO> result = lectures.stream().map(this.eventConverter.convertLectureToMiniLectureTO)
				.collect(Collectors.<MiniLectureTO>toList());
		return new ResponseEntity<>(result, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/nextactivities")
	public List<MiniLectureTO> getNextActivities(@PathVariable("event_id") Long eventId,
			@RequestParam(value = "page", required = true) int page,
			@RequestParam(value = "size", required = true) int size) {

		List<LectureDetail> lectures = this.lectureRepository.findByEventIdAndStartTimeOrderedPagined(eventId,
				new Date(), new PageRequest(page, size));
		List<MiniLectureTO> result = lectures.stream().map(this.eventConverter.convertLectureToMiniLectureTO)
				.collect(Collectors.<MiniLectureTO>toList());

		return result;
	}

	@RequestMapping(method = RequestMethod.POST, value = "/file/{leiture_id}")
	public ResponseEntity<?> saveFile(@RequestBody FileTO fileTO, @PathVariable("leiture_id") Long lectureId) {

		DownloadItem downloadItem = this.downloadItemConverter.convertFileTOToDownloadItem.apply(fileTO);
		LectureDetail lectureDetail = this.lectureRepository.findOne(lectureId);
		if (lectureDetail == null) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}

		downloadItem.setLectureDetail(lectureDetail);
		if (lectureDetail.getListFilesDownloadItem() == null) {
			lectureDetail.setListFilesDownloadItem(new ArrayList<>());
		}
		lectureDetail.getListFilesDownloadItem().add(downloadItem);

		lectureDetail = this.lectureRepository.save(lectureDetail);

		return new ResponseEntity<>(lectureDetail.getId(), HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/file/{download_item_id}")
	public ResponseEntity<?> getFile(@PathVariable("download_item_id") Long downalodItemId) {

		if (!this.downloadItemRepository.exists(downalodItemId)) {
			Map<String, String> result = new HashMap<>();
			result.put("message", "request.fail.file.notfound");
			return new ResponseEntity<>(result, HttpStatus.NOT_FOUND);
		}

		DownloadItem downloadItem = this.downloadItemRepository.findOne(downalodItemId);
		String value[] = downloadItem.getContentType().split("/");
		MediaType mediaType = new MediaType(value[0], value[1]);

		return ResponseEntity.ok().contentType(mediaType).body(downloadItem.getFile());
	}

}
