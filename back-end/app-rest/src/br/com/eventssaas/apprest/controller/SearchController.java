package br.com.eventssaas.apprest.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.com.eventssaas.apprest.TO.SearchRequestTO;

@RestController
@RequestMapping(value = "/event/{event_id}/search")
public class SearchController {

	@RequestMapping(method = RequestMethod.POST, value = "")
	public ResponseEntity getSearchByTextAndEvent(@PathVariable("event_id") Long eventId,
			@RequestBody SearchRequestTO searchRequestTO) {
		Map<String, Object> returned = new HashMap<String, Object>();

		// TODO implementar regras de busca quando o mecanismo de busca
		// estiver ativo

		returned.put("message", "request.success.event.search");
		returned.put("users", new ArrayList<>());
		returned.put("article", new ArrayList<>());
		returned.put("lectures", new ArrayList<>());
		return new ResponseEntity<>(returned, HttpStatus.OK);
	}
}
