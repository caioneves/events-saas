package br.com.eventssaas.apprest.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.com.eventssaas.apprest.TO.CommentaryRequestTO;
import br.com.eventssaas.apprest.TO.MiniProfileTO;
import br.com.eventssaas.apprest.converter.ProfileConverterUtil;
import br.com.eventssaas.generalbusiness.entity.Article;
import br.com.eventssaas.generalbusiness.entity.ArticleLike;
import br.com.eventssaas.generalbusiness.entity.Commentary;
import br.com.eventssaas.generalbusiness.entity.Event;
import br.com.eventssaas.generalbusiness.entity.Profile;
import br.com.eventssaas.generalbusiness.exceptions.NotFoundException;
import br.com.eventssaas.generalbusiness.repository.ArticleLikeRepository;
import br.com.eventssaas.generalbusiness.repository.ArticleRepository;
import br.com.eventssaas.generalbusiness.repository.CommentaryRepository;
import br.com.eventssaas.generalbusiness.repository.EventRepository;
import br.com.eventssaas.generalbusiness.repository.ProfileRepository;
import br.com.eventssaas.generalbusiness.services.BlackListService;
import br.com.eventssaas.generalbusiness.util.ProfileUtil;

@RestController
@RequestMapping(value = "/event/{event_id}/article/{article_id}")
public class ArticleController {

	@Autowired
	private ArticleRepository articleRepository;

	@Autowired
	private ProfileRepository profileRepository;

	@Autowired
	private ArticleLikeRepository likeRepository;

	@Autowired
	private CommentaryRepository commentaryRepository;

	@Autowired
	private ProfileConverterUtil profileConverter;

	@Autowired
	private BlackListService blackListService;

	@Autowired
	private EventRepository eventRepository;

	@Autowired
	private ProfileUtil profileUtil;

	@RequestMapping(method = RequestMethod.GET, value = "")
	public Article getArticle(@PathVariable("event_id") Long eventId, @PathVariable("article_id") Long articleId)
			throws NotFoundException {

		eventValidation(eventId);

		Article article = this.articleRepository.findByIdAndEventId(articleId, eventId);

		Profile userLoggedIn = this.profileUtil.getProfileLoggedIn();

		for (ArticleLike like : article.getListLikes()) {
			if (like.getProfile().equals(userLoggedIn)) {
				article.setLiked(true);
				break;
			}

		}

		return article;
	}

	@Transactional
	@RequestMapping(method = RequestMethod.POST, value = "/like")
	public ResponseEntity<?> addLikeArticle(@PathVariable("event_id") Long eventId,
			@PathVariable("article_id") Long articleId) throws NotFoundException {
		eventValidation(eventId);

		try {
			Article article = this.articleRepository.findByIdAndEventId(articleId, eventId);

			if (article == null) {
				return new ResponseEntity<>("request.fail.event.article.notfound", HttpStatus.NOT_FOUND);
			}

			Profile userLoggedIn = this.profileUtil.getProfileLoggedIn();
			Event event = this.eventRepository.findOne(eventId);

			ArticleLike like = new ArticleLike();
			like.setArticle(article);
			like.setProfile(userLoggedIn);
			like.setEvent(event);

			this.likeRepository.save(like);

			return new ResponseEntity<>(like, HttpStatus.OK);

		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@RequestMapping(method = RequestMethod.GET, value = "/like")
	public ResponseEntity<?> getProfilesLikedArticle(@PathVariable("event_id") Long eventId,
			@PathVariable("article_id") Long articleId) throws NotFoundException {

		eventValidation(eventId);

		try {
			Article article = this.articleRepository.findByIdAndEventId(articleId, eventId);

			if (article == null) {
				return new ResponseEntity<>("request.fail.event.article.notfound", HttpStatus.NOT_FOUND);
			}

			ArrayList<MiniProfileTO> profiles = new ArrayList<>();

			for (ArticleLike like : article.getListLikes()) {

				profiles.add(this.profileConverter.convertProfileToMiniProfileTO.apply(like.getProfile()));
			}

			return new ResponseEntity<>(profiles, HttpStatus.OK);

		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@Transactional
	@RequestMapping(method = RequestMethod.DELETE, value = "/like")
	public ResponseEntity<?> removeLikeArticle(@PathVariable("event_id") Long eventId,
			@PathVariable("article_id") Long articleId) throws NotFoundException {

		eventValidation(eventId);

		try {
			Profile userLoggedIn = this.profileUtil.getProfileLoggedIn();

			ArticleLike like = this.likeRepository.findByArticleIdAndProfile(articleId, userLoggedIn);
			this.likeRepository.delete(like);

			if (like == null) {
				return new ResponseEntity<>("request.success.event.article.like.notfound", HttpStatus.OK);
			}

			return new ResponseEntity<>(HttpStatus.OK);

		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@Transactional
	@RequestMapping(method = RequestMethod.POST, value = "/commentary")
	public ResponseEntity<?> addCommentaryArticle(@PathVariable("event_id") Long eventId,
			@PathVariable("article_id") Long articleId, @RequestBody CommentaryRequestTO commentaryRequestTO) {

		if (commentaryRequestTO.getComment() != null && commentaryRequestTO.getComment().isEmpty()) {
			return new ResponseEntity<>("request.fail.comment.empty", HttpStatus.NOT_FOUND);
		}

		try {
			Article article = this.articleRepository.findByIdAndEventId(articleId, eventId);

			if (article == null) {
				return new ResponseEntity<>("request.fail.event.article.notfound", HttpStatus.NOT_FOUND);
			}

			Profile userLoggedIn = this.profileUtil.getProfileLoggedIn();

			Commentary commentary = new Commentary();
			commentary.setArticle(article);
			commentary.setAuthor(userLoggedIn);
			commentary.setEvent(article.getEvent());
			commentary.setComment(commentaryRequestTO.getComment());

			// Verify black words from event
			Set<String> blackwords = this.blackListService.isCommentHasBlackWord(commentary, eventId);
			commentary.setListBlackCommentWords(blackwords);
			commentary.setBlackComment(blackwords != null && !blackwords.isEmpty());

			this.commentaryRepository.save(commentary);

			return new ResponseEntity<>(commentary, HttpStatus.OK);

		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@RequestMapping(method = RequestMethod.GET, value = "/commentary")
	public ResponseEntity<?> getCommentaryArticle(@PathVariable("event_id") Long eventId,
			@PathVariable("article_id") Long articleId) {

		Article article = this.articleRepository.findByIdAndEventId(articleId, eventId);

		if (article == null || article.getId() == null || article.getId() == 0) {
			return new ResponseEntity<>("request.fail.event.notfound", HttpStatus.NOT_FOUND);
		}

		List<Commentary> comments = this.commentaryRepository
				.findByArticleIdAndBlackCommentOrderByCreationDateDesc(articleId, false);

		return new ResponseEntity<>(comments, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.DELETE, value = "/commentary/{comment_id}")
	public ResponseEntity<?> deleteCommentaryArticle(@PathVariable("event_id") Long eventId,
			@PathVariable("article_id") Long articleId, @PathVariable("comment_id") Long commentId) {

		Commentary comment = this.commentaryRepository.findOne(commentId);

		if (comment != null) {
			this.commentaryRepository.delete(commentId);
		} else {
			return new ResponseEntity<>("request.fail.commentary.notfound", HttpStatus.NOT_FOUND);
		}

		return new ResponseEntity<>("Commentary deleted", HttpStatus.OK);
	}

	private void profileValidation(Long profile) throws NotFoundException {
		if (profile == null) {
			throw new NotFoundException("request.fail.event.profile.notfound");
		} else if (!this.profileRepository.exists(profile)) {
			throw new NotFoundException("request.fail.event.profile.notfound");
		}
	}

	private void eventValidation(Long eventId) throws NotFoundException {
		if (eventId == null) {
			throw new NotFoundException("request.fail.event.notfound");
		} else if (!this.eventRepository.exists(eventId)) {
			throw new NotFoundException("request.fail.event.notfound");
		}
	}

}
