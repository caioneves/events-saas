package br.com.eventssaas.apprest.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.com.eventssaas.apprest.TO.SurveyAnswerTOInput;
import br.com.eventssaas.apprest.TO.SurveyQuestionTO;
import br.com.eventssaas.apprest.TO.SurveyTO;
import br.com.eventssaas.apprest.converter.SurveyConverter;
import br.com.eventssaas.apprest.converter.SurveyQuestionWrapper;
import br.com.eventssaas.apprest.converter.SurveyTOConverter;
import br.com.eventssaas.generalbusiness.TO.SurveyAnswerStatisticsTO;
import br.com.eventssaas.generalbusiness.entity.Event;
import br.com.eventssaas.generalbusiness.entity.Profile;
import br.com.eventssaas.generalbusiness.entity.Survey;
import br.com.eventssaas.generalbusiness.entity.SurveyQuestion;
import br.com.eventssaas.generalbusiness.exceptions.MissingArgumentException;
import br.com.eventssaas.generalbusiness.exceptions.NotFoundException;
import br.com.eventssaas.generalbusiness.repository.EventRepository;
import br.com.eventssaas.generalbusiness.repository.ProfileRepository;
import br.com.eventssaas.generalbusiness.repository.ProfileSurveyAnswerRepository;
import br.com.eventssaas.generalbusiness.repository.SurveyQuestionRepository;
import br.com.eventssaas.generalbusiness.repository.SurveyRepository;
import br.com.eventssaas.generalbusiness.services.SurveyService;
import br.com.eventssaas.generalbusiness.util.ProfileUtil;

@RestController
@RequestMapping(value = "/event/{event_id}/survey")
public class SurveyController {

	@Autowired
	SurveyService surveyService;

	@Autowired
	private ProfileUtil profileUtil;

	@Autowired
	private EventRepository eventRepository;

	@Autowired
	private ProfileRepository profileRepository;

	@Autowired
	private SurveyRepository surveyRepository;

	@Autowired
	private SurveyQuestionRepository surveyQuestionRepository;

	@Autowired
	private SurveyConverter converter;

	@Autowired
	private SurveyTOConverter surveyTOConverter;

	@Autowired
	private ProfileSurveyAnswerRepository profileSurveyAnswerRepository;

	@RequestMapping(method = RequestMethod.GET, value = "")
	public ResponseEntity<List<SurveyTO>> getSurveys(@PathVariable("event_id") Long eventId) {
		Profile user = this.profileUtil.getProfileLoggedIn();
		try {
			List<Survey> surveys = this.surveyService.getSurveyNotAnswered(user, eventId, 0, Integer.MAX_VALUE);

			List<SurveyTO> result = surveys.stream().map(this.converter.surveyToSurveyTO)
					.collect(Collectors.<SurveyTO>toList());

			return new ResponseEntity<>(result, HttpStatus.OK);
		} catch (NotFoundException e) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@RequestMapping(value = "/{survey_id}", method = RequestMethod.GET)
	public ResponseEntity<SurveyTO> getSurvey(@PathVariable("event_id") Long eventId,
			@PathVariable("survey_id") Long surveyId) {
		Survey survey = null;
		Profile user = this.profileUtil.getProfileLoggedIn();
		try {
			survey = this.surveyService.getSurveyFromEvent(eventId, surveyId, user);

			SurveyTO result = this.converter.surveyToSurveyTO.apply(survey);

			return new ResponseEntity<>(result, HttpStatus.OK);

		} catch (NotFoundException e) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	/**
	 *
	 * Survey : Buscar Enquete
	 *
	 * Serviço: /app/rest/event/{event_id}/survey/{survey_id} Metodo: POST
	 * Request: id do evento na url e id da enquete e objeto JSON com lista com
	 * o id da pergunta e id da resposta [ { “question” : “ID_QUESTION”,
	 * “answer” : “ID_ANSWER” }, … ] Response: OK.
	 *
	 * Obs.: Enviar os ids de perguntas e das respostas respondidas pelo
	 * usuário, gerar os objetos e . Fail: HTTP 500: Caso ocorra algum erro no
	 * servidor como query invalida ou conexão com banco de dados incorreto
	 *
	 * HTTP 404 Caso path incorreto Caso não encontre nenhum resultado retornar
	 * label “request.fail.event.survey.notfound”
	 *
	 * HTTP 200 <COLOCAR JSON>
	 *
	 * @return
	 */
	@RequestMapping(value = "/{survey_id}", method = RequestMethod.POST)
	public ResponseEntity<SurveyTO> registerAnswer(@PathVariable("event_id") Long eventId,
			@PathVariable("survey_id") Long surveyId, @RequestBody SurveyAnswerTOInput[] body) {

		Survey survey = null;
		Profile user = this.profileUtil.getProfileLoggedIn();
		try {

			if (body == null || body.length == 0) {
				return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
			}

			survey = this.surveyService.getSurveyFromEvent(eventId, surveyId, user);

			for (SurveyAnswerTOInput surveyAnswerTO : body) {
				this.surveyService.answerQuestion(surveyAnswerTO.getQuestion(), surveyAnswerTO.getAnswer(),
						surveyAnswerTO.getAnswerText(), eventId, user);
			}

			// SurveyTO result = this.converter.surveyToSurveyTO.apply(survey);

			return new ResponseEntity<>(HttpStatus.OK);

		} catch (NotFoundException e) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	// TOOD : mover metodo para o service
	@RequestMapping(value = "/create", method = RequestMethod.POST)
	public ResponseEntity<?> create(@RequestBody SurveyTO surveyTO, @PathVariable("event_id") Long eventId) {

		try {

			Event event = this.eventRepository.findOne(eventId);
			if (event == null) {
				return new ResponseEntity<>("Evento não encontrado!", HttpStatus.NOT_FOUND);
			}

			Profile prof = profileRepository.findOne(surveyTO.getAuthorId());

			if (prof == null) {
				return new ResponseEntity<>("Profile não encontrado", HttpStatus.NOT_FOUND);
			}

			Survey survey = this.surveyTOConverter.surveyTOToSurvey.apply(surveyTO);
			for (SurveyQuestion surveyQuestion : survey.getListSurveyQuestion()) {
				surveyQuestion.setSurvey(survey);
			}

			survey.setAuthor(prof);
			survey.setEvent(event);
			surveyRepository.save(survey);

			return new ResponseEntity<>(HttpStatus.OK);

		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	// TOOD : mover metodo para o service
	@RequestMapping(value = "addQuestion/{survey_id}", method = RequestMethod.PUT)
	public ResponseEntity<?> addQuestion(@RequestBody SurveyQuestionWrapper surveyQuestionWrapper,
			@PathVariable("event_id") Long eventId, @PathVariable("survey_id") Long surveyId) {

		try {

			Event event = this.eventRepository.findOne(eventId);
			if (event == null) {
				return new ResponseEntity<>("Evento não encontrado!", HttpStatus.NOT_FOUND);
			}

			Survey survey = surveyRepository.findOne(surveyId);

			if (survey == null) {
				return new ResponseEntity<>("Enquete não encontrada!", HttpStatus.NOT_FOUND);
			}

			if (survey.getListSurveyQuestion() == null) {
				survey.setListSurveyQuestion(new ArrayList<>());
			}

			for (SurveyQuestionTO surveyQuestion : surveyQuestionWrapper.getSurveyQuestions()) {

				SurveyQuestion surveyQ = this.surveyTOConverter.surveyQuestionTOToSurveyQuestion.apply(surveyQuestion);
				surveyQ.setSurvey(survey);
				survey.getListSurveyQuestion().add(surveyQ);
			}

			surveyRepository.save(survey);

			return new ResponseEntity<>(HttpStatus.OK);

		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@RequestMapping(method = RequestMethod.GET, value = "/countpending")
	public ResponseEntity<Object> getSurveyQuestionPendingQuantity(@PathVariable("event_id") Long eventId) {
		try {
			Profile userLoggedIn = this.profileUtil.getProfileLoggedIn();

			Long unreadMessages = this.surveyService.getSurveyQuestionPendingQuantity(eventId, userLoggedIn.getId());
			Map<String, Long> result = new HashMap<>();
			result.put("unansweredQuestions", unreadMessages);

			return new ResponseEntity<>(result, HttpStatus.OK);

		} catch (NotFoundException e) {
			return new ResponseEntity<>(e.getMessage(), HttpStatus.NOT_FOUND);
		} catch (MissingArgumentException e) {
			return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@RequestMapping(value = "result/{survey_id}", method = RequestMethod.GET)
	public ResponseEntity<?> getResult(@PathVariable("event_id") Long eventId,
			@PathVariable("survey_id") Long surveyId) {

		try {
			Event event = this.eventRepository.findOne(eventId);
			if (event == null) {
				return new ResponseEntity<>("Evento não encontrado!", HttpStatus.NOT_FOUND);
			}

			Survey survey = surveyRepository.findOne(surveyId);
			if (survey == null) {
				return new ResponseEntity<>("Enquete não encontrada!", HttpStatus.NOT_FOUND);
			}

			List<SurveyAnswerStatisticsTO> list = profileSurveyAnswerRepository.findSurveyCount(survey.getId());

			return new ResponseEntity<>(list, HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}
