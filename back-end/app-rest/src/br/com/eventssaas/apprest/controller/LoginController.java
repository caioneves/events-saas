package br.com.eventssaas.apprest.controller;

import java.util.Date;
import java.util.List;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.com.eventssaas.apprest.TO.ForgotPasswordRequestTO;
import br.com.eventssaas.apprest.TO.LoginSocialTO;
import br.com.eventssaas.apprest.token.Credentials;
import br.com.eventssaas.apprest.token.UtilSecurity;
import br.com.eventssaas.generalbusiness.entity.Profile;
import br.com.eventssaas.generalbusiness.entity.ProfileRole;
import br.com.eventssaas.generalbusiness.entity.User;
import br.com.eventssaas.generalbusiness.exceptions.NotFoundException;
import br.com.eventssaas.generalbusiness.repository.ProfileRepository;
import br.com.eventssaas.generalbusiness.repository.ProfileRoleRepository;
import br.com.eventssaas.generalbusiness.repository.UserRepository;
import br.com.eventssaas.generalbusiness.services.IMailService;
import br.com.eventssaas.generalbusiness.services.IUserService;
import br.com.eventssaas.generalbusiness.services.ServiceToken;
import br.com.eventssaas.generalbusiness.util.ProfileUtil;

@RestController
// @RequestMapping(value = "/event/{event_id}/login")
public class LoginController {

	private static final String AUTHORIZATION_PROPERTY = "Authorization";

	private static final String TOKEN_PROPERTY = "token";

	@Autowired
	private ProfileRepository profileRepository;

	@Autowired
	private ProfileRoleRepository profileRoleRepository;

	@Autowired
	private ServiceToken serviceToken;

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private IMailService mailService;

	@Autowired
	private IUserService userService;

	@Autowired
	private ProfileUtil profileUtil;

	@RequestMapping(method = RequestMethod.POST, value = "/event/{event_id}/login")
	public ResponseEntity<?> login(@PathVariable("event_id") Long eventId, HttpServletRequest request) {

		final String authorization = request.getHeader(AUTHORIZATION_PROPERTY);
		if (authorization == null || authorization.length() == 0) {
			return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
		}

		Credentials credentials = UtilSecurity.parseHeader(authorization);

		BCryptPasswordEncoder cc = new BCryptPasswordEncoder();

		User user = userRepository.findByEmailIgnoreCase(credentials.getUsername());
		if (user == null) {
			return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
		}

		if (!cc.matches(credentials.getPassword(), user.getPassword())) {
			return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
		}

		Profile profile = profileRepository.findByUserId(user.getId());

		if (profile != null) {
			List<ProfileRole> profileRoles = profileRoleRepository.findByProfileIdAndEventId(profile.getId(), eventId);
			if (profileRoles == null) {
				return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);

			}
			UUID random = UUID.randomUUID();
			this.serviceToken.removeFromUser(profile, eventId);
			this.serviceToken.addToken(random.toString(), new Date(), profile, profileRoles, eventId);
			profile.setToken(random.toString());
			profile.setCurrentEventRoles(this.profileUtil.getRoleTypes(profileRoles));
		} else {
			return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
		}

		return new ResponseEntity<>(profile, HttpStatus.OK);

	}

	@RequestMapping(method = RequestMethod.POST, value = "/login/checkin")
	public ResponseEntity<?> loginCheckinApp(HttpServletRequest request) {

		final String authorization = request.getHeader(AUTHORIZATION_PROPERTY);
		if (authorization == null || authorization.length() == 0) {
			return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
		}

		Credentials credentials = UtilSecurity.parseHeader(authorization);

		BCryptPasswordEncoder cc = new BCryptPasswordEncoder();

		User user = userRepository.findByEmailIgnoreCase(credentials.getUsername());
		if (user == null) {
			return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
		}

		if (!cc.matches(credentials.getPassword(), user.getPassword())) {
			return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
		}

		Profile profile = profileRepository.findByUserId(user.getId());

		List<ProfileRole> profileRoles;
		if (profile != null) {
			profileRoles = profileRoleRepository.findByProfileIdAndActiveEvent(profile.getId());

		} else {
			return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
		}

		return new ResponseEntity<>(profileRoles, HttpStatus.OK);

	}

	@RequestMapping(method = RequestMethod.POST, value = "/event/{event_id}/login/social")
	public ResponseEntity<?> loginSocial(@PathVariable("event_id") Long eventId,
			@RequestBody LoginSocialTO loginSocialTO) {

		Profile profile = profileRepository.findProfileByProviderUserIdAndProviderId(loginSocialTO.getUserId(),
				loginSocialTO.getProviderId());

		if (profile != null) {
			List<ProfileRole> profileRoles = profileRoleRepository.findByProfileIdAndEventId(profile.getId(), eventId);
			if (profileRoles == null) {
				return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);

			}
			UUID random = UUID.randomUUID();
			this.serviceToken.removeFromUser(profile, eventId);
			this.serviceToken.addToken(random.toString(), new Date(), profile, profileRoles, eventId);
			profile.setToken(random.toString());
			profile.setCurrentEventRoles(this.profileUtil.getRoleTypes(profileRoles));
		} else {
			return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
		}

		return new ResponseEntity<>(profile, HttpStatus.OK);

	}

	@RequestMapping(value = "/forgotpassword", method = RequestMethod.POST)
	public ResponseEntity<?> resetPassword(@RequestBody ForgotPasswordRequestTO forgotPasswordRequestTO) {

		try {

			this.userService.forgetPassword(forgotPasswordRequestTO.getEmail());
			return new ResponseEntity<>(HttpStatus.OK);

		} catch (NotFoundException e) {
			return new ResponseEntity<>(e.getMessage(), HttpStatus.NOT_FOUND);
		} catch (Exception e) {
			return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

}