package br.com.eventssaas.apprest.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.com.eventssaas.generalbusiness.entity.QuickAccess;
import br.com.eventssaas.generalbusiness.repository.QuickAccessRepository;

@RestController
@RequestMapping(value = "/event/{event_id}/quickaccess")
public class QuickAccessController {

	@Autowired
	private QuickAccessRepository quickAccessRepository;

	@RequestMapping(method = RequestMethod.GET, value = "")
	public List<QuickAccess> getEventInformation(@PathVariable("event_id") Long eventId) {

		List<QuickAccess> quickAccessList = this.quickAccessRepository.findByEventId(eventId);
		return quickAccessList;

	}
}
