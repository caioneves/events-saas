package br.com.eventssaas.apprest.controller;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.com.eventssaas.apprest.TO.LectureTO;
import br.com.eventssaas.apprest.TO.MiniProfileTO;
import br.com.eventssaas.apprest.TO.SavedLectureTO;
import br.com.eventssaas.apprest.converter.EventConverterUtil;
import br.com.eventssaas.apprest.converter.ProfileConverterUtil;
import br.com.eventssaas.generalbusiness.entity.DownloadItem;
import br.com.eventssaas.generalbusiness.entity.Event;
import br.com.eventssaas.generalbusiness.entity.LectureDetail;
import br.com.eventssaas.generalbusiness.entity.LectureSavedAlarmProfile;
import br.com.eventssaas.generalbusiness.entity.Profile;
import br.com.eventssaas.generalbusiness.repository.DownloadItemRepository;
import br.com.eventssaas.generalbusiness.repository.EventRepository;
import br.com.eventssaas.generalbusiness.repository.LectureDetailRepository;
import br.com.eventssaas.generalbusiness.repository.LectureSavedAlarmProfileRepository;
import br.com.eventssaas.generalbusiness.repository.NoBytes;
import br.com.eventssaas.generalbusiness.repository.ProfileRepository;
import br.com.eventssaas.generalbusiness.util.ProfileUtil;

@RestController
@RequestMapping(value = "/event/{event_id}/lecture")
public class LectureController {

	@Autowired
	private LectureDetailRepository lectureDetailRepository;

	@Autowired
	private DownloadItemRepository downloadItemRepository;

	@Autowired
	private LectureSavedAlarmProfileRepository lectureSavedAlarmProfileRepository;

	@Autowired
	private EventRepository eventRepository;

	@Autowired
	private ProfileUtil profileUtil;

	@Autowired
	private EventConverterUtil eventConverter;

	@Autowired
	private ProfileRepository profileRepository;

	@Autowired
	private ProfileConverterUtil profileConverterUtil;

	@RequestMapping(method = RequestMethod.GET, value = "/{lecture_id}")
	public ResponseEntity<LectureTO> getLectureById(@PathVariable("event_id") Long eventId,
			@PathVariable("lecture_id") Long lectureId) {
		LectureDetail lecture = this.lectureDetailRepository.findByEventIdAndId(eventId, lectureId);
		LectureTO result = this.eventConverter.convertLectureToLectureTO.apply(lecture);
		return new ResponseEntity<>(result, HttpStatus.OK);
	}

	@Transactional
	@RequestMapping(method = RequestMethod.POST, value = "/{lecture_id}/save")
	public ResponseEntity saveLectureOnProfileLoggedIn(@PathVariable("event_id") Long eventId,
			@PathVariable("lecture_id") Long lectureId) {

		try {
			Event event = this.eventRepository.getOne(eventId);

			if (event == null) {
				return new ResponseEntity<>("request.fail.event.notfound", HttpStatus.NOT_FOUND);
			}

			LectureDetail lecture = this.lectureDetailRepository.getOne(lectureId);

			if (lecture == null) {
				return new ResponseEntity<>("request.fail.event.lecture.notfound", HttpStatus.NOT_FOUND);
			}

			Profile profile = this.profileUtil.getProfileLoggedIn();

			LectureSavedAlarmProfile lectureSaved = new LectureSavedAlarmProfile();
			lectureSaved.setAlarm(false);
			lectureSaved.setEvent(event);
			lectureSaved.setLectureDetail(lecture);
			lectureSaved.setProfile(profile);

			this.lectureSavedAlarmProfileRepository.save(lectureSaved);

			return new ResponseEntity<>(HttpStatus.OK);

		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@RequestMapping(method = RequestMethod.GET, value = "/{lecture_id}/saveandalarm")
	public ResponseEntity<List<SavedLectureTO>> getSaveAndAlarmLectureOnProfileLoggedIn(
			@PathVariable("event_id") Long eventId, @PathVariable("lecture_id") Long lectureId) {

		Profile profile = this.profileUtil.getProfileLoggedIn();

		List<LectureSavedAlarmProfile> lectureSaved = this.lectureSavedAlarmProfileRepository
				.findByProfileIdAndEventIdAndLectureDetailId(profile.getId(), eventId, lectureId);

		List<SavedLectureTO> result = lectureSaved.stream().map(eventConverter.convertLectureSavedAlarmProfile)
				.collect(Collectors.<SavedLectureTO>toList());
		return new ResponseEntity<List<SavedLectureTO>>(result, HttpStatus.OK);
	}

	@Transactional
	@RequestMapping(method = RequestMethod.DELETE, value = "/{lecture_id}/save")
	public ResponseEntity removeSaveLectureOnProfileLoggedIn(@PathVariable("event_id") Long eventId,
			@PathVariable("lecture_id") Long lectureId) {

		try {
			Profile profile = this.profileUtil.getProfileLoggedIn();

			LectureSavedAlarmProfile lectureSaved = this.lectureSavedAlarmProfileRepository
					.findByProfileIdAndEventIdAndLectureDetailIdAndAlarm(profile.getId(), eventId, lectureId, false);

			if (lectureSaved == null) {
				return new ResponseEntity<>("request.success.event.lecture.save.profile.notfound.delete",
						HttpStatus.OK);
			}

			lectureSavedAlarmProfileRepository.delete(lectureSaved);

			return new ResponseEntity<>(HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@Transactional
	@RequestMapping(method = RequestMethod.POST, value = "/{lecture_id}/alarm")
	public ResponseEntity alarmLectureOnProfileLoggedIn(@PathVariable("event_id") Long eventId,
			@PathVariable("lecture_id") Long lectureId) {

		try {
			Event event = this.eventRepository.getOne(eventId);

			if (event == null) {
				return new ResponseEntity<>("request.fail.event.notfound", HttpStatus.NOT_FOUND);
			}

			LectureDetail lectureDetail = this.lectureDetailRepository.getOne(lectureId);

			if (lectureDetail == null) {
				return new ResponseEntity<>("request.fail.event.lecture.notfound", HttpStatus.NOT_FOUND);
			}

			Profile profile = this.profileUtil.getProfileLoggedIn();

			LectureSavedAlarmProfile lectureAlarm = new LectureSavedAlarmProfile();
			lectureAlarm.setAlarm(true);
			lectureAlarm.setEvent(event);
			lectureAlarm.setLectureDetail(lectureDetail);
			lectureAlarm.setProfile(profile);

			this.lectureSavedAlarmProfileRepository.save(lectureAlarm);

			return new ResponseEntity<>(HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@Transactional
	@RequestMapping(method = RequestMethod.DELETE, value = "/{lecture_id}/alarm")
	public ResponseEntity removeAlarmLectureOnProfileLoggedIn(@PathVariable("event_id") Long eventId,
			@PathVariable("lecture_id") Long lectureId) {

		try {
			Profile profile = this.profileUtil.getProfileLoggedIn();

			LectureSavedAlarmProfile lectureAlarm = this.lectureSavedAlarmProfileRepository
					.findByProfileIdAndEventIdAndLectureDetailIdAndAlarm(profile.getId(), eventId, lectureId, true);

			if (lectureAlarm == null) {
				return new ResponseEntity<>("request.success.event.lecture.alarm.profile.notfound.delete",
						HttpStatus.OK);
			}

			this.lectureSavedAlarmProfileRepository.delete(lectureAlarm);

			return new ResponseEntity<>(lectureAlarm, HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@RequestMapping(method = RequestMethod.GET, value = "/{lecture_id}/downloaditems/list")
	public ResponseEntity<List<NoBytes>> getListDownloadItems(@PathVariable("event_id") Long eventId,
			@PathVariable("lecture_id") Long lectureId) {

		List<NoBytes> downloadItems = this.downloadItemRepository
				.findByLectureDetailIdOrderByCreationDateDesc(lectureId);
		return new ResponseEntity<>(downloadItems, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/{lecture_id}/download/{id}")
	public ResponseEntity<Resource> getDownload(@PathVariable("event_id") Long eventId,
			@PathVariable("lecture_id") Long lectureId, @PathVariable("id") Long downloadItemId) {

		DownloadItem downloadItems = this.downloadItemRepository.findOne(downloadItemId);

		ByteArrayResource resource = new ByteArrayResource(downloadItems.getFile());

		return ResponseEntity.ok().contentLength(downloadItems.getFile().length)
				.contentType(MediaType.parseMediaType("application/octet-stream")).body(resource);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/list")
	public ResponseEntity<?> getLectureById(@PathVariable("event_id") Long eventId) {
		List<LectureDetail> lectures = this.lectureDetailRepository.findByEventIdAndNoParent(eventId);

		List<LectureTO> result = lectures.stream().map(this.eventConverter.convertLectureToLectureTO)
				.collect(Collectors.<LectureTO>toList());

		List<Profile> profiles = this.profileRepository.findByListProfileRolesEventId(eventId);

		List<MiniProfileTO> mProfilesTOs = profiles.stream()
				.map(this.profileConverterUtil.convertProfileToMiniProfileTO)
				.collect(Collectors.<MiniProfileTO>toList());

		for (LectureTO lectureTO : result) {
			lectureTO.setRegisteredUsers(mProfilesTOs);
		}

		return new ResponseEntity<>(result, HttpStatus.OK);
	}
}
