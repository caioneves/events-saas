package br.com.eventssaas.apprest.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.com.eventssaas.apprest.TO.EventTO;
import br.com.eventssaas.apprest.TO.MiniProfileTO;
import br.com.eventssaas.apprest.TO.ProfileAdminTO;
import br.com.eventssaas.apprest.converter.EventConverterUtil;
import br.com.eventssaas.apprest.converter.ProfileConverterUtil;
import br.com.eventssaas.apprest.token.Credentials;
import br.com.eventssaas.apprest.token.UtilSecurity;
import br.com.eventssaas.generalbusiness.entity.ProfileRole;
import br.com.eventssaas.generalbusiness.entity.User;
import br.com.eventssaas.generalbusiness.repository.ProfileRepository;
import br.com.eventssaas.generalbusiness.repository.ProfileRoleRepository;
import br.com.eventssaas.generalbusiness.repository.UserRepository;
import br.com.eventssaas.generalbusiness.services.IMailService;
import br.com.eventssaas.generalbusiness.services.IUserService;
import br.com.eventssaas.generalbusiness.util.ProfileUtil;

@RestController
public class LoginAdminController {

	private static final String AUTHORIZATION_PROPERTY = "Authorization";

	private static final String TOKEN_PROPERTY = "token";

	@Autowired
	private ProfileRepository profileRepository;

	@Autowired
	private ProfileRoleRepository profileRoleRepository;

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private IMailService mailService;

	@Autowired
	private IUserService userService;

	@Autowired
	private ProfileUtil profileUtil;

	@Autowired
	private ProfileConverterUtil profileConverterUtil;

	@Autowired
	private EventConverterUtil eventConverterUtil;

	@RequestMapping(method = RequestMethod.POST, value = "/admin/login")
	public ResponseEntity<?> login(HttpServletRequest request) {

		final String authorization = request.getHeader(AUTHORIZATION_PROPERTY);
		if (authorization == null || authorization.length() == 0) {
			return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
		}

		Credentials credentials = UtilSecurity.parseHeader(authorization);

		BCryptPasswordEncoder cc = new BCryptPasswordEncoder();

		User user = userRepository.findByEmailIgnoreCase(credentials.getUsername());
		if (user == null) {
			return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
		}

		if (!cc.matches(credentials.getPassword(), user.getPassword())) {
			return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
		}

		List<ProfileRole> profiles = profileRoleRepository.findByUser(user.getId());
		if (profiles.size() > 0) {
			ProfileRole profileRole = profiles.get(0);
			MiniProfileTO miniProfile = this.profileConverterUtil.convertProfileToMiniProfileTO
					.apply(profileRole.getProfile());

			List<EventTO> eventos = new ArrayList<>();

			for (ProfileRole pro : profiles) {
				eventos.add(eventConverterUtil.convertEventToEventTO.apply(pro.getEvent()));
			}

			ProfileAdminTO profileAdminTO = new ProfileAdminTO();
			profileAdminTO.setEvents(eventos);
			profileAdminTO.setMiniProfile(miniProfile);
			return new ResponseEntity<>(profileAdminTO, HttpStatus.OK);

		}

		return new ResponseEntity<>(new ProfileAdminTO(), HttpStatus.OK);

	}

}