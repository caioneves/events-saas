package br.com.eventssaas.apprest.controller;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import br.com.eventssaas.apprest.TO.NotificationTO;
import br.com.eventssaas.generalbusiness.entity.Event;
import br.com.eventssaas.generalbusiness.entity.Notification;
import br.com.eventssaas.generalbusiness.entity.Profile;
import br.com.eventssaas.generalbusiness.entity.ProfileNotification;
import br.com.eventssaas.generalbusiness.repository.EventRepository;
import br.com.eventssaas.generalbusiness.repository.NotificationRepository;
import br.com.eventssaas.generalbusiness.repository.ProfileNotificationRepository;
import br.com.eventssaas.generalbusiness.repository.ProfileRepository;
import br.com.eventssaas.generalbusiness.services.NoticicationService;
import br.com.eventssaas.generalbusiness.util.ProfileUtil;

@Controller
@RequestMapping(value = "/event/{event_id}/notification")
public class NotificationController {

	@Autowired
	private NoticicationService noticicationService;

	@Autowired
	private NotificationRepository notificationRepository;

	@Autowired
	private ProfileRepository profileRepository;

	@Autowired
	private EventRepository eventRepository;

	@Autowired
	private ProfileNotificationRepository profileNotificationRepository;

	@Autowired
	private ProfileUtil profileUtil;

	@RequestMapping(value = "", method = RequestMethod.POST)
	private ResponseEntity sendNotification(@PathVariable("event_id") Long eventId,
			@RequestBody NotificationTO notificationTO) {

		try {
			Profile userLoggedIn = this.profileUtil.getProfileLoggedIn();
			List<Profile> profiles = profileRepository
					.findDistinctByListProfileRolesEventIdAndNotSelfProfileLoggedInOrderByFirstName(eventId,
							userLoggedIn.getId());
			Event event = eventRepository.findOne(eventId);

			Notification notification = new Notification();
			notification.setEvent(event);
			notification.setBody(notificationTO.getBody());
			notification.setTitle(notificationTO.getTitle());
			notification.setNotificationType(notificationTO.getNotificationType());

			List<ProfileNotification> listProfilesNotifications = new ArrayList<>();
			for (Profile p : profiles) {
				ProfileNotification profileNotification = new ProfileNotification();
				profileNotification.setNotification(notification);
				profileNotification.setProfile(p);
				listProfilesNotifications.add(profileNotification);

			}
			notification.setListProfileNotification(listProfilesNotifications);
			notificationRepository.save(notification);
			noticicationService.sendNotification(notificationTO.getTitle(), notificationTO.getBody(), eventId);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<>(HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.GET)
	public ResponseEntity<?> getNotificationsByProfile(@PathVariable("event_id") Long eventId) {

		Profile profile = profileRepository.findOne(profileUtil.getUserLoggedInId());

		List<Notification> notificationList = this.notificationRepository
				.findNotificationByProfileAndEvent(profile.getId(), eventId);

		return new ResponseEntity<>(notificationList, HttpStatus.OK);
	}

	@RequestMapping(value = "/markRead", method = RequestMethod.POST)
	public ResponseEntity<?> markReadNotificaton(@PathVariable("event_id") Long eventId,
			@RequestParam(value = "notification_id", required = true) long notificationId) {

		Profile profile = profileRepository.findOne(profileUtil.getUserLoggedInId());

		try {

			ProfileNotification profileNotification = profileNotificationRepository
					.findNotificationByProfileIdAndNotificationId(profile.getId(), notificationId);
			profileNotification.setReadingDate(Calendar.getInstance());
			profileNotificationRepository.save(profileNotification);

		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}

		return new ResponseEntity<>(HttpStatus.OK);
	}

}
