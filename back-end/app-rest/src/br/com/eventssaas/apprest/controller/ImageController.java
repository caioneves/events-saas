package br.com.eventssaas.apprest.controller;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.com.eventssaas.apprest.TO.ImageTO;
import br.com.eventssaas.apprest.converter.ImageConverter;
import br.com.eventssaas.generalbusiness.entity.Image;
import br.com.eventssaas.generalbusiness.repository.ImageRepository;

@RestController
public class ImageController {

	@Autowired
	private ImageConverter imageConverter;

	@Autowired
	private ImageRepository imageRepository;

	@RequestMapping(method = RequestMethod.POST, value = "/event/{event_id}/image")
	public ResponseEntity<?> saveImage(@RequestBody ImageTO imgTO, @PathVariable("event_id") Long eventId) {

		Image img = this.imageConverter.convertImageTOToImage.apply(imgTO);

		img = this.imageRepository.save(img);
		this.imageRepository.flush();

		return new ResponseEntity<>(img.getId(), HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.DELETE, value = "/event/{event_id}/image/{image_id}")
	public ResponseEntity<?> deleteImage(@PathVariable("image_id") Long imageId,
			@PathVariable("event_id") Long eventId) {

		if (!this.imageRepository.exists(imageId)) {
			Map<String, String> result = new HashMap<>();
			result.put("message", "request.fail.image.notfound");
			return new ResponseEntity<>(result, HttpStatus.NOT_FOUND);
		}

		this.imageRepository.delete(imageId);

		return new ResponseEntity<>(HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/service/image/{image_id}")
	public ResponseEntity<?> getImage(@PathVariable("image_id") Long imageId) {

		if (!this.imageRepository.exists(imageId)) {
			Map<String, String> result = new HashMap<>();
			result.put("message", "request.fail.image.notfound");
			return new ResponseEntity<>(result, HttpStatus.NOT_FOUND);
		}

		Image img = this.imageRepository.findOne(imageId);

		return ResponseEntity.ok().contentType(MediaType.IMAGE_JPEG).body(img.getBytes());
	}
}
