package br.com.eventssaas.apprest.converter;

import java.util.function.Function;
import java.util.stream.Collectors;

import org.springframework.stereotype.Component;

import br.com.eventssaas.apprest.TO.SurveyAnswerTO;
import br.com.eventssaas.apprest.TO.SurveyQuestionTO;
import br.com.eventssaas.apprest.TO.SurveyTO;
import br.com.eventssaas.generalbusiness.entity.Survey;
import br.com.eventssaas.generalbusiness.entity.SurveyAnswer;
import br.com.eventssaas.generalbusiness.entity.SurveyQuestion;
import br.com.eventssaas.generalbusiness.enums.SurveyQuestionType;

@Component
public class SurveyConverter {

	public Function<Survey, SurveyTO> surveyToSurveyTO = new Function<Survey, SurveyTO>() {

		@Override
		public SurveyTO apply(Survey survey) {
			SurveyTO to = new SurveyTO();
			to.setId(survey.getId());
			to.setName(survey.getName());

			to.setCreationDate(survey.getCreationDate());
			to.setPushNotification(survey.isPushNotification());
			to.setPushNotificationDate(survey.getPushNotificationDate());

			if (survey.getAuthor() != null) {
				to.setAuthorId(survey.getAuthor().getId());
			}
			if (survey.getEvent() != null) {
				to.setEventId(survey.getEvent().getId());
			}
			if (survey.getListSurveyQuestion() != null) {
				to.setSurveyQuestions(survey.getListSurveyQuestion().stream()
						.map(SurveyConverter.this.surveyQuestionToSurveyQuestionTO)
						.collect(Collectors.<SurveyQuestionTO>toList()));
			}
			return to;
		}
	};

	public Function<SurveyQuestion, SurveyQuestionTO> surveyQuestionToSurveyQuestionTO = new Function<SurveyQuestion, SurveyQuestionTO>() {

		@Override
		public SurveyQuestionTO apply(SurveyQuestion question) {
			SurveyQuestionTO to = new SurveyQuestionTO();
			to.setId(question.getId());
			to.setQuestion(question.getQuestion());
			to.setType(question.getType());

			if (question.getSurvey() != null) {
				to.setSurveyId(question.getSurvey().getId());
			}
			if (question.getListSurveyAnswer() != null && question.getType() != SurveyQuestionType.OPENED_QUESTION) {
				to.setSurveyAnswers(
						question.getListSurveyAnswer().stream().map(SurveyConverter.this.surveyAnswerToSurveyAnswerTO)
								.collect(Collectors.<SurveyAnswerTO>toList()));
			}

			return to;
		}
	};

	public Function<SurveyAnswer, SurveyAnswerTO> surveyAnswerToSurveyAnswerTO = new Function<SurveyAnswer, SurveyAnswerTO>() {

		@Override
		public SurveyAnswerTO apply(SurveyAnswer answer) {
			SurveyAnswerTO to = new SurveyAnswerTO();
			to.setId(answer.getId());
			to.setAnswer(answer.getAnswer());
			to.setCorrect(answer.isCorrect());

			if (answer.getSurveyQuestion() != null) {
				to.setSurveyQuestionId(answer.getSurveyQuestion().getId());
			}
			return to;
		}
	};
}
