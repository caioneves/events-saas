package br.com.eventssaas.apprest.converter;

import java.util.List;

import br.com.eventssaas.apprest.TO.SurveyQuestionTO;

public class SurveyQuestionWrapper {

	private List<SurveyQuestionTO> surveyQuestions;

	public List<SurveyQuestionTO> getSurveyQuestions() {
		return surveyQuestions;
	}

	public void setSurveyQuestions(List<SurveyQuestionTO> surveyQuestions) {
		this.surveyQuestions = surveyQuestions;
	}

}
