package br.com.eventssaas.apprest.converter;

import org.springframework.core.convert.converter.Converter;

import br.com.eventssaas.generalbusiness.enums.ProfileRoleType;

public class ProfileRoleTypeEnumConverter implements Converter<String, ProfileRoleType> {

	@Override
	public ProfileRoleType convert(String source) {
		try {
			return ProfileRoleType.valueOf(source);
		} catch (Exception e) {
			return null;
		}
	}
}