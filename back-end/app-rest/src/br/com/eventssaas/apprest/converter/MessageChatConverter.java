package br.com.eventssaas.apprest.converter;

import java.util.function.Function;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import br.com.eventssaas.apprest.TO.MessageChatTO;
import br.com.eventssaas.generalbusiness.entity.Image;
import br.com.eventssaas.generalbusiness.entity.MessageChat;

@Component
public class MessageChatConverter {

	@Autowired
	private ProfileConverterUtil profileConverterUtil;

	public Function<MessageChat, MessageChatTO> convertMessageChatToMessageChatTO = new Function<MessageChat, MessageChatTO>() {

		@Override
		public MessageChatTO apply(MessageChat t) {

			MessageChatTO mChatTO = new MessageChatTO();

			// mChatTO.setAuthor(t.getAuthor().getId());
			mChatTO.setId(t.getId());
			mChatTO.setCreationDate(t.getCreationDate());
			mChatTO.setText(t.getText());
			mChatTO.setRead(t.isRead());
			mChatTO.setDeletedByAuthor(t.isDeletedByAuthor());
			mChatTO.setDeletedByTarget(t.isDeletedByTarget());
			mChatTO.setDeletedByAuthorDate(t.getDeletedByAuthorDate());
			mChatTO.setDeletedByTargetProfileDate(t.getDeletedByTargetProfileDate());

			mChatTO.setAuthor(getProfileConverterUtil().convertProfileToMiniProfileTO.apply(t.getAuthor()));
			mChatTO.setTarget(getProfileConverterUtil().convertProfileToMiniProfileTO.apply(t.getTargetProfile()));
			if (t.getImage() != null) {
				mChatTO.setImageUrl(Image.IMAGE_REST_PATH + t.getImage().getId());
			}

			return mChatTO;
		}

	};

	public ProfileConverterUtil getProfileConverterUtil() {
		return profileConverterUtil;
	}

	public void setProfileConverterUtil(ProfileConverterUtil profileConverterUtil) {
		this.profileConverterUtil = profileConverterUtil;
	}

}
