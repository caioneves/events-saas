package br.com.eventssaas.apprest.converter;

import org.springframework.core.convert.converter.Converter;

import br.com.eventssaas.generalbusiness.enums.GenderType;

public class GenderTypeEnumConverter implements Converter<String, GenderType> {

	@Override
	public GenderType convert(String source) {
		try {
			return GenderType.valueOf(source);
		} catch (Exception e) {
			return null;
		}
	}
}