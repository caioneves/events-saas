package br.com.eventssaas.apprest.converter;

import java.util.function.Function;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import br.com.eventssaas.apprest.TO.MiniProfileTO;
import br.com.eventssaas.generalbusiness.entity.Profile;
import br.com.eventssaas.generalbusiness.services.ServiceToken;
import br.com.eventssaas.generalbusiness.util.ProfileUtil;

@Component
public class ProfileConverterUtil {

	@Autowired
	ServiceToken serviceToken;

	@Autowired
	ProfileUtil profileUtil;

	public Function<Profile, MiniProfileTO> convertProfileToMiniProfileTO = new Function<Profile, MiniProfileTO>() {
		@Override
		public MiniProfileTO apply(Profile p) {
			MiniProfileTO miniProfile = new MiniProfileTO();

			miniProfile.setId(p.getId());
			miniProfile.setPictureUrl(p.getPictureUrl());
			miniProfile.setName(p.getFirstName() + " " + p.getLastName());
			miniProfile.setJobRole(p.getJobRole());
			miniProfile.setShowJobRole(p.isShowJobRole());
			miniProfile.setCompany(p.getCompany());
			miniProfile.setShowJobRole(p.isShowJobRole());
			miniProfile.setMinibio(p.getMinibio());
			miniProfile.setDocumentNumber(p.getDocumentNumber());
			miniProfile.setOnLine(serviceToken.isOnline(p.getId(), profileUtil.getUserLoggedEvent()));
			if (p.getDocumentType() != null) {
				miniProfile.setDocumentType(p.getDocumentType().getProviderType());
			}
			if (p.getUser() != null) {
				miniProfile.setEmail(p.getUser().getEmail());
			}
			if (p.getContactInfo() != null) {
				miniProfile.setEmailAlternative(p.getContactInfo().getEmail());
				miniProfile.setPhone1(p.getContactInfo().getPhone1());
				miniProfile.setPhone2(p.getContactInfo().getPhone2());
				miniProfile.setPhone3(p.getContactInfo().getPhone3());
				miniProfile.setPhone4(p.getContactInfo().getPhone4());
			}
			miniProfile.setTwitterUrl(p.getTwitterUrl());
			miniProfile.setFacebookUrl(p.getFacebookUrl());
			miniProfile.setInstagramUrl(p.getInstagramUrl());
			miniProfile.setLinkedInUrl(p.getLinkedInUrl());
			return miniProfile;
		}
	};

}
