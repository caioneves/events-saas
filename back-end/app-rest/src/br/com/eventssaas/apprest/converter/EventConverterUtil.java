package br.com.eventssaas.apprest.converter;

import java.util.ArrayList;
import java.util.function.Function;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import br.com.eventssaas.apprest.TO.EventInformationTO;
import br.com.eventssaas.apprest.TO.EventTO;
import br.com.eventssaas.apprest.TO.LectureTO;
import br.com.eventssaas.apprest.TO.MiniLectureTO;
import br.com.eventssaas.apprest.TO.SavedLectureTO;
import br.com.eventssaas.generalbusiness.entity.DownloadItem;
import br.com.eventssaas.generalbusiness.entity.Event;
import br.com.eventssaas.generalbusiness.entity.Image;
import br.com.eventssaas.generalbusiness.entity.LectureDetail;
import br.com.eventssaas.generalbusiness.entity.LectureSavedAlarmProfile;
import br.com.eventssaas.generalbusiness.entity.Profile;

@Component
public class EventConverterUtil {

	@Autowired
	private ProfileConverterUtil profileConverterUtil;

	public Function<Event, EventInformationTO> convertEventToEventInformationTO = new Function<Event, EventInformationTO>() {
		@Override
		public EventInformationTO apply(Event e) {
			EventInformationTO eventInformation = new EventInformationTO();

			eventInformation.setIdEvent(e.getId());
			eventInformation.setId(e.getEventInformation().getId());
			if (e.getEventInformation().getImage() != null) {
				eventInformation.setImage(Image.IMAGE_REST_PATH + e.getEventInformation().getImage().getId());
			}
			eventInformation.setName(e.getName());
			// Period of event
			eventInformation.setStartEvent(e.getEventInformation().getStartEvent());
			eventInformation.setEndEvent(e.getEventInformation().getEndEvent());

			if (e.getEventInformation().getContactInfo() != null) {
				// Contact
				eventInformation.setEmail(e.getEventInformation().getContactInfo().getEmail());
				eventInformation.setPhone1(e.getEventInformation().getContactInfo().getPhone1());
				eventInformation.setPhone2(e.getEventInformation().getContactInfo().getPhone2());
				eventInformation.setPhone3(e.getEventInformation().getContactInfo().getPhone3());
				eventInformation.setPhone4(e.getEventInformation().getContactInfo().getPhone4());
				eventInformation.setSiteUrl(e.getEventInformation().getSiteUrl());

				// Place and Localization
				eventInformation.setPlaceName(e.getEventInformation().getPlaceName());
				eventInformation.setPlaceMapLabel(e.getEventInformation().getPlaceMapLabel());
				eventInformation.setPlaceMapImageUrl(e.getEventInformation().getPlaceMapImageUrl());

				eventInformation.setAddress1(e.getEventInformation().getContactInfo().getAddress1());
				eventInformation.setAddress2(e.getEventInformation().getContactInfo().getAddress2());
				eventInformation.setNumber(e.getEventInformation().getContactInfo().getNumber());
				eventInformation.setCounty(e.getEventInformation().getContactInfo().getCounty());
				eventInformation.setCity(e.getEventInformation().getContactInfo().getCity());
				eventInformation.setState(e.getEventInformation().getContactInfo().getState());
				eventInformation.setCountry(e.getEventInformation().getContactInfo().getCountry());
				eventInformation.setCEP(e.getEventInformation().getContactInfo().getCEP());
				eventInformation.setGoogleMapsLat(e.getEventInformation().getGoogleMapsLat());
				eventInformation.setGoogleMapsLong(e.getEventInformation().getGoogleMapsLong());
			}
			// Description
			eventInformation.setDescription(e.getDescription());

			return eventInformation;
		}
	};

	public Function<LectureDetail, MiniLectureTO> convertLectureToMiniLectureTO = new Function<LectureDetail, MiniLectureTO>() {

		@Override
		public MiniLectureTO apply(LectureDetail l) {
			MiniLectureTO to = new MiniLectureTO();
			to.setId(l.getId());
			to.setStartTime(l.getStartTime());
			to.setEndTime(l.getEndTime());
			to.setTitle(l.getTitle());
			to.setDetailPage(!StringUtils.isEmpty(l.getDescription()));
			to.setRoom(l.getRoom());
			to.setChildCount(0);
			to.setType(l.getType());
			to.setScheduleBackgroundColor(l.getScheduleBackgroundColor());
			to.setPriority(l.getPriority());

			to.setSpeakers(new ArrayList<>());
			if (l.getListSpeakers() != null) {
				for (Profile speaker : l.getListSpeakers()) {
					to.getSpeakers().add(speaker.getFullName());
				}
			}

			to.setTags(new ArrayList<>());
			if (l.getListTags() != null) {
				for (String tag : l.getListTags()) {
					to.getTags().add(tag);
				}
			}

			if (l.getListSubItems() != null && l.getListSubItems().size() > 0) {
				to.setChildCount(l.getListSubItems().size());
			}

			return to;
		}
	};

	public Function<LectureDetail, LectureTO> convertLectureToLectureTO = new Function<LectureDetail, LectureTO>() {

		@Override
		public LectureTO apply(LectureDetail l) {
			if (l == null) {
				return null;
			}
			LectureTO to = new LectureTO();
			to.setAuthor(getProfileConverterUtil().convertProfileToMiniProfileTO.apply(l.getAuthor()));
			to.setCreationDate(l.getCreationDate());
			to.setDescription(l.getDescription());
			to.setDetailUrl(l.getDetailUrl());
			to.setEndTime(l.getEndTime());
			to.setEvent(EventConverterUtil.this.convertEventToEventInformationTO.apply(l.getEvent()));
			to.setId(l.getId());
			if (l.getImage() != null) {
				to.setImageUrl(Image.IMAGE_REST_PATH + l.getImage().getId());
			}
			// to.setKeyNote(l.isKeyNote());

			long size = 0;
			for (DownloadItem item : l.getListFilesDownloadItem()) {
				size += item.getFile().length;
			}
			to.setFilesSize(size);
			to.setListTags(l.getListTags());
			// to.setMain(l.isMain());
			// to.setParentLecture(this.apply(l.getParentLecture()));

			to.setRoom(l.getRoom());
			to.setStartTime(l.getStartTime());
			to.setTitle(l.getTitle());

			to.setListSpeakers(new ArrayList<>());
			for (Profile user : l.getListSpeakers()) {
				to.getListSpeakers().add(getProfileConverterUtil().convertProfileToMiniProfileTO.apply(user));
			}

			to.setListSubItems(new ArrayList<>());
			for (LectureDetail lecture : l.getListSubItems()) {
				to.getListSubItems().add(this.apply(lecture));
			}

			to.setType(l.getType());
			to.setScheduleBackgroundColor(l.getScheduleBackgroundColor());
			to.setPriority(l.getPriority());

			return to;
		}
	};

	public Function<Event, EventTO> convertEventToEventTO = new Function<Event, EventTO>() {
		@Override
		public EventTO apply(Event e) {
			EventTO eventoTO = new EventTO();

			eventoTO.setId(e.getId());
			eventoTO.setName(e.getName());
			eventoTO.setDescription(e.getDescription());
			eventoTO.setSiteUrl(e.getEventInformation().getSiteUrl());
			eventoTO.setStartEvent(e.getEventInformation().getStartEvent());
			eventoTO.setEndEvent(e.getEventInformation().getEndEvent());

			return eventoTO;
		}
	};

	public Function<LectureSavedAlarmProfile, SavedLectureTO> convertLectureSavedAlarmProfile = new Function<LectureSavedAlarmProfile, SavedLectureTO>() {

		@Override
		public SavedLectureTO apply(LectureSavedAlarmProfile t) {
			SavedLectureTO to = new SavedLectureTO();
			to.setId(t.getId());
			to.setAlarm(t.isAlarm());
			to.setCreationDate(t.getCreationDate());
			to.setEvent(convertEventToEventTO.apply(t.getEvent()));
			to.setLectureDetail(convertLectureToLectureTO.apply(t.getLectureDetail()));
			to.setProfile(profileConverterUtil.convertProfileToMiniProfileTO.apply(t.getProfile()));
			return to;
		}
	};

	public ProfileConverterUtil getProfileConverterUtil() {
		return this.profileConverterUtil;
	}

	public void setProfileConverterUtil(ProfileConverterUtil profileConverterUtil) {
		this.profileConverterUtil = profileConverterUtil;
	}

}
