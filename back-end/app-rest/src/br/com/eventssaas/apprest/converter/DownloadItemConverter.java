package br.com.eventssaas.apprest.converter;

import java.util.Base64;
import java.util.function.Function;

import org.springframework.stereotype.Component;

import br.com.eventssaas.apprest.TO.FileTO;
import br.com.eventssaas.generalbusiness.entity.DownloadItem;

@Component
public class DownloadItemConverter {

	public Function<FileTO, DownloadItem> convertFileTOToDownloadItem = new Function<FileTO, DownloadItem>() {

		@Override
		public DownloadItem apply(FileTO fileTO) {
			DownloadItem downloadItem = new DownloadItem();

			byte[] bytes = Base64.getDecoder().decode(fileTO.getBytes().split(",")[1]);

			downloadItem.setFile(bytes);
			downloadItem.setFileName(fileTO.getName());
			downloadItem.setContentType(fileTO.getContentType());
			downloadItem.setSize(bytes.length);
			return downloadItem;
		}
	};

}
