package br.com.eventssaas.apprest.converter;

import java.util.function.Function;
import java.util.stream.Collectors;

import org.springframework.stereotype.Component;

import br.com.eventssaas.apprest.TO.SurveyAnswerTO;
import br.com.eventssaas.apprest.TO.SurveyQuestionTO;
import br.com.eventssaas.apprest.TO.SurveyTO;
import br.com.eventssaas.generalbusiness.entity.Survey;
import br.com.eventssaas.generalbusiness.entity.SurveyAnswer;
import br.com.eventssaas.generalbusiness.entity.SurveyQuestion;

@Component
public class SurveyTOConverter {

	public Function<SurveyTO, Survey> surveyTOToSurvey = new Function<SurveyTO, Survey>() {

		@Override
		public Survey apply(SurveyTO surveyTO) {
			Survey survey = new Survey();

			survey.setName(surveyTO.getName());

			survey.setCreationDate(surveyTO.getCreationDate());
			survey.setPushNotification(surveyTO.isPushNotification());
			survey.setPushNotificationDate(survey.getPushNotificationDate());

			if (surveyTO.getSurveyQuestions() != null) {

				survey.setListSurveyQuestion(surveyTO.getSurveyQuestions().stream()
						.map(SurveyTOConverter.this.surveyQuestionTOToSurveyQuestion)
						.collect(Collectors.<SurveyQuestion>toList()));

			}

			return survey;
		}
	};

	public Function<SurveyQuestionTO, SurveyQuestion> surveyQuestionTOToSurveyQuestion = new Function<SurveyQuestionTO, SurveyQuestion>() {

		@Override
		public SurveyQuestion apply(SurveyQuestionTO surveyQuestionTO) {
			SurveyQuestion surveyQuestion = new SurveyQuestion();
			surveyQuestion.setQuestion(surveyQuestionTO.getQuestion());
			if (surveyQuestionTO.getSurveyAnswers() != null) {
				surveyQuestion.setListSurveyAnswer(surveyQuestionTO.getSurveyAnswers().stream()
						.map(SurveyTOConverter.this.surveyAnswerTOSurveyAnswer)
						.collect(Collectors.<SurveyAnswer>toList()));

				for (SurveyAnswer sa : surveyQuestion.getListSurveyAnswer()) {
					sa.setSurveyQuestion(surveyQuestion);
				}
			}

			return surveyQuestion;
		}
	};

	public Function<SurveyAnswerTO, SurveyAnswer> surveyAnswerTOSurveyAnswer = new Function<SurveyAnswerTO, SurveyAnswer>() {

		@Override
		public SurveyAnswer apply(SurveyAnswerTO surveyAnswerTO) {
			SurveyAnswer surveyAnswer = new SurveyAnswer();
			surveyAnswer.setAnswer(surveyAnswerTO.getAnswer());

			return surveyAnswer;
		}
	};

}
