package br.com.eventssaas.apprest.converter;

import org.springframework.core.convert.converter.Converter;

import br.com.eventssaas.generalbusiness.enums.ArticleType;

public class ArticleTypeEnumConverter implements Converter<String, ArticleType> {

	@Override
	public ArticleType convert(String source) {
		try {
			return ArticleType.valueOf(source);
		} catch (Exception e) {
			return null;
		}
	}
}