package br.com.eventssaas.apprest.converter;

import java.util.Base64;
import java.util.function.Function;

import org.springframework.stereotype.Component;

import br.com.eventssaas.apprest.TO.ImageTO;
import br.com.eventssaas.generalbusiness.entity.Image;

@Component
public class ImageConverter {

	public Function<ImageTO, Image> convertImageTOToImage = new Function<ImageTO, Image>() {

		@Override
		public Image apply(ImageTO imgTo) {
			Image img = new Image();

			byte[] imageByte = Base64.getDecoder().decode(imgTo.getBytes().split(",")[1]);

			img.setBytes(imageByte);
			img.setContentType(imgTo.getContentType());

			return img;
		}
	};

}
