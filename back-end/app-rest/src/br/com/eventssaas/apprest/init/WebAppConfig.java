package br.com.eventssaas.apprest.init;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.format.support.DefaultFormattingConversionService;
import org.springframework.format.support.FormattingConversionService;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import br.com.eventssaas.apprest.converter.ArticleTypeEnumConverter;
import br.com.eventssaas.apprest.converter.GenderTypeEnumConverter;
import br.com.eventssaas.apprest.converter.ProfileRoleTypeEnumConverter;

@Configuration
@EnableWebMvc
@ComponentScan(basePackages = "br.com.eventssaas")
public class WebAppConfig extends WebMvcConfigurerAdapter {

	public void addCorsMappings(CorsRegistry registry) {
		registry.addMapping("/**").allowedHeaders("*").allowedMethods("*");
	}

	@Bean
	public FormattingConversionService mvcConversionService() {
		DefaultFormattingConversionService conversionService = new DefaultFormattingConversionService();

		conversionService.addConverter(new ArticleTypeEnumConverter());
		conversionService.addConverter(new ProfileRoleTypeEnumConverter());
		conversionService.addConverter(new GenderTypeEnumConverter());
		// conversionService.addConverter(new DateFormatter("dd/MM/yyyy"));
		return conversionService;

	}
}