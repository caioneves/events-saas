package com.eventssaas.apprest.test.survey;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashSet;
import java.util.List;

import javax.persistence.EntityManager;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import br.com.eventssaas.apprest.init.WebAppConfig;
import br.com.eventssaas.generalbusiness.entity.ContactInfo;
import br.com.eventssaas.generalbusiness.entity.DownloadItem;
import br.com.eventssaas.generalbusiness.entity.Event;
import br.com.eventssaas.generalbusiness.entity.EventInformation;
import br.com.eventssaas.generalbusiness.entity.Image;
import br.com.eventssaas.generalbusiness.entity.LectureDetail;
import br.com.eventssaas.generalbusiness.entity.Profile;
import br.com.eventssaas.generalbusiness.entity.ProfileRole;
import br.com.eventssaas.generalbusiness.entity.Survey;
import br.com.eventssaas.generalbusiness.entity.SurveyAnswer;
import br.com.eventssaas.generalbusiness.entity.SurveyQuestion;
import br.com.eventssaas.generalbusiness.enums.ProfileRoleType;
import br.com.eventssaas.generalbusiness.repository.EventRepository;
import br.com.eventssaas.generalbusiness.repository.ImageRepository;
import br.com.eventssaas.generalbusiness.repository.LectureDetailRepository;
import br.com.eventssaas.generalbusiness.repository.ProfileRepository;
import br.com.eventssaas.generalbusiness.repository.ProfileSurveyAnswerRepository;
import br.com.eventssaas.generalbusiness.repository.SurveyAnswerRepository;
import br.com.eventssaas.generalbusiness.repository.SurveyQuestionRepository;
import br.com.eventssaas.generalbusiness.repository.SurveyRepository;
import br.com.eventssaas.generalbusiness.services.SurveyService;
import br.com.eventssaas.generalbusiness.util.ProfileUtil;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { WebAppConfig.class })
@WebAppConfiguration
public class PopularBancoTest {

	@Autowired
	private EventRepository eventRepository;

	@Autowired
	private SurveyService service;

	@Autowired
	private ProfileUtil profileUtil;

	@Autowired
	private ProfileRepository profileRepository;

	@Autowired
	private SurveyRepository repository;

	@Autowired
	private ProfileSurveyAnswerRepository profileSurverRepository;

	@Autowired
	private SurveyQuestionRepository surveyQuestionRepository;

	@Autowired
	private SurveyAnswerRepository surveyAnswerRepository;

	@Autowired
	private LectureDetailRepository lectureRepository;

	@Autowired
	private ImageRepository imageRepository;

	@Autowired
	private EntityManager entityManager;

	private Profile configUser(Event event) {
		// CONFIG USER

		Profile user = this.profileRepository.findOne(1l);

		ProfileRole profileRole = new ProfileRole();
		profileRole.setEvent(event);
		profileRole.setProfile(user);
		profileRole.setType(ProfileRoleType.SPEAKER);
		user.getListProfileRoles().add(profileRole);
		user = profileRepository.save(user);
		return user;
	}

	@Test
	public void preencheEvent() {
		Event event = this.eventRepository.findOne(1l);
		event.setEventInformation(new EventInformation());
		event.getEventInformation().setContactInfo(new ContactInfo());
		event.setDescrition(
				"Mussum Ipsum, cacilds vidis litro abertis. Praesent lacinia ultrices consectetur. Sed non ipsum felis. In elementis mé pra quem é amistosis quis leo. Aenean aliquam molestie leo, vitae iaculis nisl. Quem num gosta di mim que vai caçá sua turmis!");
		this.eventRepository.save(event);

		LectureDetail l = this.lectureRepository.findOne(3l);
		l.setDescription(event.getDescription());
		this.lectureRepository.save(l);
	}

	@Test
	public void popular() {
		this.profileSurverRepository.deleteAll();
		this.repository.deleteAll();
		Event event = this.createRandomEvent();
		this.configUser(event);
		Survey survey = this.createRandomSurvey(event);

		for (int i = 0; i < 5; i++) {

			SurveyQuestion question = new SurveyQuestion();
			question.setSurvey(survey);
			question.setQuestion(i + ") e agora jose?");

			question.setListSurveyAnswer(new ArrayList<>());
			question = this.surveyQuestionRepository.save(question);

			for (int j = 0; j < 5; j++) {
				SurveyAnswer answer = new SurveyAnswer();
				answer.setAnswer("teste " + (j * 100));
				answer.setSurveyQuestion(question);
				question.getListSurveyAnswer().add(this.surveyAnswerRepository.save(answer));
			}

			question = this.surveyQuestionRepository.save(question);
		}
	}

	private Survey createRandomSurvey(Event event) {
		Survey result = new Survey();
		result.setId(new Long(444));
		result.setEvent(event);
		result.setName(new java.util.Date().toString());
		result = this.repository.save(result);
		return result;
	}

	@Test
	public void deleteAllLecture() {
		List<LectureDetail> all = this.lectureRepository.findAll();

		while (all != null && all.size() > 0) {
			for (LectureDetail lectureDetail : all) {
				if (lectureDetail.getListSubItems() == null || lectureDetail.getListSubItems().size() == 0) {

					this.lectureRepository.delete(lectureDetail);

				}
			}
			all = this.lectureRepository.findAll();
		}

	}

	@Test
	public void addLecturesRaiz() {
		// deleteAllLecture();

		LectureDetail c = this.createLecture(1);

		LectureDetail l = this.createLecture(2);
		l.setListSubItems(new ArrayList<>());
		l.getListSubItems().add(c);
		c.setParentLecture(l);
		this.lectureRepository.save(l);

		this.lectureRepository.save(this.createLecture(3));
		this.lectureRepository.save(this.createLecture(4));
	}

	@Test
	public void preecherDownloadLecture() throws IOException {
		LectureDetail l = this.lectureRepository.findOne(3l);
		l.setListFilesDownloadItem(new ArrayList<>());
		DownloadItem d = new DownloadItem();
		d.setAuthor(this.profileRepository.findOne(1l));
		d.setCreationDate(new Date());
		d.setFileName("file1");
		d.setLectureDetail(l);

		URL website = new URL(
				"http://1.bp.blogspot.com/-B9Nv5t6d7NE/Tcn7Gili_eI/AAAAAAAAABE/-_TWnA-JVTk/w1200-h630-p-k-no-nu/feio.jpg");
		InputStream is = website.openStream();
		ByteArrayOutputStream buffer = new ByteArrayOutputStream();

		int nRead;
		byte[] data = new byte[16384];

		while ((nRead = is.read(data, 0, data.length)) != -1) {
			buffer.write(data, 0, nRead);
		}

		buffer.flush();

		d.setFile(buffer.toByteArray());
		d.setSize(d.getFile().length);

		l.getListFilesDownloadItem().add(d);
		Image i = new Image();
		i.setBytes(d.getFile());
		i.setContentType("image/jpg");

		i = imageRepository.save(i);
		l.setImage(i);

		this.lectureRepository.save(l);
	}

	private LectureDetail createLecture(int i) {
		LectureDetail l = new LectureDetail();

		l.setAuthor(this.profileRepository.findOne(1l));
		l.setCreationDate(new Date());
		l.setDescription(null);
		l.setDetailUrl(null);
		l.setStartTime(new Date(new GregorianCalendar(2017, 4, 13, 8 + i, 30, 0).getTimeInMillis()));
		l.setEndTime(new Date(new GregorianCalendar(2017, 4, 13, 10 + i, 30, 0).getTimeInMillis()));
		l.setEvent(this.eventRepository.findOne(1l));
		l.setListTags(new HashSet<>());
		l.getListTags().add("mago");
		l.getListTags().add("feio");
		l.setTitle("Como ser feio igual ao mago!");
		l.setListSpeakers(new ArrayList<>());
		l.getListSpeakers().add(this.profileRepository.findOne(1l));

		return l;
	}

	@Test
	public void setProfileImage() throws IOException {
		URL website = new URL(
				"https://media.licdn.com/mpr/mpr/shrinknp_200_200/AAEAAQAAAAAAAA1nAAAAJDEwZWE4MzkwLWQ0ZWYtNDdiNi04MTQxLWUyY2IxMTdiYmE0ZQ.jpg");
		InputStream is = website.openStream();
		ByteArrayOutputStream buffer = new ByteArrayOutputStream();

		int nRead;
		byte[] data = new byte[16384];

		while ((nRead = is.read(data, 0, data.length)) != -1) {
			buffer.write(data, 0, nRead);
		}

		buffer.flush();
		Profile p = this.profileRepository.findOne(1l);
		Image i = new Image();
		i.setBytes(buffer.toByteArray());
		i.setContentType("image/jpg");

		i = imageRepository.save(i);
		p.setPicture(i);

		this.profileRepository.save(p);
	}

	private Event createRandomEvent() {
		Event event = new Event();
		event.setName(new java.util.Date().toString());
		event = this.eventRepository.save(event);
		return event;
	}
}
