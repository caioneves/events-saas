package com.eventssaas.apprest.test.survey;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.instanceOf;
import static org.junit.Assert.assertThat;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.WebApplicationContext;

import br.com.eventssaas.apprest.init.WebAppConfig;
import br.com.eventssaas.generalbusiness.entity.Event;
import br.com.eventssaas.generalbusiness.entity.Profile;
import br.com.eventssaas.generalbusiness.entity.ProfileRole;
import br.com.eventssaas.generalbusiness.entity.ProfileSurveyAnswer;
import br.com.eventssaas.generalbusiness.entity.Survey;
import br.com.eventssaas.generalbusiness.entity.SurveyAnswer;
import br.com.eventssaas.generalbusiness.entity.SurveyQuestion;
import br.com.eventssaas.generalbusiness.enums.ProfileRoleType;
import br.com.eventssaas.generalbusiness.exceptions.MissingArgumentException;
import br.com.eventssaas.generalbusiness.exceptions.NotFoundException;
import br.com.eventssaas.generalbusiness.repository.EventRepository;
import br.com.eventssaas.generalbusiness.repository.ProfileRepository;
import br.com.eventssaas.generalbusiness.repository.ProfileSurveyAnswerRepository;
import br.com.eventssaas.generalbusiness.repository.SurveyAnswerRepository;
import br.com.eventssaas.generalbusiness.repository.SurveyQuestionRepository;
import br.com.eventssaas.generalbusiness.repository.SurveyRepository;
import br.com.eventssaas.generalbusiness.services.SurveyService;
import br.com.eventssaas.generalbusiness.util.ProfileUtil;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { WebAppConfig.class })
@WebAppConfiguration
@Transactional
public class SurveyServiceTest {

	private MockMvc mockMvc;

	@Autowired
	private WebApplicationContext webApplicationContext;

	@Autowired
	private EventRepository eventRepository;

	@Autowired
	private SurveyService service;

	@Autowired
	private ProfileUtil profileUtil;

	@Autowired
	private ProfileRepository profileRepository;

	@Autowired
	private SurveyRepository repository;

	@Autowired
	private ProfileSurveyAnswerRepository profileSurverRepository;

	@Autowired
	private SurveyQuestionRepository surveyQuestionRepository;

	@Autowired
	private SurveyAnswerRepository surveyAnswerRepository;

	@Before
	public void setUp() throws Exception {
		this.mockMvc = MockMvcBuilders.webAppContextSetup(this.webApplicationContext).build();
		MockitoAnnotations.initMocks(this);
	}

	private Survey createRandomSurvey(Event event) {
		Survey result = new Survey();
		result.setId(new Long(444));
		result.setEvent(event);
		result.setName(new java.util.Date().toString());
		result = this.repository.save(result);
		return result;
	}

	private Event createRandomEvent() {
		Event event = new Event();
		event.setName(new java.util.Date().toString());
		event = this.eventRepository.save(event);
		return event;
	}

	private Profile configUser(Event event) {
		// CONFIG USER
		this.profileUtil.setUserLoggedInId(2l);

		Profile user = this.profileUtil.getProfileLoggedIn();

		ProfileRole profileRole = new ProfileRole();
		profileRole.setEvent(event);
		profileRole.setProfile(user);
		profileRole.setType(ProfileRoleType.SPEAKER);
		user.getListProfileRoles().add(profileRole);
		user = profileRepository.save(user);
		return user;
	}

	// SERVICE
	/**
	 *
	 * @throws Exception
	 */
	@Test(expected = NotFoundException.class)
	@Rollback
	public void singleSurveyNotFound() throws Exception {
		this.repository.deleteAll();
		Long id = new Long(65465465564L);
		Profile user = this.configUser(this.createRandomEvent());
		this.service.getSurveyFromEvent(id, id, user);
	}

	@Test(expected = MissingArgumentException.class)
	@Rollback
	public void singleSurveyMissingArguments() throws Exception {
		Profile user = this.configUser(this.createRandomEvent());
		this.service.getSurveyFromEvent(null, null, null);
	}

	@Test
	@Rollback
	public void singleSurveyFound() throws NotFoundException, MissingArgumentException {
		Event event = this.createRandomEvent();
		Profile user = this.configUser(event);
		Survey result = this.createRandomSurvey(event);
		ArrayList<Survey> list = new ArrayList<Survey>();
		list.add(result);
		event.setListSurvey(list);
		event = this.eventRepository.save(event);

		assertThat(result, equalTo(this.service.getSurveyFromEvent(event.getId(), result.getId(), user)));
	}

	@Test(expected = MissingArgumentException.class)
	@Rollback
	public void answerQuestionMissesArgument() throws NotFoundException, MissingArgumentException {
		Profile user = this.profileUtil.getProfileLoggedIn();
		Event event = this.createRandomEvent();
		this.service.answerQuestion(null, 321312l, null, user);

	}

	@Test(expected = NotFoundException.class)
	@Rollback
	public void questionNotFound() throws NotFoundException, MissingArgumentException {
		Profile user = this.profileUtil.getProfileLoggedIn();
		Event event = this.createRandomEvent();
		this.service.answerQuestion(123123l, 321312l, event.getId(), user);

	}

	@Test(expected = NotFoundException.class)
	@Rollback
	public void answerNotFound() throws NotFoundException, MissingArgumentException {
		Event event = this.createRandomEvent();
		Profile user = this.configUser(event);
		Survey survey = this.createRandomSurvey(event);
		ProfileSurveyAnswer psurveyAnswer = this.createProfileSurveyAnswer(user, event, survey);
		SurveyQuestion question = new SurveyQuestion();
		question.setSurvey(survey);
		question.setQuestion("e agora jose?");
		question = this.surveyQuestionRepository.save(question);
		psurveyAnswer.setSurveyQuestion(question);
		psurveyAnswer = this.profileSurverRepository.save(psurveyAnswer);
		psurveyAnswer = this.profileSurverRepository.findOne(psurveyAnswer.getId());

		this.service.answerQuestion(psurveyAnswer.getSurveyQuestion().getId(), event.getId(), 321312l, user);

	}

	@Test
	@Rollback
	public void answerQuestion() throws NotFoundException, MissingArgumentException {
		this.profileSurverRepository.deleteAll();

		Event event = this.createRandomEvent();
		Profile user = this.configUser(event);
		Survey survey = this.createRandomSurvey(event);

		SurveyQuestion question = new SurveyQuestion();
		question.setSurvey(survey);
		question.setQuestion("e agora jose?");
		SurveyAnswer answer = new SurveyAnswer();
		answer.setAnswer("teste");
		question.setListSurveyAnswer(new ArrayList<>());
		question.getListSurveyAnswer().add(this.surveyAnswerRepository.save(answer));

		question = this.surveyQuestionRepository.save(question);

		this.service.answerQuestion(question.getId(), question.getListSurveyAnswer().get(0).getId(), event.getId(),
				user);
		user = this.profileUtil.getProfileLoggedIn();
		ProfileSurveyAnswer result = this.profileSurverRepository.findAll().get(0);
		assertThat(result.getSurveyQuestion(), equalTo(question));
		assertThat(result.getSurveyAnswer(), equalTo(answer));

	}

	private ProfileSurveyAnswer createProfileSurveyAnswer(Profile user, Event event, Survey survey) {
		ProfileSurveyAnswer answer = new ProfileSurveyAnswer();
		answer.setAuthor(user);
		answer.setEvent(event);
		answer.setSurvey(survey);
		this.profileSurverRepository.save(answer);

		return answer;
	}

	@Test
	@Rollback
	public void surveysNotAnswered() throws NotFoundException, MissingArgumentException {
		this.profileSurverRepository.deleteAll();
		this.repository.deleteAll();
		Event event = this.createRandomEvent();
		Profile user = this.configUser(event);
		List<Survey> result = null;

		Exception ex = null;
		try {
			result = this.service.getSurveyNotAnswered(user, event.getId(), 0, 10);
		} catch (Exception e) {
			ex = e;
		} finally {
			assertThat(ex, instanceOf(NotFoundException.class));
		}

		Survey survey = this.createRandomSurvey(event);

		SurveyQuestion question = new SurveyQuestion();
		question.setSurvey(survey);
		question.setQuestion("e agora jose?");
		SurveyAnswer answer = new SurveyAnswer();
		answer.setAnswer("teste");
		question.setListSurveyAnswer(new ArrayList<>());
		question.getListSurveyAnswer().add(this.surveyAnswerRepository.save(answer));
		question = this.surveyQuestionRepository.save(question);
		this.service.answerQuestion(question.getId(), question.getListSurveyAnswer().get(0).getId(), event.getId(),
				user);

		ex = null;
		try {
			result = this.service.getSurveyNotAnswered(user, event.getId(), 0, 10);
		} catch (Exception e) {
			ex = e;
		} finally {
			assertThat(ex, instanceOf(NotFoundException.class));
		}
		// assertThat(result.size(), equalTo(0));

		survey = this.createRandomSurvey(event);

		question = new SurveyQuestion();
		question.setSurvey(survey);
		question.setQuestion("e agora jose?");
		answer = new SurveyAnswer();
		answer.setAnswer("teste");
		question.setListSurveyAnswer(new ArrayList<>());
		question.getListSurveyAnswer().add(answer);
		question = this.surveyQuestionRepository.save(question);
		result = this.service.getSurveyNotAnswered(user, event.getId(), 0, 10);

		assertThat(result.size(), equalTo(1));

		// creating a survey in other event
		Event event2 = this.createRandomEvent();
		user = this.configUser(event2);
		survey = addNewSurveyToEvent(event2);
		result = this.service.getSurveyNotAnswered(user, event.getId(), 0, 10);

		assertThat(result.size(), equalTo(1));

		result = this.service.getSurveyNotAnswered(user, survey.getEvent().getId(), 0, 10);

		assertThat(result.size(), equalTo(1));

		addNewSurveyToEvent(event2);
		addNewSurveyToEvent(event2);
		result = this.service.getSurveyNotAnswered(user, survey.getEvent().getId(), 0, 10);

		assertThat(result.size(), equalTo(3));

		Survey lastSurvey = addNewSurveyToEvent(event);
		result = this.service.getSurveyNotAnswered(user, event.getId(), 0, 10);

		assertThat(result.size(), equalTo(2));

		assertThat(result.get(0).getId(), equalTo(lastSurvey.getId()));

	}

	private Survey addNewSurveyToEvent(Event event) {
		Survey survey;
		SurveyQuestion question;
		SurveyAnswer answer;
		survey = this.createRandomSurvey(event);

		question = new SurveyQuestion();
		question.setSurvey(survey);
		question.setQuestion("e agora jose?");
		answer = new SurveyAnswer();
		answer.setAnswer("teste");
		question.setListSurveyAnswer(new ArrayList<>());
		question.getListSurveyAnswer().add(answer);
		question = this.surveyQuestionRepository.save(question);
		return survey;
	}
}
