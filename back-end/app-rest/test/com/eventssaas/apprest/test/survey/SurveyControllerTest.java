package com.eventssaas.apprest.test.survey;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.WebApplicationContext;

import com.google.gson.Gson;

import br.com.eventssaas.apprest.TO.SurveyAnswerTOInput;
import br.com.eventssaas.apprest.controller.SurveyController;
import br.com.eventssaas.apprest.init.WebAppConfig;
import br.com.eventssaas.generalbusiness.entity.Event;
import br.com.eventssaas.generalbusiness.entity.Profile;
import br.com.eventssaas.generalbusiness.entity.ProfileRole;
import br.com.eventssaas.generalbusiness.entity.ProfileSurveyAnswer;
import br.com.eventssaas.generalbusiness.entity.Survey;
import br.com.eventssaas.generalbusiness.entity.SurveyAnswer;
import br.com.eventssaas.generalbusiness.entity.SurveyQuestion;
import br.com.eventssaas.generalbusiness.enums.ProfileRoleType;
import br.com.eventssaas.generalbusiness.exceptions.MissingArgumentException;
import br.com.eventssaas.generalbusiness.exceptions.NotFoundException;
import br.com.eventssaas.generalbusiness.repository.EventRepository;
import br.com.eventssaas.generalbusiness.repository.ProfileRepository;
import br.com.eventssaas.generalbusiness.repository.ProfileSurveyAnswerRepository;
import br.com.eventssaas.generalbusiness.repository.SurveyQuestionRepository;
import br.com.eventssaas.generalbusiness.repository.SurveyRepository;
import br.com.eventssaas.generalbusiness.services.SurveyService;
import br.com.eventssaas.generalbusiness.util.ProfileUtil;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { WebAppConfig.class })
@WebAppConfiguration
@Transactional
public class SurveyControllerTest {

	private MockMvc mockMvc;

	@Autowired
	private WebApplicationContext webApplicationContext;

	@Autowired
	private SurveyController controller;

	@Autowired
	private EventRepository eventRepository;

	@Autowired
	private ProfileRepository profileRepository;

	@Autowired
	private SurveyService service;

	@Autowired
	private SurveyRepository repository;

	@Autowired
	private ProfileSurveyAnswerRepository profileSurverRepository;

	@Autowired
	private SurveyQuestionRepository surveyQuestionRepository;

	@Autowired
	private ProfileUtil profileUtil;

	@Before
	public void setUp() throws Exception {
		this.mockMvc = MockMvcBuilders.webAppContextSetup(this.webApplicationContext).build();
		MockitoAnnotations.initMocks(this);
	}

	private Survey createRandomSurvey(Event event) {
		Survey result = new Survey();
		result.setId(new Long(444));
		result.setEvent(event);
		result.setName(new java.util.Date().toString());
		result = this.repository.save(result);
		return result;
	}

	private Event createRandomEvent() {
		Event event = new Event();
		event.setName(new java.util.Date().toString());
		event = this.eventRepository.save(event);
		return event;
	}

	// CONTROLLER
	@Test
	@Rollback
	public void controllerSurveyNotFound() throws Exception {
		this.repository.deleteAll();
		this.eventRepository.deleteAll();
		Profile user = configUser(this.createRandomEvent());
		this.mockMvc.perform(get("/event/123123/survey/12321")).andExpect(status().isNotFound());
	}

	@Test
	@Rollback
	public void controllerSurveyNotFound2() throws Exception {
		Event event = this.createRandomEvent();
		this.repository.deleteAll();

		this.mockMvc.perform(get("/event/" + event.getId() + "/survey/21312231")).andExpect(status().isNotFound());
	}

	@Test
	@Rollback
	public void controllerSurveyFound() throws Exception {

		Event event = this.createRandomEvent();
		Profile user = configUser(event);
		Survey result = this.createRandomSurvey(event);

		this.mockMvc.perform(get("/event/" + event.getId() + "/survey/" + result.getId())).andExpect(status().isOk());
	}

	@Test
	@Rollback
	public void answerQuestion() throws NotFoundException, MissingArgumentException, Exception {
		this.profileSurverRepository.deleteAll();
		Event event = this.createRandomEvent();

		Profile user = configUser(event);

		Survey survey = this.createRandomSurvey(event);

		SurveyQuestion question = new SurveyQuestion();
		question.setSurvey(survey);
		question.setQuestion("e agora jose?");
		SurveyAnswer answer = new SurveyAnswer();
		answer.setAnswer("teste");
		question.setListSurveyAnswer(new ArrayList<>());
		question.getListSurveyAnswer().add(answer);
		question = this.surveyQuestionRepository.save(question);

		SurveyAnswerTOInput to = new SurveyAnswerTOInput();
		to.setAnswer(question.getListSurveyAnswer().get(0).getId());
		to.setQuestion(question.getId());
		ArrayList<SurveyAnswerTOInput> tos = new ArrayList<>();
		tos.add(to);
		Gson gson = new Gson();
		String content = gson.toJson(tos);

		this.mockMvc.perform(post("/event/" + event.getId() + "/survey/" + survey.getId())
				.contentType(MediaType.APPLICATION_JSON).content(content)).andExpect(status().isOk());

		this.service.answerQuestion(question.getId(), question.getListSurveyAnswer().get(0).getId(), event.getId(),
				user);
		// user = this.profileUtil.getProfileLoggedIn(null);
		ProfileSurveyAnswer result = this.profileSurverRepository.findAll().get(0);
		assertThat(result.getSurveyQuestion(), equalTo(question));
		assertThat(result.getSurveyAnswer(), equalTo(answer));
	}

	private Profile configUser(Event event) {
		// CONFIG USER
		this.profileUtil.setUserLoggedInId(2l);

		Profile user = this.profileUtil.getProfileLoggedIn();

		ProfileRole profileRole = new ProfileRole();
		profileRole.setEvent(event);
		profileRole.setProfile(user);
		profileRole.setType(ProfileRoleType.SPEAKER);
		user.getListProfileRoles().add(profileRole);
		profileRepository.save(user);
		return user;
	}

}