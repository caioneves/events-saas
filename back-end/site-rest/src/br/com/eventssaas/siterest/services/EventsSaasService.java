package br.com.eventssaas.siterest.services;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/service")
public class EventsSaasService {

	@RequestMapping("/")
	public String testService() {
		return "MODULE 	SITE-REST  IS  UP - site-rest";
	}

}