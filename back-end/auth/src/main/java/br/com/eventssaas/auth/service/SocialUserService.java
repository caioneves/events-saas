package br.com.eventssaas.auth.service;

import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.social.connect.ConnectionKey;
import org.springframework.social.security.SocialUserDetails;
import org.springframework.social.security.SocialUserDetailsService;

import br.com.eventssaas.generalbusiness.entity.User;

public interface SocialUserService extends SocialUserDetailsService, UserDetailsService {

	User loadUserByConnectionKey(ConnectionKey connectionKey);

	@Override
	SocialUserDetails loadUserByUserId(String userId);

	@Override
	UserDetails loadUserByUsername(String username);

	void updateUserDetails(User user);
}