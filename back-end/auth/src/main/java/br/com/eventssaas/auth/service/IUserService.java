package br.com.eventssaas.auth.service;

import br.com.eventssaas.generalbusiness.entity.PasswordResetToken;
import br.com.eventssaas.generalbusiness.entity.User;
import br.com.eventssaas.generalbusiness.entity.VerificationToken;
import br.com.eventssaas.generalbusiness.validation.EmailExistsException;

public interface IUserService {

	User create(User user) throws EmailExistsException;

	User findUserByEmail(String email);

	void createPasswordResetTokenForUser(User user, String token);

	PasswordResetToken getPasswordResetToken(String token);

	void changeUserPassword(final User user, final String password, final String passwordConfirmation);

	VerificationToken createVerificationTokenForUser(User user);

	VerificationToken getVerificationToken(String token);

	void save(User user);

	/**
	 * Confirm {@link User} registration.
	 *
	 * <p>
	 * Actions:
	 *
	 * <ul>
	 * Set the {@link VerificationToken} as already verified
	 * </ul>
	 * <ul>
	 * Set the {@link User} as enabled.
	 * </ul>
	 *
	 * @author René Araújo Vasconcelos [renearaujo@gmail.com] - 13 de abr de
	 *         2017 13:14:57
	 *
	 * @param token
	 *            {@link VerificationToken} param for complete the confirmation
	 */
	void confirmUserRegistration(final VerificationToken token);
}
