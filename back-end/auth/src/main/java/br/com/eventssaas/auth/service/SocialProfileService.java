package br.com.eventssaas.auth.service;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.social.connect.Connection;
import org.springframework.social.connect.ConnectionKey;
import org.springframework.social.facebook.api.Facebook;
import org.springframework.social.facebook.api.User;
import org.springframework.social.linkedin.api.LinkedIn;
import org.springframework.social.linkedin.api.LinkedInProfileFull;
import org.springframework.stereotype.Service;

import br.com.eventssaas.generalbusiness.entity.SocialProfile;
import br.com.eventssaas.generalbusiness.repository.SocialProfileRepository;

@Service
@Transactional
class SocialProfileService implements ISocialProfileService {

	@Autowired
	private SocialProfileRepository socialProfileRepository;

	public SocialProfile save(SocialProfile socialProfile) {

		return socialProfileRepository.save(socialProfile);

	}

	@Override
	public SocialProfile save(Connection<?> connection) {

		ConnectionKey key = connection.getKey();

		String userId = key.getProviderId() + "::" + key.getProviderUserId();
		SocialProfile socialProfile = socialProfileRepository.findByUsername(userId);

		if (socialProfile == null || socialProfile.getId() == null) {

			socialProfile = new SocialProfile();

			if (connection.getApi() instanceof Facebook) {
				Facebook facebook = (Facebook) connection.getApi();

				String[] fields = { "id", "email", "first_name", "gender", "last_name", "middle_name", "name" };

				User userProfile = facebook.fetchObject("me", User.class, fields);

				socialProfile.setEmail(userProfile.getEmail());

				socialProfile.setFirstName(userProfile.getFirstName());
				socialProfile.setLastName(userProfile.getMiddleName() + " " + userProfile.getLastName());
			} else if (connection.getApi() instanceof LinkedIn) {

				LinkedIn linkedin = (LinkedIn) connection.getApi();

				LinkedInProfileFull userProfile = linkedin.profileOperations().getUserProfileFull();

				socialProfile.setEmail(userProfile.getEmailAddress());
				socialProfile.setFirstName(userProfile.getFirstName());
				socialProfile.setLastName(userProfile.getLastName());
			}

			socialProfile.setUsername(userId);
			socialProfile.setUserId(key.getProviderUserId());
			socialProfile.setDisplayName(connection.getDisplayName());
			socialProfile.setImageUrl(connection.getImageUrl());
			socialProfile.setProfileUrl(connection.getProfileUrl());
			socialProfile.setProviderId(key.getProviderId());
			socialProfile.setProviderUserId(key.getProviderUserId());

			return socialProfileRepository.save(socialProfile);
		}

		return socialProfile;

	}

	@Override
	public SocialProfile findByUsername(String username) {
		return this.socialProfileRepository.findByUsername(username);
	}

	@Override
	public SocialProfile findByUserId(String userId) {
		return this.socialProfileRepository.findByUserId(userId);
	}

	@Override
	public SocialProfile findByProviderIdAndProviderUserId(String providerId, String providerUserId) {
		return this.findByProviderIdAndProviderUserId(providerId, providerUserId);
	}
}
