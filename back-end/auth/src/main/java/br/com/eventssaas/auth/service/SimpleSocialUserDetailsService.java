package br.com.eventssaas.auth.service;

import org.springframework.dao.DataAccessException;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.social.security.SocialUserDetails;
import org.springframework.social.security.SocialUserDetailsService;

import br.com.eventssaas.generalbusiness.repository.SocialProfileRepository;

public class SimpleSocialUserDetailsService implements SocialUserDetailsService {

	private SocialProfileRepository socialProfileRepository;

	public SimpleSocialUserDetailsService(SocialProfileRepository socialProfileRepository) {
		this.socialProfileRepository = socialProfileRepository;
	}

	@Override
	public SocialUserDetails loadUserByUserId(String userId) throws UsernameNotFoundException, DataAccessException {
		return socialProfileRepository.findByUserId(userId);
	}
}