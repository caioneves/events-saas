package br.com.eventssaas.auth.service;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import br.com.eventssaas.generalbusiness.entity.PasswordResetToken;
import br.com.eventssaas.generalbusiness.entity.User;
import br.com.eventssaas.generalbusiness.entity.VerificationToken;
import br.com.eventssaas.generalbusiness.repository.PasswordResetTokenRepository;
import br.com.eventssaas.generalbusiness.repository.UserRepository;
import br.com.eventssaas.generalbusiness.repository.VerificationTokenRepository;
import br.com.eventssaas.generalbusiness.validation.EmailExistsException;

@Service("br.com.eventssaas.auth.service.UserService")
@Transactional
class UserService implements IUserService {

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private VerificationTokenRepository verificationTokenRepository;

	@Autowired
	private PasswordResetTokenRepository passwordTokenRepository;

	@Autowired
	private PasswordEncoder passwordEncoder;

	@Override
	public User create(final User user) throws EmailExistsException {

		if (emailExist(user.getEmail())) {
			throw new EmailExistsException("Existe uma conta com este email: " + user.getEmail());
		}

		return userRepository.save(user);
	}

	@Override
	public User findUserByEmail(final String email) {
		return userRepository.findByEmailIgnoreCase(email);
	}

	@Override
	public void createPasswordResetTokenForUser(final User user, final String token) {
		final PasswordResetToken myToken = new PasswordResetToken(token, user);
		passwordTokenRepository.save(myToken);
	}

	@Override
	public PasswordResetToken getPasswordResetToken(final String token) {
		return passwordTokenRepository.findByToken(token);
	}

	@Override
	public void changeUserPassword(final User user, final String password, final String passwordConfirmation) {

		User userDB = this.findUserByEmail(user.getEmail());

		// encoding the password
		userDB.setPassword(passwordEncoder.encode(userDB.getPassword()));
		userDB.setPassword(password);
		userDB.setPasswordConfirmation(passwordConfirmation);

		userRepository.save(userDB);
	}

	@Override
	public VerificationToken createVerificationTokenForUser(final User user) {
		final VerificationToken myToken = new VerificationToken(user);
		return verificationTokenRepository.save(myToken);
	}

	@Override
	public VerificationToken getVerificationToken(final String token) {
		return verificationTokenRepository.findByToken(token);
	}

	@Override
	public void save(final User user) {
		userRepository.save(user);
	}

	private boolean emailExist(final String email) {
		final User user = userRepository.findByEmailIgnoreCase(email);
		return user != null;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.eventssaas.auth.service.IUserService#confirmUserRegistration(br.
	 * com.eventssaas.generalbusiness.entity.VerificationToken)
	 */
	@Override
	public void confirmUserRegistration(final VerificationToken token) {

		VerificationToken verificationTokenDB = this.verificationTokenRepository.findByToken(token.getToken());

		if (verificationTokenDB.isVerified()) {
			return;
		}

		verificationTokenDB.setVerified(true);
		verificationTokenRepository.save(verificationTokenDB);

		User userDB = this.findUserByEmail(verificationTokenDB.getUser().getEmail());
		// FIXME: ConstraintViolationImpl{interpolatedMessage='Confirmação de
		// senha é Obrigatória.', propertyPath=passwordConfirmation
		userDB.setPasswordConfirmation(userDB.getPassword());
		userDB.setEnabled(true);

		this.userRepository.save(userDB);
	}

}
