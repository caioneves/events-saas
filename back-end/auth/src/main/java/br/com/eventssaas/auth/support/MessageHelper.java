package br.com.eventssaas.auth.support;

import static br.com.eventssaas.auth.support.Message.MESSAGE_ATTRIBUTE;

import org.springframework.ui.Model;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

/**
 * Utility class to help about messages to be presented on pages
 *
 * @author René Araújo Vasconcelos [renearaujo@gmail.com] - 13 de abr de 2017
 *         16:04:25
 */
public final class MessageHelper {

	/**
	 * Default Constructor
	 *
	 * @author René Araújo Vasconcelos [renearaujo@gmail.com] - 13 de abr de
	 *         2017 16:04:42
	 */
	private MessageHelper() {
	}

	public static void addSuccessAttribute(RedirectAttributes ra, String message, Object... args) {
		addAttribute(ra, message, Message.Type.SUCCESS, args);
	}

	public static void addErrorAttribute(RedirectAttributes ra, String message, Object... args) {
		addAttribute(ra, message, Message.Type.DANGER, args);
	}

	public static void addInfoAttribute(RedirectAttributes ra, String message, Object... args) {
		addAttribute(ra, message, Message.Type.INFO, args);
	}

	public static void addWarningAttribute(RedirectAttributes ra, String message, Object... args) {
		addAttribute(ra, message, Message.Type.WARNING, args);
	}

	private static void addAttribute(RedirectAttributes ra, String message, Message.Type type, Object... args) {
		ra.addFlashAttribute(MESSAGE_ATTRIBUTE, new Message(message, type, args));
	}

	public static void addSuccessAttribute(Model model, String message, Object... args) {
		addAttribute(model, message, Message.Type.SUCCESS, args);
	}

	public static void addErrorAttribute(Model model, String message, Object... args) {
		addAttribute(model, message, Message.Type.DANGER, args);
	}

	public static void addInfoAttribute(Model model, String message, Object... args) {
		addAttribute(model, message, Message.Type.INFO, args);
	}

	public static void addWarningAttribute(Model model, String message, Object... args) {
		addAttribute(model, message, Message.Type.WARNING, args);
	}

	private static void addAttribute(Model model, String message, Message.Type type, Object... args) {
		model.addAttribute(MESSAGE_ATTRIBUTE, new Message(message, type, args));
	}
}
