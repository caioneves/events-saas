package br.com.eventssaas.auth.security;

import java.util.Arrays;
import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import br.com.eventssaas.generalbusiness.entity.User;
import br.com.eventssaas.generalbusiness.repository.UserRepository;

@Component
public class EventsSaaSUserDetailsService implements UserDetailsService {

	private static final String ROLE_USER = "ROLE_USER";

	@Autowired
	private UserRepository userRepository;

	@Override
	public UserDetails loadUserByUsername(final String email) throws UsernameNotFoundException {
		final User user = userRepository.findByEmailIgnoreCase(email);

		if (user == null) {
			throw new UsernameNotFoundException("Não existem usuários cadastrados com o email - " + email);
		}

		return new org.springframework.security.core.userdetails.User(user.getEmail(), user.getPassword(), true, true, true, true, getAuthorities(ROLE_USER));
	}

	private Collection<? extends GrantedAuthority> getAuthorities(String role) {
		return Arrays.asList(new SimpleGrantedAuthority(role));
	}

}