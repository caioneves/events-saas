package br.com.eventssaas.sitefrontend.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.security.web.savedrequest.HttpSessionRequestCache;
import org.springframework.social.UserIdSource;
import org.springframework.social.config.annotation.ConnectionFactoryConfigurer;
import org.springframework.social.config.annotation.EnableSocial;
import org.springframework.social.config.annotation.SocialConfigurer;
import org.springframework.social.connect.ConnectionFactoryLocator;
import org.springframework.social.connect.ConnectionRepository;
import org.springframework.social.connect.UsersConnectionRepository;
import org.springframework.social.connect.web.ConnectController;
import org.springframework.social.connect.web.ProviderSignInController;
import org.springframework.social.facebook.connect.FacebookConnectionFactory;
import org.springframework.social.linkedin.connect.LinkedInConnectionFactory;
import org.springframework.social.security.AuthenticationNameUserIdSource;

import br.com.eventssaas.sitefrontend.service.ISocialProfileService;
import br.com.eventssaas.sitefrontend.service.SimpleSignInAdapter;

@Configuration
@EnableSocial
@PropertySource("classpath:social.properties")
public class SocialConfig implements SocialConfigurer {

	@Autowired
	private ISocialProfileService socialProfileService;

	@Override
	public void addConnectionFactories(ConnectionFactoryConfigurer connectionConfigurer, Environment env) {

		// SETUP FACEBOOK CONNECTION
		FacebookConnectionFactory facebookConnection = new FacebookConnectionFactory(
				env.getProperty("spring.social.facebook.appId"), env.getProperty("spring.social.facebook.appSecret"));
		facebookConnection.setScope("public_profile, email");

		// SETUP FACEBOOK CONNECTION
		LinkedInConnectionFactory linkedinConnection = new LinkedInConnectionFactory(
				env.getProperty("spring.social.linkedin.app-id"), env.getProperty("spring.social.linkedin.app-secret"));

		connectionConfigurer.addConnectionFactory(facebookConnection);
		connectionConfigurer.addConnectionFactory(linkedinConnection);

	}

	@Bean
	public ConnectController connectController(ConnectionFactoryLocator connectionFactoryLocator,
			ConnectionRepository connectionRepository) {
		return new ConnectController(connectionFactoryLocator, connectionRepository);
	}

	@Override
	public UserIdSource getUserIdSource() {
		return new AuthenticationNameUserIdSource();
	}

	@Override
	public UsersConnectionRepository getUsersConnectionRepository(ConnectionFactoryLocator connectionFactoryLocator) {
		final EventsSaaSUserConnectionRepository eventsSaaSUserConnectionRepository = new EventsSaaSUserConnectionRepository(
				this.socialProfileService);
		return eventsSaaSUserConnectionRepository;
	}

	@Bean
	public ProviderSignInController providerSignInController(ConnectionFactoryLocator connectionFactoryLocator,
			UsersConnectionRepository usersConnectionRepository) {
		return new ProviderSignInController(connectionFactoryLocator, usersConnectionRepository,
				new SimpleSignInAdapter(new HttpSessionRequestCache()));
	}

}