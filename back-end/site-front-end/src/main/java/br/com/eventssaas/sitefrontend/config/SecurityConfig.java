package br.com.eventssaas.sitefrontend.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.social.security.SocialUserDetailsService;
import org.springframework.social.security.SpringSocialConfigurer;

import br.com.eventssaas.generalbusiness.repository.SocialProfileRepository;
import br.com.eventssaas.sitefrontend.security.EventsSaaSAuthenticationProvider;
import br.com.eventssaas.sitefrontend.security.EventsSaaSUserDetailsService;
import br.com.eventssaas.sitefrontend.service.SimpleSocialUserDetailsService;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(securedEnabled = true)
public class SecurityConfig extends WebSecurityConfigurerAdapter {

	@Autowired
	private EventsSaaSUserDetailsService userDetailsService;

	@Autowired
	private SocialProfileRepository socialProfileRepository;

	public SecurityConfig() {
		super();
	}

	@Autowired
	public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
		auth.authenticationProvider(eventsSaaSAuthProvider());
		// auth.userDetailsService(userDetailsService).passwordEncoder(passwordEncoder());
	}

	@Override
	protected void configure(HttpSecurity http) throws Exception { // @formatter:off

		http.authorizeRequests()
				// ONLY PERMITED WHEN AUTHENTICATED
				.antMatchers("/signup", "/user/register", "/user/registerProfile", "/registrationConfirm*", "/badUser*",
						"/forgotPassword*", "/user/resetPassword*", "/user/changePassword*", "/user/savePassword*",
						"/qrcode*", "/js/**", "/css/**")
				.permitAll().anyRequest().authenticated()
				// Adds the SocialAuthenticationFilter to Spring Security's
				// filter chain.
				.and().apply(new SpringSocialConfigurer()).postLoginUrl("/signup").alwaysUsePostLoginUrl(true)
				// LOGIN PAGE PERMITTED TO ALL
				.and().formLogin().loginPage("/login").permitAll().loginProcessingUrl("/doLogin")
				// TOKEN VALIDATION TIME --> 1 WEEK = 604800
				.and().rememberMe().key("eventsSaaSAppKey").tokenValiditySeconds(604800)
				// LOGOUT
				.and().logout().permitAll().logoutUrl("/logout")
				// Cross-site request forgery DISABLED
				.and().csrf().disable();

	}
	// @formatter:on

	/*
	 * @Bean public AuthenticationProvider authenticationProvider() {
	 * EventsSaaSAuthenticationProvider authenticationProvider = new
	 * EventsSaaSAuthenticationProvider();
	 * authenticationProvider.setUserDetailsService(userDetailsService);
	 * authenticationProvider.setPasswordEncoder(encoder()); return
	 * authenticationProvider; }
	 */

	@Bean
	public DaoAuthenticationProvider eventsSaaSAuthProvider() {
		final EventsSaaSAuthenticationProvider eventsSaaSAuthenticationProvider = new EventsSaaSAuthenticationProvider();
		eventsSaaSAuthenticationProvider.setUserDetailsService(this.userDetailsService);
		eventsSaaSAuthenticationProvider.setPasswordEncoder(passwordEncoder());
		return eventsSaaSAuthenticationProvider;
	}

	@Bean
	public PasswordEncoder passwordEncoder() {
		return new BCryptPasswordEncoder(12);
	}

	@Bean
	public SocialUserDetailsService socialUserDetailsService() {
		return new SimpleSocialUserDetailsService(this.socialProfileRepository);
	}

}
