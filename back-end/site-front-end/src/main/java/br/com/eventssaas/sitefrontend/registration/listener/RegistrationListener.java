package br.com.eventssaas.sitefrontend.registration.listener;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

import br.com.eventssaas.generalbusiness.entity.User;
import br.com.eventssaas.generalbusiness.entity.VerificationToken;
import br.com.eventssaas.generalbusiness.services.IMailService;
import br.com.eventssaas.generalbusiness.services.IUserService;
import br.com.eventssaas.sitefrontend.registration.OnRegistrationCompleteEvent;

@Component
public class RegistrationListener implements ApplicationListener<OnRegistrationCompleteEvent> {

	@Autowired
	private IUserService service;

	@Autowired
	private IMailService mailService;

	@Override
	public void onApplicationEvent(final OnRegistrationCompleteEvent event) {
		this.confirmRegistration(event);
	}

	private void confirmRegistration(final OnRegistrationCompleteEvent event) {

		final User user = event.getUser();

		VerificationToken verificationToken = this.service.createVerificationTokenForUser(user);

		final String confirmationUrl = event.getAppUrl() + "/registrationConfirm?token=" + verificationToken.getToken();

		this.mailService.sendEmailConfirmationSignUp(user, confirmationUrl);
	}

}