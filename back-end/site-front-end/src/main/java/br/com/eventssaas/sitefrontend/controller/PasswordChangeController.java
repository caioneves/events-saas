package br.com.eventssaas.sitefrontend.controller;

import java.util.Calendar;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import br.com.eventssaas.generalbusiness.entity.PasswordResetToken;
import br.com.eventssaas.generalbusiness.entity.User;
import br.com.eventssaas.generalbusiness.exceptions.NotFoundException;
import br.com.eventssaas.generalbusiness.services.IMailService;
import br.com.eventssaas.generalbusiness.services.IUserService;
import br.com.eventssaas.sitefrontend.support.MessageHelper;

@Controller
class PasswordChangeController {

	@Autowired
	private IUserService userService;

	@Autowired
	private IMailService mailService;

	@Autowired
	private UserDetailsService userDetailsService;

	@RequestMapping(value = "/user/resetPassword", method = RequestMethod.POST)
	public ModelAndView resetPassword(final HttpServletRequest request, @RequestParam("email") final String userEmail,
			final RedirectAttributes redirectAttributes) {

		try {

			this.userService.forgetPassword(userEmail);

			MessageHelper.addSuccessAttribute(redirectAttributes, "message.reset.password.success");
			return new ModelAndView("redirect:/login");

		} catch (NotFoundException e) {
			MessageHelper.addErrorAttribute(redirectAttributes, e.getMessage(), userEmail);

			return null;
		} catch (Exception e) {

			MessageHelper.addErrorAttribute(redirectAttributes, "message.reset.password.server.error", userEmail,
					e.getMessage());
			return null;
		}

	}

	@RequestMapping(value = "/user/changePassword", method = RequestMethod.GET)
	public ModelAndView showChangePasswordPage(@RequestParam("id") final long id,
			@RequestParam("token") final String token, final RedirectAttributes redirectAttributes) {
		final PasswordResetToken passToken = this.userService.getPasswordResetToken(token);
		if (passToken == null) {
			redirectAttributes.addFlashAttribute("errorMessage", "Invalid password reset token");
			return new ModelAndView("redirect:/login");
		}
		final User user = passToken.getUser();
		if (user.getId() != id) {
			redirectAttributes.addFlashAttribute("errorMessage", "Invalid password reset token");
			return new ModelAndView("redirect:/login");
		}

		final Calendar cal = Calendar.getInstance();
		if ((passToken.getExpiryDate().getTime() - cal.getTime().getTime()) <= 0) {
			redirectAttributes.addFlashAttribute("errorMessage", "Your password reset token has expired");
			return new ModelAndView("redirect:/login");
		}

		final Authentication auth = new UsernamePasswordAuthenticationToken(user, null,
				this.userDetailsService.loadUserByUsername(user.getEmail()).getAuthorities());
		SecurityContextHolder.getContext().setAuthentication(auth);

		return new ModelAndView("resetPassword");
	}

	@RequestMapping(value = "/user/savePassword", method = RequestMethod.POST)
	public ModelAndView savePassword(@RequestParam("password") final String password,
			@RequestParam("passwordConfirmation") final String passwordConfirmation,
			final RedirectAttributes redirectAttributes) {

		if (!password.equals(passwordConfirmation)) {
			MessageHelper.addSuccessAttribute(redirectAttributes, "message.change.password.notmatch");
			// return new ModelAndView("resetPassword",
			// ImmutableMap.of("errorMessage", "Passwords do not match"));
			return null;
		}

		final User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

		this.userService.changeUserPassword(user, password, passwordConfirmation);

		MessageHelper.addSuccessAttribute(redirectAttributes, "message.change.password.sucess");

		return new ModelAndView("redirect:/login");
	}
}
