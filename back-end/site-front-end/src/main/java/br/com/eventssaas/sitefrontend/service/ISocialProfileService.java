package br.com.eventssaas.sitefrontend.service;

import org.springframework.social.connect.Connection;

import br.com.eventssaas.generalbusiness.entity.SocialProfile;

public interface ISocialProfileService {

	SocialProfile findByUsername(String username);

	SocialProfile findByUserId(String userId);

	SocialProfile findByProviderIdAndProviderUserId(String providerId, String providerUserId);

	SocialProfile save(final Connection<?> connection);
	
	SocialProfile save(SocialProfile socialProfile);

}
