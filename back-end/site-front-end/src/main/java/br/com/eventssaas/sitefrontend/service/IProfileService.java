package br.com.eventssaas.sitefrontend.service;

import br.com.eventssaas.generalbusiness.entity.Profile;
import br.com.eventssaas.generalbusiness.entity.SocialProfile;
import br.com.eventssaas.generalbusiness.entity.VerificationToken;
import br.com.eventssaas.generalbusiness.validation.EmailExistsException;

public interface IProfileService {

	Profile saveParticipant(final Profile profile, final SocialProfile socialProfile) throws EmailExistsException;

	Profile confirmRegistration(VerificationToken verificationToken);

}
