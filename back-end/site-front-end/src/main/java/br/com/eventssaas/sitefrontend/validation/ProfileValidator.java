package br.com.eventssaas.sitefrontend.validation;

import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import br.com.eventssaas.generalbusiness.entity.Profile;

public class ProfileValidator implements Validator {

	@Override
	public boolean supports(Class<?> clazz) {
		return Profile.class.equals(clazz);
	}

	@Override
	public void validate(Object target, Errors errors) {

		// VALIDATION FROM PROFILE
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "firstName", "message.profile.firstname.required");

		// VALIDATION FROM USER
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "user.email", "message.user.email.required");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "user.password", "message.user.password.required");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "user.passwordConfirmation",
				"message.user.passwordconfirmation.required");

	}

}