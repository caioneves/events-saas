package br.com.eventssaas.sitefrontend.registration;

import org.springframework.context.ApplicationEvent;

import br.com.eventssaas.generalbusiness.entity.User;

public class OnRegistrationCompleteEvent extends ApplicationEvent {

	/**
	 *
	 */
	private static final long serialVersionUID = 8487962707302307952L;
	private final String appUrl;
	private final User user;

	public OnRegistrationCompleteEvent(final User user, final String appUrl) {
		super(user);
		this.user = user;
		this.appUrl = appUrl;
	}

	//

	public String getAppUrl() {
		return this.appUrl;
	}

	public User getUser() {
		return this.user;
	}

}
