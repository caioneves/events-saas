package br.com.eventssaas.sitefrontend.config;

import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.social.connect.Connection;
import org.springframework.social.connect.ConnectionKey;
import org.springframework.social.connect.ConnectionRepository;
import org.springframework.social.connect.UsersConnectionRepository;
import org.springframework.util.MultiValueMap;

import com.google.common.collect.Lists;
import com.google.common.collect.Sets;

import br.com.eventssaas.generalbusiness.entity.SocialProfile;
import br.com.eventssaas.sitefrontend.service.ISocialProfileService;

public class EventsSaaSUserConnectionRepository implements UsersConnectionRepository {

	private ISocialProfileService socialProfileService;

	@Autowired
	public EventsSaaSUserConnectionRepository(ISocialProfileService socialProfileService) {
		this.socialProfileService = socialProfileService;
	}

	@Override
	public List<String> findUserIdsWithConnection(Connection<?> connection) {

		ConnectionKey key = connection.getKey();

		String userId = key.getProviderId() + "::" + key.getProviderUserId();
		SocialProfile user = this.socialProfileService.findByUsername(userId);

		if (user == null) {
			SocialProfile socialProfile = this.socialProfileService.save(connection);
			return Lists.newArrayList(socialProfile.getProviderUserId());
		}

		return Lists.newArrayList(key.getProviderUserId());
	}

	@Override
	public Set<String> findUserIdsConnectedTo(String providerId, Set<String> providerUserIds) {
		return Sets.newHashSet();
	}

	@Override
	public ConnectionRepository createConnectionRepository(String userId) {
		if (userId == null) {
			throw new IllegalArgumentException("userId cannot be null");
		}
		return new ConnectionRepository() {
			@Override
			public void updateConnection(Connection<?> connection) {
			}

			@Override
			public void removeConnections(String providerId) {
			}

			@Override
			public void removeConnection(ConnectionKey connectionKey) {
			}

			@Override
			public <A> Connection<A> getPrimaryConnection(Class<A> apiType) {
				return null;
			}

			@Override
			public <A> Connection<A> getConnection(Class<A> apiType, String providerUserId) {
				return null;
			}

			@Override
			public Connection<?> getConnection(ConnectionKey connectionKey) {
				return null;
			}

			@Override
			public <A> Connection<A> findPrimaryConnection(Class<A> apiType) {
				return null;
			}

			@Override
			public MultiValueMap<String, Connection<?>> findConnectionsToUsers(
					MultiValueMap<String, String> providerUserIds) {
				return null;
			}

			@Override
			public <A> List<Connection<A>> findConnections(Class<A> apiType) {
				return null;
			}

			@Override
			public List<Connection<?>> findConnections(String providerId) {
				return null;
			}

			@Override
			public MultiValueMap<String, Connection<?>> findAllConnections() {
				return null;
			}

			@Override
			public void addConnection(Connection<?> connection) {
			}
		};
	}
}