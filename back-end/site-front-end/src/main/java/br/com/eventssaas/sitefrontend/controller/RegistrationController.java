package br.com.eventssaas.sitefrontend.controller;

import java.io.IOException;
import java.util.Calendar;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import br.com.eventssaas.generalbusiness.entity.Profile;
import br.com.eventssaas.generalbusiness.entity.SocialProfile;
import br.com.eventssaas.generalbusiness.entity.User;
import br.com.eventssaas.generalbusiness.entity.VerificationToken;
import br.com.eventssaas.generalbusiness.enums.MailBasicTemplateType;
import br.com.eventssaas.generalbusiness.services.IMailService;
import br.com.eventssaas.generalbusiness.services.IUserService;
import br.com.eventssaas.generalbusiness.validation.EmailExistsException;
import br.com.eventssaas.sitefrontend.registration.OnRegistrationCompleteEvent;
import br.com.eventssaas.sitefrontend.registration.listener.RegistrationListener;
import br.com.eventssaas.sitefrontend.service.IProfileService;
import br.com.eventssaas.sitefrontend.support.MessageHelper;
import br.com.eventssaas.sitefrontend.validation.ProfileValidator;

@Controller
class RegistrationController {

	@Autowired
	private ApplicationEventPublisher eventPublisher;

	@Autowired
	private IUserService userService;

	@Autowired
	private IProfileService profileService;

	@Autowired
	private IMailService mailService;

	@RequestMapping(value = "signup")
	public ModelAndView registrationForm() {

		Profile profile = new Profile();
		profile.setUser(new User());

		final Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

		if (authentication.getPrincipal() != null && authentication.getPrincipal() instanceof SocialProfile) {

			SocialProfile socialProfileDB = (SocialProfile) authentication.getPrincipal();

			profile.setFirstName(socialProfileDB.getFirstName());
			profile.setLastName(socialProfileDB.getFirstName());
			profile.getUser().setEmail(socialProfileDB.getEmail());
		}

		return new ModelAndView("registrationProfile", "profile", profile);
	}

	@RequestMapping(value = "user/registerProfile")
	public ModelAndView registerProfile(final Profile profile, final BindingResult result,
			final HttpServletRequest request, final RedirectAttributes redirectAttributes) {

		final String VIEW_NAME = "registrationProfile";

		ProfileValidator profileValidator = new ProfileValidator();
		profileValidator.validate(profile, result);

		if (result.hasErrors()) {
			return new ModelAndView(VIEW_NAME, "profile", profile);
		}
		try {

			final Object userPrincipal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
			final SocialProfile user = (userPrincipal != null && userPrincipal instanceof SocialProfile)
					? (SocialProfile) userPrincipal : null;

			final Profile profileDB = this.profileService.saveParticipant(profile, user);

			createConfirmation(request, profileDB.getUser());
			MessageHelper.addSuccessAttribute(redirectAttributes, "register.user.success");

			if (userPrincipal != null && userPrincipal instanceof SocialProfile) {
				request.logout();
			}

			return new ModelAndView("redirect:/login");
		} catch (EmailExistsException | ServletException e) {

			result.addError(new FieldError("profile", "user.email", e.getMessage()));

			return new ModelAndView(VIEW_NAME, "profile", profile);
		}

	}

	/**
	 * Setup and create the confirmation for the {@link User} registration
	 *
	 *
	 * @author René Araújo Vasconcelos [renearaujo@gmail.com] - 13 de abr de
	 *         2017 13:26:29
	 *
	 * @see RegistrationListener#onApplicationEvent(OnRegistrationCompleteEvent)
	 *
	 * @param request
	 * @param registered
	 */
	private void createConfirmation(final HttpServletRequest request, final User registered) {
		final String appUrl = "http://" + request.getServerName() + ":" + request.getServerPort()
				+ request.getContextPath();
		this.eventPublisher.publishEvent(new OnRegistrationCompleteEvent(registered, appUrl));
	}

	/**
	 * Confirm the user registration by url sent via email
	 *
	 * @author René Araújo Vasconcelos [renearaujo@gmail.com] - 13 de abr de
	 *         2017 15:25:11
	 *
	 * @param token
	 * @param request
	 * @param redirectAttributes
	 * @return
	 * @throws IOException
	 */
	@RequestMapping(value = "/registrationConfirm")
	public ModelAndView confirmRegistration(@RequestParam("token") final String token, final HttpServletRequest request,
			final RedirectAttributes redirectAttributes) throws IOException {
		final String REDIRECT = "redirect:/login";

		// CHECK IF TOKEN EXISTS

		// find the toke in database
		final VerificationToken verificationToken = this.userService.getVerificationToken(token);
		// verify if token exists
		if (verificationToken == null) {
			// add error message about invalid token
			MessageHelper.addErrorAttribute(redirectAttributes, "message.confirmation.account.token.invalid");
			return new ModelAndView(REDIRECT);
		}

		// CHECK IF TOKEN IS EXPIRED

		final User user = verificationToken.getUser();
		final Calendar cal = Calendar.getInstance();
		// verify the expiry date
		if ((verificationToken.getExpiryDate().getTime() - cal.getTime().getTime()) <= 0) {
			// resend a new confirmation
			createConfirmation(request, user);
			// add error message about token expiration
			MessageHelper.addErrorAttribute(redirectAttributes, "message.confirmation.account.token.expired");
			// return to login page
			return new ModelAndView(REDIRECT);
		}

		// CHECK IF TOKEN ALREADY VERIFIED

		// verify if user already verified your registration
		else if (verificationToken.isVerified()) {
			// add error message about user already verified your registration
			MessageHelper.addErrorAttribute(redirectAttributes, "message.confirmation.account.token.already.verified");
			// return to login page
			return new ModelAndView(REDIRECT);
		}

		// CONFIRM USER REGISTRATION

		// confirm the registration
		Profile profile = this.profileService.confirmRegistration(verificationToken);

		this.mailService.sendEmailQRCodeAndPasswordIfExist(MailBasicTemplateType.CREATE_USER,
				profile.getUser().getEmail(), profile.getFirstName(), null, profile.getQrcode());

		// add success message
		MessageHelper.addSuccessAttribute(redirectAttributes, "message.confirmation.account.verified");
		// return to login page
		return new ModelAndView(REDIRECT);
	}
}
