package br.com.eventssaas.sitefrontend.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;

import br.com.eventssaas.generalbusiness.entity.User;
import br.com.eventssaas.generalbusiness.repository.UserRepository;

public class EventsSaaSAuthenticationProvider extends DaoAuthenticationProvider {

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private MessageSource messageSource;

	@Override
	public Authentication authenticate(Authentication auth) throws AuthenticationException {

		final User user = this.userRepository.findByEmailIgnoreCase(auth.getName());

		if ((user == null)) {
			throw new BadCredentialsException(this.messageSource.getMessage("message.login.user.notfound", null,
					LocaleContextHolder.getLocale()));
		}

		if (!user.isEnabled()) {
			throw new BadCredentialsException(this.messageSource.getMessage("message.login.user.disabled", null,
					LocaleContextHolder.getLocale()));
		}
		final Authentication result = super.authenticate(auth);

		return new UsernamePasswordAuthenticationToken(user, result.getCredentials(), result.getAuthorities());
	}

	@Override
	public boolean supports(Class<?> authentication) {
		return authentication.equals(UsernamePasswordAuthenticationToken.class);
	}

}