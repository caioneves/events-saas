package br.com.eventssaas.sitefrontend.service;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import br.com.eventssaas.generalbusiness.entity.ContactInfo;
import br.com.eventssaas.generalbusiness.entity.Profile;
import br.com.eventssaas.generalbusiness.entity.SocialProfile;
import br.com.eventssaas.generalbusiness.entity.User;
import br.com.eventssaas.generalbusiness.entity.VerificationToken;
import br.com.eventssaas.generalbusiness.repository.ContactInfoRepository;
import br.com.eventssaas.generalbusiness.repository.ProfileRepository;
import br.com.eventssaas.generalbusiness.services.IUserService;
import br.com.eventssaas.generalbusiness.util.QRCodeGenerator;
import br.com.eventssaas.generalbusiness.validation.EmailExistsException;

@Service
@Transactional
class ProfileService implements IProfileService {

	@Autowired
	private ProfileRepository profileRepository;

	@Autowired
	private ContactInfoRepository contactInfoRepository;

	@Autowired
	private IUserService userService;

	@Autowired
	private ISocialProfileService socialProfileService;

	@Autowired
	private PasswordEncoder passwordEncoder;

	@Override
	public Profile saveParticipant(final Profile profile, SocialProfile socialProfile) throws EmailExistsException {

		// encoding the password
		profile.getUser().setPassword(this.passwordEncoder.encode(profile.getUser().getPassword()));
		// save the user
		final User userDB = this.userService.create(profile.getUser());

		// TODO: REMOVER APOS ATUALIZAR A TELA
		profile.setContactInfo(new ContactInfo());

		profile.setUser(userDB);
		profile.getContactInfo().setEmail(userDB.getEmail());

		if (socialProfile != null) {
			socialProfile = this.socialProfileService.findByUsername(socialProfile.getUsername());
			if (!socialProfile.getEmail().equals(userDB.getEmail())) {
				profile.getContactInfo().setEmail(socialProfile.getEmail());
			}

		}

		final ContactInfo contactInfoDB = this.contactInfoRepository.save(profile.getContactInfo());

		profile.setContactInfo(contactInfoDB);

		final Profile profileDB = this.profileRepository.save(profile);

		if (socialProfile != null) {

			socialProfile.setProfile(profileDB);
			this.socialProfileService.save(socialProfile);
		}

		return profileDB;
	}

	@Override
	public Profile confirmRegistration(final VerificationToken verificationToken) {

		User userDB = this.userService.confirmUserRegistration(verificationToken);

		Profile profile = this.profileRepository.findByUser(userDB);

		try {
			profile.setQrcode(QRCodeGenerator.qrCodePNGBase64(profile.getId().toString()));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		this.profileRepository.save(profile);

		return profile;
	}

}
