package br.com.eventssaas.adminrest.services;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/service")
public class EventsSaasService {

	@RequestMapping("/")
	public String testService() {
		return "MODULE 	APP-REST  IS  UP - admin-rest";
	}

}