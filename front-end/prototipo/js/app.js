(function() {
    'use strict';
	angular.module('eventsSaas', 
		[
			'eventsSaas.router',
			'eventsSaas.splash', 
			'eventsSaas.login',
			'eventsSaas.menu',
			'eventsSaas.user', 
			'eventsSaas.quick.access',
			'eventsSaas.search',
			'eventsSaas.timeline',
			'eventsSaas.message',
			'eventsSaas.comment',
			'eventsSaas.matter',
			'eventsSaas.profile.user',
			'eventsSaas.card.virtual',
			'eventsSaas.profile.lecturer',
			'eventsSaas.evaluation',
			'eventsSaas.survey',
			'eventsSaas.agenda',
			'eventsSaas.network',
			'eventsSaas.conquer',
			'eventsSaas.status',
			'eventsSaas.qr.code'
		]
	);

})();