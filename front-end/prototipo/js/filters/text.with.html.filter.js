(function () {
    'use strict';
    angular
        .module('eventsSaas.filter')
        .filter('textWithHtml', textWithHtml);

    textWithHtml.$inject = ['$sce'];
    function textWithHtml($sce){
    	return function(text) {
          return $sce.trustAsHtml(text);
        };
    };
})();