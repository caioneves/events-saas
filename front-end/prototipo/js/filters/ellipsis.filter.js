(function () {
    'use strict';
    angular
        .module('eventsSaas.filter')
        .filter('ellipsis', ellipsis);

    ellipsis.$inject = [];
    function ellipsis(){
    	return function(input, size){
			if(input.length <= size){ return input}
			var outinput = input.substring(0,(size || 4)) + "[...]"; 
			return outinput
		}
    };
})();