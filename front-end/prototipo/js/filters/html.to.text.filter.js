(function () {
    'use strict';
    angular
        .module('eventsSaas.filter')
        .filter('htmlToPlaintext', htmlToPlaintext);

    htmlToPlaintext.$inject = [];
    function htmlToPlaintext(){
    	return function(text) {
          return  text ? String(text).replace(/<[^>]+>/gm, '') : '';
        };
    };
})();