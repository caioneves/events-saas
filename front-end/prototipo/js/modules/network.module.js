(function () {
    'use strict';

    angular.module('eventsSaas.network', 
	    [
	    	'ngAnimate',
	    	'eventsSaas.router',
	        'eventsSaas.host',
	        'eventsSaas.loading'
	    ]);
})();