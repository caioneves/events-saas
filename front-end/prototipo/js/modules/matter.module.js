(function () {
    'use strict';

    angular.module('eventsSaas.matter',
        [
            'ngAnimate',
            'eventsSaas.router',
            'eventsSaas.host', 
            'eventsSaas.loading',
            'eventsSaas.user',
            'eventsSaas.timeline',
            'eventsSaas.filter'
        ]);
})();