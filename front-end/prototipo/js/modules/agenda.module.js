(function () {
    'use strict';

    angular.module('eventsSaas.agenda',
        [
            'ui.bootstrap',
            'ui.calendar',
            'ngAnimate',
            'eventsSaas.router',
            'eventsSaas.custom.form',
            'eventsSaas.events'

        ]);
})();