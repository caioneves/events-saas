(function () {
    'use strict';

    angular.module('eventsSaas.login',
        [
            'eventsSaas.router',
            'eventsSaas.modal',
            'eventsSaas.custom.form', 
            'eventsSaas.loading'
        ]);
})();