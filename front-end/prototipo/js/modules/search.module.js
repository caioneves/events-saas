(function () {
    'use strict';

    angular.module('eventsSaas.search',
        [
            'eventsSaas.router',
            'eventsSaas.custom.form',
            'eventsSaas.host', 
            'eventsSaas.loading'
        ]);
})();