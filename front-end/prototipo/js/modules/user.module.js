(function () {
    'use strict';

    angular.module('eventsSaas.user', [
    	'ngAnimate',
        'eventsSaas.host'
    ]);
})();