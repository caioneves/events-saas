(function () {
    'use strict';

    angular.module('eventsSaas.timeline',
        [
            'eventsSaas.router',
            'eventsSaas.custom.form',
            'eventsSaas.host', 
            'eventsSaas.loading',
            'eventsSaas.filter',
            'eventsSaas.interaction'
        ]);
})();