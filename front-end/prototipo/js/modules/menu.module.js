(function () {
    'use strict';

    angular.module('eventsSaas.menu', [
    	'ngAnimate',
        'eventsSaas.host'
    ]);
})();