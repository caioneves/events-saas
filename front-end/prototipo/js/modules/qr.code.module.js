(function () {
    'use strict';

    angular.module('eventsSaas.qr.code',
        [
            'eventsSaas.router',
            'eventsSaas.host', 
            'eventsSaas.loading'
        ]);
})();