(function () {
    'use strict';

    angular.module('eventsSaas.card.virtual',
        [
            'eventsSaas.router',
            'eventsSaas.custom.form',
            'eventsSaas.host', 
            'eventsSaas.loading',
            'eventsSaas.user'
        ]);
})();