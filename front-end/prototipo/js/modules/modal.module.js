(function () {
    'use strict';

    angular.module('eventsSaas.modal',
        [
            'ui.bootstrap',
            'ngAnimate',
            'eventsSaas.custom.form'
        ]);
})();