(function () {
    'use strict';

    angular.module('eventsSaas.conquer',
        [
            'ui.bootstrap',
            'ngAnimate',
            'eventsSaas.router'
        ]);
})();