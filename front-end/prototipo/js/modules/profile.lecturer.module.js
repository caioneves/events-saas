(function () {
    'use strict';

    angular.module('eventsSaas.profile.lecturer', [
    	'ngAnimate',
        'eventsSaas.host',
        'eventsSaas.evaluation',
        'eventsSaas.chat'
    ]);
})();