(function () {
    'use strict';

    angular.module('eventsSaas.message', 
	    [
	    	'ngAnimate',
	    	'eventsSaas.router',
	    	'eventsSaas.loading',
	        'eventsSaas.host',
	        'eventsSaas.network',
	        'eventsSaas.timeline',
	        'eventsSaas.chat'
	    ]);
})();