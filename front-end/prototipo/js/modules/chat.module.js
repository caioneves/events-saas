(function () {
    'use strict';

    angular.module('eventsSaas.chat', [
    	'ngAnimate',
        'eventsSaas.host',
        'luegg.directives'
    ]);
})();