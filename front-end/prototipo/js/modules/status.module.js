(function () {
    'use strict';

    angular.module('eventsSaas.status',
        [
            'ngAnimate',
            'eventsSaas.router',
            'eventsSaas.events'
        ]);
})();