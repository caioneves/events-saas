(function () {
    'use strict';

    angular.module('eventsSaas.events', [
    	'ngAnimate',
        'eventsSaas.host'
    ]);
})();