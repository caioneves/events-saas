(function () {
    'use strict';

    angular.module('eventsSaas.survey', [
    	'ngAnimate',
        'eventsSaas.host'
    ]);
})();