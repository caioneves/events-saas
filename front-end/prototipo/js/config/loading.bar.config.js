(function () {
    'use strict';

	angular.module('eventsSaas.loading')
	.config(LoadingBarConfig)

	LoadingBarConfig.$inject = ['cfpLoadingBarProvider']

	function LoadingBarConfig(cfpLoadingBarProvider) {
        cfpLoadingBarProvider.includeSpinner = true;
    }

})();