(function() {
    'use strict';
    
    angular.module('eventsSaas.router').config(RouterConfig);

    RouterConfig.$inject = ['$routeProvider', 'eventsSaasRoutes'];

	function RouterConfig($routeProvider, eventsSaasRoutes) {
        $routeProvider
        .when(eventsSaasRoutes.HOME, {
            templateUrl: 'view/splash/splash.html',
            controller: 'SplashController',
            controllerAs: 'vm',
            preload: true
        })
        .when(eventsSaasRoutes.ENTRAR, {
            templateUrl: 'view/login/login.html',
            controller: 'LoginController',
            controllerAs: 'vm',
            preload: true
        })
        .when(eventsSaasRoutes.QUICKACCESS, {
            templateUrl: 'view/quick-access/quick-access.html',
            controller: 'QuickAccessController',
            controllerAs: 'vm',
            preload: true
        })
        .when(eventsSaasRoutes.RESULTADOPESQUISA, {
            templateUrl: 'view/search/search.html',
            controller: 'SearchController',
            controllerAs: 'vm',
            preload: true
        })
        .when(eventsSaasRoutes.TIMELINE, {
            templateUrl: 'view/timeline/timeline.html',
            controller: 'TimeLineController',
            controllerAs: 'vm',
            preload: true
        })
        .when(eventsSaasRoutes.MENSSAGEM, {
            templateUrl: 'view/message/message.html',
            controller: 'MessageController',
            controllerAs: 'vm',
            preload: true
        })
        .when(eventsSaasRoutes.CHAT, {
            templateUrl: 'view/message/chat.html',
            controller: 'ChatController',
            controllerAs: 'vm',
            preload: true
        })
        .when(eventsSaasRoutes.AMIGOS, {
            templateUrl: 'view/network/network.html',
            controller: 'NetworkController',
            controllerAs: 'vm',
            preload: true
        })
        .when(eventsSaasRoutes.COMENTAR, {
            templateUrl: 'view/comment/comment.html',
            controller: 'CommentController',
            controllerAs: 'vm',
            preload: true
        })
        .when(eventsSaasRoutes.MATERIA, {
            templateUrl: 'view/matter/datail.html',
            controller: 'DetailMatterController',
            controllerAs: 'vm',
            preload: true
        })
        .when(eventsSaasRoutes.PERFILUSUARIO, {
            templateUrl: 'view/profile-user/profile.html',
            controller: 'ProfileUserController',
            controllerAs: 'vm',
            preload: true
        })
        .when(eventsSaasRoutes.EDITARUSUARIO, {
            templateUrl: 'view/profile-user/editar.html',
            controller: 'EditUserController',
            controllerAs: 'vm',
            preload: true
        })
        .when(eventsSaasRoutes.CARTAOVIRTUAL, {
            templateUrl: 'view/card-virtual/card-virtual.html',
            controller: 'CardVirtualController',
            controllerAs: 'vm',
            preload: true
        })
        .when(eventsSaasRoutes.PERFILPALESTRANTE, {
            templateUrl: 'view/profile-lecturer/profile.html',
            controller: 'ProfileLecturerController',
            controllerAs: 'vm',
            preload: true
        })
        .when(eventsSaasRoutes.PERFILPALESTRANTECHAT, {
            templateUrl: 'view/profile-lecturer/chat.html',
            controller: 'ProfileLecturerChatController',
            controllerAs: 'vm',
            preload: true
        })
        .when(eventsSaasRoutes.PERFILPALESTRANTECONTATO, {
            templateUrl: 'view/profile-lecturer/contact.html',
            controller: 'ProfileLecturerContactController',
            controllerAs: 'vm',
            preload: true
        })
        .when(eventsSaasRoutes.AVALIACAO, {
            templateUrl: 'view/evaluation/evaluation.html',
            controller: 'EvaluationController',
            controllerAs: 'vm',
            preload: true
        })
        .when(eventsSaasRoutes.PESQUISA, {
            templateUrl: 'view/survey/survey.html',
            controller: 'SurveyController',
            controllerAs: 'vm',
            preload: true
        })
        .when(eventsSaasRoutes.AGENDAEVENT, {
            templateUrl: 'view/agenda/agenda-event.html',
            controller: 'AgendaEventController',
            controllerAs: 'vm',
            preload: true
        })
        .when(eventsSaasRoutes.AGENDA, {
            templateUrl: 'view/agenda/agenda.html',
            controller: 'AgendaController',
            controllerAs: 'vm',
            preload: true
        })
        .when(eventsSaasRoutes.DETALHEATIVIDADE, {
            templateUrl: 'view/agenda/datail.html',
            controller: 'DetailActivityController',
            controllerAs: 'vm',
            preload: true
        })
        .when(eventsSaasRoutes.CONQUISTAS, {
            templateUrl: 'view/conquer/conquer.html',
            controller: 'ConquerController',
            controllerAs: 'vm',
            preload: true
        })
        .when(eventsSaasRoutes.STATUS, {
            templateUrl: 'view/status/check-in.html',
            controller: 'CheckInController',
            controllerAs: 'vm',
            preload: true
        })
        .when(eventsSaasRoutes.QRCODE, {
            templateUrl: 'view/qrcode/qrcode.html',
            controller: 'QrCodeController',
            controllerAs: 'vm',
            preload: true
        })
        .otherwise({
            redirectTo: eventsSaasRoutes.HOME
        });
    }

    var routes = {
        HOME: '/',
        ENTRAR: '/login',
        QUICKACCESS: '/quick-access',
        RESULTADOPESQUISA: '/search',
        TIMELINE: '/timeline',
        MENSSAGEM: '/message',
        AMIGOS: '/network',
        AMIGOSPERFIL: '/network/:networkId',
        CHAT: '/network/chat/:userId',
        COMENTAR: '/comment',
        PERFILUSUARIO: '/profile-user',
        EDITARUSUARIO: '/edit-user',
        CARTAOVIRTUAL: '/card-virtual',
        PERFILPALESTRANTE: '/profile-lecturer',
        PERFILPALESTRANTECHAT: '/profile-lecturer/chat',
        PERFILPALESTRANTECONTATO: '/profile-lecturer/contact',
        AVALIACAO: '/evaluation',
        PESQUISA: '/survey',
        AGENDAEVENT: '/agenda-event',
        AGENDA: '/agenda',
        DETALHEATIVIDADE: '/agenda/:datailId',
        MATERIA: '/matter/:datailId',
        CONQUISTAS: '/conquer',
        STATUS: '/status',
        QRCODE: '/qrcode'
    };

    angular
        .module('eventsSaas.router')
        .constant('eventsSaasRoutes', routes);
    angular
        .module('eventsSaas.router').config(HrefSanitization);
    HrefSanitization.$inject = ['$compileProvider'];
    function HrefSanitization($compileProvider) {
        $compileProvider.aHrefSanitizationWhitelist(/^\s*(https?|ftp|mailto|file|javascript):/);
    };

    angular
        .module('eventsSaas.router').run(scrollRouteTop);
    scrollRouteTop.$inject = ['$rootScope', '$window'];
    function scrollRouteTop($rootScope, $window) {
        $rootScope.$on('$locationChangeSuccess', function () {
            $window.scrollTo(0,0);
        });
    };

})();