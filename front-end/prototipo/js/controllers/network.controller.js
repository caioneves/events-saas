(function () {
    'use strict';

    angular
        .module('eventsSaas.network')
        .controller('NetworkController', NetworkController);

	NetworkController.$inject = ['$scope', 'eventsSaasNetworkService', '$location', 'eventsSaasRoutes', 'cfpLoadingBar', '$timeout']

	function NetworkController($scope, eventsSaasNetworkService, $location, eventsSaasRoutes, cfpLoadingBar, $timeout) {

		cfpLoadingBar.start();
        $timeout(function() {
            cfpLoadingBar.complete();
        }, 500);

        var vm = this;
        vm.isActive = false;
        vm.lisMenu = ['amigos', 'inscritos', 'presentes'];
        vm.networks = [];
        vm.rows = [];
        vm.selected = 0;
        vm.selectApply = selectApply;
        vm.carregarNetwork = carregarNetwork;
        vm.applyFilter = applyFilter;
        vm.filter = "";
        vm.filterSearch = "";

        vm.carregarNetwork();

        function carregarNetwork() {
            return getNetwork().then(function() {
                vm.selectNetworks = vm.networks;
				vm.rows = vm.networks;
				vm.filter = vm.networks.length +" Amigos";
            });
        };

        function getNetwork() {
            return eventsSaasNetworkService.getNetwork()
            .then(function(data) {
                vm.networks = data;
                return vm.networks;
            });
        };

		function selectApply(index, item) {
			vm.selected = index; 
			vm.rows = [];
			vm.qtd = 0;
			angular.forEach(vm.selectNetworks, function(value, key) {
				if(item == "inscritos"){
					if(vm.networks[key].inscritos){
						vm.rows.push(vm.networks[key]);
						vm.qtd++;
					}
				} 
				if(item == "presentes"){
					if(vm.networks[key].inscritos){
						if(vm.networks[key].presentes){
							vm.rows.push(vm.networks[key]);
							vm.qtd++;
						}
					}
				} 
				if(item == "amigos"){
					vm.rows.push(vm.networks[key]);
					vm.qtd++;
				}
				vm.filter = vm.qtd + " "+ item;
				vm.search = "";
				vm.filterSearch = "";
            });
		};

		function applyFilter(){
			vm.search = vm.filterSearch;
		}
    }

    function isEmpyt(value){
    	return value.length
    }

})();