(function () {
    'use strict';

    angular
        .module('eventsSaas.login')
        .controller('EditPassword', EditPassword);

    EditPassword.$inject = ['$scope', '$location', 'eventsSaasRoutes', 'titulo']

    function EditPassword($scope, $location, eventsSaasRoutes, titulo) {
        $scope.title = titulo;
		$scope.submit = function() {
			 $location.path(eventsSaasRoutes.QUICKACCESS);
		};
    }

})();