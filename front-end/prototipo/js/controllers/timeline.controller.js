(function () {
    'use strict';

    angular
        .module('eventsSaas.timeline')
        .controller('TimeLineController', TimeLineController);

	TimeLineController.$inject = ['$scope', '$location', 'eventsSaasRoutes', 'cfpLoadingBar', '$timeout', 'eventsSaasTimelineService']

	function TimeLineController($scope, $location, eventsSaasRoutes, cfpLoadingBar, $timeout, eventsSaasTimelineService) {

		cfpLoadingBar.start();
        $timeout(function() {
            cfpLoadingBar.complete();
        }, 500);
        var vm = this;
        vm.timeLine = [];
        vm.carregarTimeLine = carregarTimeLine;

        vm.carregarTimeLine();

        function carregarTimeLine() {
            return getTimeline().then(function() {
                console.log(vm.timeLine);
            });
        };
        function getTimeline() {
            return eventsSaasTimelineService.getTimeline()
            .then(function(data) {
                vm.timeLine = data;
                return vm.timeLine;
            });
        };
    };

})();