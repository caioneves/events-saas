(function () {
    'use strict';

    angular
        .module('eventsSaas.login')
        .controller('LoginController', LoginController)

    LoginController.$inject = ['$scope', '$uibModal', '$location', 'eventsSaasRoutes', 'cfpLoadingBar', '$timeout']

    function LoginController($scope, $uibModal, $location, eventsSaasRoutes, cfpLoadingBar, $timeout) {

        cfpLoadingBar.start();
        $timeout(function() {
            cfpLoadingBar.complete();
        }, 500);

        //alert(window.innerWidth)

    	$scope.submit = function() {
			$location.path(eventsSaasRoutes.QUICKACCESS);
		};
    }

})();