(function () {
    'use strict';

    angular
        .module('eventsSaas.search')
        .controller('SearchController', SearchController);

	SearchController.$inject = ['$scope', 'eventsSaasQuickAccessService', '$location', 'eventsSaasRoutes', 'cfpLoadingBar', '$timeout']

	function SearchController($scope, eventsSaasQuickAccessService, $location, eventsSaasRoutes, cfpLoadingBar, $timeout) {

		cfpLoadingBar.start();
        $timeout(function() {
            cfpLoadingBar.complete();
        }, 500);

    }

})();