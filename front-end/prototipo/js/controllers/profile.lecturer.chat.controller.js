(function () {
    'use strict';

    angular
        .module('eventsSaas.profile.lecturer')
        .controller('ProfileLecturerChatController', ProfileLecturerChatController);

	ProfileLecturerChatController.$inject = ['$scope', '$location', 'eventsSaasRoutes', 'cfpLoadingBar', '$timeout']

	function ProfileLecturerChatController($scope, $location, eventsSaasRoutes, cfpLoadingBar, $timeout) {

		cfpLoadingBar.start();
        $timeout(function() {
            cfpLoadingBar.complete();
        }, 500);
    }

})();