(function () {
    'use strict';

    angular
        .module('eventsSaas.message')
        .controller('MessageController', MessageController);

    MessageController.$inject = ['$scope', 'eventsSaasRoutes', 'cfpLoadingBar', '$routeParams', '$timeout', 'eventsSaasNetworkService', '$location']

    function MessageController($scope, eventsSaasRoutes, cfpLoadingBar, $routeParams, $timeout, eventsSaasNetworkService, $location) {

        cfpLoadingBar.start();
        $timeout(function() {
            cfpLoadingBar.complete();
        }, 500);

        var vm = this;
        vm.rows = [];
        vm.mensagens = [];
        vm.carregarMensagem = carregarMensagem;

        vm.carregarMensagem();

        function carregarMensagem() {
            return getNetwork().then(function() {
                preencherMensagem(vm.mensagens);
            });
        };

        function getNetwork() {
            return eventsSaasNetworkService.getNetwork()
            .then(function(data) {
                vm.mensagens = data;
                return vm.mensagens;
            });
        };

        function preencherMensagem(data){
            vm.message = [];
            var image, nome;
            angular.forEach(data, function(value, key) {
                image   = data[key].image;
                nome    = data[key].nome;
                angular.forEach(data[key].message, function(e, index) {
                    var date, texto, objMessage;
                    date = new Date(data[key].message[index].date);
                    var hora =  date.getHours();
                    var min = date.getMinutes();
                    hora =  hora < 10 ? '0' + hora : '' + hora;
                    min =  min < 10 ? '0' + min : '' + min;
                    var temp = hora +":"+min;
                    texto = data[key].message[index].texto;
                    objMessage = {"id":data[key].id, "message": texto, "data": date, "tempo": temp, "nome": nome, "image":image, "ler":data[key].message[index].ler}; 
                    vm.rows.push(objMessage);
                });
            });
        }
    }

})();