(function () {
    'use strict';

    angular
        .module('eventsSaas.conquer')
        .controller('ConquerDatailController', ConquerDatailController);

	ConquerDatailController.$inject = ['$scope', '$location', 'eventsSaasRoutes', 'cfpLoadingBar', '$timeout', 'titulo', 'subTitulo', 'icon', 'iconBg', 'descricao']

	function ConquerDatailController($scope, $location, eventsSaasRoutes, cfpLoadingBar, $timeout, titulo, subTitulo, icon, iconBg, descricao) {

		cfpLoadingBar.start();
        $timeout(function() {
            cfpLoadingBar.complete();
        }, 500);
        var vm = this;
        vm.titulo = titulo;
        vm.subTitulo = subTitulo;
        vm.icon = icon;
        vm.iconBg = iconBg;
        vm.descricao = descricao;
        vm.isMustache = false;
    };

})();