(function () {
    'use strict';

    angular
        .module('eventsSaas.matter')
        .controller('DetailMatterController', DetailMatterController);

    DetailMatterController.$inject = ['$scope', 'eventsSaasRoutes', 'cfpLoadingBar', '$routeParams', '$timeout', 'eventsSaasUserService', '$location', 'eventsSaasTimelineService']

    function DetailMatterController($scope, eventsSaasRoutes, cfpLoadingBar, $routeParams, $timeout, eventsSaasUserService, $location, eventsSaasTimelineService) {

        cfpLoadingBar.start();
        $timeout(function() {
            cfpLoadingBar.complete();
        }, 500);

        var vm = this;
        vm.users = [];
        vm.rede = [];
        vm.timeLine = [];
        vm.matter = [];
        vm.carregarUser = carregarUser;
        vm.isShares = false; 
        vm.viewShares = false;       
        vm.activityId = $routeParams.datailId;
        vm.comment = comment;
        vm.compartilha = compartilha;
        vm.carregarTimeLine = carregarTimeLine;

        vm.carregarTimeLine(vm.activityId)

        function carregarTimeLine(id) {
            return getTimeline().then(function() {
                preencherTimeLine(vm.timeLine, id);
            });
        };
        function getTimeline() {
            return eventsSaasTimelineService.getTimeline()
            .then(function(data) {
                vm.timeLine = data;
                return vm.timeLine;
            });
        };
        function preencherTimeLine(data, id){
            angular.forEach(data, function(value, key) {
                if(data[key].id == id){
                    var obj = {"id": data[key].id, "nome": data[key].nome};
                    vm.user = [];
                    vm.user.push(obj);
                    vm.matter = data[key].timeLine;
                }
            });
            console.log(vm.matter);
        };

        function comment(){
            $location.path(eventsSaasRoutes.COMENTAR);
        };
        function compartilha(){
            vm.isShares = !vm.isShares;
            if(vm.isShares){
                vm.carregarUser(vm.activityId);
            } else {
                vm.viewShares = vm.isShares;
            }
        };
        function carregarUser(id) {
            return getUser().then(function() {
                preencherShares(vm.users, id);
                vm.viewShares = vm.isShares;
            });
        };
        function getUser() {
            return eventsSaasUserService.getUser()
            .then(function(data) {
                vm.users = data;
                return vm.users;
            });
        };
        function preencherShares(data, id){
            angular.forEach(data, function(value, key) {
                if(data[key].id == id){
                    vm.rede = data[key].redeSocial;
                }
            });
        };
    };

})();