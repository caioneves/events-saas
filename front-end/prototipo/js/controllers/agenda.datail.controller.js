(function () {
    'use strict';

    angular
        .module('eventsSaas.agenda')
        .controller('DetailActivityController', DetailActivityController);

    DetailActivityController.$inject = ['$scope', 'eventsSaasRoutes', 'cfpLoadingBar', '$routeParams', '$timeout', 'eventsSaasEventsService']

    function DetailActivityController($scope, eventsSaasRoutes, cfpLoadingBar, $routeParams, $timeout, eventsSaasEventsService) {

        cfpLoadingBar.start();
        $timeout(function() {
            cfpLoadingBar.complete();
        }, 500);

        var vm = this;
        vm.serviceEvents = serviceEvents;        
        vm.activityId = $routeParams.datailId;
        vm.getEvents = getEvents;
        vm.serviceEvents();

         function serviceEvents(){
            eventsSaasEventsService.getEvents().success(function(data){
                vm.activity = {};
                vm.getEvents(data)
            }).error(function(data, status){
                console.log(status)
            });
         }

         function getEvents(data){
            angular.forEach(data, function(value, key) {
                angular.forEach(data[key].palestras, function(item, index) {
                    if(data[key].palestras[index].id == vm.activityId){
                        vm.activity = data[key].palestras[index]
                    }
                });
            });
        }

    }

})();