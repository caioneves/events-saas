(function () {
    'use strict';

    angular
        .module('eventsSaas.agenda')
        .controller('AgendaController', AgendaController);

    AgendaController.$inject = ['$scope', 'eventsSaasRoutes', 'cfpLoadingBar', '$compile', 'uiCalendarConfig', '$timeout', 'eventsSaasEventsService', '$location']

    function AgendaController($scope, eventsSaasRoutes, cfpLoadingBar, $compile, uiCalendarConfig, $timeout, eventsSaasEventsService, $location) {

        cfpLoadingBar.start();
        $timeout(function() {
            cfpLoadingBar.complete();
        }, 500);

        var vm = this;
        vm.hasCalendar = true;
        vm.eventSources = [];
        vm.fillEvents = fillEvents;
        vm.addUiCalendar = addUiCalendar;
        vm.eventRender = eventRender;
        vm.dayEvents = dayEvents;
        vm.applyEventDay = applyEventDay;
        vm.loadDayEvent = loadDayEvent;
        vm.serviceEvents = serviceEvents;
        vm.datail = datail;
        vm.uiConfig = {
            calendar:{
                height: 450,
                editable: true,
                header:{
                    left: 'prev',
                    center: 'title',
                    right: 'next'
                },
                lang: "pt-br",
                selectable: true,
                selectHelper: true,
                //select: $scope.selectEvent,
                editable: true,
                eventRender: vm.eventRender,
                eventClick: vm.dayEvents
            }
        };

        vm.serviceEvents();

        /* Render Tooltip */
        function eventRender(event, element, view){ 
            vm.eventRow = [];
            vm.loadDayEvent();
            var dia = new Date(event.start)
            element.attr({/*'uib-tooltip': event.title,
                         'tooltip-append-to-body': true, */'day': dia.getDate()});
            $compile(element)($scope);
        };

        function serviceEvents(){
            eventsSaasEventsService.getEvents().success(function(data){
                vm.palestras = [];
                vm.events = [];
                vm.eventRow = [];
                vm.eventSources = [];
                vm.fillEvents(data);
                vm.addUiCalendar();
            }).error(function(data, status){
                console.log(status)
            });
        }

        function fillEvents(data){
            angular.forEach(data, function(value, key) {
                angular.forEach(data[key].palestras, function(item, index) {
                    var dataStart, nome, objPalestra;
                    dataStart = data[key].palestras[index].data + "T" + data[key].palestras[index].entrada;
                    nome = data[key].palestras[index].nome;
                    objPalestra = {"id":data[key].palestras[index].id, "title": nome, "start": dataStart, "className": "not-title-agenda"}; 
                    vm.palestras.push(objPalestra);
                    vm.events.push(data[key].palestras[index]);
                });
            });
        }

        /*adicionando a diretiva ui-calendar*/
        function addUiCalendar(){
            var $element = angular.element(document.getElementById('calendar'))
            $element.attr({"ui-calendar":"vm.uiConfig.calendar"});
            $compile($element)($scope);
            vm.eventSources.push(vm.palestras)
        }

        function applyEventDay(compare){
            vm.eventRow = [];
            angular.forEach(vm.events, function(value, key) {
                if(vm.events[key].data == compare){
                    var objCheckIn = {"id":vm.events[key].id, "tipo":"atividade", "title": "check-in", "value":vm.events[key].checkIn}
                    vm.eventRow.push(objCheckIn);
                    var objPalestra = {"id":vm.events[key].id, "tipo":"atividade", "title":vm.events[key].nome, "value":vm.events[key].entrada}
                    vm.eventRow.push(objPalestra);
                    var objEnceramento = {"id":vm.events[key].id, "tipo":"atividade", "title": "encerramento", "value":vm.events[key].encerramento}
                    vm.eventRow.push(objEnceramento);
                }
            });
        }

        function dayEvents(event){
            var dateClick = new Date(event.start._d)
            var d = dateClick.getDate();
            var m = dateClick.getMonth()+1;
            var y = dateClick.getFullYear();
            d =  d < 10 ? '0' + d : '' + d;
            m =  m < 10 ? '0' + m : '' + m;
            var search = y+"-"+m+"-"+d;
            vm.applyEventDay(search);
        }

        function loadDayEvent(){
            var dataAtual = new Date();
            var dia = dataAtual.getDate();
            var mes = dataAtual.getMonth()+1;
            var ano = dataAtual.getFullYear();
            dia =  dia < 10 ? '0' + dia : '' + dia;
            mes =  mes < 10 ? '0' + mes : '' + mes;
            var data = ano+"-"+mes+"-"+dia;
            vm.applyEventDay(data);
        }

        function datail(id){
            $location.path("/agenda/"+id);
        }

        /*$scope.selectEvent = function(start, end){
            var title = prompt('Event Title:');
            var eventData;
            if (title) {
                eventData = {
                    title: title,
                    start: start,
                    end: end
                };
                uiCalendarConfig.calendars['calendar'].fullCalendar('renderEvent', eventData, true); // stick? = true
            }
            uiCalendarConfig.calendars['calendar'].fullCalendar('unselect');
        }*/
    }

})();