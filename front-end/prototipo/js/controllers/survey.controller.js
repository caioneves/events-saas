(function () {
    'use strict';

    angular
        .module('eventsSaas.survey')
        .controller('SurveyController', SurveyController);

    SurveyController.$inject = ['$scope', 'eventsSaasRoutes', 'cfpLoadingBar', '$timeout']

    function SurveyController($scope, eventsSaasRoutes, cfpLoadingBar, $timeout) {
        cfpLoadingBar.start();
        $timeout(function() {
            cfpLoadingBar.complete();
        }, 500);
    }

})();