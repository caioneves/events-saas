(function () {
    'use strict';

    angular
        .module('eventsSaas.login')
        .controller('QrCodeController', QrCodeController)

    QrCodeController.$inject = ['$scope', '$uibModal', '$location', 'eventsSaasRoutes', 'cfpLoadingBar', '$timeout']

    function QrCodeController($scope, $uibModal, $location, eventsSaasRoutes, cfpLoadingBar, $timeout) {

        cfpLoadingBar.start();
        $timeout(function() {
            cfpLoadingBar.complete();
        }, 500);

    };

})();