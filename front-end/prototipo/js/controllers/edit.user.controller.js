(function () {
    'use strict';

    angular
        .module('eventsSaas.profile.user')
        .controller('EditUserController', EditUserController);

	EditUserController.$inject = ['$scope', '$location', 'eventsSaasRoutes', 'cfpLoadingBar', '$timeout']

	function EditUserController($scope, $location, eventsSaasRoutes, cfpLoadingBar, $timeout) {

		cfpLoadingBar.start();
        $timeout(function() {
            cfpLoadingBar.complete();
        }, 500);

    }

})();