(function () {
    'use strict';

    angular
        .module('eventsSaas.card.virtual')
        .controller('CardVirtualController', CardVirtualController);

	CardVirtualController.$inject = ['$scope', 'eventsSaasUserService', '$location', 'eventsSaasRoutes', 'cfpLoadingBar', '$timeout', '$filter']

	function CardVirtualController($scope, eventsSaasUserService, $location, eventsSaasRoutes, cfpLoadingBar, $timeout, $filter) {

		cfpLoadingBar.start();
        $timeout(function() {
            cfpLoadingBar.complete();
        }, 500);

        var vm = this;
        vm.submitSearch = submitSearch;
        vm.user = [];
        vm.cards = [];
        vm.contatos = [];
        vm.carregarContatos = carregarContatos;

        vm.carregarContatos();

		function submitSearch(value){
			if(isEmpyt(value)){
				$location.path(eventsSaasRoutes.RESULTADOPESQUISA)
			}
		};

		function carregarContatos() {
            return getUser().then(function() {
                preencherCards(vm.cards);
            });
        };

        function getUser() {
            return eventsSaasUserService.getUser()
            .then(function(data) {
                vm.cards = data;
                return vm.cards;
            });
        };

        function preencherCards(data){
        	vm.user = $filter("filter")(data, {id: 3});
			angular.forEach(vm.user, function (value, key) {
				angular.forEach(vm.user[key], function (v, index) {
					if([index] == "amigos"){
						vm.contatos = vm.user[key][index]
					}
				});
			});
        };
    };

    function isEmpyt(value){
    	return value.length
    };

})();