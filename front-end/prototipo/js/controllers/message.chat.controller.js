(function () {
    'use strict';

    angular
        .module('eventsSaas.message')
        .controller('ChatController', ChatController);

    ChatController.$inject = ['$scope', 'eventsSaasRoutes', 'cfpLoadingBar', '$routeParams', '$timeout', 'eventsSaasNetworkService', '$location']

    function ChatController($scope, eventsSaasRoutes, cfpLoadingBar, $routeParams, $timeout, eventsSaasNetworkService, $location) {

        cfpLoadingBar.start();
        $timeout(function() {
            cfpLoadingBar.complete();
        }, 500);

        var vm = this;
        vm.rows = [];
        vm.mensagens = [];
        vm.userId = $routeParams.userId;

        /*vm.mensagens = [
            {
                "id": "1",
                "image": "img/user14.png",
                "texto": "Olá Ricardo! Gostou da palestra de hoje cedo? Achei a turma bastante animada... Qual a sua dúvida?",
                "time": "3 mins ago",
                "myMsg": true
            },
             {
                "id": "2",
                "image": "img/user15.png",
                "texto": "Foi bom demais! Então...",
                "time": "10 seg ago",
                "myMsg": false
            },
            {
                "id": "3",
                "image": "img/user14.png",
                "texto": "Legal...",
                "time": "2 mins ago",
                "myMsg": true
            },
            {
                "id": "4",
                "image": "img/user14.png",
                "texto": "Olá Ricardo! Gostou da palestra de hoje cedo? Achei a turma bastante animada... Qual a sua dúvida?",
                "time": "3 mins ago",
                "myMsg": true
            },
             {
                "id": "5",
                "image": "img/user15.png",
                "texto": "Foi bom demais! Então...",
                "time": "10 seg ago",
                "myMsg": false
            },
            {
                "id": "6",
                "image": "img/user14.png",
                "texto": "Legal...",
                "time": "13 mins ago",
                "myMsg": true
            }
        ]
        var totalMsg = vm.mensagens.length + 1;
        var d = new Date();
        var m = d.getMinutes() + " min";
        $timeout(function() {
            var obj = { id: totalMsg, image: "img/user15.png", texto: "Até a proxima..", time: m, myMsg: false };
            vm.mensagens.push(obj);
        }, 2500);
        vm.addMsg = function(msg){
            var obj = { id: totalMsg, image: "img/user15.png", texto: msg, time: m, myMsg: false };
            vm.mensagens.push(obj);
            vm.mensagem = "";
        }*/

        vm.network = [];
        vm.carregarMensagem = carregarMensagem;
        vm.addMsg = addMsg;
        vm.carregarMensagem();
        function carregarMensagem() {
            return getNetwork().then(function() {
                getMensagemUser(vm.network, vm.userId);
            });
        };
        function getNetwork() {
            return eventsSaasNetworkService.getNetwork()
            .then(function(data) {
                vm.network = data;
                return vm.network;
            });
        };
        function getMensagemUser(data, idUser){
            angular.forEach(data, function(value, key) {
                if(data[key].id == idUser){
                    vm.image   = data[key].image;
                    vm.nome = data[key].nome;
                    angular.forEach(data[key].message, function(e, index) {
                        var texto, objMessage;
                        var d = new Date(data[key].message[index].date);
                        var m = d.getMinutes() + " min";
                        texto = data[key].message[index].texto;
                        objMessage = {id: data[key].message[index].id, image: vm.image, texto: texto, time: m, myMsg: false };
                        vm.mensagens.push(objMessage);
                    });
                }
            });
        };
        var totalMsg = vm.mensagens.length + 1;
        function addMsg(msg){
            if(msg !=""){
                var d = new Date();
                var m = d.getMinutes() + " min atrás";
                var obj = { id: totalMsg, image: "img/user15.png", texto: msg, time: m, myMsg: true };
                vm.mensagens.push(obj);
                vm.mensagem = "";
            }
        };
    }

})();