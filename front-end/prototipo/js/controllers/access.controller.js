(function () {
    'use strict';

    angular
        .module('eventsSaas.login')
        .controller('AccessController', AccessController)

    AccessController.$inject = ['$scope', '$uibModal', '$location', 'eventsSaasRoutes', 'cfpLoadingBar', '$timeout', 'eventsSaasAuthenticationService']

    function AccessController($scope, $uibModal, $location, eventsSaasRoutes, cfpLoadingBar, $timeout, eventsSaasAuthenticationService) {

        cfpLoadingBar.start();
        $timeout(function() {
            cfpLoadingBar.complete();
        }, 500);

        // reset login status
        eventsSaasAuthenticationService.ClearCredentials();

        //alert(window.innerWidth)
        var vm = this;
        vm.submit = submit;
        function submit() {
            vm.dataLoading = true;
            eventsSaasAuthenticationService.Login(vm.username, vm.password, function(response) {
                if(response.success) {
                    eventsSaasAuthenticationService.SetCredentials(vm.username, vm.password);
                    $location.path(eventsSaasRoutes.HOME);
                } else {
                    vm.error = response.message;
                    vm.dataLoading = false;
                }
            });
        };
    }

    function isEmpyt(value){
        return value.length
    }

})();