(function () {
    'use strict';

    angular
        .module('eventsSaas.profile.lecturer')
        .controller('ProfileLecturerController', ProfileLecturerController);

	ProfileLecturerController.$inject = ['$scope', '$location', 'eventsSaasRoutes', 'cfpLoadingBar', '$timeout', '$rootScope']

	function ProfileLecturerController($scope, $location, eventsSaasRoutes, cfpLoadingBar, $timeout, $rootScope) {

		cfpLoadingBar.start();
        $timeout(function() {
            cfpLoadingBar.complete();
        }, 500);
    }

})();