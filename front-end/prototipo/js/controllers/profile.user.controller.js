(function () {
    'use strict';

    angular
        .module('eventsSaas.profile.user')
        .controller('ProfileUserController', ProfileUserController);

	ProfileUserController.$inject = ['$scope', '$location', 'eventsSaasRoutes', 'cfpLoadingBar', '$timeout', 'eventsSaasUserService', '$filter']

	function ProfileUserController($scope, $location, eventsSaasRoutes, cfpLoadingBar, $timeout, eventsSaasUserService, $filter) {

		cfpLoadingBar.start();
        $timeout(function() {
            cfpLoadingBar.complete();
        }, 500);

        var vm = this;
        vm.profile = [];
        vm.carregarUser = carregarUser;

        vm.carregarUser();
        function carregarUser() {
            return getUser().then(function() {
                console.log("get use")
            });
        };
        function getUser() {
            return eventsSaasUserService.getUser()
            .then(function(data) {
                vm.profile = $filter("filter")(data, {id: 3});
                return vm.profile;
            });
        };

    };

})();