(function () {
    'use strict';

    angular
        .module('eventsSaas.agenda')
        .controller('AgendaEventController', AgendaEventController);

    AgendaEventController.$inject = ['$scope', 'eventsSaasRoutes', 'cfpLoadingBar', '$timeout', 'eventsSaasEventsService', '$location']

    function AgendaEventController($scope, eventsSaasRoutes, cfpLoadingBar, $timeout, eventsSaasEventsService, $location) {

        cfpLoadingBar.start();
        $timeout(function() {
            cfpLoadingBar.complete();
        }, 500);

        var vm = this;
        vm.hasAgendaHome = true;
        vm.hasPalestra = false;
        vm.getEvents = [];
        vm.getCategoryEvent = getCategoryEvent;
        vm.getActivity = getActivity;
        vm.serviceEvents = serviceEvents;
        vm.filterEvent = filterEvent;
        vm.back = back;

        vm.serviceEvents();

        function serviceEvents(){
            eventsSaasEventsService.getEvents().success(function(data){
                vm.eventRow = [];
                vm.getCategoryEvent(data);
                vm.getEvents = data;
            }).error(function(data, status){
                console.log(status)
            });
        }

        function getCategoryEvent(data){
            angular.forEach(data, function(value, key) {
                var objCategoria = {"id":data[key].id, "tipo":"categoria", "title": data[key].tipo, "value":"("+data[key].palestras.length+")"}
                vm.eventRow.push(objCategoria);
            });
        }

        function getActivity(id){
            vm.eventRow = [];
            angular.forEach(vm.getEvents, function(value, key) {
                if(vm.getEvents[key].id == id){
                    angular.forEach(vm.getEvents[key].palestras, function(item, index) {
                        var objPalestra = {"id":vm.getEvents[key].palestras[index].id, "tipo":"palestra", "title": vm.getEvents[key].palestras[index].nome, "value":vm.getEvents[key].palestras[index].checkIn}
                        vm.eventRow.push(objPalestra);
                    });
                }
            });
        }

        function filterEvent(id, tipo){
            if(vm.hasAgendaHome){
                if(tipo == "categoria"){
                    vm.hasPalestra = !vm.hasPalestra;
                    vm.getActivity(id);
                } else {
                    $location.path("/agenda/"+id);
                }
            }
        }

        function back(){
            vm.hasPalestra = !vm.hasPalestra;
            vm.serviceEvents();
        }
    }

})();