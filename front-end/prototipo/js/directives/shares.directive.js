(function () {
    'use strict';

    angular
    .module('eventsSaas.shares')
    .directive("shares", shares)

    shares.$inject = [];

    function shares(eventsSaasUserService, $filter){
        return {
			restrict: 'E',
			template: '<li class="brd-bottom-sure-1 col-xs-12 col-sm-12" ng-repeat="contato in contatos"><ul><li class="has-img"><img alt="" ng-attr-src="{{contato.image}}" class="img-circle"/></li><li class="description"><h3 class="font-grey-dark-4 font-openregular font-36">{{contato.nome}}</h3><p class="font-26 font-grey-dark-trans">{{contato.profissao}}</p><p class="font-26 font-grey-dark-trans"><em class="ion-ios-email"></em> {{contato.email}}</p><p class="font-26 font-grey-dark-trans" ng-if="contato.celular"><em class="ion-iphone"></em> {{contato.celular}}</p><p class="font-26 font-grey-dark-trans" ng-if="contato.fixo"><em class="ion-iphone"></em> {{contato.fixo}}</p></li></ul></li>',
			replace: true,
			scope: {
				listar: "@"
		    },
		    link: function(scope, element, attr) {
				var user = [];
				scope.contatos = [];
		        eventsSaasUserService.getUser().success(function(data){
		           	user = $filter("filter")(data, {id: 3});
					angular.forEach(user, function (value, key) {
						angular.forEach(user[key], function (v, index) {
							if(scope.listar == [index]){
								scope.contatos = user[key][index]
							}
						});
					});
		        }).error(function(data, status){
		            
		        });
	        }
		};
    }

})();
