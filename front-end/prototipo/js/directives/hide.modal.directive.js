(function () {
    'use strict';
    angular
    .module('eventsSaas.modal')
    .directive("hideModal", HideModal)

    HideModal.$inject = ['$uibModal'];

    function HideModal($uibModal){
    	return {
            restrict: 'A', // A: attribute
            scope: { 
            },
		    link: function(scope, element, attr) {
				element.bind("click", function(){
					scope.$parent.$dismiss('cancel');
				});
	        }
		};
    }

})();