(function () {
    'use strict';

    angular
    .module('eventsSaas.timeline')
    .directive("timelineTime", TimelineTime)

    TimelineTime.$inject = ['$interval'];

    function TimelineTime($interval){
        return {
			restrict: 'E',
			template: '<span ng-transclude><em ng-if="icon" class="ion ion-ios-clock-outline"></em> {{timeline}}</span>',
			replace: true,
			scope: {
				tempo: "@",
				icon: "@"
		    },
		    transclude: true,
		    link: function(scope, element, attr) {
		    	var start = scope.tempo;
			    var timeInHours = function(e,s, h) {
				   return e - s - h
				}
				var timeInMinu = function(e,s) {
				   return e - s
				}
            	var addZero = function(i) {
				    if (i < 10) {
				        i = "0" + i;
				    }
				    return i;
				}
				var tempoLine = function() {
				  	var d = new Date();
					var h = addZero(d.getHours());
					var m = addZero(d.getMinutes());
		    		var end = h + ":" + m;
		    		var s = start.split(':');
				    var e = end.split(':');

				    var minuto = timeInMinu(e[1],s[1]);
			   		var hour_carry = 0;
				    if(minuto < 0){
				        minuto += 60;
				        hour_carry += 1;
				    }
				    var text ="";
				    if (start == end){
				    	text = "Agora mesmo";
				    } else if(addZero(timeInHours(e[0], s[0], hour_carry)) == "00"){
				    	text = minuto + " min atrás";
				    } else {
				    	text = timeInHours(e[0], s[0], hour_carry)+ " hora(s) atrás";
				    }
		    		return text ;
				}
				scope.timeline = tempoLine();
				$interval(tempoLine, 8000);
	        }
		};
    }

})();
