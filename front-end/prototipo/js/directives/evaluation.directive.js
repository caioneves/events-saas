(function () {
    'use strict';

    angular
    .module('eventsSaas.evaluation')
    .directive("evaluation", Evaluation)

    Evaluation.$inject = [];

    function Evaluation(){
        return {
			restrict: 'A',
			scope: {
		    },
		    link: function(scope, element, attr) {
				var els = angular.element('.list-evaluation li');
				element.bind("click", function(){
					els.removeClass("active");
					var end = $(element).index();
					angular.forEach(els, function(value, key){
						var a = angular.element(value);
		    			if(key <= end){
		    				a.toggleClass("active")
		    			}
					});
				})
				
	        }
		};
    }

})();
