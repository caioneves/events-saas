(function () {
    'use strict';

    angular
    .module('eventsSaas.profile.user')
    .directive("userRow", userRow)

    userRow.$inject = ['eventsSaasUserService', '$filter'];

    function userRow(eventsSaasUserService, $filter){
        return {
			restrict: 'E',
			template: '<div class="row {{rowClass}} row-alt"> <div ng-if="label"> <span class="text-uppercase font-24 font-grey-dark-3 font-opensemibold col-xs-3 col-sm-3" ng-if="!action">{{label}}</span> <span class="font-grey-dark-trans font-24 font-opensemibold col-xs-9 col-sm-9 text-ellipsis" ng-if="!action">{{texto}}</span> <label ng-attr-for="{{label}}" class="text-uppercase font-24 font-grey-dark-3 font-opensemibold" ng-if="action">{{label}}</label> <div class="group-campo" ng-if="action"> <textarea cols="2" class="campo" ng-if="typeTag==\'textarea\'" ng-attr-id="{{label}}">{{texto}}</textarea> <input type="password" class="campo" ng-if="typeTag==\'password\'" ng-attr-id="{{label}}" ng-attr-value="{{texto}}"/> <input type="text" class="campo" ng-if="!typeTag" ng-attr-id="{{label}}" ng-attr-value="{{texto}}"/> </div></div><div ng-if="title"> <span class="text-uppercase font-24 font-grey-dark-3 font-opensemibold col-xs-6 col-sm-6">{{total}} {{title}}</span> <a href="{{link}}" class="col-xs-6 col-sm-6 text-uppercase font-grey-dark-trans font-24 font-opensemibold text-right">{{texto}}<em class="ion-chevron-right mrg-left-5"></em></a> </div></div>',
			replace: true,
			scope: {
				title: "@",
				label: "@",
				texto: "@",
				rowClass: "@",
				service: "@",
				tipo: "@",
				action: "@",
				typeTag: "@",
				link: "@"
		    },
		    link: function(scope, element, attr) {
		    	scope.user = [];
				scope.profile = [];
				var item = scope.listar;
				scope.total
		    	scope.carregarUser = carregarUser;
		    	scope.carregarUser();
		    	function carregarUser() {
			        return getUser().then(function() {
			            preencherRow(scope.user);
			        });
			    };
			    function getUser() {
			        return eventsSaasUserService.getUser()
			        .then(function(data) {
			            scope.user = $filter("filter")(data, {id: 1});
			            return scope.user;
			        });
			    };
			    function preencherRow(data){
			    	angular.forEach(data, function (value, key) {
						angular.forEach(data[key], function (v, index) {
							if(scope.service == [index]){
								if(scope.tipo == "qtd"){
									scope.total = data[key][index].length;
								} else {
									scope.texto = data[key][index]
								}
							}
						});
					});
			    };
	        }
		};
    }

})();
