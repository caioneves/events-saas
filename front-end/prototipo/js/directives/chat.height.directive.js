(function () {
    'use strict';

    angular
    .module('eventsSaas.chat')
    .directive("onHeight", onHeight)

    onHeight.$inject = ['$window'];

    function onHeight($window){
        return {
			restrict: 'A',
			scope: {
				onHeightContect: "@"
		    },
		    link: function(scope, element, attr) {
		    	scope.onResize = function() {
		    		element.removeAttr("style");
	                element.height($(window).height() - ($('header').outerHeight() + $('.'+scope.onHeightContect+'').outerHeight()));
	                if(scope.onHeightContect !="" && scope.onHeightContect != undefined){
	                	element.css({minHeight: element.height()+"px", maxHeight: element.height()+"px"})
	                }
	            }
	            scope.onResize();
	            angular.element($window).bind('resize', function() {
	                scope.onResize();
	            })
				
	        }
		};
    }

})();
