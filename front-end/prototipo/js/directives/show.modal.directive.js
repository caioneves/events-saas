(function () {
    'use strict';
    angular
    .module('eventsSaas.modal')
    .directive("openModal", OpenModal)

    OpenModal.$inject = ['$uibModal'];

    function OpenModal($uibModal){
    	return {
            restrict: 'AE', // A: attribute
            scope: { // isolate scope
                'openModal': '@', // modal view url to render the modal content
                'modalController': '@', // modal view controller
                'modalTitle': '@', // modal title (optional)
                'modalSubTitle': '@', // modal subtitle (optional)
                'modalDescricao': '@', // modal Descricao (optional)
                'modalIcon': '@', // modal icon (optional)
                'modalIconBg': '@' // modal icon bg (optional)
            },
		    link: function(scope, element, attr) {
				element.bind("click", function(){
					var modalInstance = $uibModal.open({
                        animation: true,
                        templateUrl: scope.openModal,
                        controller: scope.modalController,
                        controllerAs: "vm",
                        resolve: {
                            titulo: function(){
                                return scope.modalTitle;
                            },
                            subTitulo: function(){
                                return scope.modalSubTitle;
                            },
                            descricao: function(){
                                return scope.modalDescricao;
                            },
                            icon: function(){
                                return scope.modalIcon;
                            },
                            iconBg: function(){
                                return scope.modalIconBg;
                            }
                        }       
                    });
				});
	        }
		};
    }

})();