(function () {
    'use strict';

    angular
    .module('eventsSaas.status')
    .directive("bars", bars)

    bars.$inject = ['$parse', '$filter'];

    function bars($parse, $filter){
		return {
			restrict: 'E',
			replace: true,
			template: '<div id="chart" class="col-sm-12 col-xs-12"></div>',
			link: function (scope, element, attrs) {
				var data = [
					{"valor":40, "name": "Palestra 1"}, 
					{"valor":80, "name": "Palestra 2"}, 
					{"valor":40, "name": "Palestra 3"}, 
					{"valor":60, "name": "Palestra 4"}, 
					{"valor":75, "name": "Palestra 5"}, 
					{"valor":83, "name": "Palestra 6"}, 
					{"valor":92, "name": "Palestra 7"}, 
					{"valor":88, "name": "Palestra 8"}
				]
				var chart = d3.select('#chart')
				.append("ul").attr("class", "chart")
				.selectAll('.chart-item')
				.data(data).enter()
				.append("li").attr("class","chart-group clearfix")

				.append("ul").attr("class","col-sm-10 col-xs-10 chart-row")
				.append("li").attr("class","chart-item")
				.transition().ease("elastic");

				chart.style("width", function(d) { 
					return d.valor + "%"; 
				});
				chart.text(function(d) { 
					return d.name;
				});

				$("<div class='col-sm-2 col-xs-2 chart-name'></div>").insertAfter(".chart-row");

				$.each(data, function(key, value){
					var teste = $(".chart .chart-name")[key];
					console.log($(teste).text(data[key].name))
				});
			}
		}
    }

})();
