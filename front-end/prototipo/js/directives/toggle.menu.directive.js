(function () {
    'use strict';

    angular
    .module('eventsSaas.menu')
    .directive("toggleMenu", ToggleMenu)

    ToggleMenu.$inject = ['$rootScope'];

    function ToggleMenu($rootScope){
        return {
            restrict: 'A',
            link: function(scope, element, attr) {
                $rootScope.isShowMenu = false;
                element.bind("click", function(){
                    scope.$apply(function() { 
                        $rootScope.isShowMenu = !$rootScope.isShowMenu;
                    });
                });
            }
        }
    }

})();
