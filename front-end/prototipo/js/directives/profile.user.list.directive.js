(function () {
    'use strict';

    angular
    .module('eventsSaas.profile.user')
    .directive("userList", userList)

    userList.$inject = ['eventsSaasUserService', '$filter'];

    function userList(eventsSaasUserService, $filter){
        return {
			restrict: 'E',
			template: '<ul class="list-center list-profile"> <li ng-repeat="list in profile" ng-class="{\'font-40\': !list.image}"> <a href="javascript:;" ng-if="list.image"> <img ng-attr-src="{{list.image}}" alt="" class="img-circle"/> </a> <span class="fa-stack fa-lg img-circle {{list.bg}}" ng-if="!list.image"> <em class="fa fa-circle fa-stack-2x"></em> <em class="fa {{list.ico}} fa-stack-1x font-11 fa-inverse"></em> </span> </li></ul>',
			replace: true,
			scope: {
				listar: "@"
		    },
		    link: function(scope, element, attr) {
				scope.user = [];
				scope.profile = [];
		    	scope.carregarUser = carregarUser;
		    	scope.carregarUser();
		        function carregarUser() {
			        return getUser().then(function() {
			            preencherLista(scope.user);
			        });
			    };
			    function getUser() {
			        return eventsSaasUserService.getUser()
			        .then(function(data) {
			            scope.user = $filter("filter")(data, {id: 3});
			            return scope.user;
			        });
			    };
			    function preencherLista(data){
			    	angular.forEach(data, function (value, key) {
						angular.forEach(data, function (value, key) {
							angular.forEach(data[key], function (v, index) {
								if(scope.listar == [index]){
									scope.profile = data[key][index]
								}
							});
						});
					});
			    };
	        }
		};
    }

})();
