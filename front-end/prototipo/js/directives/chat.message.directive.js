(function () {
    'use strict';

    angular
    .module('eventsSaas.chat')
    .directive("chatMessage", chatMessage)

    chatMessage.$inject = ['$timeout'];

    function chatMessage($timeout){
        return {
			restrict: 'E',
			replace: true,
			template: '<div class="chat-content"><div class="chat-mensagens"><div class="chat-container-mensagens"><div class="chat-content-mensagens"><div class="chat-overflow" scroll-glue><div class="col-xs-12 col-sm-12"><ul class="has-item-chat"><li ng-repeat="msg in mensagens" class="clearfix"><ul id="row-{{msg.id}}" ng-class="{\'my-msg\': msg.myMsg, \'friend-msg\': !msg.myMsg}"><li class="description" ng-if="msg.myMsg"><div class="content-description"><p class="font-openregular font-26 font-white">{{msg.texto}}</p></div><p class="font-grey-sure-3 font-openregular font-18 col-xs-12 text-right">{{msg.time}}</p></li><li class="has-img" ng-if="msg.myMsg"><img alt="" ng-attr-src="{{msg.image}}" class="img-circle"></li><li class="has-img text-right" ng-if="!msg.myMsg"><img alt="" ng-attr-src="{{msg.image}}" class="img-circle"></li><li class="description" ng-if="!msg.myMsg"><div class="content-description"><p class="font-grey-dark-4 font-openregular font-26">{{msg.texto}}</p></div><p class="font-grey-sure-3 font-openregular font-18 col-xs-12">{{msg.time}}</p></li></ul></li></ul></div></div></div></div></div><div class="chat-edit"><div class="chat-container-edit"><div class="chat-content-edit"><div class="edit-btn-msg"><div class="contenteditable font-26" contentedit contenteditable="true" ng-model="mensagem" placeholder="Digite sua mensagem"></div></div></div><div class="edit-btn-box"><a href="javascript:;"><em class="ion-plus font-30"></em></a><button ng-click="addMsg(mensagem)" class="bg-green-sure btn-send"><em class="ion-android-send font-30 font-white"></em></button></div></div></div></div>',
			scope: {
		    },
		    link: function(scope, element, attr) {

		    	scope.mensagens = [
		            {
		                "id": "1",
		                "image": "img/user14.png",
		                "texto": "Olá Ricardo! Gostou da palestra de hoje cedo? Achei a turma bastante animada... Qual a sua dúvida?",
		                "time": "3 mins ago",
		                "myMsg": true
		            },
		             {
		                "id": "2",
		                "image": "img/user15.png",
		                "texto": "Foi bom demais! Então...",
		                "time": "10 seg ago",
		                "myMsg": false
		            },
		            {
		                "id": "3",
		                "image": "img/user14.png",
		                "texto": "Legal...",
		                "time": "2 mins ago",
		                "myMsg": true
		            },
		            {
		                "id": "4",
		                "image": "img/user14.png",
		                "texto": "Olá Ricardo! Gostou da palestra de hoje cedo? Achei a turma bastante animada... Qual a sua dúvida?",
		                "time": "3 mins ago",
		                "myMsg": true
		            },
		             {
		                "id": "5",
		                "image": "img/user15.png",
		                "texto": "Foi bom demais! Então...",
		                "time": "10 seg ago",
		                "myMsg": false
		            },
		            {
		                "id": "6",
		                "image": "img/user14.png",
		                "texto": "Legal...",
		                "time": "13 mins ago",
		                "myMsg": true
		            }
		        ]
	        	var totalMsg = scope.mensagens.length + 1;
	        	var d = new Date();
				var m = d.getMinutes() + " min";
		        $timeout(function() {
		            var obj = { id: totalMsg, image: "img/user15.png", texto: "Até a proxima..", time: m, myMsg: false };
		    		scope.mensagens.push(obj);
		        }, 2500);
		    	scope.addMsg = function(msg){
		    		if(msg !=""){
			    		var obj = { id: totalMsg, image: "img/user14.png", texto: msg, time: m, myMsg: true };
			    		scope.mensagens.push(obj);
			    		scope.mensagem = "";
		    		}
		    	}
	        }
		};
    }

})();
