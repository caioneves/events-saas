(function () {
    'use strict';

    angular
    .module('eventsSaas.timeline')
    .directive("timelineContentPatrocinador", TimelineContentPatrocinador)

    TimelineContentPatrocinador.$inject = ['EventsSaasPatrocinadorService'];

    function TimelineContentPatrocinador(EventsSaasPatrocinadorService){
        return {
			restrict: 'E',
			template: '<li class="content-user" ng-repeat="item in patrocinador"><ul class="clearfix"><li class="has-img"><img ng-attr-src="{{item.image}}" alt="" class="img-circle"></li><li class="description brd-bottom-sure-1"><h3 class="title-3">{{item.nome}}</h3><div ng-repeat="promocao in item.promocao"><p ng-if="promocao.image" class="font-28 ph"><a href="javascript:;"><img ng-attr-src="{{promocao.image}}" alt="" /></a></p><p ng-if="promocao.texto" class="font-28 ph">{{promocao.texto}} <a href="javascript:;" class="font-red-sure more">[...]</a></p><ul class="interaction"><li class="font-grey-sure-3 pull-left font-24"> <timeline-time icon="true" ng-attr-tempo="{{promocao.tempo}}"></timeline-time></li><li class="has-like-commet pull-right font-24"><a href="javascript:;" class="font-grey-sure-3"><em class="ion-heart"></em> {{promocao.link}}</a><a href="javascript:;" class="font-grey-sure-3"><em class="ion-chatbubble"></em> {{promocao.comment}}</a></li></ul></div></li></ul></li>',
			replace: true,
			scope: {
		    },
		    link: function(scope, element, attr) {
				scope.patrocinador = [];
		        EventsSaasPatrocinadorService.getPatrocinador().success(function(data){
		            scope.patrocinador = data;
		        }).error(function(data, status){
		            
		        });
	        }
		};
    }

})();
