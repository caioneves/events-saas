(function () {
    'use strict';

    angular
    .module('eventsSaas.user')
    .directive("hasUser", HasUser)

    HasUser.$inject = ['eventsSaasUserService', '$filter'];

    function HasUser(eventsSaasUserService, $filter){
        return {
			restrict: 'E',
			template: '<li class="has-user-block" ng-repeat="item in user"><div class="user-block"><img ng-attr-src="{{item.avatar}}" alt="" class="img-circle pull-left"/><div class="user-block-info pull-left"><span class="user-block-name font-34 pull-left">{{item.nome}}</span> <em class="icon ion-android-settings font-34"></em><ul class="user-block-role clearfix"><li><a href="javascript:;"><em class="icon ion-ios-heart-outline font-green-sure font-26"></em> <span>{{item.link}}</span></a></li><li><a href="javascript:;"><em class="icon ion-ios-chatbubble-outline font-green-sure font-26"></em> <span>{{item.comment}}</span></a></li></ul></div></div></li>',
			replace: true,
			scope: {
		    },
		    link: function(scope, element, attr) {
		    	scope.user = [];
		    	scope.carregarUser = carregarUser;
		    	scope.carregarUser();
		    	function carregarUser() {
			        return getUser().then(function() {
			            console.log("get use")
			        });
			    };
			    function getUser() {
			        return eventsSaasUserService.getUser()
			        .then(function(data) {
			            scope.user = $filter("filter")(data, {id: 3});
			            return scope.user;
			        });
			    };
	        }
		};
    }

})();
