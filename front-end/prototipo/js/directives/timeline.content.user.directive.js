(function () {
    'use strict';

    angular
    .module('eventsSaas.timeline')
    .directive("timelineContentUser", TimelineContentUser)

    TimelineContentUser.$inject = ['eventsSaasUserService'];

    function TimelineContentUser(eventsSaasUserService){
        return {
			restrict: 'E',
			template: '<li class="content-user" ng-repeat="item in user"><ul class="clearfix"><li class="has-img"><img ng-attr-src="{{item.avatar}}" alt="" class="img-circle"></li><li class="description brd-bottom-sure-1"><h3 class="title-3">{{item.nome}}</h3><div ng-repeat="artigo in item.artigo"><p class="font-28 ph">{{artigo.texto | limitTo: 100}} <a href="#/matter/{{artigo.id}}" class="font-red-sure more">[...]</a></p><ul class="interaction"><li class="font-grey-sure-3 pull-left font-24"> <timeline-time icon="true" ng-attr-tempo="{{artigo.tempo}}"></timeline-time></li><li class="has-like-commet pull-right font-24"><a href="javascript:;" class="font-grey-sure-3"><em class="ion-heart"></em> {{artigo.link}}</a><a href="#/comment" class="font-grey-sure-3"><em class="ion-chatbubble"></em> {{artigo.comment}}</a></li></ul></div></li></ul></li>',
			replace: true,
			scope: {
		    },
		    link: function(scope, element, attr) {
				scope.user = [];
		        eventsSaasUserService.getUser().success(function(data){
		            scope.user = data;
		        }).error(function(data, status){
		            
		        });
	        }
		};
    }

})();
