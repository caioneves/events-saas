(function () {
    'use strict';

    angular
    .module('eventsSaas.chat')
    .directive("contentedit", contentedit)

    contentedit.$inject = [];

    function contentedit(){
        return {
			require: 'ngModel',
		    link: function(scope, element, attr, ctrl) {
		    	// view -> model
                element.bind('blur', function() {
                    scope.$apply(function() {
                        ctrl.$setViewValue(element.html());
                    });
                });

                // model -> view
                ctrl.$render = function() {
                    element.html(ctrl.$viewValue);
                };

                // load init value from DOM
                ctrl.$setViewValue(element.html());
		    }
		};
    }

})();
