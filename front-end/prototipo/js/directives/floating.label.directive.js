(function () {
    'use strict';

    angular
    .module('eventsSaas.custom.form')
    .directive("floatingLabel", FloatingLabel)

    FloatingLabel.$inject = [];

    function FloatingLabel(){
    	return {
	    	require: 'ngModel',
		    link: function(scope, element, attr, ctrl) {
				element.bind("keyup", function(){
					if(element.val() !=""){
						$(element).prev().addClass("has-input");
					} else {
						$(element).prev().removeClass("has-input");
					}
				});
				element.bind("keydown", function(){
					if(element.val() ==""){
						$(element).prev().removeClass("has-input");
					}
				});
	        }
	    }
    }

})();