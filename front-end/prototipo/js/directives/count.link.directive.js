(function () {
    'use strict';

    angular
    .module('eventsSaas.interaction')
    .directive("countLink", countLink)

    countLink.$inject = ['$compile'];

    function countLink($compile){
        return {
            restrict: 'A',
            scope: {
                countLink: "@",
                ngDisabled: "@"
            },
            link: function(scope, element, attr) {
                scope.isDisabled = false; 
                element.bind("click", function(){
                    if(!$(this).hasClass("active")){
                        var link = parseInt(element.children("span").text());
                        element.children("span").text(link+1);
                        element.addClass("active");
                    }
                });
            }
        }
    }

})();
