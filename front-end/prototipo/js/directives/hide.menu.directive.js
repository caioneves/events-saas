(function () {
    'use strict';

    angular
    .module('eventsSaas.menu')
    .directive("hideMenu", hideMenu)

    hideMenu.$inject = ['$rootScope'];

    function hideMenu($rootScope){
        return {
			restrict: 'A',
			scope: {
		    },
		    link: function(scope, element, attr) {
				$rootScope.isShowMenu = false;
                element.bind("click", function(){
                    scope.$apply(function() { 
                        $rootScope.isShowMenu = !$rootScope.isShowMenu;
                    });
                });
	        }
		};
    }

})();
