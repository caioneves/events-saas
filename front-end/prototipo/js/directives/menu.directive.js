(function () {
    'use strict';

    angular
    .module('eventsSaas.menu')
    .directive("menuItem", MenuItem)

    MenuItem.$inject = ['eventsSaasMenuService', '$rootScope'];

    function MenuItem(eventsSaasMenuService, $rootScope){
        return {
			restrict: 'E',
			template: '<li class="font-opensemibold text-uppercase" ng-repeat="item in menu"><a href="{{item.link}}" hide-menu>{{item.name}}<small class="badge bg-blue-sure" ng-if="item.alert">{{item.alert}}</small></a></li>',
			replace: true,
			scope: {
		    },
		    link: function(scope, element, attr) {
				scope.menu = [];
				$rootScope.hasNotificationMenu = false;
		        eventsSaasMenuService.getMenu().success(function(data){
		            scope.menu = data;
		            var keepGoing = true;
		            angular.forEach(data, function(value, key) {
		            	if(keepGoing){
		            		if(data[key].alert){
		            			$rootScope.hasNotificationMenu = !$rootScope.hasNotificationMenu;
		            			keepGoing = false;
		            		}
		            	}
		            });
			        }).error(function(data, status){
		            
		        });
	        }
		};
    }

})();
