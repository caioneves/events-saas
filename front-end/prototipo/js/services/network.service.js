(function () {
    'use strict';

	angular.module('eventsSaas.network')
	.factory("eventsSaasNetworkService", EventsSaasNetworkService)

	EventsSaasNetworkService.$inject = ['$http', 'HostValue']

	function EventsSaasNetworkService($http, HostValue) {
		return {
			getNetwork: getNetwork
		};
        function getNetwork(){
        	return $http.get(HostValue.baseUrl + "network.json")
            .then(getNetworkComplete)
            .catch(getNetworkFailed);

	        function getNetworkComplete(response) {
	            return response.data;
	        };

	        function getNetworkFailed(error) {
	            console.log('XHR Failed Mensagem Amigos' + error.data);
	        };
		};
    }

})();