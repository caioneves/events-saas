(function () {
    'use strict';

	angular.module('eventsSaas.menu')
	.factory("eventsSaasMenuService", eventsSaasMenuService)

	eventsSaasMenuService.$inject = ['$http', 'HostValue']

	function eventsSaasMenuService($http, HostValue) {
        var _getMenu = function(){
			return $http.get(HostValue.baseUrl + "menu.json");      
		};

		return {
			getMenu: _getMenu
		}
    }

})();