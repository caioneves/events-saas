(function () {
    'use strict';

	angular.module('eventsSaas.user')
	.factory("eventsSaasUserService", EventsSaasUserService)

	EventsSaasUserService.$inject = ['$http', 'HostValue']

	function EventsSaasUserService($http, HostValue) {
        return {
			getUser: getUser
		};
        function getUser(){
        	return $http.get(HostValue.baseUrl + "user.json")
            .then(getUserComplete)
            .catch(getUserFailed);

	        function getUserComplete(response) {
	            return response.data;
	        };

	        function getUserFailed(error) {
	            console.log('XHR Failed get User' + error.data);
	        };
		};
    };
})();