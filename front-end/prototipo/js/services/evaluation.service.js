(function () {
    'use strict';

	angular.module('eventsSaas.evaluation')
	.factory("eventsSaasEvaluationService", EventsSaasEvaluationService)

	EventsSaasEvaluationService.$inject = ['$http', 'HostValue']

	function EventsSaasEvaluationService($http, HostValue) {
        var _getEvaluation = function(){
			return $http.get(HostValue.baseUrl + "evaluation.json");      
		};

		return {
			getEvaluation: _getEvaluation
		}
    }

})();