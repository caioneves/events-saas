(function () {
    'use strict';

	angular.module('eventsSaas.quick.access')
	.factory("eventsSaasQuickAccessService", eventsSaasQuickAccessService)

	eventsSaasQuickAccessService.$inject = ['$http', 'HostValue']

	function eventsSaasQuickAccessService($http, HostValue) {
        return {
			getQuickAccess: getQuickAccess
		};
        function getQuickAccess(){
        	return $http.get(HostValue.baseUrl + "quick.access.json")
            .then(getQuickAccessComplete)
            .catch(getQuickAccessFailed);

	        function getQuickAccessComplete(response) {
	            return response.data;
	        };
	        function getQuickAccessFailed(error) {
	            console.log('XHR Failed Mensagem Amigos' + error.data);
	        };
		};
    };

})();