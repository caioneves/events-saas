(function () {
    'use strict';

	angular.module('eventsSaas.events')
	.factory("eventsSaasEventsService", EventsSaasEventsService)

	EventsSaasEventsService.$inject = ['$http', 'HostValue']

	function EventsSaasEventsService($http, HostValue) {
        var _getEvents = function(){
			return $http.get(HostValue.baseUrl + "events.json");      
		};

		return {
			getEvents: _getEvents
		}
    }

})();