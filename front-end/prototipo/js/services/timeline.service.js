(function () {
    'use strict';

	angular.module('eventsSaas.timeline')
	.factory("eventsSaasTimelineService", eventsSaasTimelineService)

	eventsSaasTimelineService.$inject = ['$http', 'HostValue']

	function eventsSaasTimelineService($http, HostValue) {
		return {
			getTimeline: getTimeline
		};
        function getTimeline(){
        	return $http.get(HostValue.baseUrl + "timeline.json")
            .then(getTimelineComplete)
            .catch(getTimelineFailed);

	        function getTimelineComplete(response) {
	            return response.data;
	        };

	        function getTimelineFailed(error) {
	            console.log('XHR Failed Timeline' + error.data);
	        };
		};
    }

})();