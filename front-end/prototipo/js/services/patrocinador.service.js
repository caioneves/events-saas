(function () {
    'use strict';

	angular.module('eventsSaas.patrocinador')
	.factory("eventsSaasPatrocinadorService", eventsSaasPatrocinadorService)

	eventsSaasPatrocinadorService.$inject = ['$http', 'HostValue']

	function eventsSaasPatrocinadorService($http, HostValue) {
        return {
			getPatrocinador: getPatrocinador
		};
        function getPatrocinador(){
        	return $http.get(HostValue.baseUrl + "patrocinador.json")
            .then(getPatrocinadorComplete)
            .catch(getPatrocinadorFailed);

	        function getPatrocinadorComplete(response) {
	            return response.data;
	        };

	        function getPatrocinadorFailed(error) {
	            console.log('XHR Failed get Patrocinador' + error.data);
	        };
		};
    };

})();