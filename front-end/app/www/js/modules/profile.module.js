(function () {
    'use strict';

    angular.module('eventsSaas.profile', [
    	'ngAnimate',
        'eventsSaas.auth',
        'eventsSaas.custom.form',
		'eventsSaas.dialog',
        'eventsSaas.host'
    ]);
})();