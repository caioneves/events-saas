(function () {
    'use strict';

    angular.module('eventsSaas.message', 
	    [
	    	'ngAnimate',
			'ngSanitize',
	    	'eventsSaas.router',
	    	'eventsSaas.loading',
	        'eventsSaas.events',
	        'eventsSaas.timeline',
			'eventsSaas.login',
	        'eventsSaas.chat',
			'eventsSaas.filter',
			'eventsSaas.dialog'
	    ]);
})();