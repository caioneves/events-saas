(function () {
    'use strict';

    angular.module('eventsSaas.login',
        [
        	'ui.bootstrap',
            'eventsSaas.router',
            'eventsSaas.modal',
            'eventsSaas.custom.form', 
            'eventsSaas.loading',
            'eventsSaas.auth',
            'eventsSaas.profile',
            'eventsSaas.dialog',
            'eventsSaas.host'
        ]);
})();