(function () {
    'use strict';

    angular.module('eventsSaas.comment',
        [
            'eventsSaas.router',
            'eventsSaas.loading',
            'eventsSaas.chat'
        ]);
})();