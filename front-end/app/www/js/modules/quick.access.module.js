(function () {
    'use strict';

    angular.module('eventsSaas.quick.access',
        [
            'eventsSaas.router',
            'eventsSaas.custom.form',
            'eventsSaas.loading'
        ]);
})();