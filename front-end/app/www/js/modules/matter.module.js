(function () {
    'use strict';

    angular.module('eventsSaas.matter',
        [
            'ngAnimate',
            'eventsSaas.router',
            'eventsSaas.loading',
            'eventsSaas.profile',
            'eventsSaas.timeline',
            'eventsSaas.filter',
            'eventsSaas.info.event'
        ]);
})();