(function () {
    'use strict';

    angular.module('eventsSaas.evaluation',
        [
            'eventsSaas.router',
            'eventsSaas.profile'
        ]);
})();