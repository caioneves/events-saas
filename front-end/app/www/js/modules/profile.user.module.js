(function () {
    'use strict';

    angular.module('eventsSaas.profile.user',
        [
            'eventsSaas.router',
            'eventsSaas.custom.form',
            'eventsSaas.loading',
            'eventsSaas.profile'
        ]);
})();