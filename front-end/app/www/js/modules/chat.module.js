(function () {
    'use strict';

    angular.module('eventsSaas.chat', [
    	'ngAnimate',
        'luegg.directives'
    ]);
})();