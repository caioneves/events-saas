(function () {
    'use strict';

    angular.module('eventsSaas.timeline',
        [
            'ui.bootstrap',
            'eventsSaas.router',
            'eventsSaas.loading',
            'eventsSaas.filter',
            'eventsSaas.chat',
            'eventsSaas.interaction'
        ]);
})();