(function () {
    'use strict';

    angular.module('eventsSaas.menu', [
        'ngAnimate',
        'eventsSaas.message',
        'eventsSaas.survey',
        'eventsSaas.host'
    ]);
})();