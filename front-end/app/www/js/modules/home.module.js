(function() {
    'use strict';

    angular
        .module('eventsSaas.home', [
            'eventsSaas.router',
            'eventsSaas.loading',
            'eventsSaas.quick.access',
            'eventsSaas.timeline',
            'eventsSaas.atividades',
            'eventsSaas.evaluation',
            'eventsSaas.events',
            'slickCarousel'
        ]);
})();