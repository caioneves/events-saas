(function() {
    'use strict';

    angular
        .module('eventsSaas.info.event', [
            'ngAnimate',
            'eventsSaas.filter'
        ]);
})();