(function () {
    'use strict';

    angular.module('eventsSaas.profile.lecturer', [
    	'ngAnimate',
        'eventsSaas.evaluation',
        'eventsSaas.chat'
    ]);
})();