(function() {
    'use strict';

    angular
        .module('eventsSaas.profile')
        .directive('hasUser', hasUser);

    hasUser.$inject = ['eventsSaasProfileService', 'eventsSaasRoutes', 'HostValue', 'localStorageService'];

    /* @ngInject */
    function hasUser(eventsSaasProfileService, eventsSaasRoutes, HostValue, localStorageService) {
        // Usage:
        //
        // Creates:
        //
        var hasUser = {
            bindToController: true,
            controller: Controller,
            template: '<li class="has-user-block"><div class="user-block"><div class="mn-person-info"><div class="mn-person-info__picture"><img ng-attr-src="{{user.pictureUrl ? baseUrl+user.pictureUrl : avatar}}" ng-class="{\'ghost-person\': !user.pictureUrl}" alt="" class="img-circle pull-left"/></div><div class="mn-person-info__details"><div class="user-block-info"><span class="user-block-name font-34 pull-left">{{user.firstName}} {{user.lastName}}</span> <a href="#{{linkEdit}}"><em class="icon ion-android-settings font-34"></em></a></div><a class="display-2 font-26 font font-openregular font-green-sure" href="#{{linkProfileUser}}">VER MEU PERFIL</a></div></div></div></li>',
            controllerAs: 'vm',
            replace: true,
            link: link,
            restrict: 'E',
            scope: {
            }
        };
        return hasUser;

        function link(scope, element, attrs) {
	    	scope.user = null;
	    	scope.viewUser = viewUser;
            scope.linkEdit = eventsSaasRoutes.EDITARUSUARIO;
            scope.baseUrl = HostValue.aws;
            scope.avatar = HostValue.ghostsPerson;
            scope.linkProfileUser = eventsSaasRoutes.PERFILUSUARIO;

            if(localStorageService.get('profile')){
                scope.user = localStorageService.get('profile');
            } else {
	    	    scope.viewUser();
            };

	    	function viewUser() {
		        return getProfileLogged(HostValue.idEvent).then(function(data) {
                    console.log("User menu "+ data)
		            scope.user = data;
		        });
		    };

		    function getProfileLogged(id) {
		        return eventsSaasProfileService.getProfile(id)
		        .then(function(data) {
		            return data;
		        })
                .catch(function(erro){
                    return erro
                });
		    };
        }
    };

    /* @ngInject */
    function Controller() {

    };

})();
