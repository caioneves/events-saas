(function () {
    'use strict';

    angular
    .module('eventsSaas.timeline')
    .directive("timelineTime", TimelineTime)

    TimelineTime.$inject = ['$interval'];

    function TimelineTime($interval){
        return {
			restrict: 'E',
			template: '<span ng-transclude><em ng-if="icon" class="fa fa-clock-o"></em> {{timeline}}</span>',
			replace: true,
			scope: {
				tempo: "@",
				icon: "@"
		    },
		    transclude: true,
		    link: function(scope, element, attr) {
				//console.log(scope.tempo);
		    	var formatDate = parseInt(scope.tempo);
				function getDateDiff(date1, date2, interval) {
					var second = 1000,
					minute = second * 60,
					hour = minute * 60,
					day = hour * 24,
					week = day * 7,
					dateone = new Date(date1).getTime(),
					datetwo = (date2) ? new Date().getTime() : new Date(date2).getTime();
					var timediff = datetwo - dateone,
					secdate = new Date(date2),
					firdate = new Date(date1);
					if (isNaN(timediff)) return NaN;
					switch (interval) {
					case "anos":
						return secdate.getFullYear() - firdate.getFullYear();
					case "meses":
						return monthDiff(firdate, secdate);
					case "semanas":
						return Math.floor(timediff / week);
					case "dias":
						return Math.floor(timediff / day);
					case "horas":
						return Math.floor(timediff / hour);
					case "minutos":
						return Math.floor(timediff / minute);
					case "segundos":
						return Math.floor(timediff / second);
					default:
						return undefined;
					}
				};

				function monthDiff(d1, d2) {
					var months;
					months = (d2.getFullYear() - d1.getFullYear()) * 12;
					months -= d1.getMonth() + 1;
					months += d2.getMonth();
					return months <= 0 ? 0 : months;
				}

				function viewTextTime(){
					var anos = getDateDiff(formatDate, new Date(), 'anos');
					var meses = getDateDiff(formatDate, new Date(), 'meses');
					var semanas = getDateDiff(formatDate, new Date(), 'semanas');
					var dias = getDateDiff(formatDate, new Date(), 'dias');
					var horas = getDateDiff(formatDate, new Date(), 'horas');
					var minutos = getDateDiff(formatDate, new Date(), 'minutos');
					var segundos = getDateDiff(formatDate, new Date(), 'segundos');
					if(anos > 0){
						return anos +" ano(s)";
					} else if(meses > 0) {
						return meses +" mese(s)";
					} else if(semanas > 0) {
						return semanas +" semana(s)";
					} else if(dias > 0){
						return dias +" dia(s)";
					} else if(horas > 0){
						return horas +" hora(s)";
					} else if(minutos > 0){
						return minutos +" minuto(s)";
					} else {
						return  "neste momento";
					}
				};
				scope.$watch('name', function() {
					scope.timeline = viewTextTime();
				});
	        }
		};
    }

})();
