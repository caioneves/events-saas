(function() {
    'use strict';

    angular
        .module('eventsSaas.card.virtual')
        .directive('addCard', addCard);

    addCard.$inject = ['eventsSaasProfileService', '$rootScope', '$cordovaBarcodeScanner'];

    /* @ngInject */
    function addCard(eventsSaasProfileService, $rootScope, $cordovaBarcodeScanner) {
        // Usage:
        //
        // Creates:
        //
        var addCard = {
            bindToController: true,
            controller: AddCarVirtual,
            controllerAs: 'vm',
            link: link,
            restrict: 'A',
            scope: {
            }
        };
        return addCard;

        function link(scope, element, attrs) {
        	element.bind("click", function(){
                 $cordovaBarcodeScanner
                 .scan()
                 .then(function(barcodeData) {
                   // Success! Barcode data is here
                    var qrCodeId = barcodeData.text; 
                    console.log(qrCodeId); 
                
                var miniProfile = getMiniprofileById(qrCodeId);
                  
                //Adicionar na lista de amigos o novo mini profile
                
                 }, function(error) {
                    console.log(error)
                 });
            });

            function getMiniprofileById(idQRcode){
                return getMiniProfile(idQRcode).then(function(data){
                    $rootScope.vm.cards = data;
                    addFriend(idQRcode);
                });
            }
            function getMiniProfile(id){
                return eventsSaasProfileService.getMiniprofileById(id)
                .then(function(data){
                    return data; 
                }, function(err){
                    console.log("Erro" + err)
                });
            }

            function addFriend(idQRcode){
                return addProfileFriend(idQRcode).then(function(data){
                    
                });
            }
            function addProfileFriend(id){
                return eventsSaasProfileService.addFriend(id)
                .then(function(data){
                    return data; 
                }, function(err){
                    console.log("Erro" + err)
                });
            }
        }
    }

    /* @ngInject */
    function AddCarVirtual() {

    }
})();