(function () {
    'use strict';

    angular
    .module('eventsSaas.chat')
    .directive("chat", chat)

    chat.$inject = [];

    function chat(){
        return {
			restrict: 'E',
			replace: true,
			transclude: true,
			template: '<div class="chat"><div class="chat-container" on-height><chat-message></chat-message></div></div>',
			scope: {
		    },
		    link: function(scope, element, attr) {

		    }
		};
    }

})();
