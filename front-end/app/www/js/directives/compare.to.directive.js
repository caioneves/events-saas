(function() {
	'use strict';

	angular
		.module('eventsSaas.custom.form')
		.directive('compareTo', compareTo);

	compareTo.$inject = [];
	function compareTo() {
		// Usage:
		//
		// Creates:
		//
		var directive = {
			link: link,
	    	require: 'ngModel'
		};
		return directive;
		
		function link(scope, elm, attrs, ctrl) {
			scope.$watch(attrs.compareTo, function (newValue) {
                  if (ctrl && ctrl.$modelValue) {
                      if (newValue === ctrl.$modelValue) {
                          ctrl.$setValidity('compareTo', true);
                          // return newValue;
                      } else {
                          ctrl.$setValidity('compareTo', false);
                          // return undefined;
                      }
                  }
              });
              
              ctrl.$parsers.unshift(function(viewValue) {
                  if (viewValue) {
                      if (viewValue === scope.$eval(attrs.compareTo)) {
                          ctrl.$setValidity('compareTo', true);
                          return viewValue;
                      } else {
                          ctrl.$setValidity('compareTo', false);
                          return undefined;
                      }
                  } else {
                      ctrl.$setValidity('compareTo', true);
                      return viewValue;
                  }
              });
		};
	};
	/* @ngInject */
	function ControllerController () {
		
	}
})();