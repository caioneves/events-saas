(function () {
    'use strict';

    angular
    .module('eventsSaas.interaction')
    .directive("countLink", countLink)

    countLink.$inject = ['$compile', 'eventsSaasTimelineService'];

    function countLink($compile, eventsSaasTimelineService){
        return {
            restrict: 'A',
		    require: 'ngModel',
            scope: {
            },
            link: function(scope, element, attr, ngModel) {
                element.bind("click", function(){
                    var name = $(this).attr('name');
                    if (!ngModel.$viewValue.liked) {
                        ngModel.$viewValue.liked = true;
                        ngModel.$viewValue.numberLike += 1;

                        if (name != 'likeDetail') {
                            element.addClass("has-like");
                        } else {
                            element.removeClass('bg-white font-grey-dark-3 brd-grey-dark').addClass('bg-blue-dark font-white');
                        }
                        scope.$apply();

                        eventsSaasTimelineService.like(ngModel.$viewValue.id);
                    } else if (ngModel.$viewValue.liked) {
                        ngModel.$viewValue.liked = false;
                        ngModel.$viewValue.numberLike -= 1;

                        if (name != 'likeDetail') {
                            element.removeClass("has-like");
                        } else {
                            element.removeClass('bg-blue-dark font-white').addClass('bg-white font-grey-dark-3 brd-grey-dark');
                        }
                        scope.$apply();

                        eventsSaasTimelineService.unlike(ngModel.$viewValue.id);


                    }
                });
            }
        }
    }

})();
