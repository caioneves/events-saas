angular
    .module('eventsSaas.chat')
.directive('contentedit', function () {
    return {
        controller: 'EditorController',
        restrict: 'E',
        replace: true,
		require: 'ngModel',
        template:'<div class="contenteditable font-26" ng-paste="handlePaste($event)" contenteditable="true" ng-model="vm.message" placeholder="Digite sua mensagem"></div>',
        link: function(scope, element, attr, ctrl) {
            // view -> model
            element.bind('blur', function() {
                scope.$apply(function() {
                    ctrl.$setViewValue(element.html());
                });
            });

            // model -> view
            ctrl.$render = function() {
                element.html(ctrl.$viewValue);
            };

            // load init value from DOM
            ctrl.$setViewValue(element.html());
        }
    }
}).
controller('EditorController', function ($scope) {
    $scope.handlePaste = function (event) {
        event.preventDefault();
        event.stopPropagation();
        var plaintext = event.originalEvent.clipboardData.getData('text/plain');
        document.execCommand('inserttext', false, plaintext);
    };
});