(function () {
    'use strict';

    angular
    .module('eventsSaas.chat')
    .directive("onHeight", onHeight)

    onHeight.$inject = ['$window', '$rootScope'];

    function onHeight($window, $rootScope){
        return {
			restrict: 'A',
			scope: {
				onHeightContect: "@"
		    },
		    link: function(scope, element, attr) {
		    	scope.onResize = function() {
		    		element.removeAttr("style");
					var footerH = 1, headerH = 0;
					if(hasFooterNavOnHeight()){
						if($(window).width() >= 768){
							footerH =  78;
						} else if($(window).width() < 768 && $(window).width() > 320 ){
							footerH =  70
						} else {
							footerH =  48
						}
					}
					var heightContent = isHeightContent(scope.onHeightContect) ? 0 : $('.'+scope.onHeightContect+'').outerHeight();
					if(!heightContent){
						heightContent = 0;
					} else {
						heightContent = heightContent + 33;
					}
					
					headerH = $rootScope.headerHeight;

					element.height($(window).height() - (heightContent + footerH + headerH));
	                if(!heightContent){
	                	element.css({minHeight: element.height()+"px", maxHeight: element.height()+"px"})
	                }
	            }
	            scope.onResize();
	            angular.element($window).bind('resize', function() {
	                scope.onResize();
	            });
				function hasFooterNavOnHeight(){
					return $('.nav-bar-footer').length;
				};
				function isHeightContent(ele){
					scope.$watch(function() {
						return "undefined" === typeof ele || ele == null || ele == "" ? true : false;
					});
				};
	        }
		};
    };

})();
