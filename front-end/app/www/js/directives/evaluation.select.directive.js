(function () {
    'use strict';

    angular
    .module('eventsSaas.evaluation')
    .directive("evaluationSelect", EvaluationSelect)

    EvaluationSelect.$inject = [];

    function EvaluationSelect(){
        return {
			restrict: 'A',
		    scope: {
				evaluation: "@evaluationSelect"
		    },
		    link: function(scope, element, attr) {
				var els = element.children();
				angular.forEach(els, function(value, key){
					var a = angular.element(value);
	    			if(key <= scope.evaluation){
	    				a.toggleClass("active")
	    			}
				});
				//console.log(element.children())
				/*element.bind("click", function(){
					els.removeClass("active");
					var end = $(element).index();
					angular.forEach(els, function(value, key){
						var a = angular.element(value);
		    			if(key <= end){
		    				a.toggleClass("active")
		    			}
					});
				})*/
				
	        }
		};
    }

})();
