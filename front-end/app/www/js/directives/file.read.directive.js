(function() {
    'use strict';

    angular
        .module('eventsSaas.message')
        .directive('fileread', FileRead);

    FileRead.inject = ['eventsSaasMessageService', '$rootScope'];
    function FileRead(eventsSaasMessageService, $rootScope) {
        // Usage:
        //
        // Creates:
        //
        var directive = {
            controller: ControllerController,
            controllerAs: 'vm',
            link: link,
		    require: 'ngModel',
            scope: {
                fileread: "="
            }
        };
        return directive;
        
        function link(scope, element, attrs, ctrl) {
            var image;
            element.bind("change", function (changeEvent) {
                //console.log(element.files[0])
                scope.applyImage = applyImage;
                var reader = new FileReader();
                reader.onload = function (loadEvent) {
                    image = scope.applyImage(element[0].files[0].type, loadEvent.target.result);
                    scope.$apply(function () {
                        ctrl.$setViewValue(image);
                    });
                }
                reader.readAsDataURL(changeEvent.target.files[0]);
            });
            function applyImage(type, result){
                return sendImage(type, result)
                .then(function(valor){
                    var message = new Object();
                    message.image = valor;
                    $rootScope.$broadcast('applyMessage', message);
                    return valor;
                })
            };
            function sendImage(type, result){
                return eventsSaasMessageService.postProfileMessageImage(type, result)
                .then(function(data){
                    return data;
                })
                .catch(function(error){
                    console.log("Error no data" +error)
                })
            };

            // model -> view
            ctrl.$render = function() {
                element.html(ctrl.$viewValue);
            };

            // load init value from DOM
            ctrl.$setViewValue(image);
        }
    }
    /* @ngInject */
    function ControllerController () {
        
    }
})();