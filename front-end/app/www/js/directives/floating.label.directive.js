(function() {
	'use strict';

	angular
		.module('eventsSaas.custom.form')
		.directive('floatingLabel', FloatingLabel);

	FloatingLabel.$inject = [];
	function FloatingLabel() {
		// Usage:
		//
		// Creates:
		//
		var directive = {
			bindToController: true,
			controller: ControllerController,
			controllerAs: 'vm',
			link: link,
	    	require: 'ngModel',
			restrict: 'A',
			scope: {
			}
		};
		return directive;
		
		function link(scope, element, attr, ctrl) {
			element.bind("keyup", function(){
				if(element.val() !=""){
					$(element).prev().addClass("has-input");
				} else {
					$(element).prev().removeClass("has-input");
				}
			});
			element.bind("keydown", function(){
				if(element.val() ==""){
					$(element).prev().removeClass("has-input");
				}
			});
		};
	};
	/* @ngInject */
	function ControllerController () {
		
	}
})();