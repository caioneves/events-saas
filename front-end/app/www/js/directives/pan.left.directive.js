(function () {
    'use strict';

    angular
    .module('eventsSaas.chat')
    .directive("panLeftSelected", panLeftSelected)

    panLeftSelected.$inject = ['$timeout'];

    function panLeftSelected($timeout){
        return {
            restrict: 'A',
            scope:{
                'panRightRemove': '@'
            },
            link: function($scope, $elm, $attrs) {
                var selected = false;
                $elm.hammer({
                    prevent_default: false
                }).bind('panleft', function(e) {
                    $elm.addClass($attrs.panLeftSelected).removeClass($scope.panRightRemove);
                    selected = true;
                }).bind('panright', function(e) {
                    if(selected){
                        $elm.removeClass($attrs.panLeftSelected).addClass($scope.panRightRemove); 
                    };
                    selected = false;
                });
            }
        };
    };

})();
