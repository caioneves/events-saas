(function () {
    'use strict';

    angular
    .module('eventsSaas.profile.user')
    .directive("headUser", HeadUser)

    HeadUser.$inject = ['eventsSaasProfileService', '$filter'];

    function HeadUser(eventsSaasProfileService, $filter){
        return {
			restrict: 'E',
			template: '<div class="head-user text-right"><div class="profile text-center" ng-repeat="item in profile"><img ng-attr-src="{{item.avatar}}" alt="" class="img-circle border-white mrg-bottom-10"/><h3 class="font-white font-30 font-openregular">{{item.nome}}</h3><p class="font-white font-22 font-openregular ph">{{item.profissao}}</p><p class="font-white-trans font-22 font-openregular ph">{{item.humor}}</p></div><a href="#/edit-user" class="text-uppercase font-white btn-text font-24">editar</a></div>',
			replace: true,
			scope: {
		    },
		    link: function(scope, element, attr) {
				/*scope.profile = [];
		        eventsSaasProfileService.getUser().success(function(data){
		           scope.profile = $filter("filter")(data, {id: 3});
		        }).error(function(data, status){
		            
		        });*/
	        }
		};
    }

})();
