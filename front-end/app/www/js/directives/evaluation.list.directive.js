(function () {
    'use strict';

    angular
    .module('eventsSaas.evaluation')
    .directive("evaluationList", EvaluationList)

    EvaluationList.$inject = ['eventsSaasEvaluationService'];

    function EvaluationList(eventsSaasEvaluationService){
        return {
			restrict: 'E',
			template: '<li class="brd-bottom-sure-1 col-xs-12 col-sm-12" ng-repeat="eva in evaluation"><ul><li class="has-img"><img alt="" ng-attr-src="{{eva.image}}" class="img-circle"/></li><li class="description"><h3 class="font-opensemibold font-30">{{eva.nome}}</h3><ul class="list-evaluation" evaluation-select="{{eva.avaliacao}}"><li><a href="javascript:;"><em class="ion-android-star font-40 font-grey-sure-4"></em></a></li><li><a href="javascript:;" ><em class="ion-android-star font-40 font-grey-sure-4"></em></a></li><li><a href="javascript:;"><em class="ion-android-star font-40 font-grey-sure-4"></em></a></li><li><a href="javascript:;"><em class="ion-android-star font-40 font-grey-sure-4"></em></a></li><li><a href="javascript:;"><em class="ion-android-star font-40 font-grey-sure-4"></em></a></li></ul><p class="font-24 ph font-openregular font-grey-dark-4">{{eva.texto}}</p><span class="time font-18 text-uppercase font-grey-dark-trans font-opensemibold">{{eva.tempo}}</span></li></ul></li>',
			replace: true,
			scope: {
				listar: "@"
		    },
		    link: function(scope, element, attr) {
				scope.evaluation = [];
		        eventsSaasEvaluationService.getEvaluation().success(function(data){
		           	scope.evaluation = data;
		        }).error(function(data, status){
		            
		        });
	        }
		};
    }

})();
