(function() {
    'use strict';

    angular
        .module('eventsSaas.menu')
        .directive('menuItem', menuItem);

    menuItem.$inject = ['eventsSaasMenuService', '$rootScope', '$timeout', '$route', 'HostValue', 'eventsSaasMessageService', 'eventsSaasSurveyService'];

    /* @ngInject */
    function menuItem(eventsSaasMenuService, $rootScope, $timeout, $route, HostValue, eventsSaasMessageService, eventsSaasSurveyService) {
        // Usage:
        //
        // Creates:
        //
        var menuItem = {
            bindToController: true,
            controller: Controller,
            controllerAs: 'vm',
            link: link,
            restrict: 'E',
            template: '<li class="font-opensemibold text-uppercase" ng-repeat="item in menu"><a href="{{item.link}}" ng-class="{\'font-green-sure\': $last}"><em class="{{item.ico}} font-18 mrg-right-5"></em> {{item.name}}<small class="badge bg-red-sure" ng-if="item.alert">{{item.alert}}</small></a></li>',
			replace: true,
            scope: {
            }
        };
        return menuItem;

        function link(scope, element, attrs) {
        	scope.menu = [];
            scope.applyMenuView = applyMenuView;
            scope.idEvent = HostValue.idEvent;
            scope.coutMsg = null;

            scope.applyMenuView()

            function applyMenuView() {
                return getMenuView().then(function(data) {
                    viewMenu(data);
                });
            };
            function getMenuView() {
                return eventsSaasMenuService.getMenu()
                .then(function(data) {
                    return data;
                })
                .catch(function(erro){
                    console.log(erro);
                });
            };

            function viewMenu(data){
	            angular.forEach(data, function(value, key) {
                    if(data[key].alert && data[key].name == "mensagens"){
                         getMsgCountPending(data[key], scope.idEvent);
                    } else if(data[key].alert && data[key].name == "Pesquisa e avaliação"){
                        getSurveyCountPending(data[key], scope.idEvent);
                    }
	            });
                scope.menu = data;
                applyMenuSwipe($timeout); 
            };
        };

        function getMsgCountPending(item, idEvento){
            return eventsSaasMessageService.getCountPending(idEvento)
            .then(function(data){
                item.alert = data.unreadMessages;
                return data.unreadMessages;
            })
            .catch(function(erro){
                return null;
            });
        };

        function getSurveyCountPending(item, idEvento){
            return eventsSaasSurveyService.getCountPending(idEvento)
            .then(function(data){
                item.alert = data.unansweredQuestions;
                return data;
            })
            .catch(function(erro){
                return null;
            });
        };

        function applyMenuSwipe($timeout){
            $timeout(function() {
                $(".button-collapse").sideNav({
                    closeOnClick: true
                });
                
                $rootScope.$on('$routeChangeSuccess', function() {
                    $(angular.element("body")).removeAttr("style").removeClass("menu-block");
                });
            }, 0);
        };
    };

    /* @ngInject */
    function Controller() {

    }

})();