(function () {
    'use strict';

    angular
    .module('eventsSaas.chat')
    .directive("pressSelected", pressSelected)

    pressSelected.$inject = ['$timeout'];

    function pressSelected($timeout){
        return {
            restrict: 'A',
            scope:{
            },
            link: function($scope, $elm, $attrs) {
                $elm.hammer({
                    prevent_default: false
                }).bind('press', function(e) {
                    $elm.addClass($attrs.pressSelected);
                }).bind('click', function(e) {
                    $elm.removeClass($attrs.pressSelected);
                })
            }
        };
    };

})();
