(function() {
    'use strict';

    angular
        .module('eventsSaas.menu.footer')
        .directive('menuFooter', MenuFooter);

    MenuFooter.inject = ['eventsSaasMenuFooterService', '$rootScope', '$location'];
    function MenuFooter(eventsSaasMenuFooterService, $rootScope, $location) {
        // Usage:
        //
        // Creates:
        //
        var directive = {
            bindToController: true,
            controller: ControllerController,
            controllerAs: 'vm',
            template: '<nav class="nav-bar-footer brd-top-sure-1 bg-white"> <div class="display-3 nav-bar-footer-container"> <ul class="display-5"> <li class="font-openregular font-18 text-center display-4" ng-class="vm.getClass(\'{{item.link}}\')" ng-repeat="item in menu"> <a href="javascript:;" ng-if="item.name==\'Menu\'" data-activates="slide-out" class="display-2 button-collapse"> <em class="{{item.ico}} font-26"></em> <span class="display-2 text-ellipsis">{{item.name}}</span> </a> <a href="{{item.link}}" ng-if="item.name !=\'Menu\'" class="display-2" ng-class="{\'line-height-21\': item.name==\'Programaçao\'}"> <em class="{{item.ico}} font-26"></em> <span class="display-2 text-ellipsis">{{item.name}}</span> </a> </li></ul> </div></nav>',
            link: link,
            replace: true,
            restrict: 'E',
            scope: {
            }
        };
        return directive;
        
        function link(scope, element, attrs) {
            scope.menu = [];
            scope.carregarMenuFooter = carregarMenuFooter;
            scope.carregarMenuFooter();

            function carregarMenuFooter() {
                return getMenuFooter().then(function(data) {
                    scope.menu = data;
                });
            };
            function getMenuFooter(){
                return eventsSaasMenuFooterService.getMenuFooter()
                .then(function(response){
                    return response;
                })
                .catch(function(error){
                    console.log(error)
                });
            }
            
            
        };

        /* @ngInject */
        function ControllerController () {
            var vm = this;
            vm.getClass = getClass;
            function getClass(path){
                return ("#"+$location.path().substr(0, path.length) === path) ? 'active' : '';
            }
        }
    }
})();