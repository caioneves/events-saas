
(function () {
	'use strict';

	var hostValue =
		{
			"env":"dev", // LOCAL, DEV, QA, DEMO and PROD
			"local": {
				"environment":"local",
				"baseUrl": "js/json/",
				"aws": "localhost:8080",
				"awsRest": "localhost:8080/app/rest",
				"awsTeste": "localhost:8080/app/rest/test/setProfileLogged",
				"idEvent":"",
				"googleMapsAPY_KEY":"AIzaSyDEkXQvjXrTA9oYQvjX2MRqigXq1aYMzUE",
				"imgAvatar":"img/avatar-default.jpg",
				"ghostsPerson": "data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7",
				"imgAvatarGroup":"img/avatar-groups.png",
				"pageSize":5
			},
			"dev": {
				"environment":"qa",
				"baseUrl": "js/json/",
				"aws": "http://dev.events-saas.com:8080",
				"awsRest": "http://dev.events-saas.com:8080/app/rest",
				"awsTeste": "http://dev.events-saas.com:8080/app/rest/test/setProfileLogged",
				"idEvent":"",
				"googleMapsAPY_KEY":"AIzaSyDEkXQvjXrTA9oYQvjX2MRqigXq1aYMzUE",
				"imgAvatar":"img/avatar-default.jpg",
				"ghostsPerson": "data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7",
				"imgAvatarGroup":"img/avatar-groups.png",
				"pageSize":5
			},
			"qa": {
				"environment":"qa",
				"baseUrl": "js/json/",
				"aws": "http://qa.events-saas.com:8080",
				"awsRest": "http://qa.events-saas.com:8080/app/rest",
				"awsTeste": "http://qa.events-saas.com:8080/app/rest/test/setProfileLogged",
				"idEvent":"",
				"googleMapsAPY_KEY":"AIzaSyDEkXQvjXrTA9oYQvjX2MRqigXq1aYMzUE",
				"imgAvatar":"img/avatar-default.jpg",
				"ghostsPerson": "data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7",
				"imgAvatarGroup":"img/avatar-groups.png",
				"pageSize":5
			},
			"demo": {
				"environment":"demo",
				"baseUrl": "js/json/",
				"aws": "http://demo.events-saas.com:8080",
				"awsRest": "http://demo.events-saas.com:8080/app/rest",
				"awsTeste": "http://demo.events-saas.com:8080/app/rest/test/setProfileLogged",
				"idEvent":"",
				"googleMapsAPY_KEY":"AIzaSyDEkXQvjXrTA9oYQvjX2MRqigXq1aYMzUE",
				"imgAvatar":"img/avatar-default.jpg",
				"ghostsPerson": "data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7",
				"imgAvatarGroup":"img/avatar-groups.png",
				"pageSize":5
			},
			"prod": {
				"environment":"prod",
				"baseUrl": "js/json/",
				"aws": "http://www.events-saas.com:8080",
				"awsRest": "http://www.events-saas.com:8080/app/rest",
				"awsTeste": "http://www.events-saas.com:8080/app/rest/test/setProfileLogged",
				"idEvent":"1",
				"googleMapsAPY_KEY":"AIzaSyDEkXQvjXrTA9oYQvjX2MRqigXq1aYMzUE",
				"imgAvatar":"img/avatar-default.jpg",
				"ghostsPerson": "data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7",
				"imgAvatarGroup":"img/avatar-groups.png",
				"pageSize":5
			}
		}
	
	function getHostValue(){
		var host = {};
		if(hostValue.env == "local"){
			host = hostValue.local;
		} else if(hostValue.env == "dev"){
			host = hostValue.dev;
		} else if(hostValue.env == "qa"){
			host = hostValue.qa;
		} else if(hostValue.env == "demo"){
			host = hostValue.demo;
		} else {
			host = hostValue.prod;
		}
		return host;
	};

	angular.module("eventsSaas.host")
	.value("HostValue", getHostValue());

})();
