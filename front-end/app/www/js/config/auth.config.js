(function () {
    'use strict';

    angular
        .module('eventsSaas.auth')
        .config(AuthInterceptor);

    AuthInterceptor.inject = ['$httpProvider', 'eventsSaasRoutes'];
    function AuthInterceptor($httpProvider, eventsSaasRoutes) {



        $httpProvider.interceptors.push(function (localStorageService, eventsSaasRoutes, $q) {
            return {
                'request': function (config) {

                    function parseUTF16(text) {
                        var ranges = [
                            '[\ud000-\udfff]',
                            '[\u3000-\u3fff]',
                            '[\u2000-\u2fff]',
                            '[\uFE00-\uFEff]'

                            ];

                        return text.replace(
                            new RegExp(ranges.join('|'), 'g'),
                            function(m)
                            {
                                function hexEncode (text){
                                    var hex, i;

                                    var result = "";

                                    for (i=0; i<text.length; i++) {
                                        hex = text.charCodeAt(i).toString(16);
                                        result += ("000"+hex).slice(-4);
                                    }


                                    return result
                                }
                                return ':|'+ hexEncode(m)+'|:';
                            });
                    }

                    if (config.url.endsWith("view/login/login.html")){
                        return config;
                    }

                    if (config.method == "POST" && typeof (config.data) == 'object') {
                        config.data = JSON.parse(parseUTF16(JSON.stringify(config.data)));
                    }

                    config.headers = config.headers || {};
                    config.headers.token = localStorageService.get('token');
                    return config;
                },
                'response': function (response) {

                    function hexDecode(text){
                        text = text.split(':|').join('').split('|:').join('');
                        var j;
                        var hexes = text.match(/.{1,4}/g) || [];
                        var back = "";
                        for(j = 0; j<hexes.length; j++) {
                            back += String.fromCharCode(parseInt(hexes[j], 16));
                        }

                        return back;
                    }


                    if (response){
                        if (typeof (response.data) == 'object') {
                            var body = JSON.stringify(response.data);
                            body = body.replace(new RegExp(':\\|(.+?)\\|:', 'g'), hexDecode);
                            response.data = JSON.parse(body);

                        }
                    }
                    return response;

                },
                'responseError': function (response) {
                    if (response.status === 401) {
                        //here I preserve login page
                        window.location = '#'+eventsSaasRoutes.ENTRAR;

                    }
                    return $q.reject(response);
                }
            };

        });
    }
})();
