(function() {
    'use strict';
	angular.module('eventsSaas', 
		[
			'LocalStorageModule',
			'eventsSaas.host',
			'eventsSaas.router',
			'eventsSaas.splash',
			'eventsSaas.auth',
			'eventsSaas.login',
            'eventsSaas.menu.footer', 
			'eventsSaas.home',
			'eventsSaas.menu',
			'eventsSaas.profile', 
			'eventsSaas.quick.access',
			'eventsSaas.info.event',
			'eventsSaas.search',
			'eventsSaas.timeline',
			'eventsSaas.message',
			'eventsSaas.comment',
			'eventsSaas.matter',
			'eventsSaas.profile.user',
			'eventsSaas.card.virtual',
			'eventsSaas.profile.lecturer',
			'eventsSaas.evaluation',
			'eventsSaas.survey',
			'eventsSaas.agenda',
			'eventsSaas.network',
			'eventsSaas.conquer',
			'eventsSaas.status',
			'ngCordovaOauth',
			'eventsSaas.qr.code'
		]
	);

})();