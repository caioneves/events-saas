(function() {
'use strict';

	angular
		.module('eventsSaas.atividades')
		.factory('eventsSaasAtividadesService', EventsSaasAtividadesService);

	EventsSaasAtividadesService.inject = ['$http', 'HostValue'];
	function EventsSaasAtividadesService($http, HostValue) {
		var service = {
			getAtividades: getAtividades
		};
		
		return service;

		////////////////
		function getAtividades() {
			return $http.get(HostValue.baseUrl  + "atividades.json")
			.then(getAtividadeComplete)
			.catch(getAtividadesFailed);

			function getAtividadeComplete(response){
				return response.data;
			};
			function getAtividadesFailed(error){
				console.log('XHR get Failed Atividade' + error.data);
			};
		 };
	};
})();