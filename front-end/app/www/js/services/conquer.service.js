(function () {
    'use strict';

	angular.module('eventsSaas.conquer')
	.factory("eventsSaasConquerService", eventsSaasConquerService)

	eventsSaasConquerService.$inject = ['$http','HostValue']

	function eventsSaasConquerService($http, HostValue) {
        return {
			getConquer: getConquer
		};
        function getConquer(){
        	return $http.get(HostValue.baseUrl + "conquistas.json")
            .then(getConquerComplete)
            .catch(getConquerFailed);

	        function getConquerComplete(response) {
	            return response.data;
	        };
	        function getConquerFailed(error) {
	            console.log('XHR Failed Mensagem Amigos' + error.data);
	        };
		};
    };

})();