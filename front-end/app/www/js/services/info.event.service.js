(function() {
    'use strict';

    angular
        .module('eventsSaas.info.event')
        .factory('eventsSaasInfoEventService', eventsSaasInfoEventService);

    eventsSaasInfoEventService.$inject = ['$http', 'HostValue', '$q', 'localStorageService'];

    /* @ngInject */
    function eventsSaasInfoEventService($http, HostValue, $q, localStorageService) {
        var infoEvent = {
            getInfoEvent: getInfoEvent
        };
        return infoEvent;

        ////////////////

        function getInfoEvent(eventId) {
            return $http.get(HostValue.awsRest + "/event/"+eventId+"/information")
            .then(getInfoEventComplete)
            .catch(getInfoEventFailed);

            function getInfoEventComplete(response){
                return response.data;
            };

            function getInfoEventFailed(error){
				return $q.reject(error);
            };
        };

    }
})();