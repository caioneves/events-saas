(function () {
    'use strict';

    angular
        .module('eventsSaas.dialog')
        .factory('DialogService', DialogService);

    DialogService.inject = ['$interval', 'localStorageService', 'HostValue', '$http'];
    function DialogService($interval, localStorageService, HostValue, $http) {
        var service = {
            alert: alert,
            confirm: confirm
        };

        return service;

        function alert(message, title, callback, buttonName) {
            navigator.notification.alert(
                message,    // message
                callback,   // callback
                title,      // title
                buttonName  // buttonName
            );

        };


        function confirm(message, callBack, title, buttonLabels) {
            navigator.notification.confirm(
                message, // message
                callBack,            // callback to invoke with index of button pressed
                title,           // title
                buttonLabels     // buttonLabels
            );
        }

    };
})();