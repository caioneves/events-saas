(function() {
'use strict';

    angular
        .module('eventsSaas.menu.footer')
        .factory('eventsSaasMenuFooterService', eventsSaasMenuFooterService);

    eventsSaasMenuFooterService.inject = ['$http', 'HostValue'];
    function eventsSaasMenuFooterService($http, HostValue) {
        var service = {
            getMenuFooter: getMenuFooter
        };
        return service;

        ////////////////
        function getMenuFooter() {
            return $http.get(HostValue.baseUrl + "menu.footer.json")
            .then(getMenuFooterComplete)
            .catch(getMenuFooterFailed);

	        function getMenuFooterComplete(response) {
	            return response.data;
	        };

	        function getMenuFooterFailed(error) {
	            console.log('XHR Failed get Menu footer' + error.data);
	        };
        };
    };
})();