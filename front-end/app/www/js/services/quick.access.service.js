(function() {
    'use strict';

    angular
        .module('eventsSaas.quick.access')
        .factory('eventsSaasQuickAccessService', eventsSaasQuickAccessService);

    eventsSaasQuickAccessService.$inject = ['$http', 'HostValue'];

    /* @ngInject */
    function eventsSaasQuickAccessService($http, HostValue) {
        var quickAccess = {
            getQuickAccess: getQuickAccess
        };
        return quickAccess;

        ////////////////

        function getQuickAccess() {
        	return $http.get(HostValue.baseUrl + "quick.access.json")
            .then(getQuickAccessComplete)
            .catch(getQuickAccessFailed);
	        function getQuickAccessComplete(response) {
	            return response.data;
	        };
	        function getQuickAccessFailed(error) {
	            console.log('XHR Failed Mensagem Amigos' + error.data);
	        };
        }
    }
})();