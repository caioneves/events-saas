(function () {
    'use strict';

	angular.module('eventsSaas.menu')
	.factory("eventsSaasMenuService", eventsSaasMenuService)

	eventsSaasMenuService.$inject = ['$http', 'HostValue']

	function eventsSaasMenuService($http, HostValue) {
		return {
			getMenu: getMenu,
		};

		function getMenu(){
        	return $http.get(HostValue.baseUrl + "menu.json")
			.then(getMenuComplete)
			.catch(getMenuFailed);

			function getMenuComplete(response){
				return response.data
			};

			function getMenuFailed(error){
				console.log('XHR Failed getMenu' + error.data);
			};
		};

    }

})();