(function() {
    'use strict';

    angular
        .module('eventsSaas.profile')
        .factory('eventsSaasProfileService', eventsSaasProfileService);

    eventsSaasProfileService.$inject = ['$http', '$q', 'HostValue', 'localStorageService'];

    /* @ngInject */
    function eventsSaasProfileService($http, $q, HostValue, localStorageService) {
        var service = {
			getProfile: getProfile,
            getUser: getUser,
            getMiniprofileById: getMiniprofileById,
            addFriend: addFriend,
			postChangeEmail: postChangeEmail,
			postChangePassword: postChangePassword,
			postSocialProfile: postSocialProfile,
			postProfile: postProfile,
			postProfileImage: postProfileImage,
			getMyActivities: getMyActivities
        };
        return service;

        ////////////////

		function getProfile(id){
			return $http.get(HostValue.awsRest + "/event/"+id+"/profile/")
			.then(getProfileComplete)
			.catch(getProfileFailed);

			function getProfileComplete(response){
				localStorageService.set('profile', response.data);
				return response.data
			};

			function getProfileFailed(error){
				localStorageService.set('profile', null);
				return $q.reject(error);
			};
		};

        function getUser(){
        	return $http.get(HostValue.baseUrl + "user.json")
            .then(getUserComplete)
            .catch(getUserFailed);

	        function getUserComplete(response) {
	            return response.data;
	        };

	        function getUserFailed(error) {
				return $q.reject(error.data);
			};

		};

		function getMiniprofileById(id){
        	return $http.get(HostValue.awsRest + "/event/"+HostValue.idEvent+"/profile/miniprofile/"+id)
			.then(getMiniProfileComplete)
			.catch(getMiniprofileByIdFailed)

			function getMiniProfileComplete(response){
				return response.data
			};

			function getMiniprofileByIdFailed(error){
				return $q.reject(error.data);
			};
		};

        function addFriend(idQRcode){
        	return $http.post(HostValue.baseUrl + "/add/friend/"+idQRcode)
            .then(addFriendComplete)
            .catch(addFriendFailed);

	        function addFriendComplete(response) {
	            return response.data;
	        };

	        function addFriendFailed(error) {
				return $q.reject(error.data);
	        };
		};

		function postChangeEmail(email, password){
			return $http({
				url: HostValue.awsRest+"/event/"+HostValue.idEvent+"/profile/changeEmail",
				method: "POST",
          		contentType: "application/json; charset=utf-8",
				dataType: "json",
				data: JSON.stringify({
					"password": btoa(password),
					"email": email
				})
			})
			.then(postChangeEmailComplete)
			.catch(postChangeEmailFailed);
			
			function postChangeEmailComplete(response){
				return response
			};

			function postChangeEmailFailed(error){
				return $q.reject(error)
			};
		};

		function postChangePassword(newPassword, oldPassword, idEvent){
			return $http({
				url: HostValue.awsRest+"/event/"+idEvent+"/profile/changePassword",
				method:"POST",
				contentType: "application/json; charset=utf-8",
				dataType: "json",
				data: JSON.stringify({
					"oldPassword": eventsSaasBase64(oldPassword),
					"newPassword": eventsSaasBase64(newPassword)
				})
			})
			.then(postChangePasswordComplete)
			.catch(postChangePasswordFailed)

			function postChangePasswordComplete(response){
				return $q.resolve(response.data);
			};

			function postChangePasswordFailed(error){
				return $q.reject(error.data)
			};
		};
		
		function postSocialProfile(data){
			return $http({
				url: HostValue.awsRest+"/event/"+HostValue.idEvent+"/profile/social/configure",
				method: "POST",
				contentType: "application/json; charset=utf-8",
				dataType: "json",
				data: data
			})
			.then(postSocialProfileComplete)
			.catch(postSocialProfileFailed)

			function postSocialProfileComplete(response){
				return response;
			};

			function postSocialProfileFailed(error){
				return $q.reject(error);
			};
		};

		function postProfile(data){
			return $http({
				url: HostValue.awsRest+"/event/"+HostValue.idEvent+"/profile/update",
				method: "POST",
				contentType: "application/json; charset=utf-8",
				dataType: "json",
				data: data
			})
			.then(postProfileComplete)
			.catch(postProfileFailed);

			function postProfileComplete(response){
				localStorageService.set('profile', null);
				return response;
			};

			function postProfileFailed(error){
				return $q.reject(error);
			};
		};

		function postProfileImage(image){
			return $http({
				method: 'POST',
          		contentType: "application/json; charset=utf-8",
				url: HostValue.awsRest + "/event/"+HostValue.idEvent+"/image/",
          		dataType: "json",
				data: JSON.stringify({
					"bytes": image,
					"contentType": "image/jpeg"
				})
			})
			.then(postProfileImageComplete)
			.catch(postProfileImageFailed)

			function postProfileImageComplete(response){
				return response.data;
			};

			function postProfileImageFailed(error){
				console.log('XHR Failed postProfileImage ' + error.data);
			};
		};

		function getMyActivities(idEvent){
			return $http.get(HostValue.awsRest + "/event/"+idEvent+"/profile/myactivities")
			.then(getMyActivitiesComplete)
			.catch(getMyActivitiesFailed);

			function getMyActivitiesComplete(response){
				return response.data
			};

			function getMyActivitiesFailed(error){
				return $q.reject(error)
			};
		};

		function eventsSaasBase64(input) {	 
			var keyStr = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=';
		
			var output = "";
			var chr1, chr2, chr3 = "";
			var enc1, enc2, enc3, enc4 = "";
			var i = 0;

			do {
				chr1 = input.charCodeAt(i++);
				chr2 = input.charCodeAt(i++);
				chr3 = input.charCodeAt(i++);

				enc1 = chr1 >> 2;
				enc2 = ((chr1 & 3) << 4) | (chr2 >> 4);
				enc3 = ((chr2 & 15) << 2) | (chr3 >> 6);
				enc4 = chr3 & 63;

				if (isNaN(chr2)) {
					enc3 = enc4 = 64;
				} else if (isNaN(chr3)) {
					enc4 = 64;
				}

				output = output +
					keyStr.charAt(enc1) +
					keyStr.charAt(enc2) +
					keyStr.charAt(enc3) +
					keyStr.charAt(enc4);
				chr1 = chr2 = chr3 = "";
				enc1 = enc2 = enc3 = enc4 = "";
			} while (i < input.length);

			return output;
		}
		
    };
})();