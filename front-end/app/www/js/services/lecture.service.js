(function() {
'use strict';

	angular
		.module('eventsSaas.events')
		.factory('eventsSaasLectureService', eventsSaasLectureService);

	eventsSaasLectureService.inject = ['$http', '$q', 'HostValue', 'localStorageService'];
	function eventsSaasLectureService($http, $q, HostValue, localStorageService) {
		var service = {
				getLecture: getLecture,
				saveLecture: saveLecture,
				deleteLecture: deleteLecture,
				saveAlarmAndLecture: saveAlarmAndLecture,
				getLectureDownloadList: getLectureDownloadList,
				getSaveAndAlarm: getSaveAndAlarm, 
				downloadFile: downloadFile
		};
		
		return service;

		////////////////
		function getLectureDownloadList(id){
			return $http.get(HostValue.awsRest +"/event/"+HostValue.idEvent+"/lecture/"+id+"/downloaditems/list")
				.then(function (response){
					return response.data;
				});
		};
		
		function getLecture(id){
			return $http.get(HostValue.awsRest + "/event/"+HostValue.idEvent+"/lecture/"+id)
				.then(function(response){
					return response.data;
				});
		};
		
		function saveLecture(id){
			return $http.post(HostValue.awsRest +"/event/"+HostValue.idEvent+"/lecture/"+id+"/save");
		};

		function deleteLecture(id){
			return $http.delete(HostValue.awsRest +"/event/"+HostValue.idEvent+"/lecture/"+id+"/save");
		};

		function saveAlarmAndLecture(id){
			return $http.post(HostValue.awsRest +"/event/"+HostValue.idEvent+"/lecture/"+id+"/alarm");
		};
		
		function downloadFile(id, downloadId){
			return HostValue.awsRest +"/event/"+HostValue.idEvent+"/lecture/"+id+"/download/"+downloadId;
		};

		function getSaveAndAlarm(idEvent, idLecture){
			return $http.get(HostValue.awsRest+"/event/"+idEvent+"/lecture/"+idLecture+"/saveandalarm")
			.then(getSaveAndAlarmComplete)
			.catch(getSaveAndAlarmFailed);

			function getSaveAndAlarmComplete(response){
				return response.data
			};

			function getSaveAndAlarmFailed(error){
				return $q.reject(error);
			};
		};
	};
})();