(function () {
    'use strict';

	angular.module('eventsSaas.timeline')
	.factory("eventsSaasTimelineService", eventsSaasTimelineService)

	eventsSaasTimelineService.$inject = ['$http', 'HostValue', '$q']

	function eventsSaasTimelineService($http, HostValue, $q) {
		var service = {
			getTimeline: getTimeline,
			like: like,
			unlike: unlike,
			getArticle: getArticle,
			getComments: getComments,
			postComment: postComment,
			postTimeline: postTimeline,
			deletePostTimeline: deletePostTimeline,
			addImageTimeLine: addImageTimeLine
		};
		
		return service;

        function getTimeline(eventId, page, size){
        	return $http.get(HostValue.awsRest + "/event/"+eventId+"/timeline?page="+page+"&size="+size)
            .then(getTimelineComplete)
            .catch(getTimelineFailed);

	        function getTimelineComplete(response) {
	            return response.data;
	        };

	        function getTimelineFailed(error) {
				return $q.reject(error);
	        };
		};

		function postTimeline(eventId, msg, imgId){
        	return $http({
				url: HostValue.awsRest + "/event/"+eventId+"/timeline/post",
				method: "POST",
				contentType: "application/json; charset=utf-8",
				dataType: "json",
				data: JSON.stringify({
					"postMessage": msg,
					"title": "",
					"articleType": "APP_POST",
					"imageId": imgId
				})
			})
            .then(postTimelineComplete)
            .catch(postTimelineFailed);

	        function postTimelineComplete(response) {
	            return response.data;
	        };

	        function postTimelineFailed(error) {
				return $q.reject(error);
	        };
		};


		function like(articleId){
			return $http.post(HostValue.awsRest + "/event/"+HostValue.idEvent+"/article/"+articleId+"/like")
				.then(likeComplete)
				.catch(likeFailed);

			function likeComplete(response) {
				return response.data;
			};

			function likeFailed(error) {
				console.log('XHR Failed Timeline' + error.data);
			};
		};


		function unlike(articleId){
			return $http({
						url: HostValue.awsRest + "/event/"+HostValue.idEvent+"/article/"+articleId+"/like",
						method: "DELETE"
					})
				.then(unlikeComplete)
				.catch(unlikeFailed);

			function unlikeComplete(response) {
				return response.data;
			};

			function unlikeFailed(error) {
				console.log('XHR Failed Timeline' + error.data);
			};
		};

		function getArticle(articleId){
			return $http.get(HostValue.awsRest + "/event/"+HostValue.idEvent+"/article/"+articleId)
				.then(getArticleComplete)
				.catch(getArticleFailed);

			function getArticleComplete(response) {
				return response.data;
			};

			function getArticleFailed(error) {
				console.log('XHR Failed Timeline' + error.data);
			};
		};

		function getComments(articleId){
			return $http.get(HostValue.awsRest + "/event/"+HostValue.idEvent+"/article/"+articleId+"/commentary")
				.then(getCommentsComplete)
				.catch(getCommentsFailed);

			function getCommentsComplete(response) {
				return response.data;
			};

			function getCommentsFailed(error) {
				console.log('XHR Failed Timeline' + error.data);
			};
		};

		function postComment(articleId, comment){
			return $http.post(HostValue.awsRest + "/event/"+HostValue.idEvent+"/article/"+articleId+"/commentary", comment)
				.then(postCommentComplete)
				.catch(postCommentFailed);

			function postCommentComplete(response) {
				return response.data;
			};

			function postCommentFailed(error) {
				console.log('XHR Failed Timeline' + error.data);
			};
		};

		function deletePostTimeline(idEvent, idPost){
			return $http.delete(HostValue.awsRest+"/event/"+idEvent+"/timeline/post/"+idPost)
			.then(deletePostTimelineComplete)
			.catch(deletePostTimelineFailed)

			function deletePostTimelineComplete(response){
				return response.data
			};

			function deletePostTimelineFailed(error){
				return $q.reject(error);
			};
		};

		function addImageTimeLine(image){
			return $http({
				method: 'POST',
          		contentType: "application/json; charset=utf-8",
				url: HostValue.awsRest + "/event/"+HostValue.idEvent+"/image/",
          		dataType: "json",
				data: JSON.stringify({
					"bytes": image,
					"contentType": "image/jpeg"
				})
			})
			.then(addImageTimeLineComplete)
			.catch(addImageTimeLineFailed)

			function addImageTimeLineComplete(response){
				return response.data;
			};

			function addImageTimeLineFailed(error){
				console.log('XHR Failed addImageTimeLine ' + error.data);
			};
		};
    };

})();