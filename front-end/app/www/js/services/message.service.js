(function () {
    'use strict';

	angular.module('eventsSaas.message')
	.factory("eventsSaasMessageService", eventsSaasMessageService)

	eventsSaasMessageService.$inject = ['$http', 'HostValue', '$q']

	function eventsSaasMessageService($http, HostValue, $q) {
		var service = {
			getProfileMessages: getProfileMessages,
			getChatProfile: getChatProfile, 
			postProfileMessage: postProfileMessage,
			postProfileMessageImage: postProfileMessageImage,
			deleteMessageChat: deleteMessageChat,
			sendEmailContato: sendEmailContato,
			getCountPending: getCountPending
		};

		return service;

		////////////////
        function getProfileMessages(idEvent, idTarget){
        	return $http.get(HostValue.awsRest + "/event/"+idEvent+"/message/"+idTarget)
			.then(getProfileMessagesComplete)
			.catch(getProfileMessagesFailed);

			function getProfileMessagesComplete(response){
				return response.data
			};

			function getProfileMessagesFailed(error){
				return $q.reject(error);
			};
		};

		function getChatProfile(idEvent, idProfile){
			return $http.get(HostValue.awsRest + "/event/"+idEvent+"/profile/miniprofile/"+idProfile)
			.then(getChatProfileComplete)
			.catch(getChatProfileFailed)

			function getChatProfileComplete(response){
				return response.data
			};

			function getChatProfileFailed(error){
				return $q.reject(error);
			};
		};

		function deleteMessageChat(idEvent, idMsg){
			return $http.delete(HostValue.awsRest+"/event/"+idEvent+"/message/"+idMsg)
			.then(deleteProfileMessagesComplete)
			.catch(deleteProfileMessagesFailed)

			function deleteProfileMessagesComplete(response){
				return response.data
			};

			function deleteProfileMessagesFailed(error){
				return $q.reject(error);
			};
		};

        function postProfileMessage(idEvent, idTarget, message){
        	return $http.post(HostValue.awsRest + "/event/"+idEvent+"/message/"+idTarget, message)
			.then(postProfileMessageComplete)
			.catch(postProfileMessageFailed);

			function postProfileMessageComplete(response){
                console.log(response)
				return response.data
			};

			function postProfileMessageFailed(error){
				return $q.reject(error);
			};
		};

		function postProfileMessageImage(type, image){
			return $http({
				method: 'POST',
          		contentType: "application/json; charset=utf-8",
				url: HostValue.awsRest + "/event/"+HostValue.idEvent+"/image/",
          		dataType: "json",
				data: JSON.stringify({
					"bytes": image,
					"contentType": type
				})
			})
			.then(postProfileMessageImageComplete)
			.catch(postProfileMessageImageFailed)

			function postProfileMessageImageComplete(response){
				return response.data;
			};

			function postProfileMessageImageFailed(error){
				return $q.reject(error);
			};
		};

		function sendEmailContato(idEvent, idTarget, message){
			return $http.post(HostValue.awsRest + "/event/"+idEvent+"/profile/contact/"+idTarget, message)
				.then(postProfileMessageComplete)
				.catch(postProfileMessageFailed);

			function postProfileMessageComplete(response){
				console.log(response)
				return response.data
			};

			function postProfileMessageFailed(error){
				return $q.reject(error);
			};
		};

		function getCountPending(idEvent){
			return $http.get(HostValue.awsRest + "/event/"+idEvent+"/message/countpending")
				.then(getCountPendingComplete)
				.catch(getCountPendingFailed);

			function getCountPendingComplete(response){
				return response.data
			};

			function getCountPendingFailed(error){
				return $q.reject(error);
			};
		};

    };

})();