(function() {
'use strict';

	angular
		.module('eventsSaas.events')
		.factory('eventsSaasEventsService', eventsSaasEventsService);

	eventsSaasEventsService.inject = ['$http', '$q', 'HostValue', 'localStorageService'];
	function eventsSaasEventsService($http, $q, HostValue, localStorageService) {
		var service = {
			getEvents:getEvents,
			getEventListProfiles:getEventListProfiles,
			getEventListMessages: getEventListMessages,
			getLectureSummary: getLectureSummary,
			deleteMessagesProfile: deleteMessagesProfile,
			getEventListCheckedinProfiles: getEventListCheckedinProfiles,
			getNextActivities: getNextActivities
		};
		
		return service;

		////////////////
		function getEvents() { 
			return $http.get(HostValue.baseUrl + "events.json")
			.then(getEventsComplete)
			.catch(getEventsFailed);

			function getEventsComplete(response){
				return response.data;
			};

			function getEventsFailed(error){
				return $q.reject(error);
			};
		};

		function getEventListProfiles(id){
			return $http.get(HostValue.awsRest + "/event/"+id+"/profile/registered/list")
			.then(getEventListProfilesComplete)
			.catch(getEventListProfilesFailed);

			function getEventListProfilesComplete(response){
				return response.data;
			};

			function getEventListProfilesFailed(error){
				return $q.reject(error);
			};
		};

		function getEventListCheckedinProfiles(id){
			return $http.get(HostValue.awsRest + "/event/"+id+"/profile/checkedin/list")
				.then(getEventListCheckedinProfilesComplete)
				.catch(getEventListCheckedinProfilesFailed);

			function getEventListCheckedinProfilesComplete(response){
				return response.data;
			};

			function getEventListCheckedinProfilesFailed(error){
				return $q.reject(error);
			};
		};

		function getEventListMessages(id){
			return $http.get(HostValue.awsRest + "/event/"+id+"/message/list")
			.then(getEventListMessagesComplete)
			.catch(getEventListMessagesFailed);

			function getEventListMessagesComplete(response){
				return response.data;
			};

			function getEventListMessagesFailed(error){
				console.log('XHR Failed for getEventListMessages.' + error.data);
			};
		};

		function deleteMessagesProfile(idEvent, idProfile){
			return $http.delete(HostValue.awsRest +"/event/"+idEvent+"/message/chat/"+idProfile)
			.then(deleteMessagesProfileComplete)
			.catch(deleteMessagesProfileFailed)

			function deleteMessagesProfileComplete(response){
				return response.data
			};
			function deleteMessagesProfileFailed(error){
				return $q.reject(error);
			};
		};

		function getLectureSummary(id){
			var url = HostValue.awsRest + "/event/"+HostValue.idEvent+"/schedule/summary";
			if (id){
				url += "/" + id;
			}			
			return $http.get(url)
			.then(function(response){
				return response.data;
			})
			.catch(function(error){
				return [];
			});
		};

        function getNextActivities(eventId, page, size){
        	return $http.get(HostValue.awsRest + "/event/"+eventId+"/nextactivities?page="+page+"&size="+size)
            .then(getNextActivitiesComplete)
            .catch(getNextActivitiesFailed);

	        function getNextActivitiesComplete(response) {
	            return response.data;
	        };

	        function getNextActivitiesFailed(error) {
				return $q.reject(error);
	        };
		};
	};
})();