(function () {
	'use strict';

	angular
		.module('eventsSaas.login')
		.factory('eventsSaasLoginService', eventsSaasLoginService);

	eventsSaasLoginService.inject = ['$q', '$http', 'localStorageService', 'HostValue']
	function eventsSaasLoginService($q, $http, localStorageService, HostValue) {
		var service = {
			login: login,
			loginSocial: loginSocial,
			forgotPassword: forgotPassword
		};

		return service;

		////////////////
		function login(user, password) {
			var hash = user + ":" + password;
			return $http(
				{
					url: HostValue.awsRest+"/event/"+HostValue.idEvent+"/login/",
					headers: { "Authorization": "Basic " + btoa(hash) },
					method: "POST"
				})
				.then(postLoginComplete)
				.catch(postLoginFailed);

			function postLoginComplete(response) {
				localStorageService.set('token', response.data.token);
				localStorageService.set('user', response.data);
				return response.data;
			};

			function postLoginFailed(error) {
				localStorageService.set('token', null);
				localStorageService.set('user', null);
				return $q.reject(error);
			};
		};

		function loginSocial(type, id){
			return $http({
				url: HostValue.awsRest+"/event/"+HostValue.idEvent+"/login/social/",
				method: "POST",
				contentType: "application/json; charset=utf-8",
				dataType: "json",
				data: JSON.stringify({
					"providerId": type,
					"userId": id
				})
			})
			.then(loginSocialComplete)
			.catch(loginSocialFailed);

			function loginSocialComplete(response){
				localStorageService.set('token', response.data.token);
				localStorageService.set('user', response.data);
				return response.data;
			};

			function loginSocialFailed(error) {
				localStorageService.set('token', null);
				localStorageService.set('user', null);
				return $q.reject(error);
			};
		};

		function forgotPassword(email){
			return $http({
				url: HostValue.awsRest+"/forgotpassword/",
				method: "POST",
				contentType: "application/json; charset=utf-8",
				dataType: "json",
				data: JSON.stringify({
					"email": email
				})
			})
			.then(forgotPasswordComplete)
			.catch(forgotPasswordFailed);
			
			function forgotPasswordComplete(response){
				return response.data;
			};

			function forgotPasswordFailed(error){
				return $q.reject(error);
			};
		};
	};
})();