(function () {
    'use strict';

    angular
        .module('eventsSaas.agenda')
        .controller('EventController', EventController);

    EventController.$inject = ['$scope', 'eventsSaasRoutes', 'cfpLoadingBar', '$timeout', 'eventsSaasEventsService', '$location', '$routeParams']

    function EventController($scope, eventsSaasRoutes, cfpLoadingBar, $timeout, eventsSaasEventsService, $location, $routeParams) {

        cfpLoadingBar.start();
        $timeout(function() {
            cfpLoadingBar.complete();
        }, 500);

        var vm = this;
        vm.title = "";
        vm.getActivity = getActivity;
        vm.serviceEvents = serviceEvents;
        vm.activityId = $routeParams.detailId;
        vm.filterEvent = filterEvent;

        vm.serviceEvents();

        function serviceEvents(){
            return getServiceEvents().then(function(data){
                vm.eventRow = [];
                vm.getActivity(data);
            })
            .catch(function(error){
                 return console.log(error)
            });
        };

        function getServiceEvents(){
            return  eventsSaasEventsService.getEvents()
            .then(function(data){
                return data;
            })
            .catch(function(){
                return console.log(error)
            });
        };

        function getActivity(data){
            vm.eventRow = [];
            angular.forEach(data, function(value, key) {
                if(data[key].id == vm.activityId){
                    vm.title = data[key].tipo;
                    angular.forEach(data[key].palestras, function(item, index) {
                        var objPalestra = {"id":data[key].palestras[index].id, "palestrante":data[key].palestras[index].palestrante, "title": data[key].palestras[index].title, "value":data[key].palestras[index].checkIn, "tag":data[key].palestras[index].tag }
                        vm.eventRow.push(objPalestra);
                        vm.title = data[key].tipo;
                    });
                    console.log(vm.eventRow)
                }
            });
        };

        function filterEvent(id, tipo){
            $location.path("/agenda/"+id);
        };
    }

})();