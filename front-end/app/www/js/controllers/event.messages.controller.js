(function () {
    'use strict';

    angular
        .module('eventsSaas.message')
        .controller('MessageController', MessageController);

    MessageController.$inject = ['$scope', 'eventsSaasRoutes', 'eventsSaasMessageService', 'eventsSaasEventsService', 'localStorageService', 'HostValue', 'cfpLoadingBar', '$routeParams', '$timeout', '$location', 'DialogService']

    function MessageController($scope, eventsSaasRoutes, eventsSaasMessageService, eventsSaasEventsService, localStorageService, HostValue, cfpLoadingBar, $routeParams, $timeout, $location, DialogService) {

        cfpLoadingBar.start();
        $timeout(function() {
            cfpLoadingBar.complete();
        }, 500);

        var vm = this;
        vm.init = init;
        vm.rows = [];
        vm.messagesEvent = [];
        vm.root = HostValue.aws;
        vm.avatar = HostValue.ghostsPerson;
        vm.linkParticipant = eventsSaasRoutes.PARTICIPANTS;
        vm.openLinkChat = openLinkChat;
        vm.deleteMsg = deleteMsg;
        vm.idLogado = localStorageService.get('user').id;
        
        function init(){
            viewMessagesEvent();
        };

        function viewMessagesEvent() {
            return getMessages().then(function(data) {
                messagesList(data);
                console.log(data)
            });
        };

        function getMessages() {
            return eventsSaasEventsService.getEventListMessages(HostValue.idEvent)
            .then(function(data) {
                vm.messagesEvent = data;
                return vm.messagesEvent;
            });
        };

        function messagesList(data){
            var image, text, status, dateCriation, autor, temp, date, targetId, dateOrder;
            angular.forEach(data, function(value, key) {
                text            = data[key].text;
                status          = data[key].read;
                dateCriation    = data[key].creationDate;
                dateOrder       = parseInt(data[key].creationDate);

                targetId = data[key].target.id;
                if(vm.idLogado == targetId){
                    autor = data[key].author.name;
                    targetId = data[key].author.id;
                    image = data[key].author.pictureUrl;
                } else {
                    autor = data[key].target.name;
                    image = data[key].target.pictureUrl;
                }

                var objMessage  = {};
                objMessage = {"id":data[key].id, "message": text, "date": dateCriation, "order":dateOrder, "nome": autor, "image":image, "ler":status, "targetId":targetId}; 
                vm.rows.push(objMessage);
            });
        };

        function openLinkChat(id){
            $location.path("/participant/chat/"+id);
        };

        function deleteMsg(profile, index){
            DialogService.confirm("Apagar todas as mensagens?", deleteCallback, "Mensagens de "+profile.nome, ["Não", "Sim"])
            function deleteCallback(button){
                if(button == 2){
                    vm.rows.splice(index, 1);
                    return eventsSaasEventsService.deleteMessagesProfile(HostValue.idEvent, profile.targetId)
                    .then(function(data){
                        console.log(data)
                    })
                    .catch(function(error){
                        console.log(error)
                    });
                };
            };
        };
    };

})();