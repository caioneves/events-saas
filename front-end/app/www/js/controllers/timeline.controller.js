(function () {
    'use strict';

    angular
        .module('eventsSaas.timeline')
        .controller('TimeLineController', TimeLineController);

	TimeLineController.$inject = ['$scope', '$location', 'eventsSaasRoutes', 'cfpLoadingBar', '$timeout', 'eventsSaasTimelineService', 'HostValue', 'localStorageService', 'DialogService']

	function TimeLineController($scope, $location, eventsSaasRoutes, cfpLoadingBar, $timeout, eventsSaasTimelineService, HostValue, localStorageService, DialogService) {

		cfpLoadingBar.start();
        $timeout(function() {
            cfpLoadingBar.complete();
        }, 500);
        var vm = this;
        vm.page=0;
        vm.timeLine = [];
        vm.carregarTimeLine = carregarTimeLine;
        vm.root = HostValue.aws;
        vm.avatar = HostValue.ghostsPerson;
        vm.containsPost = containsPost;
        vm.loadingBar =loadingBar;
        vm.idEvent = HostValue.idEvent;
        vm.openParticipant = openParticipant;
        vm.user = localStorageService.get('user');
        vm.admin = false;
        vm.deletePost = deletePost;

        vm.carregarTimeLine();
        isAdmin();
        vm.like = like;

        function carregarTimeLine() {
            return getTimeline(vm.page).then(function(data) {
                angular.forEach(data, function(value, key){
                    if (!vm.containsPost(value)) {
                        vm.timeLine.push(value);
                    }
                });
                if (data.length == HostValue.pageSize){
                    vm.page+=1;
                    vm.morePost = true;
                } else {
                    vm.morePost = false;
                }
            });
        };

        function containsPost(post) {
            for (var i = 0; i < vm.timeLine.length; i++) {
                if (vm.timeLine[i].id == post.id) {
                    return true;
                }
            }
            return false;
        }

        function getTimeline(page) {
            return eventsSaasTimelineService.getTimeline(HostValue.idEvent, page, HostValue.pageSize)
            .then(function(data) {
                return data;
            })
            .catch(function(erro){
                console.log(erro);
            });
        };

        function like(article) {
            if (article.liked) {
                return;
            }
            return eventsSaasTimelineService.setLike(HostValue.idEvent, article.id)
                .then(function(data) {
                    article.numberLike+=1;
                    article.liked=true;
                    loadingBar();
                });
        };

        function loadingBar() {
            cfpLoadingBar.start();
            $timeout(function() {
                cfpLoadingBar.complete();
            }, 500);
        };

        function openParticipant(id){
            if(vm.user.id != id){
                $location.path("/participant/"+id);
            } else {
                $location.path("/profile-user")
            }
        };
        
        function isAdmin(){
            angular.forEach(vm.user.currentEventRoles, function(value, key){
                if(vm.user.currentEventRoles[key] == "ADMIN_EVENT" || vm.user.currentEventRoles[key] == "ADMIN"){
                    vm.admin = true;
                };
            });
        };

        function deletePost(id, index){
            DialogService.confirm("Apagar post?", deleteCallback, "", ["Não", "Sim"])

            function deleteCallback(button){
                if(button == 2){
                    return eventsSaasTimelineService.deletePostTimeline(vm.idEvent, id)
                    .then(function(data){
                        vm.timeLine.splice(index, 1)
                        DialogService.alert("Post excluído com sucesso","Sucesso" , null, ["Fechar"]);
                    })
                    .catch(function(error){
                        console.log(error);
                    });
                }
            };
        };
    };

})();