(function () {
    'use strict';

    angular
        .module('eventsSaas.conquer')
        .controller('ConquerController', ConquerController);

	ConquerController.$inject = ['$scope', 'eventsSaasConquerService', '$location', 'eventsSaasRoutes', 'cfpLoadingBar', '$timeout']

	function ConquerController($scope, eventsSaasConquerService, $location, eventsSaasRoutes, cfpLoadingBar, $timeout) {

		cfpLoadingBar.start();
        $timeout(function() {
            cfpLoadingBar.complete();
        }, 500);

        var vm = this;
        vm.listConquer = [];
        vm.carregarConquer = carregarConquer;

        vm.carregarConquer();

		function carregarConquer() {
            return getConquer().then(function() {
                console.log("All get lista Conquer");
            });
        };
        function getConquer() {
            return eventsSaasConquerService.getConquer()
            .then(function(data) {
                vm.listConquer = data;
                return vm.listConquer;
            });
        };
    };

})();