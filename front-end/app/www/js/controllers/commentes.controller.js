(function () {
    'use strict';

    angular
        .module('eventsSaas.comment')
        .controller('CommentesController', CommentesController);

	CommentesController.$inject = ['$scope', '$location', 'eventsSaasRoutes', 'cfpLoadingBar', '$timeout', '$rootScope', '$routeParams', 'eventsSaasTimelineService', 'HostValue', 'localStorageService', 'DialogService']

	function CommentesController($scope, $location, eventsSaasRoutes, cfpLoadingBar, $timeout, $rootScope, $routeParams, eventsSaasTimelineService, HostValue, localStorageService, DialogService) {

		cfpLoadingBar.start();
        $timeout(function() {
            cfpLoadingBar.complete();
        }, 500);

        $rootScope.headerHeight = $('.elHeight').outerHeight();

        var vm = this;
        vm.title = "Deixe seu Comentário";
        vm.comments = [];
        vm.getComments = getComments;
        vm.articleId = $routeParams.articleId;
        vm.root = HostValue.aws;
        vm.avatar = HostValue.ghostsPerson;
        vm.commentText = "";
        vm.postComment = postComment;
        vm.addComment = addComment;
        vm.getArticle = getArticle;
        vm.openParticipant = openParticipant;
        vm.idLogado = localStorageService.get('user').id;
        vm.article = null;

        vm.getComments(vm.articleId);
        vm.getArticle(vm.articleId);

        function getComments(articleId) {
            return eventsSaasTimelineService.getComments(articleId)
                .then(function(data) {
                    vm.comments = data;
                    $(".chat-overflow").scrollTop(0);
                });
        };

        function getArticle(articleId) {
            return eventsSaasTimelineService.getArticle(articleId)
                .then(function(data) {
                    vm.article = data;
                });
        };

        function addComment(texto) {
            if(texto !=""){
                var comment = {'comment':texto};
                vm.message = "";
                vm.postComment(vm.articleId, comment)
                .then(function (data) {
                    if (data.blackComment != true) {
                        vm.comments.unshift(data);
                        $(".chat-overflow").scrollTop(0);
                    } else {
                        DialogService.alert("Seu comentário foi submetido para avaliação por uso de palavras não autorizadas.","Atenção" , null, ["Fechar"]);

                    }
                });
            };
        };

        function postComment(articleId, comment) {
            return eventsSaasTimelineService.postComment(articleId, comment)
            .then(function(data) {
                return data;
            });
        };

        function openParticipant(id){
            if(vm.idLogado != id){
                $location.path("/participant/"+id);
            } else {
                $location.path("/profile-user")
            }
        };
    };

})();