(function() {
	'use strict';

	angular.module('eventsSaas.agenda').controller('DetailActivityController',
			DetailActivityController);

	DetailActivityController.$inject = [ '$scope', '$location',
			'eventsSaasRoutes', 'cfpLoadingBar', '$routeParams', '$timeout',
			'eventsSaasEventsService', 'eventsSaasLectureService', 'HostValue', 'localStorageService', 'DialogService']

	function DetailActivityController($scope, $location, eventsSaasRoutes,
			cfpLoadingBar, $routeParams, $timeout, eventsSaasEventsService, eventsSaasLectureService, HostValue, localStorageService, DialogService) {

		cfpLoadingBar.start();
		$timeout(function() {
			cfpLoadingBar.complete();
		}, 500);

		var vm = this;
		vm.detailId = $routeParams.detailId;
		vm.download = download;
		vm.lecture = null;
		vm.onSave = onSave;
		vm.onSaveAndAlarm = onSaveAndAlarm;
		vm.byteToMega = byteToMega;
		vm.context = HostValue.aws;
		vm.avatarGroup = HostValue.imgAvatarGroup;
		vm.idEvent = HostValue.idEvent;
		vm.getShareSocial = getShareSocial;
		vm.disabledSave = false;
        vm.idLogado = localStorageService.get('user').id;
		vm.disabledButton = true;
		vm.palestrate = "";
		vm.openLink = openLink;

        vm.profile = localStorageService.get('profile');

		activate();
		checkedOnSave();

		function activate() {
			return eventsSaasLectureService.getLecture(vm.detailId)
			.then(function(data) {
				vm.lecture = data;
				vm.lecture.startTime = new Date(vm.lecture.startTime);
				vm.lecture.endTime = new Date(vm.lecture.endTime);
				vm.lecture.schedule = getSchedule();
				vm.lecture.dateStart = getFormatedDate(vm.lecture.startTime);
				listSpeakersId(data.listSpeakers);
			});
		};

		function listSpeakersId(data){
			if(data.length){
				data.forEach(function(element, key) {
					vm.palestrate += data.length == key+1 ? data[key].name : data[key].name +", ";
					if(!vm.lectureId){
						vm.lectureId = element.id;
						vm.lecture.avatar = data[key].pictureUrl ? vm.context+data[key].pictureUrl : HostValue.ghostsPerson;
					};
				});
			} else {
				vm.lectureId = null;
			}
		};

		function download() {
			$location.path("/agenda/" + vm.detailId + "/download");
		};
		
		function onSave(){
			vm.disabledButton = false;
			if(!vm.disabledSave){

				return eventsSaasLectureService.saveLecture(vm.detailId)
				.then(function (){
					DialogService.alert("Palestra salva com sucesso.", "Atenção", null, "Fechar");
					vm.disabledSave = true;
					vm.disabledButton = true;
				});
			} else {
				return eventsSaasLectureService.deleteLecture(vm.detailId)
				.then(function (){
					DialogService.alert("Palestra removida com sucesso.", "Atenção", null, "Fechar");
					vm.disabledSave = false;
					vm.disabledButton = true;
				});
			}
		};

		function checkedOnSave(){
			return eventsSaasLectureService.getSaveAndAlarm(vm.idEvent, vm.detailId)
			.then(function(data){
				if(data.length){
					vm.disabledSave = !data.alarm ? true : false;
				};
				return data 
			})
			.catch(function(erro){
				console.log(erro)
			});
		};
		
		function onSaveAndAlarm(){
			return eventsSaasLectureService.saveAlarmAndLecture(vm.detailId)
			.then(function (){
				var startDate = new Date(vm.lecture.startTime); // beware: month 0 = january, 11 = december
				var endDate = new Date(vm.lecture.endTime);
				var title = vm.lecture.title;
				var notes = vm.lecture.description;
				var success = function(message) {
					DialogService.alert("Alerta criado com sucesso, palestra incluida ao seu calendario", "Atenção", null, "Ok");

				};
				var error = function(message) {
					DialogService.alert("Erro ao criar o alerta.", "Atenção", null, "Ok");
				};


				// if you want to create a calendar with a specific color, pass in a JS object like this:
				var createCalOptions = window.plugins.calendar.getCreateCalendarOptions();
				createCalOptions.calendarName = vm.lecture.title;
				createCalOptions.calendarColor = "#FF0000"; // an optional hex color (with the # char), default is null, so the OS picks a color


				// create an event silently (on Android < 4 an interactive dialog is shown)
				window.plugins.calendar.createEvent(title,"",notes,startDate,endDate,success,error);
			});
		};
		
		function getSchedule(){
			var item = vm.lecture;
        	var time = formatTimeSchedule(item.startTime.getHours(), "h")+ ":" + formatTimeSchedule(item.startTime.getMinutes(), "m");
        	time += " - " + formatTimeSchedule(item.endTime.getHours(), "h")+ ":" + formatTimeSchedule(item.endTime.getMinutes(), "m");
        	return time;
        };

		function getFormatedDate(date) {
			return putZero(date.getDate())+"/"+putZero(date.getMonth()+1)+"/"+date.getFullYear();
		};

		function putZero(value) {
			return (value < 10)? "0"+value:value;
		}

        function formatTimeSchedule(t, type){
            var time = t.toString();
            if(type == "h") {
                if(time.length == 1){
                    time = "0"+time
                }
            } else {
                if(time.length == 1){
                    time = time+"0";
                }
            }
            return time;
        };
		
		function byteToMega(size){
			return (size/1024/1024).toFixed(2);
		};

		function getShareSocial(){
			var message = vm.lecture.event.name ? "Evento: "+vm.lecture.event.name+"\n" : "";
				message += vm.lecture.title ? "Título: "+vm.lecture.title+"\n" : "";
				message += vm.palestrate ? "Palestrante(s): "+vm.palestrate+"\n" : "";
				message += vm.lecture.dateStart ? "Data: "+vm.lecture.dateStart+"\n" : "";
				message += vm.lecture.schedule ? "Horário: "+vm.lecture.schedule+"\n\n" : "\n\n";
				message += vm.lecture.description ? vm.lecture.description+"\n" : "";
				message += vm.lecture.detailUrl ? "URL: "+vm.lecture.detailUrl : "";

			var onSuccess = function(result) {
				console.log("Share completed? " + result.completed); // On Android apps mostly return false even while it's true
				console.log("Shared to app: " + result.app); // On Android result.app is currently empty. On iOS it's empty when sharing is cancelled (result.completed=false)
			};

			var onError = function(msg) {
				console.log("Sharing failed with message: " + msg);
			};

			window.plugins.socialsharing.share(message);
		};
		
		function openLink(url){
			cordova.InAppBrowser.open(url, '_blank', 'location=yes');
		};
	};

})();