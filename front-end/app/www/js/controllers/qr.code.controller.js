(function () {
    'use strict';

    angular
        .module('eventsSaas.login')
        .controller('QrCodeController', QrCodeController)

    QrCodeController.$inject = ['$scope', '$uibModal', '$location', 'eventsSaasRoutes', 'cfpLoadingBar', '$timeout', '$cordovaBarcodeScanner']

    function QrCodeController($scope, $uibModal, $location, eventsSaasRoutes, cfpLoadingBar, $timeout, $cordovaBarcodeScanner) {

        cfpLoadingBar.start();
        $timeout(function() {
            cfpLoadingBar.complete();
        }, 500);

        var vm = this;
        vm.scan = scan;

        function scan(){
            $cordovaBarcodeScanner
              .scan()
              .then(function(barcodeData) {
                // Success! Barcode data is here

                if(!result.cancelled)
                {
                    if(result.format == "QR_CODE")
                    {
                        navigator.notification.prompt("Please enter name of data",  function(input){
                            var name = input.input1;
                            var value = result.text;
                            var data = localStorage.getItem("LocalData");
                            console.log(data);
                            data = JSON.parse(data);
                            data[data.length] = [name, value];
                            localStorage.setItem("LocalData", JSON.stringify(data));
                            alert("Done");
                        });
                    }
                }

                alert(barcodeData.text);
                console.log("Barcode format ->" + barcodeData.format);
                console.log("Cancelled ->" + barcodeData.cancelled);
              }, function(error) {
                console.log("An error happened ->" + error);
              });
        }

    };

})();