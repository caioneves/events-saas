(function () {
    'use strict';

    angular
        .module('eventsSaas.login')
        .controller('LoginController', LoginController)

    LoginController.$inject = ['$scope', '$rootScope', '$uibModal', '$location', 'eventsSaasRoutes', '$timeout', 'eventsSaasLoginService', 'localStorageService', '$cordovaOauth', '$http', 'DialogService', 'eventsSaasProfileService', 'HostValue']

    function LoginController($scope, $rootScope, $uibModal, $location, eventsSaasRoutes, $timeout, eventsSaasLoginService, localStorageService, $cordovaOauth, $http, DialogService, eventsSaasProfileService, HostValue) {
        var vm = this;
        
        vm.submit = submit;
        vm.facebook = facebook;
        vm.linkedin = linkedin;
        vm.hashLogin =  true;
        vm.fieldsMSG = false;
        vm.scrollField = scrollField;
        vm.socialData = false;
        vm.init = init;
        vm.numberOnly = numberOnly;
        vm.noProd = HostValue.environment != "prod" ? true : false;
        vm.msgAtencaoSocialLogin = "O usuário não tem permissão ou ainda não habilitou o login por meio de rede social";
        vm.idEvent = localStorageService.get('idEvent') ? localStorageService.get('idEvent') : "";
        HostValue.idEvent = vm.noProd ? vm.idEvent : HostValue.idEvent;

        if(vm.noProd && vm.idEvent){
            vm.login = {
                "evento" : vm.idEvent
            }
        }

        window.addEventListener('native.keyboardshow', keyboardShowHandler);
        function keyboardShowHandler(e){
            //Prevent the native UIScrollView from moving when an input is focused.
            cordova.plugins.Keyboard.disableScroll(true);
            $("body").addClass("hide-footer");
        };
        
        window.addEventListener('native.keyboardhide', keyboardHideHandler);
        function keyboardHideHandler(e){
            $("body").removeClass("hide-footer");
        };

        function init(){
            $("#password, #email").on("focus", function(){
                vm.scrollField();
            });
            document.addEventListener("backbutton", onBackKeyDown, false);
            function onBackKeyDown(e) {
                var url = window.location.href;
                var i = url.indexOf("#");
                var r = url.substring(i+1);
                if(r == "/login"){
                    navigator.app.exitApp();
                    //e.preventDefault();
                } else {
                    window.history.back();
                }
            };
        };

        function scrollField(){
            $(".calc-height").animate({scrollTop: $("#password").offset().top}, "slow");
        };

        function facebook(){
            if(vm.hashLogin){
                vm.hashLogin = false;
                var fbLoginFailed = function(error){
                    vm.socialData = false;
                    vm.hashLogin = true;
                };

                var fbLoginSuccess = function (userData) {
                    vm.socialData = {};
                    vm.socialData.providerId = "facebook";
                    vm.socialData.providerUserId = userData.data.id;
                    vm.socialData.displayName = userData.data.name;
                    vm.socialData.profileUrl = "https://www.facebook.com/"+userData.data.id;
                    vm.socialData.imageUrl = userData.data.picture.data.url;
                    vm.socialData.enabled = "true";

                    if(vm.noProd){
                        HostValue.idEvent = vm.login && vm.login.evento ? vm.login.evento : "";
                        HostValue.idEvent ? localStorageService.set('idEvent', vm.login.evento) : localStorageService.set('idEvent', null);
                    };

                    eventsSaasLoginService.loginSocial("facebook", userData.data.id)
                    .then(function(data){
                        if(data.hasFacebookSocialProfileEnable){
                            $location.path(eventsSaasRoutes.HOME);
                        } else {
                            DialogService.alert(vm.msgAtencaoSocialLogin,"Atenção" , null, ["Fechar"]);
                        };
                        vm.hashLogin = true;
                    })
                    .catch(function(error){
                        DialogService.alert(vm.msgAtencaoSocialLogin,"Atenção" , null, ["Fechar"]);
                        vm.hashLogin = true;
                    });
                };

                $cordovaOauth.facebook('1301857159850937', ["email", "user_website", "user_location", "user_relationships"])
                .then(function(result) {
                    $http.get("https://graph.facebook.com/v2.2/me", { params: { access_token: result.access_token, fields: "id,name,gender,location,website,picture,relationship_status", format: "json" }})
                    .then(fbLoginSuccess)
                    .catch(fbLoginFailed)
                })
                .catch(fbLoginFailed)
            };
        };

        function linkedin(){
            if(vm.hashLogin){
                vm.hashLogin = false;
                var lkLoginFailed = function(userData) { 
                    vm.socialData = false;
                    vm.hashLogin = true;
                };
                var lkLoginSuccess = function(userData) { 
                    vm.socialData = {};
                    vm.socialData.providerId = "linkedin";
                    vm.socialData.providerUserId = userData.data.id;
                    vm.socialData.displayName = userData.data.firstName+" "+userData.data.lastName;
                    vm.socialData.profileUrl = userData.data.siteStandardProfileRequest.url;
                    vm.socialData.imageUrl = "";
                    vm.socialData.enabled = "true";

                    if(vm.noProd){
                        HostValue.idEvent = vm.login && vm.login.evento ? vm.login.evento : "";
                        HostValue.idEvent ? localStorageService.set('idEvent', vm.login.evento) : localStorageService.set('idEvent', null);
                    };

                    eventsSaasLoginService.loginSocial("linkedin", userData.data.id)
                    .then(function(data){
                        if(data.hasLinkedSocialProfileEnable){
                            $location.path(eventsSaasRoutes.HOME);
                        } else{
                            DialogService.alert(vm.msgAtencaoSocialLogin,"Atenção" , null, ["Fechar"]);
                        }
                        vm.hashLogin = true;
                    })
                    .catch(function(error){
                        DialogService.alert(vm.msgAtencaoSocialLogin,"Atenção" , null, ["Fechar"]);
                        vm.hashLogin = true;
                    });

                };

                var scopes = ['r_basicprofile', 'r_emailaddress', 'rw_company_admin', 'w_share'];

                $cordovaOauth.linkedin('7835lqfe5t7mxr', '24wER6jBO8JO6VET', scopes, 'inLogin')
                .then(function(result) {
                    $http.get("https://api.linkedin.com/v1/people/~?format=json&oauth2_access_token=" +  result.access_token)
                    .then(lkLoginSuccess)
                    .catch(lkLoginFailed)
                })
                .catch(lkLoginFailed) 
            };
        };


        //////////////////////////////
    	function submit(form, login) {
             if(form.$valid && vm.hashLogin){
                vm.hashLogin = false;

                if(vm.noProd){
                    HostValue.idEvent = vm.login && vm.login.evento ? vm.login.evento : "";
                    HostValue.idEvent ? localStorageService.set('idEvent', vm.login.evento) : localStorageService.set('idEvent', null);
                };

                eventsSaasLoginService.login(login.email, login.senha)
                .then(function(data){

                    if(vm.socialData){
                        postSocialProfile(vm.socialData)
                    };

                    if(data.user.resetedPassword){
                        $location.path(eventsSaasRoutes.CHANGEPASSWORD); 
                    } else {
                        $location.path(eventsSaasRoutes.HOME);
                    }
                    $rootScope.forgot = data.user.resetedPassword;
                })
                .catch(function(error){
                    DialogService.alert("Nome de usuário ou senha está incorreto.","Atenção" , null, ["Fechar"]);
                    vm.hashLogin = true;
                });
             } else {
                 vm.fieldsMSG = true; 
             }
        };
            
        function postSocialProfile(data){
            return eventsSaasProfileService.postSocialProfile(data)
            .then(function(data){
                return data;
            })
            .catch(function(error){
                return error;
            });
        };

        function numberOnly(field){
            var valor = field.$viewValue.replace(/\D/g,"");
            vm.login.evento = valor;
        };
    };

})();