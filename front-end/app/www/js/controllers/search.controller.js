(function() {
    'use strict';

    angular
        .module('eventsSaas.search')
        .controller('SearchController', SearchController);

    SearchController.$inject = ['$scope', 'eventsSaasQuickAccessService', '$location', 'eventsSaasRoutes', 'cfpLoadingBar', '$timeout'];

    /* @ngInject */
    function SearchController($scope, eventsSaasQuickAccessService, $location, eventsSaasRoutes, cfpLoadingBar, $timeout) {
        var vm = this;
        vm.search = { campo: 'Brinkle'}
        vm.openMaps = openMaps;
        vm.noEmoji = noEmoji;

        function openMaps(){
            var start, latitude, longitude;
            launchnavigator.isAppAvailable(launchnavigator.APP.GOOGLE_MAPS, function(isAvailable){
                var app;
                if(isAvailable){
                    app = launchnavigator.APP.GOOGLE_MAPS;
                }else{
                    console.warn("Google Maps not available - falling back to user selection");
                    app = launchnavigator.APP.USER_SELECT;
                }
                launchnavigator.navigate([-23.6742228, -46.54360029999998], {
                    app: app
                });
            });
        }

        function noEmoji(field, model){
            var valor = parseUTF16(field.$viewValue);
            vm.search[model] = valor;
        };
        
        function parseUTF16(text) {
            var ranges = [
                '[\ud000-\udfff]',
                '[\u3000-\u3fff]',
                '[\u2000-\u2fff]',
                '[\uFE00-\uFEff]'
            ];

            return text.replace(
                new RegExp(ranges.join('|'), 'g'),
                function(m)
                {
                    function hexEncode (text){
                        var hex, i;

                        var result = "";

                        for (i=0; i<text.length; i++) {
                            hex = text.charCodeAt(i).toString(16);
                            result += ("000"+hex).slice(-4);
                        }
                        return result
                    }
                    return '';
                });
        };

        activate();

        ////////////////

        function activate() {
        };
    }
})();