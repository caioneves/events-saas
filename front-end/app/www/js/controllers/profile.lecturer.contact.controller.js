(function () {
    'use strict';

    angular
        .module('eventsSaas.profile.lecturer')
        .controller('ProfileLecturerContactController', ProfileLecturerContactController);

	ProfileLecturerContactController.$inject = ['$scope', '$location', 'eventsSaasRoutes', 'cfpLoadingBar', '$timeout', 'eventsSaasProfileService', 'HostValue', '$routeParams', 'eventsSaasMessageService', 'DialogService']

	function ProfileLecturerContactController($scope, $location, eventsSaasRoutes, cfpLoadingBar, $timeout, eventsSaasProfileService, HostValue, $routeParams, eventsSaasMessageService, DialogService) {

		cfpLoadingBar.start();
        $timeout(function() {
            cfpLoadingBar.complete();
        }, 500);


        var vm = this;

        vm.profile = null;
        vm.profileId = $routeParams.id;
        vm.hasSocial = [];
        vm.viewSocial = false;
        vm.contato = {};
        vm.contato.subject = "";
        vm.contato.message = "";
        vm.sendEmail = sendEmail;
        vm.clicked = false;
        activate();

        function activate() {
            cfpLoadingBar.start();
            return eventsSaasProfileService.getMiniprofileById(vm.profileId)
                .then(function (data) {
                    vm.profile = data;
                    vm.pictureUrl = HostValue.aws + vm.profile.pictureUrl;

                }).finally(function () {
                    cfpLoadingBar.complete();
                });
        }

        function sendEmail() {
            if (!vm.clicked) {
                vm.clicked = true;
            } else {
                return
            }

            cfpLoadingBar.start();

            return eventsSaasMessageService.sendEmailContato(HostValue.idEvent, vm.profileId, vm.contato)
                .then(function (data) {
                    DialogService.alert("Mensagem enviada com sucesso.", "Atenção",null, "OK");
                    vm.contato.subject="";
                    vm.contato.message="";

                }).catch(function (data) {
                    DialogService.alert("Erro ao enviar mensagem.", "Atenção", "OK");
                }).finally(function () {
                    cfpLoadingBar.complete();
                    vm.clicked = false;
                });
        }



    }

})();