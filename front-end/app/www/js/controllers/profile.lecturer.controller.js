(function () {
    'use strict';

    angular
        .module('eventsSaas.profile.lecturer')
        .controller('ProfileLecturerController', ProfileLecturerController);

    ProfileLecturerController.$inject = ['$scope', '$location', '$routeParams', 'cfpLoadingBar', 'eventsSaasProfileService', 'HostValue']

    function ProfileLecturerController($scope, $location, $routeParams, cfpLoadingBar, eventsSaasProfileService, HostValue) {


        var vm = this;

        vm.profile = null;
        vm.profileId = $routeParams.id;
        vm.hasSocial = [];
        vm.viewSocial = false;
        vm.openLink = openLink;
        activate();

        function activate() {
            cfpLoadingBar.start();
            return eventsSaasProfileService.getMiniprofileById(vm.profileId)
                .then(function (data) {
                    vm.profile = data;
                    vm.pictureUrl = vm.profile.pictureUrl ? HostValue.aws + vm.profile.pictureUrl : null;
                    socialNetwork(data)
                }).finally(function () {
                    cfpLoadingBar.complete();
                });
        }

        function socialNetwork(data) {
            var array = [
                { "nome": "linkedin", "ico": "fa-linkedin", "bg": "font-grey-sure-5", "url": data.linkedInUrl ? "https://www.linkedin.com/in/" + data.linkedInUrl : data.linkedInUrl },
                { "nome": "facebook", "ico": "fa-facebook", "bg": "font-blue-dark-2", "url": data.facebookUrl ? "https://www.facebook.com/" + data.facebookUrl : data.facebookUrl },
                { "nome": "twitter", "ico": "fa-twitter", "bg": "font-blue-sure", "url": data.twitterUrl ? "https://twitter.com/" + data.twitterUrl : data.twitterUrl },
                { "nome": "instagram", "ico": "fa-instagram", "bg": "font-orange-sure", "url": data.instagramUrl ? "https://www.instagram.com/" + data.instagramUrl : data.instagramUrl }
            ]
            $.each(array, function (index, value) {
                if (array[index].url) {
                    vm.hasSocial.push(array[index]);
                    vm.viewSocial = true;
                };
            });
        };

        function openLink(url) {
            cordova.InAppBrowser.open(url, '_blank', 'location=yes');
        };
    }

})();