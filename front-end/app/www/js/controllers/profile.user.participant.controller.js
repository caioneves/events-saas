(function () {
    'use strict';

    angular
        .module('eventsSaas.profile.user')
        .controller('ProfileParticipantController', ProfileParticipantController);

	ProfileParticipantController.$inject = ['$scope', '$location', 'eventsSaasRoutes', 'cfpLoadingBar', '$timeout', 'eventsSaasProfileService', 'HostValue', '$routeParams']

	function ProfileParticipantController($scope, $location, eventsSaasRoutes, cfpLoadingBar, $timeout, eventsSaasProfileService, HostValue, $routeParams) {

		cfpLoadingBar.start();
        $timeout(function() {
            cfpLoadingBar.complete();
        }, 500);

        var vm = this;
        vm.profile = [];
        vm.carregarUser = carregarUser;   
        vm.participant = $routeParams.detailId;
        vm.linkMensagem = "/participant/chat/"+ vm.participant;
        vm.openLinkChat = openLinkChat;
        vm.openLinkSocial = openLinkSocial;
        vm.avatar = HostValue.ghostsPerson;
        vm.baseUrl = HostValue.aws+"/";
        vm.viewSocial = false;
        vm.hasSocial = [];

        vm.carregarUser();

        function carregarUser() {
            return getUser().then(function(data) {
                vm.profile = data;
                vm.pictureUrl = vm.profile.pictureUrl ? HostValue.aws + vm.profile.pictureUrl : null;
                socialNetwork(data);
            });
        };
        function getUser() {
            return eventsSaasProfileService.getMiniprofileById(vm.participant)
            .then(function(data) {
                return data;
            })
            .catch(function(data){
                console.log(data);
            });
        };

        function openLinkChat(){
            $location.path("/participant/chat/"+vm.participant);
        };

        function socialNetwork(data){
            var array = [
                {"nome": "linkedin", "ico": "fa-linkedin", "bg": "font-grey-sure-5", "url":data.linkedInUrl ? "https://www.linkedin.com/in/"+data.linkedInUrl : data.linkedInUrl},
                {"nome": "facebook", "ico": "fa-facebook", "bg": "font-blue-dark-2", "url":data.facebookUrl ? "https://www.facebook.com/"+data.facebookUrl : data.facebookUrl},
                {"nome": "twitter", "ico": "fa-twitter", "bg": "font-blue-sure", "url":data.twitterUrl ? "https://twitter.com/"+data.twitterUrl : data.twitterUrl},
                {"nome": "instagram", "ico": "fa-instagram", "bg": "font-orange-sure", "url":data.instagramUrl ? "https://www.instagram.com/"+data.instagramUrl : data.instagramUrl}
            ]
            $.each(array, function(index, value){
                if(array[index].url){
                    vm.hasSocial.push(array[index]);
                    vm.viewSocial = true;
                };
            });
        };

        function openLinkSocial(url){
            cordova.InAppBrowser.open(url, '_blank', 'location=yes');
        };

    };

})();