(function () {
    'use strict';

    angular
        .module('eventsSaas.profile')
        .controller('ProfileUserController', ProfileUserController);

	ProfileUserController.$inject = ['$scope', '$location', 'eventsSaasRoutes', 'eventsSaasProfileService', 'HostValue', 'cfpLoadingBar', '$timeout', 'localStorageService', '$cordovaAppAvailability', '$cordovaDevice']

	function ProfileUserController($scope, $location, eventsSaasRoutes, eventsSaasProfileService, HostValue, cfpLoadingBar, $timeout, localStorageService, $cordovaAppAvailability, $cordovaDevice) {

		cfpLoadingBar.start();
        $timeout(function() {
            cfpLoadingBar.complete();
        }, 500);

        var vm = this;
        vm.profile = [];
        vm.hasSocial = [];
        vm.viewSocial = false;
        vm.viewProfile = viewProfile;
        vm.baseUrl = HostValue.aws;
        vm.linkEdit = eventsSaasRoutes.EDITARUSUARIO;
        vm.openLink = openLink;
        vm.openLinkChangePassword = openLinkChangePassword;


        if(localStorageService.get('profile')){
            vm.profile = localStorageService.get('profile');
            vm.profile.bg = vm.profile.pictureUrl ? vm.baseUrl + vm.profile.pictureUrl : null;
            $('#img-qr-code').qrcode(vm.profile.id+"");
            socialNetwork(localStorageService.get('profile'));
        } else {
            vm.viewProfile();
        };

        function viewProfile() {
            return getProfileLogged(HostValue.idEvent).then(function(data) {
                vm.profile = data;
                vm.profile.bg = vm.profile.pictureUrl ? vm.baseUrl + vm.profile.pictureUrl : null;
                $('#img-qr-code').qrcode(data.id+"");
                socialNetwork(data);
                console.log("get profile");
            });
        };
        function getProfileLogged(id) {
            return eventsSaasProfileService.getProfile(id)
            .then(function(data) {
                return data;
            });
        };

        function socialNetwork(data){
            var array = [
                {"nome": "linkedin", "ico": "fa-linkedin", "bg": "font-grey-sure-5", "url":data.linkedInUrl ? "https://www.linkedin.com/in/"+data.linkedInUrl : data.linkedInUrl},
                {"nome": "facebook", "ico": "fa-facebook", "bg": "font-blue-dark-2", "url":data.facebookUrl ? "https://www.facebook.com/"+data.facebookUrl : data.facebookUrl},
                {"nome": "twitter", "ico": "fa-twitter", "bg": "font-blue-sure", "url":data.twitterUrl ? "https://twitter.com/"+data.twitterUrl : data.twitterUrl},
                {"nome": "instagram", "ico": "fa-instagram", "bg": "font-orange-sure", "url":data.instagramUrl ? "https://www.instagram.com/"+data.instagramUrl : data.instagramUrl}
            ]
            $.each(array, function(index, value){
                if(array[index].url){
                    vm.hasSocial.push(array[index]);
                    vm.viewSocial = true;
                };
            });
        };

        function openLink(url){
            cordova.InAppBrowser.open(url, '_blank', 'location=yes');
        };

        function openLinkChangePassword(){
            $location.path(eventsSaasRoutes.CHANGEPASSWORD); 
        };
    };

})();