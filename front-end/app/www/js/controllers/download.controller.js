(function() {
    'use strict';

    angular
        .module('eventsSaas.agenda')
        .controller('DownloadController', DownloadController);

    DownloadController.$inject = ['$scope', 'eventsSaasRoutes', 'cfpLoadingBar', '$routeParams', '$timeout', 'eventsSaasLectureService', '$q'];

    /* @ngInject */
    function DownloadController($scope, eventsSaasRoutes, cfpLoadingBar, $routeParams, $timeout, eventsSaasLectureService, $q) {
        var vm = this;
        vm.title = "Lista de Downloads";
        vm.detailId = $routeParams.detailId;
        vm.downloads = null;
        vm.totalSizeInMB = 0;

        vm.getDownloadUrl = getDownloadUrl;
        vm.byteToMega = byteToMega;
        vm.downloadAll = downloadAll;
        vm.downloadFile = downloadFile;
        activate();

        var rootPath = (cordova.file.externalApplicationStorageDirectory ? cordova.file.externalApplicationStorageDirectory : cordova.file.dataDirectory);
        // //////////////

        function activate(){
        	cfpLoadingBar.start();
            return eventsSaasLectureService.getLectureDownloadList(vm.detailId)
            	.then(function(data){
                vm.downloads = data;
                vm.totalSizeInMB = totalSizeInMB(vm.downloads);
                // check if file is already downloaded
                return $q(function(resolve, reject){
                	var i = vm.downloads.length;
                	vm.downloads.forEach(function(item){
                    	item.downloading = false;
                		item.downloaded = false;
                		var fileUrl = rootPath + item.fileName;
                    	window.resolveLocalFileSystemURL(fileUrl,
                    	function(fs){
                    			item.file = fs.nativeURL;
                        		item.downloaded = true;
                        		i--;
                        		if (i == 0 ){
                            		resolve();
                            	}
                    		}, function(){
                        		i--;
                        		if (i == 0 ){
                            		resolve();
                            	}
                        	});
                    });

                });
            })
            .catch(function(error){
                 return console.log(error)
            }).finally(function(){
            	cfpLoadingBar.complete();
            });
        };

        function getDownloadUrl(id, downloadId){
        	return eventsSaasLectureService.downloadFile(id, downloadId);
        }

        function totalSizeInMB(downloads){
        	var total = 0;
        	if (downloads){
        		downloads.forEach(function (item){
        			total += item.size/1048576;
        		});
        	}
        	return total.toFixed(2);
        }

        function byteToMega(size){
			return (size/1048576).toFixed(2);
		}

        function downloadFile(id, item, open){

        		$q(function(resolve, reject){
        			if (item.downloaded) {
        				resolve();
        				return;
        			}
        			var fileTransfer = new FileTransfer();
            		var uri = encodeURI(getDownloadUrl(id, item.id));
            		item.downloading = true;
            		fileTransfer.download(
            		    uri,
            		    rootPath + item.fileName,
            		    function(entry) {
            		    	item.downloading = false;
            		    	item.downloaded = true;
            		    	item.file = entry.toURL();
            		    	resolve();
            		    },
            		    function(error) {
            		    	item.downloading = false;
            		    	item.downloaded = false;
            		    	reject();
            		    },
            		    false,{}
            		);
        		}).finally(function(){
        			if(open){
                cordova.plugins.fileOpener2.open(
                    item.file,
                    mimeType.lookup(item.file),
                    {
                        error : function(e){
                          console.log(e);
                        },
                        success : function(s){
                          console.log(s);
                        }
                    }
                );
        			}
        		});



        }

        function downloadAll(i){
        	vm.downloads.forEach(function(item){
        		downloadFile(vm.detailId, item, false);
        	});
        }
    }
})();
