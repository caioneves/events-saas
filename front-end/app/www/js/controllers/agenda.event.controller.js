(function () {
    'use strict';

    angular
        .module('eventsSaas.agenda')
        .controller('AgendaEventController', AgendaEventController);

    AgendaEventController.$inject = ['$scope', 'eventsSaasRoutes', 'cfpLoadingBar', '$routeParams', 'eventsSaasEventsService', '$location']

    function AgendaEventController($scope, eventsSaasRoutes, cfpLoadingBar, $routeParams, eventsSaasEventsService, $location) {

        var vm = this;
        vm.title = "Programação";
        vm.lecturePerDay = new Map();
        vm.keys = [];
        vm.selected = null;
        vm.detailId= null;
        
        vm.handleSummary = handleSummary;
        vm.onSelect = onSelect;
        vm.showDay = showDay;
        vm.showMonth = showMonth;
        vm.onClick = onClick;
        vm.onDetail = onDetail;

        activate();

        function activate(){
        	cfpLoadingBar.start();
			vm.detailId = $routeParams.detailId;
			
			return eventsSaasEventsService.getLectureSummary(vm.detailId)
	    	.then(function(data){
	    		vm.handleSummary(data);
	    	}).finally(function(){
	    		 cfpLoadingBar.complete();
	    	});
        };
        
        function handleSummary(data){
        	if (data && data.length > 0 ){
	        	data.forEach(function(item){
	        		item.startTime = new Date(item.startTime);
	        		item.endTime = new Date(item.endTime);
                    item.schedule = getSchedule(item);
                    item.speakers = item.speakers.length ? item.speakers : null;  
	        		put(vm.lecturePerDay, getDayKey(item.startTime), item);
	        		
	        	});
	        	
	        	vm.keys = Array.from(vm.lecturePerDay.keys()).sort();
                vm.selected = vm.keys[0];
        	}
        };
        
        function getDayKey(dateObj){
        	var month = dateObj.getUTCMonth() + 1; // months from 1-12
            var day = dateObj.getUTCDate();
            var year = dateObj.getUTCFullYear();
            
            return month + '' + day + '' + year;
        };
        
        function put(map, key, value){
        	if (!map.has(key)){
        		map.set(key, []);
        	}
        	map.get(key).push(value);
        };
        
        function onSelect(item){
        	vm.selected = item;
        };
        
        function showDay(item){
        	return new Date(item[0].startTime).getUTCDate();
        };

        function showMonth(item){
        	var monthNames = ['Jan','Fev','Mar','Abr','Mai','Jun','Jul','Ago','Set','Out','Nov','Dez'];
        	return monthNames[item[0].startTime.getUTCMonth()];
        };
        
        function onClick(id){
        	$location.path("/agenda-event/"+id);
        };
        
        function onDetail(id){
        	$location.path("/agenda/"+id);
        };
        
        function getSchedule(item){
        	var time = formatTimeSchedule(item.startTime.getHours(), "h")+ ":" + formatTimeSchedule(item.startTime.getMinutes(), "m");
        	time += " - " + formatTimeSchedule(item.endTime.getHours(), "h")+ ":" + formatTimeSchedule(item.endTime.getMinutes(), "m");
        	return time;
        };

        function formatTimeSchedule(t, type){
            var time = t.toString();
            if(type == "h") {
                if(time.length == 1){
                    time = "0"+time
                }
            } else {
                if(time.length == 1){
                    time = time+"0";
                }
            }
            return time;
        };
    };
})();