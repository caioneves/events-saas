(function () {
    'use strict';

    angular
        .module('eventsSaas.matter')
        .controller('DetailMatterController', DetailMatterController);

    DetailMatterController.$inject = ['$scope', 'eventsSaasRoutes', 'cfpLoadingBar', '$routeParams', '$timeout', 'eventsSaasProfileService', '$location', 'eventsSaasTimelineService', 'HostValue', 'localStorageService', 'eventsSaasInfoEventService']

    function DetailMatterController($scope, eventsSaasRoutes, cfpLoadingBar, $routeParams, $timeout, eventsSaasProfileService, $location, eventsSaasTimelineService, HostValue, localStorageService, eventsSaasInfoEventService) {

        cfpLoadingBar.start();
        $timeout(function() {
            cfpLoadingBar.complete();
        }, 500);

        var vm = this;
        vm.users = [];
        vm.rede = [];
        vm.matter = [];
        vm.comments = [];
        vm.carregarUser = carregarUser;
        vm.isShares = false; 
        vm.viewShares = false;       
        vm.activityId = $routeParams.detailId;
        vm.commentes = commentes;
        vm.compartilha = compartilha;
        vm.getArticle = getArticle;
        vm.getComments = getComments;
        vm.novocomentar = eventsSaasRoutes.COMENTAR;
        vm.root = HostValue.aws;
        vm.idEvent = HostValue.idEvent;
		vm.ghostsPerson = HostValue.ghostsPerson;
        vm.openParticipant = openParticipant;
        vm.getShareSocial = getShareSocial;
        vm.idLogado = localStorageService.get('user').id;

        vm.getArticle(vm.activityId);
        vm.getComments(vm.activityId);

        function getArticle(articleId) {
            return eventsSaasTimelineService.getArticle(articleId)
            .then(function(data) {
                vm.matter = data;
                getEvent();
                //vm.matter.liked=true;
                //preencherArticle(vm.matter);
            });
        };

        function getComments(articleId) {
            return eventsSaasTimelineService.getComments(articleId)
            .then(function(data) {
                vm.comments = data;
            });
        };

        function commentes(){
            $location.path('/commentes/'+vm.activityId);
        };
        function compartilha(){
            vm.isShares = !vm.isShares;
            if(vm.isShares){
                vm.carregarUser(vm.activityId);
            } else {
                vm.viewShares = vm.isShares;
            }
        };
        function getEvent(){
            return eventsSaasInfoEventService.getInfoEvent(vm.idEvent)
            .then(function(data){
                vm.matter.nameEvent = data.name;
                return data;
            })
            .catch(function(erro){
                console.log(erro);
            });
        };
        function carregarUser(id) {
            return getUser().then(function() {
                //preencherShares(vm.users, id);
                vm.viewShares = vm.isShares;
            });
        };
        function getUser() {
            return eventsSaasProfileService.getUser()
            .then(function(data) {
                vm.users = data;
                return vm.users;
            });
        };
        function preencherShares(data, id){
            angular.forEach(data, function(value, key) {
                if(data[key].id == id){
                    vm.rede = data[key].redeSocial;
                }
            });
        };

        function openParticipant(id){
            if(vm.idLogado != id){
                $location.path("/participant/"+id);
            } else {
                $location.path("/profile-user")
            }
        };

        function getShareSocial(){
			var message = vm.matter.nameEvent ? "Evento: "+vm.matter.nameEvent+"\n" : "";
				message += vm.matter.title ? "Título: "+vm.matter.title+"\n" : "";
				message += vm.matter.author.fullName ? "Autor: "+vm.matter.author.fullName+"\n\n" : "\n";
				message += vm.matter.description ? $('.text-matter').text().trim()+"\n" : "";
				message += vm.matter.detaillUrl ? "URL: "+vm.matter.detaillUrl : "";

			var onSuccess = function(result) {
				console.log("Share completed? " + result.completed); // On Android apps mostly return false even while it's true
				console.log("Shared to app: " + result.app); // On Android result.app is currently empty. On iOS it's empty when sharing is cancelled (result.completed=false)
			};

			var onError = function(msg) {
				console.log("Sharing failed with message: " + msg);
			};

			window.plugins.socialsharing.share(message);
		};
    };

})();