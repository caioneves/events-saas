(function () {
    'use strict';

    angular
        .module('eventsSaas.timeline')
        .controller('TimeLinePostController', TimeLinePostController);

    TimeLinePostController.$inject = ['$scope', '$location', 'eventsSaasRoutes', 'cfpLoadingBar', '$timeout', 'HostValue', 'localStorageService', 'eventsSaasTimelineService', 'DialogService']

    function TimeLinePostController($scope, $location, eventsSaasRoutes, cfpLoadingBar, $timeout, HostValue, localStorageService, eventsSaasTimelineService, DialogService) {

        var vm = this;
        vm.title = "Post";
        vm.openCamera = openCamera;
        vm.hasImg = false;
        vm.openGallery = openGallery;
        vm.srcImg = "";
        vm.idEvento = HostValue.idEvent;
        vm.idImg = null;
        vm.addPostTimeLine = addPostTimeLine;
        vm.habilitar = false;

        window.addEventListener('native.keyboardshow', keyboardShowHandler);
        function keyboardShowHandler(e){
            //Prevent the native UIScrollView from moving when an input is focused.
            cordova.plugins.Keyboard.disableScroll(true);
            $("body").addClass("hide-footer");
        };

        window.addEventListener('native.keyboardhide', keyboardHideHandler);
        function keyboardHideHandler(e){
            $("body").removeClass("hide-footer");
        };
        
        function addPostTimeLine(msg){
            var txt = msg.trim();
            if(txt !="" || vm.hasImg){
                vm.habilitar = true;
                cfpLoadingBar.start(); 
                 if(vm.hasImg){
                    txt = txt ? txt : null;
                    return sendImg(vm.srcImg)
                    .then(function(id){
                        sendPost(vm.idEvento, txt, id);
                    })
                    .catch(function(erro){
                        console.log(erro);
                        cfpLoadingBar.complete();
                    });
                } else {
                    sendPost(vm.idEvento, txt, null);
                } 
            };
        };

        function sendPost(idEvento, txt, idImg){
            return eventsSaasTimelineService.postTimeline(idEvento, txt, idImg)
            .then(function(data){
                vm.habilitar = false;
                    if (data.blackPost) {
                        DialogService.alert("Seu post foi submetido para avaliação por uso de palavras não autorizadas.","Atenção" , openTimeline, ["Fechar"]);
                    } else {
                        DialogService.alert("Artigo publicado com sucesso","Sucesso" , openTimeline, ["Fechar"]);
                    }

                return data;
            })
            .finally(function(){
                vm.habilitar = false;
                cfpLoadingBar.complete();
            })
        };

        function openTimeline(){
            $location.path(eventsSaasRoutes.TIMELINE);
            $scope.$apply()
        };

        function sendImg(image){
            return eventsSaasTimelineService.addImageTimeLine(image)
            .then(function(data){
                return data;
            })
            .catch(function(error){
                return error
            })
        };

        vm.dynamicPopover = {
            templateUrl: 'menuOption.html'
        };

        function setOptions(srcType) {
            var options = {
                // Some common settings are 20, 50, and 100
                quality: 100,
                destinationType: Camera.DestinationType.FILE_URI,
                // In this app, dynamically set the picture source, Camera or photo gallery
                sourceType: srcType,
                encodingType: Camera.EncodingType.JPEG,
                mediaType: Camera.MediaType.PICTURE,
                allowEdit: true,
                correctOrientation: true  //Corrects Android orientation quirks
            }
            return options;
        }

        function openCamera(){
            var srcType = Camera.PictureSourceType.CAMERA;
            var options = setOptions(srcType);

            navigator.camera.getPicture(onSuccess, onFail, options);

            function onSuccess(imageURI) {
                vm.hasImg = true;
                $(".content-text .post-img").remove();
                var $content = $('<div class="text-center mrg-top-10 relative post-img"><a href="javascript:;" class="display-1 pding-left-right-5 pding-top-bottom-5 absolute close-top"><em class="ion-close-circled font-30"></em></a><img src="" alt="" class="full-img" id="img-post" /></div>');
                $(".content-text").append($content);
                var smallImage = document.getElementById('img-post');
                toDataURL(imageURI, function(dataUrl) {
                    smallImage.src = dataUrl;
                    vm.srcImg = dataUrl;
                    removeImg();
                });
            };

            function onFail(message) {
                
            };
        };

        function setOptionsGallery(srcType) {
            var options = {
                // Some common settings are 20, 50, and 100
                quality: 100,
                destinationType: Camera.DestinationType.FILE_URI,
                // In this app, dynamically set the picture source, Camera or photo gallery
                sourceType: srcType,
                encodingType: Camera.EncodingType.JPEG,
                mediaType: Camera.MediaType.PICTURE,
                allowEdit: true,
                correctOrientation: true  //Corrects Android orientation quirks
            }
            return options;
        }

        function openGallery(){
            var srcType = Camera.PictureSourceType.PHOTOLIBRARY;
            var options = setOptions(srcType);

            navigator.camera.getPicture(onSuccess, onFail, options);

            function onSuccess(imageURI) {
                vm.hasImg = true;
                $(".content-text .post-img").remove();
                var $content = $('<div class="text-center mrg-top-10 relative post-img"><a href="javascript:;" ng-click="vm.removeImg()" class="display-1 pding-left-right-5 pding-top-bottom-5 absolute close-top"><em class="ion-close-circled font-30"></em></a><img src="" alt="" class="full-img" id="img-post" /></div>');
                $(".content-text").append($content);
                var smallImage = document.getElementById('img-post');
                toDataURL(imageURI, function(dataUrl) {
                    smallImage.src = dataUrl;
                    removeImg();
                    vm.srcImg = dataUrl;
                });
            };

            function onFail(message) {
                
            };
        };

        function toDataURL(url, callback) {
            var xhr = new XMLHttpRequest();
            xhr.onload = function() {
                var reader = new FileReader();
                reader.onloadend = function() {
                    callback(reader.result);
                }
                reader.readAsDataURL(xhr.response);
            };
            xhr.open('GET', url);
            xhr.responseType = 'blob';
            xhr.send();
        };

        function removeImg(){
            $(".content-text").on("click", ".close-top", function(){
                $(this).closest("div").remove();
                vm.srcImg = "";
                vm.hasImg = false;
            });
        };
    };

})();