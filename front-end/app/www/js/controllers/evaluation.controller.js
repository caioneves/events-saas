(function () {
    'use strict';

    angular
        .module('eventsSaas.evaluation')
        .controller('EvaluationController', EvaluationController);

	EvaluationController.$inject = ['$scope', '$location', 'eventsSaasRoutes', 'cfpLoadingBar', '$timeout', '$rootScope']

	function EvaluationController($scope, $location, eventsSaasRoutes, cfpLoadingBar, $timeout, $rootScope) {

		cfpLoadingBar.start();
        $timeout(function() {
            cfpLoadingBar.complete();
        }, 500);
    }

})();