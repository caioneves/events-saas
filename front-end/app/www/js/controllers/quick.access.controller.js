(function () {
    'use strict';

    angular
        .module('eventsSaas.quick.access')
        .controller('QuickAccessController', QuickAccessController);

	QuickAccessController.$inject = ['$scope', 'eventsSaasQuickAccessService', '$location', 'eventsSaasRoutes', 'cfpLoadingBar', '$timeout', '$route']

	function QuickAccessController($scope, eventsSaasQuickAccessService, $location, eventsSaasRoutes, cfpLoadingBar, $timeout, $route) {

		cfpLoadingBar.start();
        $timeout(function() {
            cfpLoadingBar.complete();
        }, 500);

        var vm = this;
        vm.listQuickAcess = [];
        vm.submitSearch = submitSearch;
        vm.carregarQuickAccess = carregarQuickAccess;

        vm.carregarQuickAccess();
        
		function submitSearch(){
			if(isEmpyt(vm.search)){
				$location.path(eventsSaasRoutes.RESULTADOPESQUISA)
			}
		};

		function carregarQuickAccess() {
            return getQuickAccess().then(function() {
                console.log("All get lista Quick Acess");
            });
        };
        function getQuickAccess() {
            return eventsSaasQuickAccessService.getQuickAccess()
            .then(function(data) {
                vm.listQuickAcess = data;
                return vm.listQuickAcess;
            });
        };
	    function isEmpyt(value){
	    	return value.length
	    };
    };

})();