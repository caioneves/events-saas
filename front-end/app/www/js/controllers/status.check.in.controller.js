(function () {
    'use strict';

    angular
        .module('eventsSaas.status')
        .controller('CheckInController', CheckInController);

    CheckInController.$inject = ['$scope', '$location', 'eventsSaasRoutes', 'cfpLoadingBar', '$timeout']

    function CheckInController($scope, $location, eventsSaasRoutes, cfpLoadingBar, $timeout) {

        cfpLoadingBar.start();
        $timeout(function() {
            cfpLoadingBar.complete();
        }, 500);

        var vm = this;
        vm.total = 1.345;
        vm.credenciado = 1.027;
        var data = [
            {"quantidade":1.653, "name": "Palestra 1", "total": 1.654}, 
            {"quantidade":0.600, "name": "Palestra 2", "total": 1.297}, 
            {"quantidade":3.212, "name": "Palestra 3", "total": 3.462}, 
            {"quantidade":2.632, "name": "Palestra 4", "total": 3.017}, 
            {"quantidade":1.234, "name": "Palestra 5", "total": 1.489}, 
            {"quantidade":2.221, "name": "Palestra 6", "total": 2.893}, 
            {"quantidade":1.222, "name": "Palestra 7", "total": 1.987}, 
            {"quantidade":4.060, "name": "Palestra 8", "total": 4.094}
        ]
        var chart = d3.select('#chart')
        .append("ul").attr("class", "chart")
        .selectAll('.chart-item')
        .data(data).enter()
        .append("li").attr("class","chart-group clearfix")
        .append("ul").attr("class","col-sm-10 col-xs-10 chart-row")
        .append("li").attr("class","chart-item")
        .transition().ease("elastic");
        chart.style("width", function(d) { 
            return (d.quantidade * 100)/d.total + "%";
        });
        chart.text(function(d) { 
            return d.name;
        });

        $("<div class='col-sm-2 col-xs-2 chart-name'></div>").insertAfter(".chart-row");

        $.each(data, function(key, value){
            var element = $(".chart .chart-name")[key];
            $(element).text(data[key].total).append("<em class='ion-man'></em>")
        });
    };

})();