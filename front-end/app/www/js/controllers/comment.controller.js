(function () {
    'use strict';

    angular
        .module('eventsSaas.comment')
        .controller('CommentController', CommentController);

	CommentController.$inject = ['$scope', '$location', 'eventsSaasRoutes', 'cfpLoadingBar', '$timeout', '$rootScope']

	function CommentController($scope, $location, eventsSaasRoutes, cfpLoadingBar, $timeout, $rootScope) {

		cfpLoadingBar.start();
        $timeout(function() {
            cfpLoadingBar.complete();
        }, 500);

        $rootScope.headerHeight = $('.elHeight').outerHeight();

        var vm = this;
        vm.comentar = "The sight of the tumblers restored Bob Sawyer to a degree of equanimity which he had not possessed since his interview with his landlady. His face brightened up, and he began to feel quite convivial.";
        vm.back = back;

        function back(){
            $location.path(eventsSaasRoutes.TIMELINE);
        }
    }

})();