(function () {

    angular
        .module('eventsSaas.splash')
        .controller('SplashController', SplashController);

    SplashController.$inject = ['$scope', 'eventsSaasRoutes', '$location', '$timeout']

    function SplashController($scope, eventsSaasRoutes, $location, $timeout) {
        $timeout(
            function(){
                 $location.path(eventsSaasRoutes.ENTRAR);
        }, 4000);
    }

})();