(function () {
    'use strict';

    angular
        .module('eventsSaas.conquer')
        .controller('ConquerdetailController', ConquerdetailController);

	ConquerdetailController.$inject = ['$scope', '$location', 'eventsSaasRoutes', 'cfpLoadingBar', '$timeout', 'titulo', 'subTitulo', 'icon', 'iconBg', 'descricao']

	function ConquerdetailController($scope, $location, eventsSaasRoutes, cfpLoadingBar, $timeout, titulo, subTitulo, icon, iconBg, descricao) {

		cfpLoadingBar.start();
        $timeout(function() {
            cfpLoadingBar.complete();
        }, 500);
        var vm = this;
        vm.titulo = titulo;
        vm.subTitulo = subTitulo;
        vm.icon = icon;
        vm.iconBg = iconBg;
        vm.descricao = descricao;
        vm.isMustache = false;
    };

})();