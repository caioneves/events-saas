(function () {
    'use strict';

    angular
        .module('eventsSaas.profile.lecturer')
        .controller('ProfileLecturerChatController', ProfileLecturerChatController);

	ProfileLecturerChatController.$inject = ['$scope', '$location', 'eventsSaasRoutes', 'cfpLoadingBar', '$timeout', '$rootScope']

	function ProfileLecturerChatController($scope, $location, eventsSaasRoutes, cfpLoadingBar, $timeout, $rootScope) {

		cfpLoadingBar.start();
        $timeout(function() {
            cfpLoadingBar.complete();
        }, 500);

        var vm = this;
        
        $rootScope.headerHeight = $('.elHeight').outerHeight();
    }

})();