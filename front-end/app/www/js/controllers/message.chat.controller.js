(function () {
    'use strict';

    angular
        .module('eventsSaas.message')
        .controller('ChatController', ChatController);

    ChatController.$inject = ['$scope', 'eventsSaasRoutes', 'localStorageService', 'cfpLoadingBar', '$routeParams', '$timeout', 'eventsSaasMessageService', '$location', '$rootScope', 'HostValue', 'DialogService']

    function ChatController($scope, eventsSaasRoutes, localStorageService, cfpLoadingBar, $routeParams, $timeout, eventsSaasMessageService, $location, $rootScope, HostValue, DialogService) {

        var vm = this;
        vm.rows = [];
        vm.title = "";
        vm.targetId = $routeParams.userId;
        vm.network = [];
        vm.viewMessages = viewMessages;
        vm.viewProfileChat = viewProfileChat;
        vm.addMsg = addMsg;
        vm.addImage = addImage;
        vm.viewFullScreenImage = viewFullScreenImage;
        vm.avatar = HostValue.ghostsPerson;
        vm.idEvent = HostValue.idEvent;
        vm.idLogado = localStorageService.get('user').id;
        vm.deleteMsg = deleteMsg;
        vm.viewPerfil = viewPerfil;
        
        $rootScope.headerHeight = $('.elHeight').outerHeight();

        vm.viewMessages();
        vm.viewProfileChat();

        function viewProfileChat(){
            return eventsSaasMessageService.getChatProfile(vm.idEvent, vm.targetId)
            .then(function(data){
                vm.title = data.name;
            })
            .catch(function(error){
                console.log(error);
            });
        };

        function viewMessages() {
            return getMessages().then(function(data) {
                getMensagemProfile(data);
            });
        };
        function getMessages() {
            return eventsSaasMessageService.getProfileMessages(vm.idEvent, vm.targetId)
            .then(function(data) {
                return data;
            });
        };
        function getMensagemProfile(data){
            vm.mensagens = [];
            angular.forEach(data, function(value, key) {
                var min, objMessage, creationDate, texto, targetId;
                vm.avatar = data[key].author.pictureUrl ? HostValue.aws + data[key].author.pictureUrl : HostValue.ghostsPerson;
                vm.image = data[key].imageUrl ? HostValue.aws + data[key].imageUrl : data[key].imageUrl;
                vm.nome = data[key].author.name;
                creationDate= data[key].creationDate;
                texto = data[key].text;
                targetId = data[key].author.id;

                objMessage = {"id": data[key].id, "avatar": vm.avatar, "image":vm.image, "texto": texto, "date":creationDate, "author": targetId, "read":data[key].read };
                vm.mensagens.unshift(objMessage);
            });
            $timeout(function() {
                $(".chat-overflow").scrollTop($('.chat-overflow')[0].scrollHeight)
            }, 700);
        };

        function deleteMsg(id, ele, index){
            DialogService.confirm("Apagar mensagem?", deleteCallback, "", ["Não", "Sim"])

            function deleteCallback(button){
                $(ele).addClass($(ele).attr("press-selected"));
                if(button == 2){
                    vm.mensagens.splice(index, 1)
                    return eventsSaasMessageService.deleteMessageChat(vm.idEvent, id)
                    .then(function(data){
                        console.log(data)
                    })
                    .catch(function(error){
                        console.log(error);
                    });
                }
            };
        };

        function addMsg(texto){
            if(texto !=""){
                var message = new Object();
                message.text = texto;
                vm.message = "";
                applyMessage(message);
            };
        };

        function applyMessage(message){
            return eventsSaasMessageService.postProfileMessage(vm.idEvent, vm.targetId, message)
            .then(function(data) {
                vm.viewMessages();
                return data;
            })
            .catch(function(error){
                return console.log(error)
            });
        };

        function setOptions(srcType) {
            var options = {
                // Some common settings are 20, 50, and 100
                quality: 100,
                destinationType: Camera.DestinationType.FILE_URI,
                // In this app, dynamically set the picture source, Camera or photo gallery
                sourceType: srcType,
                encodingType: Camera.EncodingType.JPEG,
                mediaType: Camera.MediaType.PICTURE,
                allowEdit: true,
                correctOrientation: true  //Corrects Android orientation quirks
            }
            return options;
        }

        function addImage(){
            var srcType = Camera.PictureSourceType.PHOTOLIBRARY;
            var options = setOptions(srcType);
            
            navigator.camera.getPicture(onSuccess, onFail, options);

            function onSuccess(imageURI) {
                toDataURL(imageURI, function(dataUrl, type) {
                    applyImage(type, dataUrl);
                });
            };

            function onFail(message) {
                
            };
        };

        function toDataURL(url, callback) {
            var xhr = new XMLHttpRequest();
            xhr.onload = function() {
                var reader = new FileReader();
                reader.onloadend = function() {
                    callback(reader.result, xhr.response.type);
                }
                reader.readAsDataURL(xhr.response);
            };
            xhr.open('GET', url);
            xhr.responseType = 'blob';
            xhr.send();
        };

        function applyImage(type, result){
            return sendImage(type, result)
            .then(function(valor){
                var message = new Object();
                message.image = valor;
                applyMessage(message);
                return valor;
            })
        };
        function sendImage(type, result){
            return eventsSaasMessageService.postProfileMessageImage(type, result)
            .then(function(data){
                return data;
            })
            .catch(function(error){
                console.log("Error no data" +error)
            })
        };

        function viewFullScreenImage(src){
            PhotoViewer.show(src, " ");
        };

        function viewPerfil(id){
            $location.path("/participant/"+id);
        };
    };

})();