(function () {
    'use strict';

    angular
        .module('eventsSaas.agenda')
        .controller('AgendaController', AgendaController);

    AgendaController.$inject = ['$scope', 'HostValue', '$routeParams', 'eventsSaasRoutes', 'cfpLoadingBar', '$compile', 'uiCalendarConfig', '$timeout', 'eventsSaasEventsService', '$location', 'eventsSaasProfileService']

    function AgendaController($scope, HostValue, $routeParams, eventsSaasRoutes, cfpLoadingBar, $compile, uiCalendarConfig, $timeout, eventsSaasEventsService, $location, eventsSaasProfileService) {

        var vm = this;
        vm.lecturePerDay = new Map();
        vm.keys = [];
        vm.selected = null;
        vm.detailId= null;


        vm.hasCalendar = true;
        vm.alarm = [];
        vm.eventSources = [];
        vm.eventRow = [];

        vm.fillEvents = fillEvents;
        vm.addUiCalendar = addUiCalendar;
        vm.dayEvents = dayEvents; 
        vm.idEvent = HostValue.idEvent;
        vm.onDetail = onDetail;
        
        vm.uiConfig = {
            calendar:{
                defaultDate: new Date(),
                height: 450,
                editable: true,
                header:{
                    left: 'prev',
                    center: 'title',
                    right: 'next'
                },
                lang: "pt-br",
                selectable: true,
                selectHelper: true,
                //select: $scope.selectEvent,
                eventRender: eventRender,
                eventClick: dayEvents
            }
        };

        serviceMyActivities();

        /* Render Tooltip */
        function eventRender(event, element, view){ 
            vm.dayEvents();
            var dia = new Date(event.startTime)
            $(element).attr({/*'uib-tooltip': event.title,
                         'tooltip-append-to-body': true, */'day': dia.getDate()});
            $compile(element)($scope);
            $(element).closest("tr")
        };

        function serviceMyActivities(){
            cfpLoadingBar.start();
			vm.detailId = $routeParams.detailId;

            return getServiceMyActivities(vm.idEvent).then(function(data){
                if (data && data.length > 0 ){
	        	    data.forEach(function(item){
                        if(!item.alarm){
                            vm.alarm.push(item);
                        };
                    });
                };

                vm.fillEvents(vm.alarm);
                vm.addUiCalendar();
            })
            .finally(function(){
	    		 cfpLoadingBar.complete();
            });
        };

        function getServiceMyActivities(id){
            return eventsSaasProfileService.getMyActivities(id)
            .then(function(data){
                return data;
            })
            .catch(function(error){
                return console.log(error)
            });
        };

        function fillEvents(data){

            data.forEach(function(item, index) {
                item.lectureDetail.startTime = new Date(item.lectureDetail.startTime);
                item.lectureDetail.endTime = new Date(item.lectureDetail.endTime);

                var horaStart = item.lectureDetail.startTime.getHours()+ ":" + item.lectureDetail.startTime.getMinutes() +":00";
                var calc = getDayKey(item.lectureDetail.startTime);
                var calc2 = getDayKey(item.lectureDetail.endTime);
                item.lectureDetail.start = calc;
                item.lectureDetail.end = calc2;
                item.lectureDetail.schedule = getSchedule(item.lectureDetail);
                item.lectureDetail.className = "not-title-agenda";
                put(vm.lecturePerDay, getDayKey(item.lectureDetail.startTime), item.lectureDetail);
            });
            vm.keys = Array.from(vm.lecturePerDay.keys()).sort();
            apllyRow(vm.keys[0], vm.lecturePerDay);
        };

        function apllyRow(select, data){
            var flag = true;
            for(var [key, value] of data) {
                $.each(value, function(index, item){
                    vm.eventRow.push(item);
                    if(select == key && flag){ // exibir o mes do evento salvo
                        vm.uiConfig.calendar.defaultDate = new Date(item.startTime);
                        flag = false;
                    }
                });
            };
        }

        /*adicionando a diretiva ui-calendar*/
        function addUiCalendar(){
            var $element = angular.element(document.getElementById('calendar'))
            $element.attr({"ui-calendar":"vm.uiConfig.calendar"});
            $compile($element)($scope);
            console.log(vm.eventRow)
            vm.eventSources.push(vm.eventRow)
        };

        function dayEvents(event){
            var dateClick = event ? new Date(event.startTime) : new Date();
            var search = getDayKey(dateClick);
            applyEventDay(search, vm.lecturePerDay);
        };

        function applyEventDay(compare, data) {
            for(var [key, value] of data) {
                if(key == compare){
        	        vm.selected = compare;
                };
            };
        };
        
        function put(map, key, value){
        	if (!map.has(key)){
        		map.set(key, []);
        	}
        	map.get(key).push(value);
        }
        
        function getDayKey(dateObj){
        	var m = dateObj.getUTCMonth() + 1; // months from 1-12
            var d = dateObj.getUTCDate();
            var y = dateObj.getUTCFullYear();
            d =  d < 10 ? '0' + d : '' + d;
            m =  m < 10 ? '0' + m : '' + m;
            return y + '-' + m + '-' + d;
        };
        
        function onClick(id){
        	$location.path("/agenda-event/"+id);
        };
        
        function onDetail(id){
        	$location.path("/agenda/"+id);
        };
        
        function getSchedule(item){
        	var time = formatTime(item.startTime.getHours(), "h")+ ":" + formatTime(item.startTime.getMinutes(), "m");
        	time += " - " + formatTime(item.endTime.getHours(), "h")+ ":" + formatTime(item.endTime.getMinutes(), "m");
        	return time;
        };

        function formatTime(m, type){
            var time = m.toString();
            if(type == "h") {
                if(time.length == 1){
                    time = "0"+time
                }
            } else {
                if(time.length == 1){
                    time = time+"0";
                }
            }
            return time;
        };
    };

})();