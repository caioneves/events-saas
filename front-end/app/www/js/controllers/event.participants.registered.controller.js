(function () {
    'use strict';

    angular
        .module('eventsSaas.events')
        .controller('EventParticipantRegisteredController', EventParticipantRegisteredController);

	EventParticipantRegisteredController.$inject = ['$scope', 'eventsSaasEventsService', 'HostValue', '$location', 'eventsSaasRoutes', 'cfpLoadingBar', '$timeout', 'localStorageService']

	function EventParticipantRegisteredController($scope, eventsSaasEventsService, HostValue, $location, eventsSaasRoutes, cfpLoadingBar, $timeout, localStorageService) {

		cfpLoadingBar.start();
        $timeout(function() {
            cfpLoadingBar.complete();
        }, 500);

        var vm = this;
        vm.isActive = false;
        vm.lisMenu = ['inscritos', 'presentes'];
        vm.rows = [];
        vm.selected = 0;
        vm.selectApply = selectApply;
        vm.viewParticipants = viewParticipants;
        vm.filter = "";
        vm.filterSearch = "";
        vm.search = search;
        vm.baseUrl = HostValue.aws;
        vm.avatar = HostValue.ghostsPerson;
        vm.idLogado = localStorageService.get('user').id;

        vm.viewParticipants();

        function viewParticipants() {
            return getParticipants().then(function(data) {
                if(data && data.length){
                    vm.rows = data;
                    vm.filter = data.length +" inscrito(s)";
                } else {
                    vm.filter = "Nenhum participante(s) inscrito(s)";
                }
            })
            .catch(function(erro){
                vm.filter = "Nenhum participante(s) inscrito(s)";
            });
        };

        function getParticipants() {
            vm.filter = null;
            return eventsSaasEventsService.getEventListProfiles(HostValue.idEvent)
            .then(function(data) {
                return data;
            })
            .catch(function(erro){
                return erro;
            });
        };

        function viewCheckedinParticipants() {
            return getCheckedinParticipants().then(function(data) {
                if(data && data.length){
                    vm.rows = data;
                    vm.filter = data.length +" presente(s)";
                } else {
                    vm.filter = "Nenhum participante(s) presente(s)";
                }
            })
            .catch(function(erro){
                vm.filter = "Nenhum participante(s) presente(s)";
                console.log(erro)
            });
        };

        function getCheckedinParticipants() {
            vm.filter = null;
            return eventsSaasEventsService.getEventListCheckedinProfiles(HostValue.idEvent)
            .then(function(data) {
                return data;
            })
            .catch(function(erro){
                return erro;
            });
        };

		function selectApply(index, item) {
			vm.selected = index; 
			vm.rows = [];
            vm.qtd = 0;
            vm.filterSearch = "";

            if(item == "inscritos"){
                viewParticipants();
            } else if (item == "presentes") {
                viewCheckedinParticipants();
            }
		};
        
        function search(item){
            if(vm.filterSearch == undefined){
                return true
            } else {
                if(item.name.toLowerCase().indexOf(vm.filterSearch.toLowerCase()) != -1 || 
                item.jobRole.toLowerCase().indexOf(vm.filterSearch.toLowerCase()) != -1 || 
                item.company.toLowerCase().indexOf(vm.filterSearch.toLowerCase()) != -1){
                    return true;
                };
            };
            return false;
        };
    };

})();