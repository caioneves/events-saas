(function() {
    'use strict';

    angular
        .module('eventsSaas.profile')
        .controller('ChangePassword', ChangePassword);

    ChangePassword.$inject = ['$scope', '$rootScope', '$location','eventsSaasRoutes', 'eventsSaasProfileService', 'HostValue', 'cfpLoadingBar', 'DialogService'];
    function ChangePassword($scope, $rootScope, $location, eventsSaasRoutes, eventsSaasProfileService, HostValue, cfpLoadingBar, DialogService) {
        var vm = this;
        vm.fieldsMSG = false;
        vm.submitedButton = false;
        vm.submitChangePassword = submitChangePassword;
        vm.idEvent = HostValue.idEvent;
        
        ////////////////
        function submitChangePassword(form, login) {
             if(form.$valid){
                vm.submitedButton = true;
        	    cfpLoadingBar.start();
                return eventsSaasProfileService.postChangePassword(login.passwordNew, login.passwordOld, vm.idEvent)
                .then(function(response){
                    DialogService.alert("Senha alterada com sucesso!","Sucesso" , callbackSucess, ["Fechar"]);
                    function callbackSucess(){
                        if($rootScope.forgot){
                            window.location = "#"+eventsSaasRoutes.HOME;
                            $rootScope.forgot = false;
                            //$location.path(eventsSaasRoutes.HOME);
                            //return $q.resolve(response);
                        } else {
                            window.location = "#"+eventsSaasRoutes.PERFILUSUARIO
                           // $location.path(eventsSaasRoutes.PERFILUSUARIO);
                        }
                    };
                })
                .catch(function(error){
                    DialogService.alert(specialCaracter(error.message),"Atenção" , callback, ["Fechar"]);
                    function callback(){
	    		        cfpLoadingBar.complete();
                        vm.submitedButton = false;
                        return $q.resolve(response);
                    };
                });
             } else {
                 vm.fieldsMSG = true; 
             }
		};

        function specialCaracter(text){
            var val= "", i, hash = true, str = text.split("");
            for(i=0; i < str.length; i++){
                if(str[i].indexOf('�') != -1){
                    if(hash){
                        str.splice(i,1, "ã")
                        hash = false;
                    } else {
                        str.splice(i,1, "")
                    }
                }
                val += str[i];
            };
            return val;
        };
    };

})();