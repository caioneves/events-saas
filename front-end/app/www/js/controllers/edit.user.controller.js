(function () {
    'use strict';

    angular
        .module('eventsSaas.profile')
        .controller('EditUserController', EditUserController);

    EditUserController.$inject = ['$scope', '$location', 'eventsSaasRoutes', 'eventsSaasProfileService', 'cfpLoadingBar', '$timeout', 'HostValue', 'localStorageService', '$cordovaOauth', '$http']

    function EditUserController($scope, $location, eventsSaasRoutes, eventsSaasProfileService, cfpLoadingBar, $timeout, HostValue, localStorageService, $cordovaOauth, $http) {

        cfpLoadingBar.start();
        $timeout(function() {
            cfpLoadingBar.complete();
        }, 500);

        var vm = this;
        vm.getPicture = getPicture;
        vm.viewProfileEdit = viewProfileEdit;
        vm.submitForm = submitForm;
        vm.baseUrl = HostValue.aws;
        vm.avatar = HostValue.ghostsPerson;
        vm.configLinkedIn = configLinkedIn;
        vm.configFacebook = configFacebook;
        vm.cleanUrlSocial = cleanUrlSocial;
        vm.checkedItem = checkedItem;
        vm.pictureUrl = null;
        vm.setImg = null;
        vm.setSocialConfFB = false;
        vm.setSocialConfLK = false;
        vm.socialData = false;
        vm.update = false;
        vm.noEmoji = noEmoji;

        vm.profile = {
            firstName: "",
            lastName: "",
            gender: "",
            birthDate: "",
            documentNumber: "",
            documentType: "",
            jobRole: "",
            showJobRole: false,
            company: "",
            facebookUrl: "",
            twitterUrl: "",
            linkedInUrl: "",
            instagramUrl: "",
            imageId: "",
            minibio: "",
            hasFacebookSocialProfileEnable: false,
            hasLinkedSocialProfileEnable: false
        };

        if(localStorageService.get("profile")){
            setProfile(localStorageService.get("profile"));
        } else {
            vm.viewProfileEdit();
        }

        function submitForm(profile){
            if(vm.setImg){
                var image = document.getElementById('img-perfil');
                return updateImage(image.src)
                .then(function(id){
                    profile.imageId = id;
                    updateProfile(profile);
                })
                .catch(function(error){
                    console.log(error);
                });
            } else {
                updateProfile(profile);
            }

            if(vm.socialData){
                postSocialProfile(vm.socialData);
            };
        };

        function viewProfileEdit(){
            return getProfileEdit(HostValue.idEvent).then(function(data) {
                setProfile(data);
            });
        };

        function getProfileEdit(id){
            return eventsSaasProfileService.getProfile(id)
            .then(function(data){
                return data;
            });
        };

        function setProfile(data){
            var imgId = null;
            if(data.pictureUrl){
                imgId = data.pictureUrl.split("/");
                imgId = imgId[imgId.length-1];
            }
            vm.pictureUrl = data.pictureUrl;            

            vm.profile.firstName = parseUTF16(data.firstName);
            vm.profile.lastName = parseUTF16(data.lastName);
            vm.profile.gender = data.gender;
            vm.profile.birthDate = data.birthDate;
            vm.profile.documentNumber = data.documentNumber;
            vm.profile.documentType = data.documentType;
            vm.profile.jobRole = parseUTF16(data.jobRole);
            vm.profile.company = parseUTF16(data.company);
            vm.profile.facebookUrl = data.facebookUrl;
            vm.profile.twitterUrl = data.twitterUrl;
            vm.profile.linkedInUrl = data.linkedInUrl;
            vm.profile.instagramUrl = data.instagramUrl;
            vm.profile.imageId = imgId;
            vm.profile.minibio = parseUTF16(data.minibio);
            vm.profile.showJobRole = data.showJobRole;
            vm.profile.hasFacebookSocialProfileEnable = data.hasFacebookSocialProfileEnable;
            vm.profile.hasLinkedSocialProfileEnable = data.hasLinkedSocialProfileEnable;
            vm.setSocialConfFB = data.hasFacebookSocialProfileEnable;
            vm.setSocialConfLK = data.hasLinkedSocialProfileEnable;
        };

        function setOptions(srcType) {
            var options = {
                // Some common settings are 20, 50, and 100
                quality: 100,
                destinationType: Camera.DestinationType.FILE_URI,
                // In this app, dynamically set the picture source, Camera or photo gallery
                sourceType: srcType,
                encodingType: Camera.EncodingType.JPEG,
                mediaType: Camera.MediaType.PICTURE,
                allowEdit: true,
                correctOrientation: true  //Corrects Android orientation quirks
            }
            return options;
        }

        function getPicture(){
            var srcType = Camera.PictureSourceType.CAMERA;
            var options = setOptions(srcType);

            //if (selection == "camera-thmb") {
                options.targetHeight = 130;
                options.targetWidth = 130;
            //}
           navigator.camera.getPicture(onSuccess, onFail, options);

            function onSuccess(imageURI) {
                var smallImage = document.getElementById('img-perfil');
                toDataURL(imageURI, function(dataUrl) {
                    smallImage.src = dataUrl;
                    vm.setImg = true;
                });
            };

            function onFail(message) {
                
            };
        };

        function toDataURL(url, callback) {
            var xhr = new XMLHttpRequest();
            xhr.onload = function() {
                var reader = new FileReader();
                reader.onloadend = function() {
                    callback(reader.result);
                }
                reader.readAsDataURL(xhr.response);
            };
            xhr.open('GET', url);
            xhr.responseType = 'blob';
            xhr.send();
        }

        function updateEmail(email, password){
            return eventsSaasProfileService.postChangeEmail(email, password)
            .then(function(data){
                return data;
            })
            .catch(function(error){
                return console.log(error);
            })
        };

        function updateImage(image){
            return eventsSaasProfileService.postProfileImage(image)
            .then(function(data){
                return data;
            })
            .catch(function(error){
                return error
            })
        };

        function updateProfile(profile){
            return eventsSaasProfileService.postProfile(profile)
            .then(function(data){
                $location.path(eventsSaasRoutes.PERFILUSUARIO);
				localStorageService.set('profile', null);
            })
            .catch(function(error){
                console.log(error)
            });
        };

        function configLinkedIn(){
            var lkLoginSuccess = function(userData) {
                vm.socialData = {};
                vm.socialData.providerId = "linkedin";
                vm.socialData.providerUserId = userData.data.id;
                vm.socialData.displayName = userData.data.firstName+" "+userData.data.lastName;
                vm.socialData.profileUrl = userData.data.siteStandardProfileRequest.url;
                vm.socialData.imageUrl = "";
                vm.socialData.enabled = "true";
            };
            var lkLoginFailed = function(userData) { 
                console.log(userData);
            };

            var scopes = ['r_basicprofile', 'r_emailaddress', 'rw_company_admin', 'w_share'];

            $cordovaOauth.linkedin('7835lqfe5t7mxr', '24wER6jBO8JO6VET', scopes, 'inLogin')
            .then(function(result) {
                $http.get("https://api.linkedin.com/v1/people/~?format=json&oauth2_access_token=" +  result.access_token)
                .then(lkLoginSuccess)
                .catch(lkLoginFailed)
            })
            .catch(lkLoginFailed)
        };

        function configFacebook(){
            var fbLoginSuccess = function (userData) {
                vm.socialData = {};
                vm.socialData.providerId = "facebook";
                vm.socialData.providerUserId = userData.data.id;
                vm.socialData.displayName = userData.data.name;
                vm.socialData.profileUrl = "https://www.facebook.com/"+userData.data.id;
                vm.socialData.imageUrl = userData.data.picture.data.url;
                vm.socialData.enabled = "true";
            };

            var fbLoginFailed = function(error){
                console.log(error);
            };

            $cordovaOauth.facebook('1301857159850937', ["email", "user_website", "user_location", "user_relationships"])
            .then(function(result) {
                $http.get("https://graph.facebook.com/v2.2/me", { params: { access_token: result.access_token, fields: "id,name,gender,location,website,picture,relationship_status", format: "json" }})
                .then(fbLoginSuccess)
                .catch(fbLoginFailed)
            })
            .catch(fbLoginFailed)
        };

        function postSocialProfile(data){
            var social = data.providerId;
            return eventsSaasProfileService.postSocialProfile(data)
            .then(function(data){
                social == "facebook" ? vm.setSocialConfFB = true : vm.setSocialConfLK = true;
                return data
            })
            .catch(function(error){
                return error
            });
        };

        function cleanUrlSocial(val, model){
            var array = val.split("/");
            var setVal= array[array.length -1];
            vm.profile[model] = parseUTF16(setVal);
        };

        function checkedItem(obj){
            obj = !obj;
        };

        function noEmoji(field, model){
            var valor = parseUTF16(field.$viewValue);
            vm.profile[model] = valor;
        };
        
        function parseUTF16(text) {
            if(!text) return ;
            var ranges = [
                '[\ud000-\udfff]',
                '[\u3000-\u3fff]',
                '[\u2000-\u2fff]',
                '[\uFE00-\uFEff]'
            ];

            return text.replace(
                new RegExp(ranges.join('|'), 'g'),
                function(m)
                {
                    function hexEncode (text){
                        var hex, i;

                        var result = "";

                        for (i=0; i<text.length; i++) {
                            hex = text.charCodeAt(i).toString(16);
                            result += ("000"+hex).slice(-4);
                        }
                        return result
                    }
                    return '';
                });
        };
    };

})();