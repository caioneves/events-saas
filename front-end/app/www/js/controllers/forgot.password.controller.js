(function () {
    'use strict';

    angular
        .module('eventsSaas.login')
        .controller('ForgotPassword', ForgotPassword);

    ForgotPassword.$inject = ['$scope', '$location', 'eventsSaasRoutes', 'titulo', 'eventsSaasLoginService', 'DialogService']

    function ForgotPassword($scope, $location, eventsSaasRoutes, titulo, eventsSaasLoginService, DialogService) {
        var vm = this;
        vm.title = titulo;
        vm.submit = submit;
        vm.fieldsMSG = false;

		function submit(form){
            // verifica se o formulário é válido
            if(form.$valid){
                eventsSaasLoginService.forgotPassword(form.email.$viewValue)
                .then(function(data){
                    DialogService.alert("Por favor verifique seu email.","Sucesso" , null, ["Fechar"]);
                })
                .catch(function(erro){
                    DialogService.alert("Operaçao não realizada, tente novamente mais tarde.","Atenção" , callback, ["Fechar"]);                    
                });
            } else {
                vm.fieldsMSG = true; 
            };
        };
        
        function callback(){
            $scope.$dismiss('cancel'); 
        };
    };

})();