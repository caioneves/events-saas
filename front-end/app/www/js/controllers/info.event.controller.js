(function() {
    'use strict';

    angular
        .module('eventsSaas.info.event')
        .controller('InfoEventController', InfoEventController);

    InfoEventController.$inject = ['$scope', 'eventsSaasRoutes', 'cfpLoadingBar', '$timeout', 'eventsSaasInfoEventService', '$location', '$sce', 'HostValue'];

    /* @ngInject */
    function InfoEventController($scope, eventsSaasRoutes, cfpLoadingBar, $timeout, eventsSaasInfoEventService, $location, $sce, HostValue) {
        var vm = this;
        vm.title = 'Evento';

        vm.openMaps = openMaps;

        getInfoEvent();

        ////////////////

        function getInfoEvent() {
            return getEvent(HostValue.idEvent).then(function(data){
                vm.infoEvent = [];
                activate(data)
            })
            .catch(function(erro){
                console.log(erro);
            });
        }

        function getEvent(idEvent){
            return eventsSaasInfoEventService.getInfoEvent(idEvent)
            .then(function(data){
                return data;
            })
            .catch(function(erro){
                console.log(erro);
            });
        };

        function activate(data){
            vm.infoEvent = data;
            vm.infoEvent.baseURL = HostValue.aws;
            vm.infoEvent.mapsKEY = HostValue.googleMapsAPY_KEY;
            vm.infoEvent.endEvent = formatDate(data.endEvent);
            vm.infoEvent.startEvent = formatDate(data.startEvent);
        };

        function openMaps(googleMapsLat, googleMapsLong){
            launchnavigator.navigate([googleMapsLat, googleMapsLong], {
                app: launchnavigator.APP.USER_SELECT
            });
        };

        function formatDate(date){
            var data = new Date(parseInt(date));
            return data.getDate() +" de "+ mesPorExtenso(data.getMonth()) +", "+ data.getFullYear() +" &#8226; "+ formatTime(data.getHours(), "h")+":"+formatTime(data.getMinutes(),"m");
        };

        function mesPorExtenso(mes){
            var meses   = new Array("Janeiro","Fevereiro","Março","Abril","Maio","Junho","Julho","Agosto","Setembro","Outubro","Novembro","Dezembro");
            return meses[mes];
        };

        function formatTime(m, type){
            var time = m.toString();
            if(type == "h") {
                if(time.length == 1){
                    time = "0"+time
                }
            } else {
                if(time.length == 1){
                    time = time+"0";
                }
            }
            return time;
        };
    };
})();