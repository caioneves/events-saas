(function() {
    'use strict';

    angular
        .module('eventsSaas.home')
        .controller('HomeController', HomeController);

    HomeController.$inject = ['$scope', '$location', 'eventsSaasRoutes', 'cfpLoadingBar', '$timeout', 'eventsSaasQuickAccessService', 'eventsSaasTimelineService', 'eventsSaasAtividadesService', '$route', 'HostValue', 'localStorageService', 'eventsSaasEventsService'];

    /* @ngInject */
    function HomeController($scope, $location, eventsSaasRoutes, cfpLoadingBar, $timeout, eventsSaasQuickAccessService, eventsSaasTimelineService, eventsSaasAtividadesService, $route, HostValue, localStorageService, eventsSaasEventsService) {
        var vm = this;
        vm.submitSearch = submitSearch;
        vm.listQuickAcess = null;
        vm.timeLine = null;
        vm.loadingBar = loadingBar;
        vm.carregarQuickAccess = carregarQuickAccess;
        vm.carregarTimeLine = carregarTimeLine;
        vm.carregarAtividades = carregarAtividades;
        vm.activeQuick = activeQuick;
        vm.isEmpyt = isEmpyt;
        vm.atividades = null;
        vm.root = HostValue.aws;
        vm.avatar = HostValue.ghostsPerson;
        vm.openParticipant = openParticipant;
        vm.openDetailLecture = openDetailLecture;
        vm.noEmoji = noEmoji;
        vm.idLogado = localStorageService.get('user').id;

        vm.pageTimeLine=0;

        vm.carregarQuickAccess();
        vm.loadingBar();
        vm.carregarTimeLine();
        vm.carregarAtividades();
        vm.like = like;

        function activeQuick(list){
            list.active = true;
        };

        function like(article) {
            if (article.liked) {
                return;
            }
            return eventsSaasTimelineService.setLike(HostValue.idEvent, article.id)
                .then(function(data) {
                    article.numberLike+=1;
                    article.liked=true;
                    loadingBar();
                });
        };

        function loadingBar() {
            cfpLoadingBar.start();
            $timeout(function() {
                cfpLoadingBar.complete();
            }, 500);
        };

        function submitSearch(value){
            if(vm.isEmpyt(value)){
                $location.path(eventsSaasRoutes.RESULTADOPESQUISA)
            }
        };
        function carregarQuickAccess() {
            return getQuickAccess().then(function(data) {
                if(data.length){
                    vm.listQuickAcess = data;
                    applySlickQuickAccess();
                };
            });
        };
        function getQuickAccess() {
            return eventsSaasQuickAccessService.getQuickAccess()
            .then(function(data) {
                vm.listQuickAcess = [];
                return data;
            });
        };

        function applySlickQuickAccess(){
            vm.slickConfig = {
                  method: {},
                  dots: false,
                  infinite: true,
                  speed: 300,
                  slidesToShow: 3,
                  slidesToScroll: 2,
                  variableWidth: true,
                  arrows: false
            }
        };

        function carregarTimeLine() {
            return getTimeline().then(function(data) {
                if(data.length){
                    vm.timeLine = [];
                    applySlickTimeLine();
                    preencherTimeLine(data);
                };
            });
        };
        function getTimeline() {
            return eventsSaasTimelineService.getTimeline(HostValue.idEvent, vm.pageTimeLine, HostValue.pageSize)
            .then(function(data){
                return data;
            })
            .catch(function(error){
                console.log(error);
            });
        };
        function preencherTimeLine(data){
            angular.forEach(data, function(value, key) {
                if(key < 5){
                    vm.timeLine.push(data[key]); 
                }
            });
            vm.pageTimeLine+=1;
        };
        function applySlickTimeLine(){
            vm.slickConfigTimeLine = {
                  method: {},
                  dots: true,
                  infinite: false,
                  speed: 300,
                  slidesToShow: 1,
                  slidesToScroll: 1,
                  variableWidth: false,
                  arrows: false
            }
        };

        function carregarAtividades(){
            return getAtividades().then(function(data) {
                if(data.length){
                    vm.atividades = [];
                    preencherAtividades(data);
                    applySlickAtividade()
                };
            });
        };

        function getAtividades(){
            return eventsSaasEventsService.getNextActivities(HostValue.idEvent, 0, HostValue.pageSize)
            .then(function(data){
                return data;
            })
            .catch(function(error){
                console.log(error);
            });
        };
        function preencherAtividades(ativi){
            angular.forEach(ativi, function(value, key) {
                if(key < HostValue.pageSize){
                    ativi[key].startTime = new Date(ativi[key].startTime);
                    ativi[key].endTime = new Date(ativi[key].endTime);
                    ativi[key].schedule = getSchedule(ativi[key]);
                    if(ativi[key].type == "LECTURE"){
                        ativi[key].speakers = ativi[key].speakers.length >=1 ? ativi[key].speakers[0] : 'Palestra'; 
                    }
                    vm.atividades.push(ativi[key]);
                };
            });
        };

        function applySlickAtividade(){
            vm.slickConfigAtividade = {
                  method: {},
                  dots: false,
                  infinite: true,
                  speed: 300,
                  slidesToShow: 3,
                  slidesToScroll: 2,
                  variableWidth: true,
                  arrows: false
            }
        };

        function isEmpyt(value){
            return value.length
        };

        function openParticipant(id){
            if(vm.idLogado != id){
                $location.path("/participant/"+id);
            } else {
                $location.path("/profile-user")
            }
        };

        function openDetailLecture(id, page){
            if(page){
                $location.path("/agenda/"+id);
            };
        };

        function noEmoji(field, model){
            var valor = parseUTF16(field.$viewValue);
            vm.buscar[model] = valor;
        };
        
        function parseUTF16(text) {
            var ranges = [
                '[\ud000-\udfff]',
                '[\u3000-\u3fff]',
                '[\u2000-\u2fff]',
                '[\uFE00-\uFEff]'
            ];

            return text.replace(
                new RegExp(ranges.join('|'), 'g'),
                function(m)
                {
                    function hexEncode (text){
                        var hex, i;

                        var result = "";

                        for (i=0; i<text.length; i++) {
                            hex = text.charCodeAt(i).toString(16);
                            result += ("000"+hex).slice(-4);
                        }
                        return result
                    }
                    return '';
                });
        };
        
        function getSchedule(item){
        	var time = formatTime(item.startTime.getHours(), "h")+ ":" + formatTime(item.startTime.getMinutes(), "m");
        	time += " - " + formatTime(item.endTime.getHours(), "h")+ ":" + formatTime(item.endTime.getMinutes(), "m");
        	return time;
        };

        function formatTime(m, type){
            var time = m.toString();
            if(type == "h") {
                if(time.length == 1){
                    time = "0"+time
                }
            } else {
                if(time.length == 1){
                    time = time+"0";
                }
            }
            return time;
        };
    };

})();