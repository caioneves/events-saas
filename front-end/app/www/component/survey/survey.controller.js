(function () {
    'use strict';

    angular
        .module('eventsSaas.survey')
        .controller('SurveyController', SurveyController);

    SurveyController.$inject = ['$scope', 'cfpLoadingBar', '$timeout', 'eventsSaasSurveyService']

    function SurveyController($scope, cfpLoadingBar, $timeout, eventsSaasSurveyService) {

        var vm = this;

        vm.questions = null;
        vm.currentQuestion = null;
        vm.currentQuestionIndex = null;
        vm.currentAnswerId = null;
        vm.currentAnswerText = null;
        activate();

        vm.clickinput = function (radioId) {
            var target = document.getElementById(radioId);
            target.click();
        };

        function activate() {
            cfpLoadingBar.start();
            eventsSaasSurveyService.fetchSurveys()
            .then(function (surveys) {
                if (surveys && surveys.length > 0) {
                    vm.questions = [];
                    surveys.forEach(function (survey) {
                        survey.surveyQuestions.forEach(function (question) {
                            vm.questions.push(question);
                        }, this);
                    }, this);
                    vm.currentQuestionIndex = 1;
                    vm.currentQuestion = vm.questions[0];
                }
            })
            .catch(function (error) {

            }).finally(function () {
                cfpLoadingBar.complete();
            });
        };

        vm.answerQuestion = function() {
            if (vm.currentAnswerId || vm.currentQuestion || vm.currentAnswerText) {
                eventsSaasSurveyService.answerQuestion(vm.currentQuestion.surveyId, vm.currentQuestion.id, vm.currentAnswerId, vm.currentAnswerText)
                .then(function () {
                    if (vm.currentQuestionIndex == vm.questions.length) {
                        vm.wipeData();
                    } else {
                        vm.currentQuestion = vm.questions[vm.currentQuestionIndex];
                        vm.currentQuestionIndex++;
                        vm.currentAnswerId = null;
                        vm.currentAnswerText = null;
                    }
                }).catch(function(){
                    vm.wipeData();
                });
            };
        };

        vm.wipeData = function() {
            vm.questions = null;
            vm.currentQuestion = null;
            vm.currentQuestionIndex = null;
            vm.currentAnswerId = null;
            vm.currentAnswerText = null;
        };
    };

})();