(function() {
'use strict';

    angular
        .module('eventsSaas.survey')
        .factory('eventsSaasSurveyService', eventsSaasSurveyService);

    eventsSaasSurveyService.inject = ['$http', '$q', 'HostValue'];
    function eventsSaasSurveyService($http, $q, HostValue) {
        var service = {
            fetchSurveys:fetchNotAnswerdSurveysByEventId,
            answerQuestion:answerQuestion,
            getCountPending: getCountPending
        };

        var sm = this;
        sm.surveys = null;
        sm.NOT_ANSWERED_URL = HostValue.awsRest+"/event/"+HostValue.idEvent+'/survey';
        sm.ANSWER_QUESTION_URL = HostValue.awsRest+'/event/'+HostValue.idEvent+'/survey';
        
        return service;

        ////////////////
        function fetchNotAnswerdSurveysByEventId() {
            return $http.get(sm.NOT_ANSWERED_URL)
                .then(function(response){
                    return response.data;
                })
                .catch(function(){
                    return $q.reject([]);
                });
        };

        function answerQuestion(sId, qId, aId, text){
             return $http.post(sm.ANSWER_QUESTION_URL + '/' + sId,
             [{question: qId, answer:aId, answerText:text}])
                .then(function(){
                    return "ok";
                }).catch(function(){
                    return $q.reject(null);
                })
        };

        function getCountPending(idEvent){
			return $http.get(HostValue.awsRest + "/event/"+idEvent+"/survey/countpending")
				.then(getCountPendingComplete)
				.catch(getCountPendingFailed);

			function getCountPendingComplete(response){
				return response.data
			};

			function getCountPendingFailed(error){
				return $q.reject(error);
			};
		};
    }
})();