/**
 * Created by luna on 7/22/17.
 */
(function() {
    'use strict';

    angular
        .module('eventsSaas.event')
        .controller('ActivityController', ActivityController);

    ActivityController.$inject = ['$scope', '$location', 'eventsSaasRoutes', '$timeout', '$route', 'HostValue', 'localStorageService', 'loginService'];

    /* @ngInject */
    function ActivityController($scope, $location, eventsSaasRoutes, $timeout, $route, HostValue, localStorageService, loginService) {
        var vm = this;
        vm.root = HostValue.aws;
        vm.event = localStorageService.get('activity');
        vm.onClick = onClick;
        vm.schedule = [
            {
                "tipo":"atividade",
                "title":"Comic Con",
                "img": "img/img-event-2.png",
                "desc": "Si num tem leite então bota uma pinga aí cumpadi! Mussum Ipsum, cacilds vidis litro abertis. Si num tem leite então bota uma pinga aí cumpadi!"
            },
            {
                "tipo":"atividade",
                "title":"Tech Summit",
                "img": "img/img-event-3.png",
                "desc": "Si num tem leite então bota uma pinga aí cumpadi! Mussum Ipsum, cacilds vidis litro abertis. Si num tem leite então bota uma pinga aí cumpadi!"
            }
        ];

        window.addEventListener('native.keyboardshow', keyboardShowHandler);
        window.addEventListener('native.keyboardhide', keyboardHideHandler);
        function keyboardShowHandler(e){
            $("body, html").removeClass("overflow-hidden");
        };
        function keyboardHideHandler(e){
            $("body, html").addClass("overflow-hidden");
        };
        function onClick(text){
            $location.path("/checkin/"+text)
        };
    };

})();
