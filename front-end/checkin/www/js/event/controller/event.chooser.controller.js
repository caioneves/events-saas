(function () {
    'use strict';

    angular
        .module('eventsSaas.event')
        .controller('EventChooser', EventChooser);

    EventChooser.$inject = ['$scope', '$location', 'eventsSaasRoutes', 'titulo', 'data', 'loginService', 'localStorageService', 'ProfileService'];

    function EventChooser($scope, $location, eventsSaasRoutes, titulo, data, loginService, localStorageService, ProfileService) {
        var vm = this;
        vm.title = titulo;
        vm.data = data;
        vm.selectedEvent = -1;
        vm.resetPassword = {
            email: "",
            cpf: ""
        };

        vm.submit = submit;

        //vm.fieldResetPassword = fieldResetPassword;

        function submit(event) {
            if(event.id != '-1'){

                loginService.loginEvent(event.id)
                    .then(function(data){
                        localStorageService.set('event', event);
                        ProfileService.getProfiles(event.id);
                        $scope.$dismiss('cancel');
                        $location.path(eventsSaasRoutes.HOME);
                    })
                    .catch(function(error){
                        console.log(error);
                    });
            }
        };

    };

})();