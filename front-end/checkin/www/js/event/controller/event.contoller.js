/**
 * Created by luna on 7/22/17.
 */
(function() {
    'use strict';

    angular
        .module('eventsSaas.event')
        .controller('EventController', EventController);

    EventController.$inject = ['$scope', '$location', 'eventsSaasRoutes', '$timeout', '$route', 'HostValue', 'ProfileService', 'localStorageService', 'DialogService', 'loginService', 'cfpLoadingBar'];

    /* @ngInject */
    function EventController($scope, $location, eventsSaasRoutes, $timeout, $route, HostValue, ProfileService, localStorageService, DialogService, loginService, cfpLoadingBar) {
        var vm = this;
        vm.root = HostValue.awsRest+"/service/image/";
        vm.avatar = HostValue.avatar;
        vm.disabled = false;
        vm.onClick = onClick;

        window.addEventListener('native.keyboardshow', keyboardShowHandler);
        window.addEventListener('native.keyboardhide', keyboardHideHandler);
        function keyboardShowHandler(e){
            $("body, html").removeClass("overflow-hidden");
        };
        function keyboardHideHandler(e){
            $("body, html").addClass("overflow-hidden");
        };

        vm.onClick = onClick;

        vm.schedule = localStorageService.get('event');
        function onClick(event) {
            if(event.id){
                vm.disabled = true;
                cfpLoadingBar.start();
                loginService.loginEvent(event.id)
                .then(function(data){
                    localStorageService.set('activity', event);
                    ProfileService.getProfiles(event.id);
                    $location.path(eventsSaasRoutes.ACTIVITY);
                    cfpLoadingBar.complete();
                })
                .catch(function(error){
                    vm.disabled = false;
                    DialogService.alert("Erro ao selecionar o evento: "+ event.name+". Entre em contato com administrador do aplicativo.","Erro" , null, ["Fechar"]);
                });
            }
        };

    };

})();
