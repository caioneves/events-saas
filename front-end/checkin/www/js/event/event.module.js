(function() {
    'use strict';

    angular
        .module('eventsSaas.event', [
            'eventsSaas.router',
            'eventsSaas.loading',
            'eventsSaas.profile'
        ]);
})();