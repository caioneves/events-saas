(function() {
    'use strict';
    
    angular.module('eventsSaas.router').config(RouterConfig);

    RouterConfig.$inject = ['$routeProvider', 'eventsSaasRoutes'];

	function RouterConfig($routeProvider, eventsSaasRoutes) {
        $routeProvider
        .when(eventsSaasRoutes.SPLASH, {
            templateUrl: 'js/splash/view/splash.html',
            controller: 'SplashController',
            controllerAs: 'vm',
            preload: true
        })
        .when(eventsSaasRoutes.ENTRAR, {
            templateUrl: 'js/login/view/login.html',
            controller: 'LoginController',
            controllerAs: 'vm',
            preload: true
        })
        .when(eventsSaasRoutes.HOME, {
            templateUrl: 'js/home/view/home.html',
            controller: 'HomeController',
            controllerAs: 'vm',
            preload: true
        })
        .when(eventsSaasRoutes.EVENT, {
            templateUrl: 'js/event/view/event.html',
            controller: 'EventController',
            controllerAs: 'vm',
            preload: true
        })
        .when(eventsSaasRoutes.ACTIVITY, {
            templateUrl: 'js/event/view/activity.html',
            controller: 'ActivityController',
            controllerAs: 'vm',
            preload: true
        })
        .when(eventsSaasRoutes.CHECKIN, {
            templateUrl: 'js/checkin/view/checkin.html',
            controller: 'CheckinController',
            controllerAs: 'vm',
            preload: true
        })
        .when(eventsSaasRoutes.CHECKINEVENT, {
            templateUrl: 'js/checkin/view/checkin-event.html',
            controller: 'CheckinEventController',
            controllerAs: 'vm',
            preload: true
        })
        .when(eventsSaasRoutes.PRINT, {
            templateUrl: 'js/print/view/print.html',
            controller: 'PrintController',
            controllerAs: 'vm',
            preload: true
        })
        .otherwise({
            redirectTo: eventsSaasRoutes.ENTRAR
        });
    }

    var routes = {
        SPLASH: '/',
        ENTRAR: '/login',
        HOME: '/home',
        EVENT: '/event',
        ACTIVITY: '/activity',
        PRINT: '/print',
        CHECKIN: '/checkin/:checkinType',
        CHECKINEVENT: '/checkin/:checkinType/:feedbackType'
    };

    angular
        .module('eventsSaas.router')
        .constant('eventsSaasRoutes', routes);
    angular
        .module('eventsSaas.router').config(HrefSanitization);
    HrefSanitization.$inject = ['$compileProvider'];
    function HrefSanitization($compileProvider) {
        $compileProvider.aHrefSanitizationWhitelist(/^\s*(https?|ftp|mailto|file|javascript):/);
    };

    angular
        .module('eventsSaas.router').run(scrollRouteTop);
    scrollRouteTop.$inject = ['$rootScope', '$window'];
    function scrollRouteTop($rootScope, $window) {
        $rootScope.$on('$locationChangeSuccess', function () {
            $window.scrollTo(0,0);
        });
    };

})();