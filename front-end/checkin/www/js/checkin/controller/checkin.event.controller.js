/**
 * Created by caioneves on 6/2/17.
 */
(function() {
    'use strict';

    angular
        .module('eventsSaas.checkin')
        .controller('CheckinEventController', CheckinEventController);

    CheckinEventController.$inject = ['$scope', '$location', 'eventsSaasRoutes', '$timeout', '$route', 'HostValue', 'CheckinService', 'localStorageService', 'DialogService', '$routeParams'];

    /* @ngInject */
    function CheckinEventController($scope, $location, eventsSaasRoutes, $timeout, $route, HostValue, CheckinService, localStorageService, DialogService, $routeParams) {
        var vm = this;
        vm.root = HostValue.aws;
        vm.title = "Credenciamento";
        vm.print = print;
        vm.back = back;
        vm.profile = localStorageService.get('print');
        vm.feedback = $routeParams.feedbackType == "success" ? true : false;
        vm.type = $routeParams.checkinType == "evento" ? true : false;
        
        vm.profile.title = "checkin-in realizado com sucesso";
        vm.profile.msg = "Bem-vindo!";
        vm.profile.ico = "ion-android-happy font-green-light";

        if(!vm.type && vm.feedback){
            vm.profile.company = "Tenha uma ótima palestra/atividade.";
        };

        if(!vm.feedback){
            vm.profile.title = "ops! algo está errado.";
            vm.profile.ico = "ion-android-sad font-red-light";
            if(!vm.type){
                vm.profile.msg = "Parece que você não está cadastrado para essa palestra/atividade";
                vm.profile.desc = "Será que você não está cadastrado para outra atividade? Informe ao atendente e tente novamente.";
            } else {
                vm.profile.msg = "Infelizmente não conseguimos te identificar";
                vm.profile.desc = "Isso pode acontecer por diversas razões, se você tem certeza que está registrado para esse evento clique no botão abaixo e tente novamente.";
            }
        };

        function print() {
            $location.path(eventsSaasRoutes.PRINT);
        };

        function back(url){
            var path = url || eventsSaasRoutes.HOME; 
            $location.path(path);
        };
    };

})();
