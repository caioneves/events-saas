/**
 * Created by caioneves on 6/2/17.
 */
(function() {
    'use strict';

    angular
        .module('eventsSaas.checkin')
        .controller('CheckinController', CheckinController);

    CheckinController.$inject = ['$scope', '$location', '$rootScope', '$routeParams', 'eventsSaasRoutes', '$timeout', '$route', 'HostValue', 'CheckinService', 'localStorageService', 'DialogService'];

    /* @ngInject */
    function CheckinController($scope, $location, $rootScope, $routeParams, eventsSaasRoutes, $timeout, $route, HostValue, CheckinService, localStorageService, DialogService) {
        var vm = this;
        vm.root = HostValue.aws;
        vm.title = "Credenciamento";
        vm.type = $routeParams.checkinType == "evento" ? true : false;
        vm.schedulo = vm.type ? "evento" : "atividade";
        vm.cpf = "";

        vm.checkinByCPF = checkinByCPF;
        vm.checkinQRCode = checkinQRCode;
        vm.print = print;

        window.addEventListener('native.keyboardshow', keyboardShowHandler);
        window.addEventListener('native.keyboardhide', keyboardHideHandler);
        function keyboardShowHandler(e){
            $("body, html").removeClass("overflow-hidden");
            scrollField($(".checkin"));
        };
        function keyboardHideHandler(e){
            $("body, html").addClass("overflow-hidden");
        };
        function scrollField($element){
            if($element.length){
                $element.scrollTop($element[0].scrollHeight);
            };
        };

        function checkinByCPF(cpf) {

            if (CPF.isValid(cpf)) {
                var profile = CheckinService.getProfileByCPF(cpf);
                if (profile != null) {
                     var checkinProfiles = localStorageService.get('checkinProfiles') || [];
                    checkinProfiles.push(profile);
                    localStorageService.set('print', profile);
                    localStorageService.set('checkinProfiles', checkinProfiles); 
                    vm.cpf = "";
                    //DialogService.confirm("Deseja imprimir o crachá?", vm.print, "Checkin realizado", ["Não", "Sim"]);
                    $location.path("/checkin/"+vm.schedulo+"/success");
                } else {
                    // DialogService.alert("Usuário não credenciado", "Atenção", null, "Ok");
                    $location.path("/checkin/"+vm.schedulo+"/error");
                }
            } else {
                DialogService.alert("Documento inválido", "Atenção", null, "Ok");
            }
        }

        function print(index) {
            if (index == 2) {
                $location.path(eventsSaasRoutes.PRINT);
                $scope.$apply();
            }
        }

        function checkinQRCode() {
             cordova.plugins.barcodeScanner.scan(
                function (result) {

                    if (result.format == "QR_CODE") {
                        var profile = CheckinService.getProfileById(result.text);
                        if (profile != null) {
                            var checkinProfiles = localStorageService.get('checkinProfiles') || [];
                            checkinProfiles.push(profile);
                            localStorageService.set('checkinProfiles', checkinProfiles);
                            localStorageService.set('print', profile);
                            vm.cpf = "";
                            //DialogService.confirm("Deseja imprimir o crachá?", vm.print, "Checkin realizado", ["Não", "Sim"]);
                            $rootScope.$apply(function() {
                                $location.path("/checkin/"+vm.schedulo+"/success");
                            });
                        } else {
                            $rootScope.$apply(function() {
                                $location.path("/checkin/"+vm.schedulo+"/error");
                            });
                        }
                    }

                },
                function (error) {
                    DialogService.alert("Falha ao escanear o código", "Atenção", null, "Ok");
                },
                {
                    preferFrontCamera : false, // iOS and Android
                    showFlipCameraButton : true, // iOS and Android
                    showTorchButton : true, // iOS and Android
                    torchOn: true, // Android, launch with the torch switched on (if available)
                    prompt : "Place a barcode inside the scan area", // Android
                    resultDisplayDuration: 500, // Android, display scanned text for X ms. 0 suppresses it entirely, default 1500
                    formats : "QR_CODE,PDF_417", // default: all but PDF_417 and RSS_EXPANDED
                    orientation : "landscape", // Android only (portrait|landscape), default unset so it rotates with the device
                    disableAnimations : true, // iOS
                    disableSuccessBeep: false // iOS
                }
            ); 
        }

    };

})();
