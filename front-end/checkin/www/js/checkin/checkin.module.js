(function() {
    'use strict';

    angular
        .module('eventsSaas.checkin', [
            'eventsSaas.router',
        ]);
})();