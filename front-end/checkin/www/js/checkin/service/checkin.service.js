(function () {
	'use strict';

	angular
		.module('eventsSaas.checkin')
		.factory('CheckinService', CheckinService);

	CheckinService.inject = ['$q', '$http', 'localStorageService', 'HostValue', 'ProfileService']
	function CheckinService($q, $http, localStorageService, HostValue, ProfileService) {
		var service = {
			getProfileByCPF: getProfileByCPF,
			getProfileById: getProfileById
		};

		return service;

		////////////////
		function getProfileByCPF(cpf) {
			var profiles = localStorageService.get('profiles');

			for (var i = 0; i < profiles.length; i++) {
				if (profiles[i].documentNumber == CPF.strip(cpf)){
					return profiles[i];
				}
			}

			return null;
		};

		function getProfileById(id) {
			var profiles = localStorageService.get('profiles');

			for (var i = 0; i < profiles.length; i++) {
				if (profiles[i].id == id){
					return profiles[i];
				}
			}

			return null;
		};
	};
})();