(function() {
    'use strict';

    angular
        .module('eventsSaas.menu')
        .directive('menuItem', menuItem);

    menuItem.$inject = ['$rootScope', '$timeout', '$route', 'localStorageService'];

    /* @ngInject */
    function menuItem($rootScope, $timeout, $route, localStorageService) {
        // Usage:
        //
        // Creates:
        //
        var menuItem = {
            bindToController: true,
            controller: Controller,
            controllerAs: 'vm',
            link: link,
            restrict: 'E',
            template: '<li class="font-opensemibold text-uppercase" ng-repeat="item in menu">'+
                            '<a href="{{item.link}}" ng-class="{\'font-green-sure\': $last}"><em class="fa {{item.ico}} font-18 mrg-right-5"></em> {{item.name}}</a>'+
                      '</li>',
			replace: true,
            scope: {
            }
        };
        return menuItem;

        function link(scope, element, attrs) {
            applyMenu()
            
            function applyMenu(){
                if(localStorageService.get('event').length > 1){
                    scope.menu = [
                        {
                            "name": "Home",
                            "link": "#/activity",
                            "ico" : "fa-home"
                        },
                        {
                            "name": "Evento",
                            "link": "#/event",
                            "ico" : "ion-ios-pricetag"
                        },
                        {
                            "name": "logout",
                            "link": "#/login",
                            "ico" : "fa-sign-out"
                        }
                    ];
                } else {
                    scope.menu = [
                        {
                            "name": "Home",
                            "link": "#/activity",
                            "ico" : "fa-home"
                        },
                        {
                            "name": "logout",
                            "link": "#/login",
                            "ico" : "fa-sign-out"
                        }
                    ];
                }
            };
        	$timeout(function() {
                $(".button-collapse").sideNav({
                    closeOnClick: true
                });
            }, 0);
        };
    };

    /* @ngInject */
    function Controller() {

    }

})();