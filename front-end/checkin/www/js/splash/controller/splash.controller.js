(function () {

    angular
        .module('eventsSaas.splash')
        .controller('SplashController', SplashController);

    SplashController.$inject = ['$scope', 'eventsSaasRoutes', '$location', '$timeout', 'localStorageService', 'ProfileService', 'SyncService']

    function SplashController($scope, eventsSaasRoutes, $location, $timeout, localStorageService, ProfileService, SyncService) {
        $timeout(
            function(){
                SyncService.startUp();
                if (localStorageService.get('profile')) {
                    $location.path(eventsSaasRoutes.ACTIVITY);
                    if (navigator.connection.type != 'none') {
                        ProfileService.getProfiles(localStorageService.get('event').id);
                    }
                } else {
                    $location.path(eventsSaasRoutes.ENTRAR);
                    $scope.$apply();
                }
        }, 4000);
    };

})();