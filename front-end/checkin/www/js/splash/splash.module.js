(function () {
    'use strict';

    angular.module('eventsSaas.splash',
        [
            'eventsSaas.router',
            'eventsSaas.sync'
        ]);
})();