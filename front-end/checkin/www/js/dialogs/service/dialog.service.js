/**
 * Created by caioneves on 6/5/17.
 */
(function () {
    'use strict';

    angular
        .module('eventsSaas.dialog')
        .factory('DialogService', DialogService);

    DialogService.inject = [];
    function DialogService($interval, localStorageService, ProfileService, HostValue, $http) {
        var service = {
            alert: alert,
            confirm: confirm
        };



        return service;


        function alert(message, title, callback, buttonName) {
            navigator.notification.alert(
                message,    // message
                callback,   // callback
                title,      // title
                buttonName  // buttonName
            );
        };


        function confirm(message, callBack, title, buttonLabels) {
            navigator.notification.confirm(
                message, // message
                callBack,            // callback to invoke with index of button pressed
                title,           // title
                buttonLabels     // buttonLabels
            );
        }

    };
})();