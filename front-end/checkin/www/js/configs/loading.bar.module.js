(function () {
    'use strict';

    angular.module('eventsSaas.loading',
        ['chieffancypants.loadingBar', 'ngAnimate']
    );
})();