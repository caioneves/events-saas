(function () {
    'use strict';

    angular
        .module('eventsSaas.auth')
        .config(AuthInterceptor);

    AuthInterceptor.inject = ['$httpProvider'];
    function AuthInterceptor($httpProvider) {
        $httpProvider.interceptors.push(function (localStorageService) {
            return {
                'request': function (config) {
                    if (config.url.endsWith("view/login/login.html")){
                        return config;
                    }
                    config.headers = config.headers || {};
                    config.headers.token = localStorageService.get('token');
                    return config;
                }
            };

        });
    }
})();
