(function () {
    'use strict';

    angular
        .module('eventsSaas')
        .config(StorageConfig);

    StorageConfig.inject = ['localStorageServiceProvider'];
    function StorageConfig(localStorageServiceProvider) {
        var vm = this;


        activate();

        ////////////////

        function activate() {
            localStorageServiceProvider
                .setPrefix('eventsSaas');
            localStorageServiceProvider
                .setStorageType('localStorage');
        }
    }
})();