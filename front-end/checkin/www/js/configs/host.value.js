(function () {
    'use strict';

	var hostValue = {
		aws: /*"http://localhost:8080",*/"http://dev.events-saas.com.br:8080",
		awsRest: /*"http://localhost:8080/app/rest",*/"http://dev.events-saas.com.br:8080/app/rest",
		awsTeste: "http://dev.events-saas.com.br:8080/app/rest/test/setProfileLogged",
		syncTime:60000,
		avatar: 'img/img-default.jpg'
	};

	angular.module("eventsSaas.host")
	.value("HostValue", hostValue);

})();
