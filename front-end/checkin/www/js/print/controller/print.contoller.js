/**
 * Created by caioneves on 6/2/17.
 */
(function() {
    'use strict';

    angular
        .module('eventsSaas.print')
        .controller('PrintController', PrintController);

    PrintController.$inject = ['$scope', '$location', 'eventsSaasRoutes', '$timeout', '$route', 'HostValue', 'CheckinService', 'localStorageService', 'DialogService'];

    /* @ngInject */
    function PrintController($scope, $location, eventsSaasRoutes, $timeout, $route, HostValue, CheckinService, localStorageService, DialogService) {
        var vm = this;
        vm.root = HostValue.aws;
        vm.title = "Realizar Checkin";
        vm.profile = localStorageService.get('print');

        vm.print = print;

        vm.print();

        function print() {



            $('#qrcode').qrcode({text: vm.profile.id+''});

            var doc = location.href;
            $timeout(function () {
                cordova.plugins.printer.print(doc, { duplex: 'long' }, function (res) {
                    //alert(res ? 'Done' : 'Canceled');
                    $location.path(eventsSaasRoutes.HOME);
                    $scope.$apply();
                });
            }, 500)

        }

    };

})();
