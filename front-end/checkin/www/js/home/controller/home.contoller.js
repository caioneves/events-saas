/**
 * Created by caioneves on 6/2/17.
 */
(function() {
    'use strict';

    angular
        .module('eventsSaas.home')
        .controller('HomeController', HomeController);

    HomeController.$inject = ['$scope', '$location', 'eventsSaasRoutes', '$timeout', '$route', 'HostValue', 'CheckinService', 'localStorageService', 'DialogService'];

    /* @ngInject */
    function HomeController($scope, $location, eventsSaasRoutes, $timeout, $route, HostValue, CheckinService, localStorageService, DialogService) {
        var vm = this;
        vm.root = HostValue.aws;
        vm.title = "Credenciamento";

        window.addEventListener('native.keyboardshow', keyboardShowHandler);
        window.addEventListener('native.keyboardhide', keyboardHideHandler);
        function keyboardShowHandler(e){
            $("body, html").removeClass("overflow-hidden");
        };
        function keyboardHideHandler(e){
            $("body, html").addClass("overflow-hidden");
        };

        vm.selectedSchedule = selectedSchedule;
        vm.onClick = onClick;

        function selectedSchedule(text){
            vm.type = text;    
        };
        function onClick(text){
            $location.path("/checkin/"+text)
        };
        vm.type = "evento";
        vm.schedule = [
            {
                "tipo":"evento",
                "title":"SIEI 20017",
                "img": "img/img-event-1.png",
                "desc": "Mussum Ipsum, cacilds vidis litro abertis. Si num tem leite então bota uma pinga aí cumpadi!"
            },
            {
                "tipo":"evento",
                "title":"Tech Summit",
                "img": "img/img-event-2.png",
                "desc": "Mussum Ipsum, cacilds vidis litro abertis. Si num tem leite então bota uma pinga aí cumpadi!"
            },
            {
                "tipo":"evento",
                "title":"Comic Con",
                "img": "img/img-event-3.png",
                "desc": "Mussum Ipsum, cacilds vidis litro abertis. Si num tem leite então bota uma pinga aí cumpadi!"
            },
            {
                "tipo":"atividade",
                "title":"Comic Con",
                "img": "img/img-event-2.png",
                "desc": "Si num tem leite então bota uma pinga aí cumpadi! Mussum Ipsum, cacilds vidis litro abertis. Si num tem leite então bota uma pinga aí cumpadi!"
            },
            {
                "tipo":"atividade",
                "title":"Tech Summit",
                "img": "img/img-event-3.png",
                "desc": "Si num tem leite então bota uma pinga aí cumpadi! Mussum Ipsum, cacilds vidis litro abertis. Si num tem leite então bota uma pinga aí cumpadi!"
            }
        ]

    };

})();
