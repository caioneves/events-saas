(function() {
    'use strict';

    angular
        .module('eventsSaas.home', [
            'eventsSaas.router',
            'eventsSaas.dialog',
            'eventsSaas.loading',
            'ui.mask'
        ]);
})();