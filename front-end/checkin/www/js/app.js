(function() {
    'use strict';
	angular.module('eventsSaas', 
		[
			'LocalStorageModule',
			'eventsSaas.splash',
            'eventsSaas.menu',
			'eventsSaas.router',
			'eventsSaas.modal',
			'eventsSaas.login',
			'eventsSaas.host',
			'eventsSaas.home',
			'eventsSaas.event',
			'eventsSaas.auth',
			'eventsSaas.profile',
			'eventsSaas.checkin',
			'eventsSaas.sync',
			'eventsSaas.dialog',
			'eventsSaas.print',
			'ui.mask'
		]
	);

})();