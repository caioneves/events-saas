/**
 * Created by caioneves on 6/5/17.
 */
(function () {
    'use strict';

    angular
        .module('eventsSaas.sync')
        .factory('SyncService', SyncService);

    SyncService.inject = ['$interval', 'localStorageService', 'ProfileService', 'HostValue', '$http'];
    function SyncService($interval, localStorageService, ProfileService, HostValue, $http) {
        var service = {
            startUp: startUp
        };



        return service;


        function startUp(eventId) {

            var vm = this;
            vm.sync = sync;
            vm.stop = undefined;


            function sync() {
                if (localStorageService.get('event')) {
                    ProfileService.checkin(localStorageService.get('event').id);
                }
            }

            function onOnline() {
                // Handle the online event
                console.log($interval);
                vm.stop = $interval(function () {
                    if (localStorageService.get('checkinProfiles') != null) {
                        vm.sync();
                    }
                }, HostValue.syncTime);
            }

            function onOffline() {
                console.log("stop");
                if (vm.stop != undefined) {
                    $interval.cancel(vm.stop);
                }
            }

            if (navigator.connection) {

                if (navigator.connection.type != 'none') {
                    console.log('startOnline');
                    onOnline();
                }
            }

            document.addEventListener("offline", onOffline, false);
            document.addEventListener("online", onOnline, false);
        };

    };
})();