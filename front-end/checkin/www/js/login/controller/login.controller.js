(function () {
    'use strict';

    angular
        .module('eventsSaas.login')
        .controller('LoginController', LoginController)

    LoginController.$inject = ['$scope', '$uibModal', '$location', 'eventsSaasRoutes', '$timeout', 'loginService', 'localStorageService', 'ProfileService', 'SyncService', 'cfpLoadingBar']

    function LoginController($scope, $uibModal, $location, eventsSaasRoutes, $timeout, loginService, localStorageService, ProfileService, SyncService, cfpLoadingBar) {
        
        var vm = this;
        vm.submit = submit;
        vm.fieldsMSG = false;
        vm.disabled = false;

        window.addEventListener('native.keyboardshow', keyboardShowHandler);
        window.addEventListener('native.keyboardhide', keyboardHideHandler);
        function keyboardShowHandler(e){
            $("body, html").removeClass("overflow-hidden");
            scrollField($("#password"));
        };
        function keyboardHideHandler(e){
            $("body, html").addClass("overflow-hidden");
        };

        document.addEventListener("backbutton", onBackKeyDown, false);
        function onBackKeyDown(e) {
            e.preventDefault();
        }

        //////////////////////////////        
        function scrollField($element){
            if($element.length){
                $("#wrapper, body, html").animate({scrollTop: $element.offset().top}, "slow");
            };
        };

    	function submit(form, login) {
             if(form.$valid && form.email.$valid){
                vm.disabled = true;
                cfpLoadingBar.start();
                loginService.login(login.email, login.senha)
                .then(function(data){
                    if (data.length > 1) {

                        localStorageService.set('event',data);
                        $location.path(eventsSaasRoutes.EVENT);

                    } else if (data.length != 0) {
                        
                        localStorageService.set('event', data[0].event);
                        loginService.loginEvent(data[0].event.id)
                        .then(function () {
                            ProfileService.getProfiles(localStorageService.get('event').id);
                            $location.path(eventsSaasRoutes.ACTIVITY);
                        });
                    }   
                    cfpLoadingBar.complete();
                })
                .catch(function(error){
                    vm.disabled = false;
                    DialogService.alert("Usuário ou senha está incorreto.","Atenção" , null, ["Fechar"]);
                });
             } else {
                vm.fieldsMSG = true;
                vm.disabled = false;
             }
		};
    };

})();