(function () {
    'use strict';

    angular.module('eventsSaas.login',
        [
        	'ui.bootstrap',
            'eventsSaas.router',
            'eventsSaas.modal',
            'eventsSaas.loading',
            'eventsSaas.custom.form',
            'eventsSaas.profile'
        ]);
})();