(function () {
	'use strict';

	angular
		.module('eventsSaas.login')
		.factory('loginService', loginService);

	loginService.inject = ['$q', '$http', 'localStorageService', 'HostValue', 'ProfileService']
	function loginService($q, $http, localStorageService, HostValue, ProfileService) {
		var service = {
			login: login,
			loginEvent: loginEvent
		};

		return service;

		////////////////
		function login(user, password) {
			var hash = user + ":" + password;
			localStorageService.set('hash', hash);
			return $http(
				{
					url: HostValue.awsRest+"/login/checkin",
					headers: { "Authorization": "Basic " + btoa(hash) },
					method: "POST"
				})
				.then(postLoginComplete)
				.catch(postLoginFailed);

			function postLoginComplete(response) {
				return response.data;
			};

			function postLoginFailed(error) {
				localStorageService.set('hash', null);
				return $q.reject(error);
			};
		};

		function loginEvent(eventId) {
			var hash = localStorageService.get('hash');
			return $http(
				{
					url: HostValue.awsRest+"/event/"+eventId+"/login/",
					headers: { "Authorization": "Basic " + btoa(hash) },
					method: "POST"
				})
				.then(postLoginComplete)
				.catch(postLoginFailed);

			function postLoginComplete(response) {
				localStorageService.set('token', response.data.token);
				localStorageService.set('profile', response.data);
				return response.data;
			};

			function postLoginFailed(error) {
				localStorageService.set('token', null);
				localStorageService.set('profile', null);
				return $q.reject(error);
			};
		};

	};
})();