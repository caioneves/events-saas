(function () {
	'use strict';

	angular
		.module('eventsSaas.profile')
		.factory('ProfileService', ProfileService);

	ProfileService.inject = ['$q', '$http', 'localStorageService', 'HostValue'];
	function ProfileService($q, $http, localStorageService, HostValue) {
		var service = {
			getProfiles: getProfiles,
			checkin: checkin
		};

		return service;

		function getProfiles(eventId) {
			return $http.get(HostValue.awsRest+"/event/"+eventId+"/profile/list")
				.then(getProfilesComplete)
				.catch(getProfilesFailed);

			function getProfilesComplete(response) {
				localStorageService.set('profiles', response.data);
				return response.data;
			};

			function getProfilesFailed(error) {
				localStorageService.set('profiles', null);
				return $q.reject(error);
			};
		};

		function checkin(eventId) {
			var ids = [];
			var profiles = localStorageService.get('checkinProfiles');

			for (var i = 0; i < profiles.length; i++) {
				ids.push(parseInt(profiles[i].id));
			}

			return $http.post(HostValue.awsRest+"/event/"+eventId+"/profile/checkins", ids)
				.then(postCheckinComplete)
				.catch(postCheckinFailed);

			function postCheckinComplete(response) {
				localStorageService.set('checkinProfiles', null)
				return response.data;
			};

			function postCheckinFailed(error) {
				return $q.reject(error);
			};
		};

	};
})();